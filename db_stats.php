<?php

	if ($DEBUG == true){
		ini_set('display_errors',1);  
		error_reporting(E_ALL);
	}

	require_once("db_connect.php");

	# Connect to MySQL database
	$conn = new PDO("mysql:host=$server;dbname=$database;charset=utf8",$username,$password);
	$conn->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
	$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

	// If there is an error with the query...
	if (!$conn) {
		echo "\nPDO::errorInfo():\n";
		print_r($conn->errorInfo());
		exit;
	}

	// Get the absolute number of stations for each category...
	$rows_air = $conn->query("SELECT count(*) FROM stations WHERE category='air_quality' AND op_state='active'")->fetchColumn(); 
	$rows_weather = $conn->query("SELECT count(*) FROM stations WHERE category='weather' AND op_state='active'")->fetchColumn(); 
	$rows_radiation = $conn->query("SELECT count(*) FROM stations WHERE category='radiation' AND op_state='active'")->fetchColumn(); 
	$rows_waterways = $conn->query("SELECT count(*) FROM stations WHERE category='pegel' AND op_state='active'")->fetchColumn(); 
	$rows_bw_quality = $conn->query("SELECT count(*) FROM stations WHERE category='bw_quality' AND op_state='active'")->fetchColumn(); 

?>
