			<div id="leftBar" class="clearfix">

				<div id="searchOuter" class="round border">
					
					<div class="boxHeader">
						
						<h3><? echo $SEARCH ?></h3>
						<div class="clearfix">
							<input type="checkbox" id="showAdvancedSearch" class="boxButton" alt="<? echo $ADVANCED_SEARCH_ALT ?>">
							<label id="advancedSearchLabel" class="boxButton" for="showAdvancedSearch" title="<? echo $ADVANCED_SEARCH_ALT ?>" ><i class="icon-cog"></i></label>
							
						</div>
						
					</div>
					
					<div id="searchRow" class="clearfix"> <!-- Start Address input -->
					
						<div id="addressInputOuter" class="clearfix">
							
							<div class="labels"><? echo $ADDRESS ?></div>
							
							<input id="addressInput" type="text" title="<? echo $PLEASE_ENTER_ADDRESS ?>" data-lat="" data-lng="" value="" />
							
							<div id="addressGeoLoc" title="<? echo $GEOLOCATION_TOOLTIP ?>">
								<input type="checkbox" id="locate2" />
								<label role="button" for="locate2"><i class="icon-location"></i></label>
							</div>
						
							<div id="spinnerSearch">
								<div style="">
									<img src="/img/loading22.gif" style="width:20px; height:20px;" alt="Loading gif" />
								</div>
							</div>		
						
						</div>
						
						<div style="clear:both;"></div>
						
						<div id="nominatimResultsOuter">
							<div id="nominatim_results" class="round border"></div>
						</div>

						<div class="clearfix" style="margin-top:10px; display:flex; flex-wrap:wrap;">
							<div id="categorySelectOuter" class="selectOuter">
								<div class="labels"><? echo $CATEGORY ?></div>
								<select id="categorySelect" title="<? echo $PLEASE_SELECT_CATEGORY ?>">
									<optgroup label="<? echo $CATEGORY ?>">
										<option value="all"><? echo $ALL_CATEGORIES ?></option>
										<option value="air_quality"><? echo $AIR_QUALITY ?></option>
										<!--<option value="weather"><? echo $WEATHER ?></option>-->
										<option value="radiation"><? echo $RADIATION ?></option>
										<option value="pegel"><? echo $WATERWAYS ?></option>
										<!--<option value="bw"><? echo $BATHING ?></option>-->
										<!--<option value="eprtr"><? echo $INDUSTRIAL_POLLUTION ?></option>-->
										<!--<option value="energy"><? echo $RENEWABLE_ENERGY ?></option>-->
									</optgroup>
								</select>
							</div>

							<div id="radiusSelectOuter" class="selectOuter">
								<div class="labels"><? echo $RADIUS ?></div>
								<select id="radiusSelect" title="<? echo $PLEASE_SELECT_RADIUS ?>">
									<optgroup label="<? echo $RADIUS ?>">
										<option value="5" class="distanceUnit">5<? echo $DISTANCE_UNIT ?></option>
										<option value="10" class="distanceUnit">10<? echo $DISTANCE_UNIT ?></option>
										<option value="20" class="distanceUnit">20<? echo $DISTANCE_UNIT ?></option>
										<option value="50" class="distanceUnit" selected="selected">50<? echo $DISTANCE_UNIT ?></option>
										<option value="100" class="distanceUnit">100<? echo $DISTANCE_UNIT ?></option>
										<option value="250" class="distanceUnit">250<? echo $DISTANCE_UNIT ?></option>
										<option value="500" class="distanceUnit">500<? echo $DISTANCE_UNIT ?></option>
										<option value="1000" class="distanceUnit">1000<? echo $DISTANCE_UNIT ?></option>
									</optgroup>
								</select>
							</div>

							<div id="amountSelectOuter" class="selectOuter">
								<div class="labels"><? echo $LIMIT ?></div>
								<select id="amountSelect" title="<? echo $PLEASE_SELECT_LIMIT ?>">
									<option value="1">1</option>
									<option value="5">5</option>
									<option value="10">10</option>
									<option value="25">25</option>
									<option value="50">50</option>
									<option value="100">100</option>
									<option value="250">250</option>
									<option value="500">500</option>
									<option value="1000">1000</option>
									<option value="5000">5000</option>
									<option value="10000">10000</option>
									<option value="999999" selected="selected"><? echo $NO_LIMIT ?></option>
								</select>
							</div>
													
							<div id="searchButtonOuter">
								<div class="labels"></div>
								<button id="suche2" title="<? echo $SEARCH ?>" alt="<? echo $SEARCH ?>"><i class="icon-search"></i> <? echo $SEARCH ?></button>
								
							</div>
							
						</div>
										
					</div> <!-- End address Input -->

					<div id="searchOptions" class="clearfix"> 
						
						<div style="padding:0 10px;">
							<div title="<? echo $SHOW_SELECTED_COMPOUNDS_TOOLTIP ?>">
								<input type="checkbox" id="multiSelectCheckbox" name="multiSelectCheckbox" style="float:left;"><p><? echo $SHOW_SELECTED_COMPOUNDS ?></p>
								
								<div id="multiSelectOuter">
									<div id="multiSelect" class="clearfix">
										<select data-placeholder="<? echo $PLEASE_SELECT_MULTI ?>" style="" class="chosen" multiple="" tabindex="-1" title="<? echo $SHOW_SELECTED_COMPOUNDS_TOOLTIP ?>">
											<option value=""></option>
											<optgroup label="<? echo $AIR_QUALITY ?>">
												<option value="pm10_1tmw"><? echo $PM10 ?></option>
												<option value="no2_1smw"><? echo $NO2 ?></option>
												<option value="o3_1smw"><? echo $O3 ?></option>
												<option value="co_8smw"><? echo $CO ?></option>
												<option value="so2_1smw"><? echo $SO2 ?></option>
											</optgroup>
											<optgroup label="<? echo $RADIATION ?>">
												<option value="odl_1smw"><? echo $ODL ?></option>
											</optgroup>
											<optgroup label="<? echo $WATERWAYS ?>">
												<option value="w"><? echo $W ?></option>
												<option value="lt"><? echo $LT ?></option>
												<option value="wt"><? echo $WT ?></option>
												<option value="q"><? echo $Q ?></option>
												<option value="va"><? echo $VA ?></option>
												<option value="dfh"><? echo $DFH ?></option>
												<option value="lf"><? echo $LF ?></option>
												<option value="wg"><? echo $WG ?></option>
												<option value="wr"><? echo $WR ?></option>
												<option value="gru"><? echo $GRU ?></option>
												<option value="hl"><? echo $HL ?></option>
												<option value="pl"><? echo $PL ?></option>
												<option value="n"><? echo $N ?></option>
												<option value="n_intensity"><? echo $N_INTENSITY ?></option>
												<option value="o2"><? echo $O2 ?></option>
												<option value="ph"><? echo $PH ?></option>
												<option value="cl"><? echo $CL ?></option>
												<option value="orp"><? echo $ORP ?></option>
											</optgroup>
										</select>
									</div>
								</div>
							</div>
						</div>

						<div style="padding:0 10px;">
							<div title="<? echo $SHOW_WITHIN_BOUNDARY_TOOLTIP ?>">
								<input type="checkbox" id="limitToPolygon" name="limitToPolygon" style="float:left;"><p><? echo $SHOW_WITHIN_BOUNDARY ?></p>
							</div>
						</div>

					</div>
				
				</div>
			
				<div id="resultListOuter">
						
					<div id="ergebnisse" class="clearfix">
				
						<div style="display:flex;" class="round border">
							

								<div id="locationSelectOuter" style="float:left; flex:1">
									<select id="locationSelect" name="locationSelect" title="<? echo $RESULTS ?>">
										<option value="hidden"></option>
									</select>
								</div>

							
							<button class="resultListToggle boxButton" title="<? echo $RESULT_LIST_TOGGLE ?>" alt="<? echo $RESULT_LIST_TOGGLE ?>"><i class="icon-resize-full"></i></button>
					
						</div>
						
					</div>
					
					<div id="resultListInner" class="border round">
						
						<div class="boxHeader clearfix">
							<h3 id="resultListHeader" class=""></h3>
							
							<button class="boxButton resultListToggle" title="<? echo $RESULT_LIST_TOGGLE ?>" alt="<? echo $RESULT_LIST_TOGGLE ?>"><i class="icon-resize-small"></i></button>
						</div>
						
						<div id="resultList" class="scroll round">
							
							<ul id="resultsListUL"></ul>
					
						</div>
						
					</div>
					
				</div>
				
			</div>
			

				
					
