function highlightResult(uuid){
	if (debug == true) console.log("highlightResult: " + uuid);
	
	var marker = markerMap[uuid];
	
	if ((typeof oldMarker != 'undefined') && (oldMarker != marker)){
		// Remove css border of previous highlighted marker
		$(oldMarker._icon).removeClass('selectedMarker');
	};
	
	// Resultlist: Remove highlight from previous element and highlight new one
	$("ul.resultsListUL").children().removeClass("hovered");
	$("#locationSelect option").removeAttr("selected");
	
	var el = $("ul.resultsListUL").find("[data-uuid='" + uuid + "']");
	el.addClass("hovered");
	
	// Resultlist: Scroll only if needed
	if ((typeof el != 'undefined') && (isScrolledIntoView(el) == false)){
		//alert(el);
		$("#resultList").animate({scrollTop: el.offset().top - $("#resultList").offset().top + $("#resultList").scrollTop()});
	}
	
	// Result Selectmenu
	var selectOption = $("#locationSelect").find("[data-uuid='" + uuid + "']");
	selectOption.prop('selected', true)
}

function isScrolledIntoView(elem){
    var $elem = $(elem);
    var $window = $(window);
	
    var docViewTop = $window.scrollTop() + 160;
    var docViewBottom = docViewTop + $window.height() - 160;

    var elemTop = $elem.offset().top;
    var elemBottom = elemTop + $elem.height();

	var result = ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
	//if (debug == true) console.log('isScrolledIntoView: ' + result + ' / docViewTop: ' + docViewTop + ' / docViewBottom: ' + docViewBottom + ' / elemTop: ' + elemTop + ' / elembottom: ' + elemBottom);
	return result;
	
}

function showResults(){
	// display result related buttons

	resizeLeftBar();
	searchWidth();
	
	document.getElementById("resultListOuter").style.display = "block";
	if (isSmallDisplay() == true) {
		document.getElementById("resultListInner").style.display = "none";
		document.getElementById("ergebnisse").style.display = "block";
	} else {
		document.getElementById("ergebnisse").style.display = "none";
		document.getElementById("resultList").style.display = "block";
	}
	
	//$("#resultListOuter").show("blind",500);
	//$("#resultList").show("blind",500);
	
	// Check if the results menu is shown or hidden; if hidden simulate click on it to make it visible
	if ($("#results").is(':checked')){
		// do nothing
	} else {
		$("#results").click();	
	}
	
	locationSelect.style.visibility = "visible";
	var warning_message;
	var date_warning_message;
	if (warning_counter > 0){
		//warning_message = " (" + warning_counter + " mit Warnungen &#x26a0;)";
		var warning_message = warning_counter + ' Warnungen (<span style="color:red;">&#x26a0;</span>)';
	};
	
	if (dateWarningCounter > 0){
		
		if (warning_counter > 0) {
			var spacer = " / ";
		} else {
			var spacer = "";
		}
		//warning_message = " (" + warning_counter + " mit Warnungen &#x26a0;)";
		var date_warning_message = spacer + dateWarningCounter + ' veraltet (<span style="color:red;">&#9201;</span>)';
	};
	
	var a = $("#locationSelect").children(":first"); //.value;

	//$("#locationSelect").children(":first").html(counter + " Stationen: " + (warning_message || '') + (date_warning_message || ''));
	$("#locationSelect").children(":first").html(counter + " Ergebnisse ");
			
	// If needed show scrollbar after result list is displayed
	$("#resultList").scrollTop(0);
	
}

function createOption(feature, distance, stationWarningColor){

	// Create result in the select menu
	var option = document.createElement("option");
	option.value = feature.properties.uid;
	option.setAttribute("data-class", feature.properties.category );
	//option.setAttribute("data-uid", feature.properties.uid);
	option.setAttribute("data-uuid", feature.properties.uuid);
	option.setAttribute("style", "color:" + stationWarningColor + ";");
	option.innerHTML = warningName + " (" + distance + "km)";
	locationSelect.appendChild(option);	
};

function createResultListItemsArray(feature,distance,compounds){
	// Push markers into array and create list elements for each in the resultList elemtent
	resultItems.push( 
		'<li class="resultListItem clearfix" style="display:flex;" data-uid=' + feature.properties.uid + ' data-uuid=' + feature.properties.uuid + '>' + 
			'<div style="float:left;padding:5px 10px;">' +
				'<img class="resultListItemImage" src="/img/' + feature.properties.category + '.png" />' +
				'<div style="margin:0 auto;"><div class="resultListItemDistance"> ' + distance + '</div><div class="resultListItemDistanceUnit">' + distanceUnit + '</div><div class="clearfix"></div></div>' +
			'</div>' +
			'<div style="float:left;flex-grow:1;padding:10px 10px 10px 0;">' +
				'<div class="clearfix">' +
					'<div>' +
						'<div class="resultListItemHeadline" style="float:left; flex-grow:1;">' + feature.properties.original_name + '</div>' +
						'<div style="float:right;margin-top:-5px;">' + ( warningIcon || "" ) + ( dateWarningIcon || "" ) +' </div>' +
						'<div style="clear:both;"></div>' +
					'</div>' +
					
					'<div>' + 
						'<div class="resultListItemText"><b>' + lang_sensors + ': </b>' + compounds.join(", ") + '</div>' + 
						//'<div style="float:right;">' + ( warningIcon || "" ) + '</div>' + 
						//'<div style="clear:both;"></div>' + 
					'</div>' +
					'<div>' +
						'<div class="resultListItemText"><b>' + lang_last_update + ': </b><span class="timeAgo" data-timeUTC="' + feature.properties.last_update  + '" style="color:' + dateWarningColor + ';" data-livestamp="'+ userTime(feature.properties.last_update,userTimezone) + '"></span></div>' + 
						//'<div style="float:right;">' + ( dateWarningIcon || "" ) + '</div>' +
						//'<div style="clear:both;"></div>' +
					'</div>' + 
					'<div style="clear:both;"></div>' +
				'</div>' +
			'</div>' +
		'</li>'
	);
}

function createResultListItems(){
	var warning_message;
	var date_warning_message;
	var spacer;
	
	if (warning_counter > 0){
		var warning_message = warning_counter + ' ' + lang_warnings + ' (<i style="color:red;" class="fontIcon icon-attention"></i>)';
		//if (debug == true) console.log(warning_message);
	}
	
	if (dateWarningCounter > 0){
		//warning_message = " (" + warning_counter + " mit Warnungen &#x26a0;)";
		if (warning_counter > 0) {
			var spacer = " / ";
		} else {
			var spacer = "";
		}
		var date_warning_message = spacer + dateWarningCounter + ' ' + lang_outdated + ' (<i style="color:red;" class="fontIcon icon-back-in-time"></i>)';
	};
	
	// Create result in the resultList
	$('#resultListHeader').empty();
	$('#resultListToggle').empty();
	//$('#resultListHeader').html(counter + " Stationen: " + (warning_message || '') + (date_warning_message || ''));
	$('#resultListHeader').html(counter + ' ' + lang_results);
	$('<div>', { id: 'toggleResults', html: 'Liste einklappen' }).appendTo('#resultListToggle');
	$('<div>', { style: 'clear:both;' }).appendTo('#resultListHeader');
	$('#resultList').empty();
	$('<ul />', {
		'class': 'resultsListUL',
		'html': resultItems.join('')
	}).appendTo('#resultList');
}

function onChangeResult(){
	
	// Simulate click on marker when selecting locations from the locationselect menu
	locationSelect = document.getElementById("locationSelect");
	locationSelect.onchange = function(){
		//Get the id of the element clicked
		var uuid = $('option:selected', this).attr("data-uuid");
		clickMarker(uuid);
	};	
	
	//var resultListItem = document.getElementsByClassName("resultListItem");

	
	// Simulate click on marker when selecting locations from the resultList 
	
	$(document).on('click', '.resultListItem', function(e){
		
		//var uid = $(this).attr("data-uid");
		var uuid = $(this).attr("data-uuid");
		
		// Check if this is a doubleclick event: if yes, do nothing; the on.dblclick event listener MUST come afterwards!
		if(e.originalEvent.detail > 1){
			if (debug == true) console.log('dblclick event');
			return;
			// if you are returning a value from this function then return false or cancel the event some other way
		} else {
			//Get the id of the element clicked
			map.closePopup();
			$(marker._icon).removeClass('selectedMarker');
			
			clickMarker(uuid);
		}
	});	
	
	$(document).on('dblclick', '.resultListItem', function(){
		//alert("on: dblclick");
		//Get the id of the element clicked
		var uuid = $(this).attr("data-uuid");
		var marker = markerMap[uuid];
		//clickMarker(uuid);
		
		//map.setZoom(maximumZoom);
		//map.panTo(marker.getLatLng());
		//map.panBy(new L.Point(-offsetLeft/2, -offsetTop/2), {animate: false});
		
		//map.once('zoomend', function(){
		//	if (debug == true) console.log("moveend once");
		//	clickMarker(uuid);
		//	map.setZoom(maximumZoom);
		//});
	});	
	
	var timer;
	//var oldMarkerParentID;
	$(document).on('mouseenter', '.resultListItem', function(){	
		//var uid = $(this).attr("data-uid");
		var uuid = $(this).attr("data-uuid");
		highlightMarker(uuid);
		
		// 
		
		//map.closePopup();
		//window.history.pushState('none', 'New Title', '/' + lang + '/map/near/' + $("#addressInput").attr('data-lat') + ',' + $("#addressInput").attr('data-lng') + '/' + $("#addressInput").attr('data-radius') + '/' + $("#addressInput").attr('data-category'));	
	}//, function() {
		// on mouse out, cancel the timer
	//	alert("out");
	//	clearTimeout(timer);
	//}
	);
	
	$(document).on('mouseleave', '.resultListItem', function(){
		//var uid = $(this).attr("data-uid");
		var uuid = $(this).attr("data-uuid");
		var marker = markerMap[uuid];
		$(marker._icon).removeClass('selectedMarker');
		
	});

}
