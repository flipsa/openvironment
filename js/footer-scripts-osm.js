$(window).load(function(){
	$('#spinnerFull').hide("fade",500);
	$('#header').show("fade",0);
	$('#tab_select1').show("fade",0);
	$('#searchOuter').show("fade",0);
	menu_w = $("#tab_select1").width();
	searchResize();
	//$("#layersMenu").click();
	//control._expand();
});

// See: http://stackoverflow.com/questions/2854407/javascript-jquery-window-resize-how-to-fire-after-the-resize-is-completed
var waitForFinalEvent = (function () {
  var timers = {};
  return function (callback, ms, uniqueId) {
    if (!uniqueId) {
      uniqueId = "Don't call this twice without a uniqueId";
    }
    if (timers[uniqueId]) {
      clearTimeout (timers[uniqueId]);
    }
    timers[uniqueId] = setTimeout(callback, ms);
  };
})();

// Listen for window resize event
window.addEventListener('resize', function(event){
    searchResize();
    //resizePopup(map.layer._popup);

});

$(window).bind( 'orientationchange', function(e){
	//alert("orientationchange"); 
	$(window).resize();
	// ugly hack to recalculate and recenter the map
	searchLocationsNear();
});

function searchResize(){
	
	// resize search area width
	headerWidth = $("#header").width();
	//alert(headerWidth);
	//alert($("#map").width() / $("#map").height());
	headlineWidth = $("#headline").width();
	
	if (headerWidth < 481) {
		//alert("small display");
		searchOptionsWidth = $("#searchOptions").width();
		addressInputWidth = Math.round(headerWidth - searchOptionsWidth - 24); // 10: 5px padding left/right; 5 for borders
		//alert(addressInputWidth);
		$("#addressInput").css("width",addressInputWidth);
		wastedSpace = Math.round(headerWidth - headlineWidth - 20 ) // 16 = padding headline (2x5) + padding search (2x5)
		//alert(wastedSpace);
		var panelWidth = $("#panel").width(); 
		var leftBarWidth =  Math.round(headerWidth - panelWidth - 30 );
		$("#leftBar").css("max-width",leftBarWidth);
	}

	// resize header height
	//headerHeight = $("#header").height();
	//zoomHeight = $(".leaflet-control-zoom").height()
	//zoomMargin = parseInt($(".leaflet-control").css("margin-top"));
	//$(".leaflet-top").css("top",headerHeight);
	////$("#ergebnisse").css("top",headerHeight + zoomMargin);
	//$("#panel").css("top",headerHeight + zoomHeight + zoomMargin); // zoom control height:67px
	
}

function sliderInit(){
	
	$(".slider").simpleSlider({
		//slides: '.slide', // The name of a slide in the slidesContainer
		//swipe: true,    // Add possibility to Swipe
		//magneticSwipe: true, // Add 'magnetic' swiping. When the user swipes over the screen the slides will attach to the mouse's position
		//transition: "slide", // Accepts "slide" and "fade" for a slide or fade transition
		//slideTracker: true, // Add a UL with list items to track the current slide
		//slideTrackerID: 'slideposition', // The name of the UL that tracks the slides
		//slideOnInterval: true, // Slide on interval
		//interval: 5000, // Interval to slide on if slideOnInterval is enabled
		//animateDuration: 1500, // Duration of an animation
		//animationEasing: 'easeInOut', // Accepts: linear ease in out in-out snap easeOutCubic easeInOutCubic easeInCirc easeOutCirc easeInOutCirc easeInExpo easeOutExpo easeInOutExpo easeInQuad easeOutQuad easeInOutQuad easeInQuart easeOutQuart easeInOutQuart easeInQuint easeOutQuint easeInOutQuint easeInSine easeOutSine easeInOutSine easeInBack easeOutBack easeInOutBack
		//pauseOnHover: false, // Pause when user hovers the slide container
		//useDefaultCSS: false, // Add default CSS for positioning the slides
		//neverEnding: true, // Create a 'neverending/repeating' slider effect.	
		//updateTransit: false
	});
	
	$(".slider").on("beforeSliding", function(event){
        var prevSlide = event.prevSlide;
        var newSlide = event.newSlide;
        $(".slider .slide[data-index='" + prevSlide + "'] .slidecontent").fadeOut();
        $(".slider .slide[data-index='" + newSlide + "'] .slidecontent").hide();

        sliding = true;
    });

    $(".slider").on("afterSliding", function(event){
        var prevSlide = event.prevSlide;
        var newSlide = event.newSlide;
        $(".slider .slide[data-index='"+newSlide+"'] .slidecontent").fadeIn();
        sliding = false;
    });		
}

function tooltips(){
	
	// tooltips but only on non touch devices
	if (isTouchSupported() == false){
		
		$('.tooltip-left, .leaflet-control-zoom-in, .leaflet-control-zoom-out').tooltip({
			content:function(){
				return this.getAttribute("title");
			},
			position: { my: "right", at: "left-10", collision: "flipfit" },
			track: false
		});
		
		$('.tooltip-right').tooltip({
			content:function(){
				return this.getAttribute("title");
			},
			position: { my: "left", at: "right-10", collision: "flipfit" },
			track: false
		});
		
		$(".tooltip, .help-tooltip").tooltip({
			content:function(){
				return this.getAttribute("title");
			},
			track: false
		});	
		
		if (debug == true) console.log("DEBUG(tooltips):Loading tooltips");
	};
}

$(document).ready(function() {
	
	tooltips();
	
	sliderInit();
	
	// ACCORDIONS
	$( ".accordion" ).accordion({
		activate: function(event, ui) {
			// Resize nicescroll 
            $( ".scroll" ).getNiceScroll().resize();
        },
		heightStyle: "content"
	});
	
	var sidebarOpen = false;
	
	// SEARCH BUTTON
	$( "#suche2" )
		.button()
		.click(function() {

			renew_PHP_session(settings_units);
			
			var location = document.getElementById('addressInput').value;
			var category = document.getElementById('categorySelect').value;
			var radius = document.getElementById('radiusSelect').value;
			
			//updateSearchAttributes('','',radius,category);
			
			//$("#searchOptions").hide('blind',{direction:'up'},250);

			map.closePopup();
			if ($("#sidebar").css('display') == "block") {
				openSidebar(false);
			}
			
			//updateURL(lang,'map','near',location,radius,category);
			
			// If we already have a location from geolocating the user, jump straight to our internal search, otherwise get latlng from nominatim
			if ($("#addressInput").attr('data-newSearch') == 'yes') {
				searchLocations();  
			} else {
				if (debug == true) console.log("searching near with latlng from geolocation");
				
				var lat = $("#addressInput").attr('data-lat');
				var lng = $("#addressInput").attr('data-lng');
				addressInput.setAttribute('data-radius', $("#radiusSelect").val());
				addressInput.setAttribute('data-category', $("#categorySelect").val());				
				updateURLNEW(lang,'map','near',lat,lng,radius,category);
				clearLocations();
				searchLocationsNear(lat,lng);
			}
			$("#addressInput").blur();

		});
	
	$( "#showAdvancedSearch" )
		.button()
		.click(function() {
			var $this = $(this);
			if ($this.is(':checked')) {
				$("#advancedSearchLabel").css("background","rgba(220,220,220,0.9)");
				//$("#multiSelect select").prop('disabled', 'disabled').trigger("chosen:updated");
				showAdvancedSearch();
				//resizeLeftBar();
			} else {
				document.getElementById('advancedSearchLabel').style.removeProperty('background');
				showAdvancedSearch();
				//resizeLeftBar();
			}
			
			
        $("#categorySelect").blur();
        
        //window.focus();
        
		});
	
	$( ".closeSidebar" )
		.button()
		.click(function() {
			openSidebar(false);
			
		});
	
	$( ".resultListToggle" )
		.button()
		.click(function() {
		searchWidth();
	
		if( !$('#ergebnisse').is(':visible') ){
			$( ".scroll" ).getNiceScroll().hide();
			$( "#resultList" ).hide( "fade", 500 );
			$( "#resultListInner" ).hide( "blind", 500 );
			//$("#ergebnisse").css("width",widthSearchRow);
			setTimeout(function(){
				$("#ergebnisse").show( "blind", 500 );
			},500);
		} else {
			$("#ergebnisse").hide( "blind", 500 );
			setTimeout(function(){
				$( "#resultList" ).show( "fade", 500 );
				$( "#resultListInner" ).show( "blind", 500 );
				//$("#ergebnisse").css("width",widthSearchRow);
				setTimeout(function(){
					$( ".scroll" ).getNiceScroll().show();
					
					
					// Resultlist: Scroll only if needed
					var sel = $('select[name=locationSelect]').val();
					if (sel != "none") {
						var el = $("ul.resultsListUL").find("[data-uuid='" + sel + "']");
						
						if ((typeof el != undefined) && (el != "")){
							//if (debug == true) console.log(el);
							if (isScrolledIntoView(el) == false){
								$("#resultList").animate({scrollTop: el.offset().top - $("#resultList").offset().top + $("#resultList").scrollTop()});
							}
						}
					}
				},600);
			},500);
			
		}

	});
	
	// TABS1
	$( "#tabs1" ).tabs( {
		beforeActivate: function(event, ui) {

			$( ".scroll" ).getNiceScroll().hide();
			if ((ui.newTab.length == 0 ) && (ui.newPanel.length == 0)) {
				//location.hash = '';	
				//_paq.push(['trackEvent', 'Menu', 'closed' ]);
				$(".leaflet-right").show("fade", 450 );
				$("#leftBar").show("fade", 450 );
				$("#panel").show("fade", 450 );
				
				setTimeout(function(){
					//alert($("#header").height());
					$("#tabs1").css("top", $("#header").height()+"px");
				},450);
				$("#tab_select1 li a").blur();
				
			} else {
				
				//location.hash = ui.newTab.context.hash;
				//$(window).resize();
				map.closePopup();
				$("#panel").hide("fade", 450 );
				$(".leaflet-right").hide("fade", 450 );
				$("#leftBar").hide("fade", 450 );
				if ($("#sidebar").is(':visible')) {
					openSidebar(false);
				};
				setTimeout(function(){
					//alert($("#header").height());
					$("#tabs1").css("top", $("#header").height()+"px");
				},501);
				var menu_item = ui.newTab.context.hash;
				
				if (piwik_loaded == true){
					_paq.push(['trackEvent', 'Menu', menu_item ]);
				};
				
			};
		},
		activate: function(event, ui) {				
			// Resize nicescroll 
			
			$("#sliderContainer").fadeIn();
			$("#sliderContainer").resize();
			$(".slidecontent").fadeIn();
			
			$('.scroll').getNiceScroll().resize();
			$('.scroll').getNiceScroll().show();
		},
		hide: { effect: "slide", direction: "up", duration: 500 },
		show: { effect: "slide", direction: "up", duration: 500 },
		active: false,
		collapsible: true
	});
	
	// TABS2
	$( "#tabs2" ).tabs({
		beforeActivate: function(event, ui) {

			//location.hash = ui.newTab.context.hash;
			var legend = ui.newTab.context.hash; //location.hash;
			$(".tabs2_toggle").show();
			$( ".tabs2_icon" ).switchClass("icon-resize-full", "icon-resize-small", 500);
			
			if (piwik_loaded == true){
				_paq.push(['trackEvent', 'Legende', legend ]);
			}
		},
		beforeLoad: function( event, ui ) {
			ui.jqXHR.error(function() {
				ui.panel.html(
				"Couldn't load this tab. We'll try to fix this as soon as possible. " +
				"If this wouldn't be a demo." );
			});
		},
		activate: function(event, ui) {
			$( ".scroll" ).getNiceScroll().resize();
			$("#sliderContainer").resize();
			$(".slidecontent").fadeIn();
		}
	});

	
	$( "#locate2")
		.button()
		.click(function(){
			
			var $this = $(this);
			if ($this.is(':checked')) {
				
				$(".icon-location").css("color","steelblue");
				$("#nominatimResultsOuter").hide('blind',{direction:'up'});
				
				if (map.hasLayer(polyBounds)){
					//alert("has bounds");
					map.removeLayer(polyBounds)
				};
				
				//clearLocMarker();
				clearLocations();
				doGeolocation();
				
			
			} else {
			
				clearLocMarker();
				//document.getElementById('addressInput').style.color = "grey";
				document.getElementById('addressInput').value = '';
				$(".icon-location").css("color","black");
			
			}
		});
		
	$("#addressInput")
		.click(function(){
			// If geolocation is on
			if ($('#locate2').is(':checked')) {
				$('#locate2').click(); 
			}	
			
			$('#nominatimResultsOuter').hide('blind',{direction:'up'}); 
			this.value=''; 
			this.style.color='#2d2d2d'; 
			//this.setAttribute('data-lat',''); 
			//this.setAttribute('data-lng',''); 
			this.setAttribute('data-newSearch','yes'); 
			geoLocOn = false; 
			oldAddress='';
			
		})
		.keydown(function(e){
			
			var key = e.keyCode || e.which;

			if(key == 13) {
				document.getElementById('suche2').click();
			}
			
			
			//if (event.keyCode == 13) document.getElementById('suche2').click();
		});
		
	// LAYERS MENU BUTTON
	$( "#layersMenu" )
		.button()
		.click(function() {
			$('#legend').prop('checked', false);
			$('#settings').prop('checked', false);
			var $this = $(this);
			
			if ($('#sidebar').css('display') == 'block'){
				
				if ($this.is(':checked')) {
				//if (!$this.hasClass('pressed')) {
					$this.prop('checked', true);
					$("#layersMenuLabel").removeClass("pressed2");
					//openSidebar(true);
					
					showLayersMinimap(true);
					$(".locateLabel").removeClass("pressed");
					$("#layersMenuLabel").addClass("pressed");
					
				} else {
					openSidebar(false);
					showLayersMinimap(false);
					//openSidebar(false);	
					$("#layersMenuLabel").removeClass("pressed");
				
				}
				
			} else {
				$this.prop('checked', true);
				
				showLayersMinimap(true);
				
				$(".locateLabel").removeClass("pressed");
				$("#layersMenuLabel").addClass("pressed");
				
			}
		});
		$("#layersMenuLabel")
		.mouseenter(function() {
			//alert("mousenetr");
			$("#layersMenuLabel").removeClass("ui-state-hover");
			$("#layersMenuLabel").addClass("pressed2");
		})
		.mouseleave(function() {
			//alert("mouseout");
			$("#layersMenuLabel").removeClass("pressed2");
		})
	
	// LEGEND MENU BUTTON
	$( "#legend" )
		.button()
		.click(function() {
			$('#layersMenu').prop('checked', false);
			$('#settings').prop('checked', false);
			var $this = $(this);
			
			if ($('#sidebar').css('display') == 'block'){
				if ($this.is(':checked')) {
				//if (!$this.hasClass('pressed')){
					$this.prop('checked', true);
					//$this.attr('checked','checked')
					$("#legendMenuLabel").removeClass("pressed2");
					
					showLegend(true);
					$(".locateLabel").removeClass("pressed");
					$("#legendLabel").addClass("pressed");
				} else {
					openSidebar(false);
					showLegend(false);
					$("#legendLabel").removeClass("pressed");
				}
			} else {
				$this.prop('checked', true);
							
				showLegend(true);
				$(".locateLabel").removeClass("pressed");
				$("#legendLabel").addClass("pressed");
				
			}
		});
		$("#legendLabel")
		.mouseenter(function() {
			//alert("mousenetr");
			$("#legendLabel").removeClass("ui-state-hover");
			$("#legendLabel").addClass("pressed2");
		})
		.mouseleave(function() {
			//alert("mouseout");
			$("#legendLabel").removeClass("pressed2");
		})

	// SETTINGS MENU BUTTON
	$( "#settings" )
		.button()
		.click(function() {
			$('#layersMenu').prop('checked', false);
			$('#legend').prop('checked', false);
			var $this = $(this);
			
			if ($('#sidebar').css('display') == 'block'){
				if ($this.is(':checked')) {
				//if (!$this.hasClass('pressed')){
					$this.prop('checked', true);
					//$this.attr('checked','checked')
					$("#settingsMenuLabel").removeClass("pressed2");
					if (debug == true) console.log("showing settings");
					showSettings(true);
					//$("#layersMenuLabel").removeClass("pressed");
					//$("#legendMenuLabel").removeClass("pressed");
					$(".locateLabel").removeClass("pressed");
					$("#settingsLabel").addClass("pressed");
				} else {
					openSidebar(false);
					showSettings(false);
					$("#settingsLabel").removeClass("pressed");
				}
			} else {
				$this.prop('checked', true);
							
				showSettings(true);
				$(".locateLabel").removeClass("pressed");
				$("#settingsLabel").addClass("pressed");
				
			}
		});
		$("#settingsLabel")
		.mouseenter(function() {
			//alert("mousenetr");
			$("#settingsLabel").removeClass("ui-state-hover");
			$("#settingsLabel").addClass("pressed2");
		})
		.mouseleave(function() {
			//alert("mouseout");
			$("#settingsLabel").removeClass("pressed2");
		})


	// LEGEND BUTTON
	$( "#DEACT_legend" )
		.button()

		.click(function() {
			var $this = $(this);
			if ($this.is(':checked')) {
				$("#legendLabel").removeClass("pressed2");
				$("#tabs2").show();
				$("#legendLabel").addClass("pressed");

			} else {
				$("#tabs2").hide();
				$("#legendLabel").removeClass("pressed");
				$("#legendLabel").removeClass("pressed2");
			}
		});

		$("#legendLabel")
		.mouseenter(function() {
			//alert("mousenetr");
			$("#legendLabel").removeClass("ui-state-hover");
			$("#legendLabel").addClass("pressed2");
		})

		.mouseleave(function() {
			//alert("mouseout");
			$("#legendLabel").removeClass("pressed2");
		})

	// FULLSCREEN BUTTON
	$( "#fullscreen" )
		.button()

		.click(function() {
			var $this = $(this);
			if ($this.is(':checked')) {
				$("#fullscreenLabel").removeClass("pressed2");		
				if (screenfull.enabled) {
					//alert("fs");
					screenfull.request();
				} else {
					alertify.alert("Sorry, Ihr Browser unterstützt die Anzeige im Fullscreen Modus leider nicht :/");
				}	
				$("#fullscreenLabel").addClass("pressed");
				$("#fullscreenLabel").children().html('<i class="icon-resize-small">');

			} else {
				screenfull.exit();
				$("#fullscreenLabel").removeClass("pressed");
				$("#fullscreenLabel").removeClass("pressed2");
				$("#fullscreenLabel").children().html('<i class="icon-resize-full">');
			}
			
			// Resize leftBar
			setTimeout(function(){
				resizeLeftBar();
			},100);
		});
		
		
		$("#fullscreenLabel")
		.mouseenter(function() {
			//alert("mousenetr");
			$("#fullscreenLabel").removeClass("ui-state-hover");
			$("#fullscreenLabel").addClass("pressed2");
		})

		.mouseleave(function() {
			//alert("mouseout");
			$("#fullscreenLabel").removeClass("pressed2");
		})


	// HELP BUTTON
	$( "#help" )
		.button()

		.click(function() {
			var $this = $(this);
			if ($this.is(':checked')) {
				$("#helpLabel").removeClass("pressed2");	
				
				startIntro();
				$("#helpLabel").addClass("pressed");

			} else {
				introJs().exit()
				$("#helpLabel").removeClass("pressed");
				$("#helpLabel").removeClass("pressed2");
			}
		});

		$("#helpLabel")
		.mouseenter(function() {
			//alert("mousenetr");
			$("#helpLabel").removeClass("ui-state-hover");
			$("#helpLabel").addClass("pressed2");
		})

		.mouseleave(function() {
			//alert("mouseout");
			$("#helpLabel").removeClass("pressed2");
		})

		
		$("")
		.mouseenter(function() {
			//alert("mousenetr");
			$(this).removeClass("ui-state-hover");
			$(this).css('color','white');
			//$(this).css('background','rgb(100,100,100)');
			//alert($(this));
			$(this).parent().css('background','url(/img/down_arrow_white.png) no-repeat 99% rgb(100,100,100)');
		})

		.mouseleave(function() {
			//alert("mouseout");
			$(this).removeClass("pressed2");
		})


	$( "#multiSelectCheckbox" ).change(function(){
		if($('#multiSelectCheckbox').prop('checked')) {
			$('#multiSelectOuter').show('blind');
			$("#categorySelect").find("[value='all']").prop('selected', true);
			$("#categorySelect").prop('disabled', 'disabled');
			var bg = "url(/img/down_arrow.png) no-repeat 96% rgb(220,220,220)";
			$("#categorySelectOuter").css('background',bg);
			$("#categorySelect").css('color','rgb(150,150,150)');
			$("#multiSelect select").removeAttr('disabled').trigger("chosen:updated");
		} else {
			$("#categorySelect").removeAttr('disabled');
			//$("#categorySelectOuter").css('background','url(/img/down_arrow.png) no-repeat 96% rgb(240,240,240)');
			document.getElementById('categorySelectOuter').style.removeProperty('background');
			$("#categorySelect").css('color','#2d2d2d');
			$("#multiSelect select option").prop('selected', false).trigger("chosen:close");
			$("#multiSelect select").prop('disabled', 'disabled').trigger("chosen:updated");
			$('#multiSelectOuter').hide('blind');
		}
	});

	$( "#limitToPolygon" ).change(function(){
		if($('#limitToPolygon').prop('checked')) {
			$("#radiusSelect").prop('disabled', 'disabled');
			var bg = "url(/img/down_arrow.png) no-repeat 92% rgb(220,220,220)";
			$("#radiusSelectOuter").css('background',bg);
			$("#radiusSelect").css('color','rgb(150,150,150)');
		} else {
			$("#radiusSelect").removeAttr('disabled');
			//$("#categorySelectOuter").css('background','url(/img/down_arrow.png) no-repeat 96% rgb(240,240,240)');
			document.getElementById('radiusSelectOuter').style.removeProperty('background');
			$("#radiusSelect").css('color','#2d2d2d');
		}
	});

	
	// NICESCROLL
	// Overlays for 'About', 'Impressum' etc.
	nice = $( ".scroll" ).niceScroll({horizrailenabled:false, autohidemode:false, cursorborder:'1px solid #ffffff', cursorcolor:'rgb(169,169,169)', cursorwidth: 4, railpadding: { top: 0, right: 0, left: 9, bottom: 0 }});

	$( ".scroll" ).getNiceScroll().resize();
	
	// Results Box Overlay
	//$( "#searchOuter" ).niceScroll({horizrailenabled:false, autohidemode:false, cursorwidth: 3, railpadding: { top: 0, right: 0, left: 0, bottom: 0 }});
	//$( "#searchOuter" ).getNiceScroll().resize();
	
	


	

	$( "#hide_results" ).click(function() {
		$( "#results" ).toggle( "blind", 500 );
    });

    // set effect from select menu value
    $( ".toggle_highlights" ).click(function() {
		$( ".highlights" ).toggle( "blind", 500 );
		setTimeout (function() {
			$( ".scroll" ).getNiceScroll().resize();
		},500);
    });
    
	$( ".tabs2_toggle" ).click(function() {
		event.preventDefault();
		$( ".tabs2_icon" ).toggleClass("icon-resize-small icon-resize-full", 500);
		$( ".toggle_content" ).toggle( "blind", 500 );
    });
    

	// FLASHY WARNINGS
	function pulsate() {
		$(".pulsate").animate({opacity: 0.1}, 1000, 'linear').animate({opacity: 1}, 1000, 'linear', pulsate);
	}
	pulsate();
	
	// SOCIALSHAREPRIVACY
	if($('.socialshareprivacy').length > 0){
		$('.socialshareprivacy').socialSharePrivacy( {
			services : {
				facebook : {
				  'perma_option' : 'off',
				  'action' : 'recommend',
				  'dummy_img' : 'socialshareprivacy/images/dummy_facebook.png',
				  //'layout' : 'box_count'
				}, 
				twitter : {
				  //'count' : 'vertical'
				},
				gplus : {
				  //'size' : 'tall'
				}
			},
			'css_path'      : '/css/new-osm.css'
		}); 
	}

	$(function() {
		$.widget( "custom.iconselectmenu", $.ui.selectmenu, {
			_renderItem: function( ul, item ) {
				var li = $( "<li>", { text: item.label } );
		 
				if ( item.disabled ) {
				  li.addClass( "ui-state-disabled" );
				}
		 
				$( "<span>", {
				  style: item.element.attr( "data-style" ),
				  "class": "ui-icon " + item.element.attr( "data-class" )
				})
				  .appendTo( li );
		 
				return li.appendTo( ul );
			}
		});
		
		$( "#locationSelect_DEACT" )
			.iconselectmenu()
			.iconselectmenu( "menuWidget")
			.addClass( "ui-menu-icons customicons" );
	});
	
	$(".chosen").chosen({
		//disable_search_threshold: 10,
		no_results_text: "Oops, nothing found!",
		width: "100%",
		padding: "10px"
	});
	
	function startIntro(){
		//document.getElementById('addressInput').value = "Berlin, Frankfurter Allee";
		//document.getElementById('categorySelect').value = "air_quality";
		//$("#suche2").click();
		//setTimeout(function(){
		//	$('select[name=locationSelect] option[value=DEBE065]').attr('selected', 'selected');
		//	$('select[name=locationSelect]').change();
		//},1000);
		
		//setTimeout(function(){
			introJs()
				.setOptions({
					steps: [
						{
							intro: "<h4>Willkommen!</h4><p>Die folgende Tour dauert ca. 30 Sekunden und wird Ihnen die wichtigsten Funktionen zeigen.</p><p>  Sie können die Tour jederzeit abbrechen.</p>"
						},
						{
							element: '#searchOuter',
							intro: '<h4>Suche</h4><p>Geben Sie hier den Suchort ein, oder nutzen Sie die Geolocation Funktion (<i class="icon-location"></i>). Wählen Sie anschließend eine Kategorie aus, passen den Such-Radius an, und schicken dann Ihre Suche mit dem grünen Button ab.</p><p>Die Ergebnisse erscheinen dann nach wenigen Augenblicken als farbige Marker auf der Karte. Berühren Sie die Marker, um alle Infos anzuzeigen.</p>',
							position: 'right'
							
						},

						{
							element: '.leaflet-control-zoom',
							intro: "<h4>Zoom </h4><p>Kartenansicht vergrößern oder verkleinern. Sie können auch Ihr Mausrad, oder auf mobilen Geräten Ihre Finger benutzen.</p>",
							position: 'left'
						},
						{
							element: '#panel',
							intro: "<h4>Fullscreen an/aus</h4><p>Fullscreen Anzeige ein oder ausschalten. Dies ist besonders auf kleinen Displays hilfreich, und den gesamten auf Ihrem Gerät zur Verfügung stehenden Platz auszunutzen. Viele aber nicht alle Geräte und Browser unterstützden dies bereits.</p><h4>Layer Auswahl</h4><p>Hier k&ouml;nnen Sie den Kartenhintergrund sowie Karten-Overlays auswählen</p><h4>Legende an/aus</h4><p>Karte-Legende mit Informationen zu den Datensätzen ein- oder ausblenden.</p><h4>Hilfe an/aus</h4><p>Die interaktive Hilfe-Funktion (die Sie gerade nutzen) ein- oder ausblenden</p>",
							position: 'left'
						},
						{
							element: '#tab_select1',
							intro:  "<h4>Navigations Menu</h4><p>Infos zu weiteren Funktionen wie z.B. der Umweltdaten API, Hintergrund-Infos, sowie das Impressum.</p>",
							position: 'left'
						},
						{
							intro:  "<h4>Tour beendet</h4><p>Wenn Sie weitere Fragen haben, die hiermit nicht beantwortet wurden, dann können Sie auch eine Email an 'info@offene-umweltdaten.de' schreiben.</p>"
						}
					],
					nextLabel: 'Weiter',
					prevLabel: 'Zurück',
					skipLabel: 'Abbrechen',
					doneLabel: 'Alles klar!',
					highlightClass: 'tour-highlight'
				})
				.start()
				.oncomplete(function() {
					$("#help").click();
				})
				.onexit(function() {
					$("#help").click();
				})

			//},2000);
	};
	
	function showAdvancedSearch(){
		$("#searchOptions").toggle("blind",{direction: 'up'},500,function(){
			resizeLeftBar();
			searchWidth();
			$('.scroll').getNiceScroll().resize();
		});
		
	}
	
	if (settings_units === "metric") {
		settings_units_bool = true;
	} else {
		settings_units_bool = false;
	};
	
	$("#unitToggle").toggles({text:{on:lang_settings_unit_metric,off:lang_settings_unit_imperial}, type:'select', on:settings_units_bool});
	$('#unitToggle').on('toggle', function(e, active) {
		if (active) {
			var unit = lang_settings_unit_metric;
			var UNIT = 'metric';
			DISTANCEUNIT = 'km'; // GLOBAL
			//if (debug == true) console.log('Unit is now METRIC');
		} else {
			var unit = lang_settings_unit_imperial;
			var UNIT = 'imperial';
			DISTANCEUNIT = 'mi.'; // GLOBAL
			//if (debug == true) console.log('Unit is now IMPERIAL!');
		}
		toggleUnits(unit,UNIT,DISTANCEUNIT);
	});
	
	$("#timezoneToggle").toggles({text:{on:lang_settings_timezone_local,off:lang_settings_timezone_utc}, type:'select', on:'true'});
	$('#timezoneToggle').on('toggle', function(e, active) {
		if (active) {
			var timezone = lang_settings_timezone_local;
			//if (debug == true) console.log('Timezone is now ' + timezone);
		} else {
			var timezone = lang_settings_timezone_utc;
			//if (debug == true) console.log('Timezone is now ' + timezone);
		}
		toggleTimezone(timezone);
	});
});

function renew_PHP_session(settings_units) {
	if (debug == true) console.log("renewing php session");
	$.ajax({
		cache:false,
		type: "GET",
		url: "/renew_session.php?UNITS=" + settings_units ,
		success : function() { 

			// here is the code that will run on client side after running clear.php on server

			// function below reloads current page
			//location.reload();

		}
	});
}

function toggleUnits(unit,UNIT,DISTANCEUNIT) {
	//document.cookie = "UNITS="+UNIT+"; expires=; path=/";
	settings_units = UNIT;
	//document.cookie = "DISTANCE_UNIT="+distanceUnit+"; expires=; path=/";
	distanceUnit = DISTANCEUNIT;
	
	//if (debug == true) console.log($(".distanceUnit").val()); 
	
	$(".distanceUnit").map( function() {
		var val = $(this).val();
		$(this).html(val + distanceUnit);
	});
	
	$('.resultListItemDistance').map( function() {
		var val = parseFloat($(this).html());
		
		if (distanceUnit == "km") {
			var val = Math.round( (val * 1.60934) * 10 ) / 10;
		} else {	
			var val = Math.round( (val * 0.621371) * 10 ) / 10;
		};
		
		if (debug == true) console.log('val: ' + val);
		$(this).html(val);
	});
	
	$('.resultListItemDistanceUnit').map( function() {
		var val = $(this).html();
		$(this).html(distanceUnit);
	});

	renew_PHP_session(settings_units);

	if (debug == true) console.log(unit);
}

function toggleTimezone(timezone) {
	//document.cookie = TIMEZONE + " = " + timezone + "" + "; path=/";
	if (debug == true) console.log(timezone);
	var myDate = new Date();
	if (debug == true) console.log('device time: ' + myDate);
	
}

function updateURL(lang,doctype,searchtype,location,radius,category){
	if ((typeof category != 'undefined') && (typeof location != 'undefined') && (typeof radius != 'undefined')){
		window.history.pushState('object', 'New Title', ('/' + lang + '/' + doctype + '/' + searchtype + '/' + location + '/' + radius + '/' + category));
	}
}

function updateURLNEW(lang,doctype,searchtype,lat,lng,radius,category){
	if ((typeof category != 'undefined') && (typeof location != 'undefined') && (typeof radius != 'undefined')){
		
		// the following is a nasty workaround: 
		// we need to call this twice, because of the way we manipulate the url when opening popups (replace instead of push). this make a much "cleaner" experience when the user moves back and forward in the browser history...
		window.history.pushState('near', 'New Title', ('/' + lang + '/' + doctype + '/' + searchtype + '/' + lat + ',' + lng + '/' + radius + '/' + category));
		//window.history.pushState('near', 'New Title', ('/' + lang + '/' + doctype + '/' + searchtype + '/' + lat + ',' + lng + '/' + radius + '/' + category));
	}
}


function searchWidth(){
	
	so = $("#searchOuter").width() + 'px';
	sw = ($("#searchRow").width() +22) + 'px';
	nw = ($("#searchRow").width() -2) + 'px';
	
	if (debug == true) console.log('searchwidth -> so: ' + so + ' / sw: ' + sw + ' / nw: ' + nw);
	
	$("#nominatim_results").css("width",nw);
	$("#ergebnisse").css("width",sw);
	$("#resultListInner").css("max-width",so);
	$(".chosen-container").css("max-width",nw);
}

function showLayersMinimap(show){
	
	if (show == true) {

		if ($('#sidebar').css('display') == 'none'){
			
			//$('#layersBar').show('fade');
			$('#layersBar').css('display','block');
			openSidebar(true);
			
			//$( ".scroll" ).getNiceScroll().resize();
		} else {
			
			if (debug == true) console.log('sidebar already open');
			$('.panelElement').hide('fade',250);
			$('#layersBar').show('fade',250);
			//control._expand();
		}
		
		
		
	} else {
		
		$('.panelElement').hide('fade',250);
		
	}
	
	// Use private method to initialize the minimaps layers control
	control._expand();

	// Wait till slide out animation is finished, then resize nicescroll (otherwise it won't appear)
	setTimeout (function() {
		$( ".scroll" ).getNiceScroll().resize();
	},260);
	
};

function showLegend(show){
	
	if (show == true) {
		
		if ($('#sidebar').css('display') == 'none'){
			
			//$('#legendBar').show('fade');
			$('#legendBar').css('display','block');
			openSidebar(true);
			
		} else {
			
			if (debug == true) console.log('sidebar already open');
			$('.panelElement').hide('fade',250);
			$('#legendBar').show('fade',250);
			
		}
		
		// this triggers a minimap resize; needed if the container is hidden
		setTimeout (function() {
			
			$( ".scroll" ).getNiceScroll().resize();
		},260);
		
		//$(".sidebarElement").css("display","none");
		//$("#layersMinimap").css("display","block");
		
	} else {
		
		
		$('.panelElement').hide('fade',250);
		
	}
	
};	

function showSettings(show){
	
	if (show == true) {
		
		if ($('#sidebar').css('display') == 'none'){
			
			//$('#legendBar').show('fade');
			$('#settingsBar').css('display','block');
			openSidebar(true);
			
		} else {
			
			if (debug == true) console.log('sidebar already open');
			$('.panelElement').hide('fade',250);
			$('#settingsBar').show('fade',250);
			
		}
		
		// this triggers a minimap resize; needed if the container is hidden
		setTimeout (function() {
			
			$( ".scroll" ).getNiceScroll().resize();
		},260);
		
		//$(".sidebarElement").css("display","none");
		//$("#layersMinimap").css("display","block");
		
	} else {
		
		
		$('.panelElement').hide('fade',250);
		
	}
	
};	

function openSidebar(show){
	
	var sidebarWidth = Math.round($("#sidebar").width() + 10);
	
	//$(window).resize();
	
		// Check if the sidebar is already open
		if (show == true) {
			if (debug == true) console.log('opening sidebar');
			
			if (openMarkerUID != false) {
				markerWasOpen = openMarkerUID;
				map.closePopup();
			}
			$("#sidebar").show("slide", { direction: 'right', duration:250, });
			$(".leaflet-right").animate({"right": "+=" + sidebarWidth},250);
			$("#panel").animate({"right": "+=" + sidebarWidth}, 250);
			//map.panBy([sidebarWidth,0],{easeLinearity:0.25});
			//$(".leaflet-control-layers-expanded.leaflet-control-layers-minimap").scrollTop(1);
		} else {
			if (debug == true) console.log('closing sidebar');
			if (markerWasOpen != false) {
				if (debug == true) console.log(openMarkerUID);
				setTimeout(function(){
					clickMarker(markerWasOpen);
					markerWasOpen = false;
				},300);
			}
			
			$("#sidebar").hide("slide", { direction: 'right', duration:250 });
			$(".leaflet-right").animate({"right": "-=" + sidebarWidth},250);
			$("#panel").animate({"right": "-=" + sidebarWidth},250);
			$(".locateLabel").removeClass("pressed");
			$(".panelElement").hide("fade",250);
			//map.panBy([-sidebarWidth,0],{easeLinearity:0.25,});
			setTimeout (function() {
				$( ".scroll" ).getNiceScroll().resize();
			},260);
		}

};

// KAZZOOM!1!11!
function init(){
		
		//setLanguage();
		
		searchWidth();
		
		searchFromURL();

};

function setLanguage(){
	
	// Set Language for moment.js 

}

// Do a search when incoming deep-link is used
function searchFromURL(){
	// Check if we have a path or not (watch out for trainling slash)
	if((window.location.pathname) && (window.location.pathname != '/')) {
		//g = window.location.hash.substr(1);
		// make sure we decode umlauts correctly!
		var pathArray = window.location.pathname.split( '/' );
		
		var path_language = pathArray[1];
		var path_doctype = pathArray[2];
		if (path_doctype == 'map') {
			var path_searchtype = pathArray[3];
			
			if (path_searchtype == "near") {
				
				var path_location = pathArray[4];
				var path_location = path_location.split( ',' );
				var path_location_lat = path_location[0];
				var path_location_lng = path_location[1];
				var path_radius = pathArray[5];
				var path_category = pathArray[6];
				
				if ((path_radius === 'undefined') || (path_radius == null) || (path_radius == '/') || (path_radius == '')){
					document.getElementById('radiusSelect').value = '50';				
				} else {
					document.getElementById('radiusSelect').value = path_radius;
					addressInput.setAttribute('data-radius', path_radius);
				}
				
				if ((path_location === 'undefined') || (path_location == null) || (path_location == '/') || (path_location == '') || (path_location == '#')){
					
				} else {
					var path_location = decodeURIComponent(path_location);
					document.getElementById('addressInput').style.color = 'black';
					document.getElementById('addressInput').value = path_location;
					addressInput.setAttribute('data-lat', path_location_lat);
					addressInput.setAttribute('data-lng', path_location_lng);
					clearLocations();
					searchLocationsNear(path_location_lat,path_location_lng);
				}
				
				if ((path_category === 'undefined') || (path_category == null) || (path_category == '/') || (path_category == '')){
					document.getElementById('categorySelect').value = 'all';
				} else {
					document.getElementById('categorySelect').value = path_category;
					addressInput.setAttribute('data-category', path_category);
				}
				
			} else if (path_searchtype == "station") {
				
				clearLocations();
				//var path_uuid = pathArray[4];
				searchLocationsNear();
			}

		} else if (path_doctype == 'api') {
			// NOT YET IMPLEMENTED
		} else if (path_doctype == 'text') {
			// NOT YET IMPLEMENTED
		}
		
		//alert('path_category: ' + path_category + ' / path_uid: ' +path_uid + ' / path_location: ' + path_location);
				
	} 	
}

window.addEventListener('popstate', function(e) {
	if (debug == true) console.log(e.state);
	var pathArray = window.location.pathname.split( '/' );
	if (debug == true) console.log("popstate patharray: " + pathArray);
	
	if (e.state == "station") {
		var path_uuid = pathArray[4];
		var marker = markerMap[path_uuid];

		// if marker is part of a markercluster, spiderfy first before opneing popup
		if (!marker._icon) marker.__parent.spiderfy();
		
		// Open the marker popup
		marker.openPopup(marker);
		
	} else if (e.state == "near") {
		var path_location = pathArray[4];
		var path_location = path_location.split( ',' );
		var path_location_lat = path_location[0];
		var path_location_lng = path_location[1];
		clearLocations();
		searchLocationsNear(path_location_lat,path_location_lng);
	}
	
	//
});
