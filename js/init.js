var debug = true;

var piwik_loaded = false; // needs to be set here to prevent UI bugs if user blocks piwik

function getDimensions(){

	// Leaflet / General Layout
	w = $("#map").width();
	heightLeafLetBottom = $(".leaflet-bottom").height();

	// Header & footer
	heightHeader = $("#header").height();
	heightFooter = $("#footer").height();

	// Width of the searchbar (left) and the sidebar (right) so we can trigger show/hide effects Left and right sidebars
	widthSearch = $("#searchOuter").width();
	widthSidebar = parseFloat($("#panel").css('right'));

	// Width of the panel on the right
	widthPanel = $("#panel").width();

	// Search box
	heightSearch = $("#searchOuter").height();
	heightSearchRow = $("#searchRow").height();
	widthSearchRow = $("#searchRow").width();

	// Resultlist
	heightResultListHeader = $("#resultListHeader").height();
	heightResultListOuter = $("#resultListOuter").height();

}

function isSmallDisplay(){

	if (w < (widthSearch * 2)) {
		return true;
	} else {
		return false;
	}
}

function getOffsets(){

	// Map offsets ("padding")
	offsetLeft = widthSearch + 22;
	offsetTop = heightHeader + 11;
	offsetRight = widthSidebar + widthPanel + 22;
	offsetBottom = heightFooter + 10;

	if (isSmallDisplay() == true) {
		offsetLeft = 11;
		offsetTop = heightHeader + heightSearch + heightResultListOuter + 32;
	}

	// Get the maximum height we can use
	h = ($("#map").height() - heightHeader - heightFooter);

	// Get the maximum height for the resultList in the leftbar
	leftBarHeight = h - heightHeader - heightSearch - heightLeafLetBottom -30; // 30 is sum of all margins in between

	// Get the maximum width for the popup
	wmax = w - offsetLeft - offsetRight + 10;
	if (wmax > 800) {
		wmax = 800;
	}
	wmin = wmax;
	hmax = h - offsetTop - heightHeader - offsetBottom - 20;


	// IMPORTANT: if the width of css class iw_box changes, change here as well!!!
	iw_box_width = (wmax - 10);
	bar_width = (iw_box_width - 23);
	g_container_w = (iw_box_width - 30)+"px";


	if (debug == true) console.log('DEBUG(getOffsets): offsetTop: ' + offsetTop + ' / offsetRight: ' + offsetRight + ' / offsetBottom: ' + offsetBottom + '/ offsetLeft: ' + offsetLeft + ' / popup-wmin: ' + wmin + ' / popup-wmax: ' + wmax + ' / popup-hmax: ' + hmax);

}

function resizeLeftBar(){
	getDimensions();
	getOffsets();
	// This resize the search + results box on the left
	$('.nominatimResultListUL').css('max-height',leftBarHeight - 100 + 'px');
	$('#resultList').css('max-height',leftBarHeight + 'px');

	if (debug == true) console.log('DEBUG(resizeleftbar): #resultList max-height: ' + leftBarHeight + 'px');
}

// Check if we have a touch device
function isTouchSupported(){
    var msTouchEnabled = window.navigator.msMaxTouchPoints;
    var generalTouchEnabled = "ontouchstart" in document.createElement("div");

    if (msTouchEnabled || generalTouchEnabled){
		//alert("TOUCH");
		if (debug == true) console.log("DEBUG(isTouchSupported): Touch device: TRUE");
        return true;
    }
    if (debug == true) console.log("DEBUG(isTouchSupported): Touch device: FALSE");
    return false;
}

