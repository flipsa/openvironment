// Show dygraph
function show_graphNEW(uuid,uid,category,compound,compound_period,limit){
	//alert(uid + category + compound + compound_period + limit);
	//var compound = $("#graph").parents("[data-compound]:first").attr('data-compound');

	var graph = 'graph';
	//var graph = document.getElementById('g_container');

	// Set the labels according to category

	if ( category == "air_quality"){
		if (compound == ""){
			var LABELS = ["Datum", "Kohlenmonoxid / CO (mg/m³)", "Stickstoffdioxid / NO2 (µg/m³)", "Ozon / O3 (µg/m³)", "Feinstaub / PM10 (µg/m³)", "Schwefeldioxid / SO2(µg/m³)" ];
			var VISIBILITY = [true, true, true, true, true];
		} else if (compound == "co"){
			LABELS = ["Datum", "CO (mg/m³)"];
			VISIBILITY = [true];
		} else if (compound == "no2"){
			LABELS = ["Datum", "NO2 (mg/m³)"];
			VISIBILITY = [true];
		} else if (compound == "o3"){
			LABELS = ["Datum", "O3 (µg/m³)"];
			VISIBILITY = [true];
		} else if (compound == "pm10"){
			LABELS = ["Datum", "PM10 (µg/m³)"];
			VISIBILITY = [true];
		} else if (compound == "so2"){
			LABELS = ["Datum", "SO2(µg/m³)"];
			VISIBILITY = [true];
		}
	} else if (category == "weather"){
		if (compound == ""){
			var LABELS = ["Datum", "Luftdruck (hPa)", "Temperatur(°C)", "Niederschlag(l/m²)", "Windgeschw.(km/h)", "Windspitzen(km/h)" ];
			var VISIBILITY = [false, true, true, true, true, true, true, true, true];
		} else if (compound == "pressure"){
			LABELS = ["Datum", "Luftdruck (hPa)"];
			VISIBILITY = [true];
		} else if (compound == "temperature"){
			LABELS = ["Datum", "Temperatur(°C)"];
			VISIBILITY = [true];
		} else if (compound == "precipitation"){
			LABELS = ["Datum", "Niederschlag(l/m²)"];
			VISIBILITY = [true];
		} else if (compound == "wind_speed"){
			LABELS = ["Datum", "Windgeschw.(km/h)"];
			VISIBILITY = [true];
		} else if (compound == "wind_peak"){
			LABELS = ["Datum", "Windspitzen(km/h)"];
			VISIBILITY = [true];
		}
	} else if (category == "radiation"){
		if (compound == ""){
			var LABELS = ["Datum", "Gamma ODL (µSv/h)"];
			var VISIBILITY = [true];
		} else if (compound == "odl"){
			LABELS = ["Datum", "Gamma ODL (µSv/h)"];
			VISIBILITY = [true];
		}
	} else if (category == "pegel"){
		if (compound == ""){
			var LABELS = ["Datum", "Pegel(cm)", "Temp. Luft(C°)", "Temp. Wasser(C°)", "Abfluss(m³/s)", "Fliessgeschw.(m/s)", "Durchfahrtsh&ouml;he(cm)", "Elektr. Leitf&auml;higk.(µS/c)", "Windgeschw.(m/s)", "Windrichtung(Grad)", "Grundwasser(m+NN)", "Luftfeuchte(%)", "Luftdruck(hPa)", "Niederschlag(mm/h)" ];
			var VISIBILITY = [true, true, true, true, true, true, true, true, true, true, true, true, true];
		} else if (compound == "w"){
			LABELS = ["Datum", "Pegel(cm)"];
			VISIBILITY = [true];
		} else if (compound == "lt"){
			LABELS = ["Datum", "Temp. Luft(C°)"];
			VISIBILITY = [true];
		} else if (compound == "wt"){
			LABELS = ["Datum", "Temp. Wasser(C°)"];
			VISIBILITY = [true];
		} else if (compound == "q"){
			LABELS = ["Datum", "Abfluss(m³/s)"];
			VISIBILITY = [true];
		} else if (compound == "va"){
			LABELS = ["Datum", "Fliessgeschw.(m/s)"];
			VISIBILITY = [true];
		} else if (compound == "dfh"){
			LABELS = ["Datum", "Durchfahrtsh&ouml;he(cm)"];
			VISIBILITY = [true];
		} else if (compound == "lf"){
			LABELS = ["Datum", "Elektr. Leitf&auml;higk.(µS/cm)"];
			VISIBILITY = [true];
		} else if (compound == "wg"){
			LABELS = ["Datum", "Windgeschw.(m/s)"];
			VISIBILITY = [true];
		} else if (compound == "wr"){
			LABELS = ["Datum", "Windrichtung(Grad)"];
			VISIBILITY = [true];
		} else if (compound == "gru"){
			LABELS = ["Datum", "Grundwasser(m+NN)"];
			VISIBILITY = [true];
		} else if (compound == "hl"){
			LABELS = ["Datum", "Luftfeuchte(%)"];
			VISIBILITY = [true];
		} else if (compound == "pl"){
			LABELS = ["Datum", "Luftdruck(hPa)"];
			VISIBILITY = [true];
		} else if (compound == "n"){
			LABELS = ["Datum", "Niederschlag(mm)"];
			VISIBILITY = [true];
		} else if (compound == "n_intensity"){
			LABELS = ["Datum", "Niederschlagsintentsität(mm/h)"];
			VISIBILITY = [true];
		} else if (compound == "o2"){
			LABELS = ["Datum", "Sauerstoffgehalt(mg/l)"];
			VISIBILITY = [true];
		} else if (compound == "ph"){
			LABELS = ["Datum", "pH Wert"];
			VISIBILITY = [true];
		} else if (compound == "cl"){
			LABELS = ["Datum", "Chlorid"];
			VISIBILITY = [true];
		} else if (compound == "orp"){
			LABELS = ["Datum", "Redoxspannung"];
			VISIBILITY = [true];
		}
	} else if (category == "bw"){
		if (compound == ""){
			var LABELS = ["Datum", "Gew&auml;sserqualit&auml;t (1 (exzellent) - 4 (schlecht))"];
			var VISIBILITY = [true];
		} else if (compound == "bw_quality"){
			LABELS = ["Datum", "Gew&auml;sserqualit&auml;t (1 (exzellent) - 4 (schlecht))"];
			VISIBILITY = [true];
		}
	}

	// if we have a touch device always display the legend because tap events don't bring it up...
	if (isTouchSupported() == true){
		//alert("touch");
		var LEGEND = "'always'";
	} else {
		var LEGEND = "'onmouseover'";
		//alert("no touch");
	}

	//alert(period);
	//url = '/db2csv.php?uid=' + uid + '&category=' + category + '&compound=' + compound;
	var url = '/data.php?type=timeline&uid=' + uid + '&category=' + category + '&compound=' + compound;
	document.getElementById('spinner').style.display = 'block';

	// Timezone function for dygraphs!
	// See: http://stackoverflow.com/questions/24196183/dygraph-showing-dates-in-an-arbitrary-timezone
	function getMomentTZ(d, interpret) {
		// Always setting a timezone seems to prevent issues with daylight savings time boundaries, even when the timezone we are setting is the same as the browser: https://github.com/moment/moment/issues/1709
		// The moment tz docs state this:
		//  moment.tz(..., String) is used to create a moment with a timezone, and moment().tz(String) is used to change the timezone on an existing moment.
		// Here is some code demonstrating the difference.
		//  d = new Date()
		//  d.getTime() / 1000                                   // 1448297005.27
		//  moment(d).tz(tzStringName).toDate().getTime() / 1000 // 1448297005.27
		//  moment.tz(d, tzStringName).toDate().getTime() / 1000 // 1448300605.27
		if (interpret) {
			return moment.tz(d, userTimezone); // if d is a javascript Date object, the resulting moment may have a *different* epoch than the input Date d.
		} else {
			return moment(d).tz(userTimezone); // does not change epoch value, just outputs same epoch value as different timezone
		}
	}

	/** Elegant hack: overwrite Dygraph's DateAccessorsUTC to return values
	 * according to the currently selected timezone (which is stored in
	 * g_timezoneName) instead of UTC.
	 * This hack has no effect unless the 'labelsUTC' setting is true. See Dygraph
	 * documentation regarding labelsUTC flag.
	 */
	Dygraph.DateAccessorsUTC = {
		getFullYear:     function(d) {return getMomentTZ(d, false).year();},
		getMonth:        function(d) {return getMomentTZ(d, false).month();},
		getDate:         function(d) {return getMomentTZ(d, false).date();},
		getHours:        function(d) {return getMomentTZ(d, false).hour();},
		getMinutes:      function(d) {return getMomentTZ(d, false).minute();},
		getSeconds:      function(d) {return getMomentTZ(d, false).second();},
		getMilliseconds: function(d) {return getMomentTZ(d, false).millisecond();},
		getDay:          function(d) {return getMomentTZ(d, false).day();},
		makeDate:        function(y, m, d, hh, mm, ss, ms) {
			return getMomentTZ({
				year: y,
				month: m,
				day: d,
				hour: hh,
				minute: mm,
				second: ss,
				millisecond: ms,
			}, true).toDate();
		},
	};



	// Initialize the graph
	g = new Dygraph(

		//document.getElementById(graph),
		graph, url + '&period=' + compound_period + '&limit=' + limit, {
			drawCallback: function(g){
				document.getElementById('spinner').style.display = 'none';
				g.resize();
			},
			rollPeriod: 1,
			//showRoller: true,
			fillGraph: true,
			//stepPlot: true,
			//customBars: true,
			//yAxisLabelWidth: 30,
			axes : {
				y : { axisLabelWidth : 30 }
			},
			//legend: LEGEND ,
			//legend: "always",
			labels: LABELS,
			labelsDiv: 'g_labels',
			labelsDivStyles: { 'textAlign': 'right' },
			labelsShowZeroValues: true,
			strokeWidth: 2,
			//highlightSeriesOpts: { strokeWidth: 2 },
			digitsAfterDecimal: 3,
			//labelsSeparateLines: true,
			visibility: VISIBILITY,
			//drawXAxis: true,
			//drawYAxis: true,
			//drawXGrid: false,
			//drawYGrid: false,
			//animatedZooms: true,
			showRangeSelector: true,
			rangeSelectorHeight: 30,
			labelsUTC: true
		}
	);


}

function onChangeGraphPeriodNEW(uuid,uid,category,compound,compound_period,limit){
	document.getElementById('periodSelect').onchange = function(e){
		var compound = $('#compoundSelect').children(":selected").attr('data-compound');
		var compound_period = $(this).children(":selected").attr("data-period");
		var compound_period_text = $(this).children(":selected").text();
		var limit = $("#limitSelect").children(":selected").attr("data-limit");
		var compound_longname = $('#compoundSelect').children(":selected").text();
		updatePopupNEW(uuid,uid,category,compound,compound_period,limit,compound_longname,compound_period_text);
	};
}

function onChangeGraphLimitNEW(uuid,uid,category,compound,compound_period,limit){
	document.getElementById('limitSelect').onchange = function(e){
		var compound = $('#compoundSelect').children(":selected").attr('data-compound');
		var compound_period = $("#periodSelect").children(":selected").attr("data-period");
		//alert(compound_period);
		var limit = $(this).children(":selected").attr("data-limit");
		//updateGraph(compound_period,limit);
		show_graphNEW(uuid,uid,category,compound,compound_period,limit);
	};
}

function updateGraph(compound_period,limit){
	//alert(compound_period + limit);
	document.getElementById('spinner' + compound).style.display = 'block';
	g.updateOptions({
		drawCallback: function(g){
			document.getElementById('spinner' + compound).style.display = 'none';
		},

		'file': url + '&period=' + compound_period + '&limit=' + limit
		//dateWindow: [minX, maxX]//,
		//showRangeSelector: true
	});

	$('.dygraph-rangesel-zoomhandle').css('left','0px');
}

// Only show selected data in dygraph
function change(el){
	g.setVisibility(parseInt(el.id), el.checked);
	g.resize();
}
