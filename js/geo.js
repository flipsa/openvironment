// Get position via HTML5 Geolocation API if browser supports it
function doGeolocation(){
	if (navigator.geolocation){
		oldAddress = '';
		oldBoundary = '';
		addressInput.style.color = "black";
		addressInput.value = lang_get_current_location;
		navigator.geolocation.getCurrentPosition(positionSuccess, positionError);
		//alert("test");
	} else {
		positionError(-1);
	}
}

// Update the search field in case of geolocation error.
function positionError(err){

	var msg;
	switch(err.code){

		case err.UNKNOWN_ERROR:
		msg = lang_current_location_unavailable;
       	break;

		case err.PERMISSION_DENIED:
		msg = lang_curent_location_denied;
		break;

		case err.POSITION_UNAVAILABLE:
		msg = lang_curent_location_temporary_error;
		break;

		case err.BREAK:
		msg = lang_curent_location_timeout;
		break;

		default:
		msg = lang_curent_location_not_supported_in_browser;
	}

	addressInput.value = msg;
	addressInput.style.color = 'red';
}

// Browser supports Geolocation API
function positionSuccess(position){

	// We need to reset boundary and boundingboxRadius here!
	LLS = '';
	bbRadius = '';
	
	// Centre the map on the new location
	var coords = position.coords || position.coordinate || position;
	var geocode_lat = coords.latitude;
	var geocode_lon = coords.longitude;
	
	// call the geocoder function early to save time
	geocoder(geocode_lat,geocode_lon);
	
	var accuracy = coords.accuracy;
	var radius = accuracy / 2;
	
	if (map.hasLayer(loc_marker) == false){
		loc_marker = L.marker([coords.latitude,coords.longitude], {
			icon: L.icon({
				//iconUrl: '/img/user.gif',
				iconUrl: '/img/location.png',
				iconSize:     [35, 35], // size of the icon
				iconAnchor:   [17, 17], 
				popupAnchor:  [0, 0]
			})
		});//.bindPopup("Ihr Standort auf " + Math.round(radius) + " Meter genau", {className:"loc_popup"});
		map.addLayer(loc_marker);	
		//loc_marker.openPopup();
	}
	
	if (map.hasLayer(loc_circle) == false){
		loc_circle = L.circle([coords.latitude,coords.longitude], radius);
		map.addLayer(loc_circle);
	}

	var user_location = loc_circle.getBounds();
	var boundsOptions = {paddingTopLeft: [offsetLeft,offsetTop], paddingBottomRight: [offsetRight,offsetBottom]};
	map.fitBounds(loc_circle.getBounds(),boundsOptions);
}

// Get an address for the position we have
function geocoder(geocode_lat,geocode_lon){
	//alert (geocode_lat + ', ' + geocode_lon);
	$.getJSON('https://nominatim.openstreetmap.org/reverse?format=json&lat=' + geocode_lat + '&lon=' + geocode_lon + '&addressdetails=1', function(data){
		if (data){	
			// If nominatim finds an address update search field
			addressInput.value = '' + (data.address.road || '') + ' ' + (data.address.house_number || '') + ', ' + (data.address.postcode || '') + ' ' + (data.address.city || data.address.state) + '';
			
			addressInput.setAttribute('data-lat', geocode_lat);
			addressInput.setAttribute('data-lng', geocode_lon);

			
			lat = data.lat;
			oldLat = lat;
			lon = data.lon;
			oldLon = lon;
			
			// Set the geolocation flag to true
			geoLocOn = true;
			
			//searchLocationsNear(lat,lon);
		} else if (!data){
			// If nominatim does not find an address print error message
			addressInput.value = lang_unknown_address;
			addressInput.style.color = 'red';
			
			// Set the geolocation flag to true
			geoLocOn = false;
		}
	});
	//searchLocations();
}

// Try to get a rough geoIP location
function geoip(defaultLayer){
	
	var start_time = new Date().getTime();

	// FreeGeoIP is a bit slow...
	//var url = 'https://freegeoip.net/json/';
	
	// Nekudo is fast!
	var lang = 'de';
	var url = 'https://geoip.nekudo.com/api/' + lang;

	$.ajax({
		url: url,
		success: function(data, status, xhr) {
			var request_time = new Date().getTime() - start_time;
			
			//if (debug == true) console.log('start_time: ' + request_time);                              
			
			// FreeGeoIP 
			//var geoipLat = data.latitude;
			//var geoipLng = data.longitude;
			
			// Nekudo
			var geoipLat = data.location.latitude;
			var geoipLng = data.location.longitude;
			
			// Locating at country level is usually safe, lower levels have too many false positives;
			//if (data.city == ''){
				var zoom = 6;
				addressInput.value = '';
			//} else {
			//	var zoom = 10;
			//	addressInput.value = data.city;
			//};
			
			//if (debug == true) console.log("got geoIP address: " + geoipLat + ',' + geoipLng + ' in ' + request_time + 'ms');
			map.setView(new L.LatLng(geoipLat, geoipLng), zoom);
		},
		error: function(x, t, m) {
			if(t==="timeout") {
				if (debug == true) console.log('DEBUG(geoip(defaultLayer)): timeout waiting for geoIP');
			} else {
				if (debug == true) console.log('DEBUG(geoip(defaultLayer)): error getting geoIP');
			}
			map.setView(new L.LatLng(51.0834196, 10.4234469), 3);
		},
		timeout: 500,
		async: true
	});
	
}

// search for the location the user entered in the search field
function searchLocations(){

	// Clear previous searches
	clearLocations();	
	
	// use nominatim to get lat/lng for an address
	var address = document.getElementById("addressInput").value;
	var radius = document.getElementById("radiusSelect").value;
	var category = document.getElementById("categorySelect").value;
	
	// If same as previous search
	if ((address === oldAddress) && (oldAddress != '') && (oldAddress != lang_unknown_address)){
		//if (debug == true) console.log('position: ' + new L.LatLng(oldLat,oldLon));
		
		//map.panTo(new L.LatLng(oldLat,oldLon));
			
		// Check if GeoLocation is in use; if yes, don't show boundary polygon
		if ((typeof oldBoundary != undefined) && (oldBoundary != '')){
			showBounds(oldBoundary,oldBoundingBox);
		}
		addressInput.setAttribute('data-lat', oldLat);
		addressInput.setAttribute('data-lng', oldLon);
		updateURLNEW(lang,'map','near',oldLat,oldLon,radius,category);
		searchLocationsNear(oldLat,oldLon,oldBoundary);
	
	// If first search or new search
	} else {

		// Resize the search bar height
		resizeLeftBar();

		oldAddress = address; // GLOBAL
		
		$('#spinnerSearch').show();
		
		$.getJSON('https://nominatim.openstreetmap.org/search?format=json&polygon_text=1&limit=20&addressdetails=1&dedupe=1&q=' + encodeURIComponent(address), function(data){
			
			var items = [];

			// For each result nominatim gives back
			for (var i = 0, len = data.length; i < len; i++){
				
				var obj = data[i];
				
				var boundingBox = [];
				var boundary = [];
				
				var name = obj.display_name;
				var lat = obj.lat;
				var lon = obj.lon;
				var type = obj.type;
				var osm_type = obj.osm_type;
				var type = obj.type;
				var address = obj.address;
				var postcode = obj.address.postcode;
				var city = obj.address.city;
				var town = obj.address.town;
				var village = obj.address.village;
				var state = obj.address.state;
				var country = obj.address.country;
				var building = obj.address.building + "<br />";
				var road = obj.address.road + " ";
				var house_number = obj.address.house_naumber;
				var icon = obj.icon;
				var boundary = obj.geotext;
				var boundingBox = obj.boundingbox;
				
				if (lang == "de") {
					switch(type) {
						case "administrative":
							var type = "Gebietsgrenze";
							break;
						case "political":
							var type = "Verwaltungseinheit";
							break;
						case "country":
							var type = "Land";
							break;
						case "region":
							var type = "Region";
							break;
						case "state":
							var type = "Bundesland";
							break;
						case "county":
							var type = "Regierungsbezirk";
							break;
						case "city":
							var type = "Stadt";
							break;
						case "town":
							var type = "Kleinstadt";
							break;
						case "village":
							var type = "Dorf";
							break;	
						case "hamlet":
							var type = "Weiler";
							break;
						case "isolated dwelling":
							var type = "Einzelgeh&ouml;ft";
							break;
						case "locality":
							var type = "Flur";
							break;
						case "peak":
							var type = "Gipfel";
							break;
						case "house":
							var type = "Gebäude";
							break;
						case "forest":
							var type = "Wald";
							break;
						case "residential":
							var type = "Wohngebiet";
							break;
						case "station":
							var type = "Bahnhof";
							break;
						case "yes":
							var type = "Geb&auml;de";
							break;
						case "island":
							var type = "Insel";
							break;
						
						
						default:
							var type = type;
					}
				}
				
				if (boundary.substring(0, 5) != "POINT") {
					var boundaryIcon = '<img src="/img/polygon.png" style="float:left; padding:3px; height:14px; width:14px;">';
				} else {
					var boundaryIcon = '<img src="/img/marker-icon.png" style="float:left; padding:3px 5px 3px 6px; height:14px; width:9px;">';
				}
				
				//items.push( "<li><div><a href='#' class='nominatim_result' data-lat='" + lat + "' data-lon='" + lon + "' data-boundary='" + boundary + "' data-bbox='" + boundingBox + "'>" +(name || "") + " (" + (type || "") + ")</a></div></li>");
				items.push( '<li><div class="clearfix">' + (boundaryIcon || "") + '<span class="nominatim_result" data-lat="' + lat + '" data-lon="' + lon + '" data-boundary="' + boundary + '" data-bbox="' + boundingBox + '">' +(name || "") + ' (' + (type || "") + ')</span></div></li>');
			}
		
			// If nominatim does not find an address print error message
			if  (items.length == 0){
				addressInput.value = lang_unknown_address;
				addressInput.style.color = 'red';
			
			// If nominatim finds one address
			} else if (items.length == 1){	
				
				// Set old location data to prevent nominatim result picker showing up for multiple searches at same location
				oldLat = lat; // GLOBAL
				oldLon = lon; // GLOBAL
				oldBoundary = boundary; // GLOBAL
				oldBoundingBox = boundingBox; // GLOBAL
				
				// Check if boundary is really a polygon boundary (= array of points); can also be a point, but then we can't show boundary 
				showBounds(boundary,boundingBox);
				updateSearchAttributes(lat,lon,radius,category)	
				updateURLNEW(lang,'map','near',lat,lon,radius,category);
				
				// Search near the location
				searchLocationsNear(lat,lon,boundary);
				
			// If nominatim finds more than one address
			} else {
				
				// Show the nominatim result list			
				$('#nominatim_results').empty();
				$('<h3>', { html: lang_multiple_choices_please_select, style: 'border-bottom:1px solid rgb(169,169,169);font-size:13px; line-height:15px;' }).appendTo('#nominatim_results');
				$('<ul />', {
					'class': 'nominatimResultListUL',
					html: items.join(''),
					style: 'max-height:' + leftBarHeight + 'px;'
				}).appendTo('#nominatim_results');
				
				$('#nominatim_results').css('display','block');
				$('#nominatimResultsOuter').show("blind",250);
				setTimeout(function(){
					if (debug == true) console.log('DEBUG(searchLocations): resizing search');
					$('.nominatimResultListUL').niceScroll({horizrailenabled:false, autohidemode:false, cursorborder:'1px solid #ffffff', cursorcolor:'rgb(169,169,169)', cursorwidth: 4, railpadding: { top: 0, right: 0, left: 9, bottom: 0 }});
				},0);
				
			}
		
			$('#spinnerSearch').hide();
		
		});
	};
}

// Show the map object polygon boundary (parsed by omnivore)
function showBounds(boundary,boundingBox){
	
	// Reset boundary and bounding box radius
	LLS = '';
	bbRadius = '';
	
	// Check if we are using geolocation; if yes, don't show boundary to prevent double marker at user location
	if (geoLocOn != true) {

		// Check if boundary exists, is not null and starts with the word "POLYGON"
		if ((typeof boundary != undefined) && (boundary != '')){

			// Show administrative bounds
			polyBounds = omnivore.wkt.parse(boundary);
			polyBounds.setStyle({clickable:false, className:"polyLayer", color:"red", fillColor:"none"}); 
			polyBounds.addTo(map);
			
			// Center the map around the bounds --> Disabled for now because it's hectic when this pans, and then the searchlocationsnear function pans afterwards. best would be to check here, if the map viewport already shows the bounds, and if yes then don't pan at all.
			//map.fitBounds(polyBounds);

			if (boundary.substring(0, 5) != "POINT") {

				// Get the array of polypoints of the boundary
				var key = Object.keys(polyBounds._layers)[0];
				LLS = polyBounds._layers[key].getLatLngs();
			
			
				if ((typeof boundingBox != undefined) && (boundingBox != '')) {
					//if (debug == true) console.log('BB is array: ' + boundingBox);
					bbArray = boundingBox;
					bbRadius = (getDistanceFromLatLonInKm(bbArray[0],bbArray[1],bbArray[2],bbArray[3])/2);
				}
			}
		};
	}
}

// swap the nominamtim polyline order nested array from lng/lat to lat/lng so we can use it with leaflet
// Not needed if we are using omnivore
function swapLatLng(choordsToSwap){
	var b = choordsToSwap[0];
	choordsToSwap[0] = choordsToSwap[1];
	choordsToSwap[1] = b;
	return choordsToSwap
};

function isMarkerInsidePolygon(marker, poly, polyBB) {

	if ((typeof LLS == "undefined") || (LLS == "")){
                if (debug == true) console.log('no boundaries');
		return true;
	}
	
	// Check if we have a multipolygon (i.e. array is multimensional)
	if (typeof LLS[0][0] != "undefined") {
		
		//if (debug == true) console.log('multi polygon');
		
		for (i = 0; i <= LLS.length -1; i++) {
			var polyPoints = LLS[i];
			return pip(marker,polyPoints);
		}
	} else {
		
		//if (debug == true) console.log('single polygon');
		
		var polyPoints = LLS;
		return pip(marker,polyPoints);
	}
	
    function pip(marker,polyPoints) {
		
		var x = marker._latlng.lat, y = marker._latlng.lng;
		
		// First, before doing complex point in polygon calculations, let's check if the point is inside the bounding box of the polygon
		if (x < bbArray[0] || x > bbArray[1] || y < bbArray[2] || y > bbArray[3]) {
			//if (debug == true) console.log('marker not in polygon bounding box');
			return false;
		}

		// If the above test did not return false, do a real p-i-p test with a ray casting algo
		var inside = false;
		for (var i = 0, j = polyPoints.length - 1; i < polyPoints.length; j = i++) {
			var xi = polyPoints[i].lat, yi = polyPoints[i].lng;
			var xj = polyPoints[j].lat, yj = polyPoints[j].lng;

			var intersect = ((yi > y) != (yj > y))
				&& (x < (xj - xi) * (y - yi) / (yj - yi) + xi);
			if (intersect) inside = !inside;
		}
		//if (debug == true) console.log('marker in polygon: ' + inside);
		return inside;
	}
};

function getDistanceFromLatLonInKm(lat1,lat2,lon1,lon2) {
	var R = 6371; // Radius of the earth in km
	var dLat = deg2rad(lat2-lat1);  // deg2rad below
	var dLon = deg2rad(lon2-lon1); 
	var a = 
	Math.sin(dLat/2) * Math.sin(dLat/2) +
	Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * 
	Math.sin(dLon/2) * Math.sin(dLon/2)
	; 
	var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
	var d = R * c; // Distance in km
	return d;
}

function deg2rad(deg) {
	return deg * (Math.PI/180)
}	

// Update attributes of the search field with lat and lng so we can access them later
function updateSearchAttributes(lat,lng,radius,category){
	$("#addressInput").attr("data-lat",lat);
	$("#addressInput").attr("data-lng",lng);
	$("#addressInput").attr("data-radius",radius);
	$("#addressInput").attr("data-category",category);
}

$(document).on('click', '.nominatim_result', function(){

	// Hide the nicescroll bar immediately
	$('.nominatimResultListUL').getNiceScroll().remove();
	
	// Remove old boundary
	if (map.hasLayer(polyBounds)){
		//alert("has bounds");
		map.removeLayer(polyBounds)
	};	

	var lat = $(this).attr('data-lat');
	oldLat = lat;
	var lon = $(this).attr('data-lon');
	oldLon = lon;
	var boundary = $(this).attr('data-boundary');
	oldBoundary = boundary;
	
	// CAREFUL HERE! when clicking we get a string from the data-boundingBox attribute, but we need arrays for further processing!
	var boundingBox = $(this).attr('data-bbox');
	var boundingBox = boundingBox.split(',');
	oldBoundingBox = boundingBox;
	
	showBounds(boundary,boundingBox);
	
	var radius = document.getElementById("radiusSelect").value;
	var category = document.getElementById("categorySelect").value;
	
	updateSearchAttributes(lat,lon,radius,category);
	updateURLNEW(lang,'map','near',lat,lon,radius,category);
	
	// Small timeout needed here to prevent simultaneous movements from showbounds function and then searchlocationsnear
	searchLocationsNear(lat,lon,boundary);
	
	$('#nominatimResultsOuter').hide('blind',250);
	
});
