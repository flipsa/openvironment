// PIWIK ANALYTICS
var _paq = _paq || [];
_paq.push(["setCookieDomain", "*.openvironment.org"]);
_paq.push(['trackPageView']);
_paq.push(['trackAllContentImpressions']);
_paq.push(['enableLinkTracking']);

window.addEventListener('load', function() {
	var u="//www.openvironment.org/piwik/";
	_paq.push(['setTrackerUrl', u+'piwik.php']);
	_paq.push(['setSiteId', 1]);
	var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
	g.type='text/javascript'; g.async=true; g.defer=false; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
});
