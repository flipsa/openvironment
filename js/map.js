var map;
var t;
var searchBox;
var center;
var markers;
var loc_marker;
var loc_circle;
var polyline;
var geoLocOn = false;
var search_marker;
var layers;
var polyBounds;
var mobileMaxHeight;
var markerMap = {};
var oldAddress;
var minimumZoom = 1;
var maximumZoom = 18;
var warningIcon;
var dateWarningIcon;
var dateWarningCounter;
var g;
var openMarkerUID = false;
var markerWasOpen = false;

function load(){

	getDimensions();

	getOffsets();

	getTimezone();

	// create the OpenStreetMap layer
	//var osmTile = "https://b.tile.openstreetmap.org/{z}/{x}/{y}.png";
	//osmCopyright = "Map data &copy; 2012 OpenStreetMap contributors";
	//var osmLayer = new L.TileLayer(osmTile, { maxZoom: maximumZoom, minZoom: minimumZoom } );
	// Add the OSM layer
	//osmLayer.addTo(map);

	// Add the default layer so we don't start with an empty map
	var defaultLayer = L.tileLayer.provider('Esri.WorldTopoMap',  {maxZoom: maximumZoom, minZoom: minimumZoom});

	// Dirty hack to make the popup change size dynamically!
	//L.Map = L.Map.extend({
	//	openPopup: function(popup) {

	//		resizePopup(popup);

	//		this.closePopup();  // just comment this
	//		this._popup = popup;

	//		return this.addLayer(popup).fire('popupopen', {
	//			popup: this._popup
	//		});

	//	}
	//}); /***  end of hack ***/

	map = L.map('map', {
		//zoomsliderControl: true,
		zoomControl:false,
		attributionControl: false
	});

	// Attribution first so it hides neatly at the bottom left
	var attributionControl = L.control.attribution({position: 'bottomleft'}).addTo(map);

	map.addLayer(defaultLayer);

	//Only use geoIP location if we get a non-empty URL path
	if ((window.location.pathname) && (window.location.pathname != '/' + lang + '/map/')){
		//if (debug == true) console.log("not using geoIP");
		map.setView(new L.LatLng(51.0834196, 10.4234469), 3);
	} else {
		//if (debug == true) console.log("using geoIP");
		geoip();
	}

	// Base Layers
	// NOTE: Nokia is not supported in this version of the plugin, but in the old one. Maybe backport??
	// ALSO: MapQuest stopped serving free tiles (without need for an API key) as of July 11, 2016. Free Plan allows 15.000 transactions.
	var baseLayers = {
		'Topographic': defaultLayer,
		//'Hydda Full': L.tileLayer.provider('Hydda.Full'), DEFECT
		//'OpenStreetMap': L.tileLayer.provider('OpenStreetMap.Mapnik'),
		//'OSM Deutsch': L.tileLayer.provider('OpenStreetMap.DE'),
		//'OSM Black & White': L.tileLayer.provider('OpenStreetMap.BlackAndWhite'),
		//'OpenStreetMap H.O.T.': L.tileLayer.provider('OpenStreetMap.HOT'),
		//'Thunderforest OpenCycleMap': L.tileLayer.pro layer controlvider('Thunderforest.OpenCycleMap'),
		//'Thunderforest Transport': L.tileLayer.provider('Thunderforest.Transport'),
		//'Thunderforest Landscape': L.tileLayer.provider('Thunderforest.Landscape'),
		//'MapQuest OSM': L.tileLayer.provider('MapQuestOpen.OSM'),
		//'MapQuest Aerial': L.tileLayer.provider('MapQuestOpen.Aerial'),
		//'MapBox': L.tileLayer.provider('MapBox'),
		'CartoDB Positron': L.tileLayer.provider('CartoDB.Positron'),
		'CartoDB Dark Matter': L.tileLayer.provider('CartoDB.DarkMatter'),
		//'MapBox Example': L.tileLayer.provider('MapBox.examples.map-zr0njcqy'),
		//'Stamen Toner': L.tileLayer.provider('Stamen.Toner'),
		//'Stamen Terrain': L.tileLayer.provider('Stamen.Terrain'), KAPUTT?
		//'Stamen Watercolor': L.tileLayer.provider('Stamen.Watercolor'),
		'Esri WorldStreetMap': L.tileLayer.provider('Esri.WorldStreetMap')
		//'Esri DeLorme': L.tileLayer.provider('Esri.DeLorme'),
		//'Esri WorldTopoMap': L.tileLayer.provider('Esri.WorldTopoMap'),
		//'Esri WorldImagery': L.tileLayer.provider('Esri.WorldImagery'),
		//'Esri WorldTerrain': L.tileLayer.provider('Esri.WorldTerrain'),
		//'Esri WorldShadedRelief': L.tileLayer.provider('Esri.WorldShadedRelief'),
		//'Esri WorldPhysical': L.tileLayer.provider('Esri.WorldPhysical'),
		//'Esri OceanBasemap': L.tileLayer.provider('Esri.OceanBasemap'),
		//'Esri NatGeoWorldMap': L.tileLayer.provider('Esri.NatGeoWorldMap'),
		//'Esri WorldGrayCanvas': L.tileLayer.provider('Esri.WorldGrayCanvas'),
		//'Nokia': L.tileLayer.provider('Nokia.normalDay'),
		//'Nokia Normal Day Grey': L.tileLayer.provider('Nokia.normalGreyDay'),
		//'Nokia Satellite': L.tileLayer.provider('Nokia.satelliteNoLabelsDay'),
		//'Nokia Satellite': L.tileLayer.provider('Nokia.satelliteYesLabelsDay')
		//'Nokia Terrain': L.tileLayer.provider('Nokia.terrainDay'),
		//'Acetate': L.tileLayer.provider('Acetate')
	};

	// Real-Time Overlay Layers
	var overlayLayers = {

		'OpenSeaMap': L.tileLayer.provider('OpenSeaMap'),
		//'OpenWeatherMap Clouds': L.tileLayer.provider('OpenWeatherMap.Clouds'),
		//'OWM Clouds': L.tileLayer.provider('OpenWeatherMap.CloudsClassic'),
		//'OpenWeatherMap Precipitation': L.tileLayer.provider('OpenWeatherMap.Precipitation'),
		//'OWM Precipitation': L.tileLayer.provider('OpenWeatherMap.PrecipitationClassic'),
		//'OpenWeatherMap Rain': L.tileLayer.provider('OpenWeatherMap.Rain'),
		//'OWM Rain': L.tileLayer.provider('OpenWeatherMap.RainClassic'),
		//'OpenWeatherMap Pressure': L.tileLayer.provider('OpenWeatherMap.Pressure'),
		//'OWM Pressure': L.tileLayer.provider('OpenWeatherMap.PressureContour'),
		//'OWM Wind': L.tileLayer.provider('OpenWeatherMap.Wind'),
		//'OWM Temperature': L.tileLayer.provider('OpenWeatherMap.Temperature')
		//'OWM Snow': L.tileLayer.provider('OpenWeatherMap.Snow')
	};

	// Options for control.layer.minimap
	var options = {
		//position: 'topright',
		topPadding:0,
		bottomPadding:0,
		collapsed: false
	};

	// initialize the minimaps layer control and add it to map
	control = L.control.layers.minimap(baseLayers, overlayLayers, options);
	control.addTo(map);

	// Reposition the minimaps container outside of the map so we can work with it more easily
	// Call the getContainer routine.
	var htmlObject = control.getContainer();
	// Get the desired parent node.
	var a = document.getElementById('layersMinimap');

	// Finally append that node to the new parent, recursively searching out and re-parenting nodes.
	function setParent(el, newParent){
		newParent.appendChild(el);
	}
	setParent(htmlObject, a);

	//var L.Control.geocoder({position: 'topleft', collapsed: false}).addTo(map);
	//L.Control.geocoder().addTo(map);
	//var geocoder = L.Control.geocoder({position: 'topleft'}).addTo(map);

	//geocoder.markGeocode = function(result){
	//	var bbox = result.bbox;
	//	var poly = L.polygon([
	//		 bbox.getSouthEast(),
	//		 bbox.getNorthEast(),
	//		 bbox.getNorthWest(),
	//		 bbox.getSouthWest()
	//	]).addTo(map);
	//	map.fitBounds(poly.getBounds());
	//};

	// Layer Control
	//var layerControl = L.control.layers(baseLayers, overlayLayers, {position: 'topright', collapsed: false}).addTo(map);

	//var groupedOverlays = {
	//	"Open Weather Map": overlayLayers
	//};
	//var groupedOptions = {
	//	// Make the "Landmarks" group exclusive (use radio inputs)
	//	exclusiveGroups: ["Landmarks"],
	//	// Show a checkbox next to non-exclusive group labels for toggling all
	//	groupCheckboxes: true,
	//	position: 'topright',
	//	collapsed: false
	//};

	//L.control.groupedLayers(baseLayers, groupedOverlays, groupedOptions).addTo(map);

	// Zoom Control
	new L.Control.Zoom({position: 'topright'}).addTo(map);

	// Scale control
	L.control.scale({position: 'bottomright'}).addTo(map);

	// Initiate terminator plugin (night-and-day overlay) and update every minute
	//var t = L.terminator();
	//t.addTo(map);
	//setInterval(function(){updateTerminator(t);}, 60000);
	//function updateTerminator(t){
	//	var t2 = L.terminator();
	//	t.setLatLngs(t2.getLatLngs());
	//	t.redraw();
	//}

	//var zoomslider = L.control.zoomslider({position: 'bottomleft'}).addTo(map);

	onChangeResult();

	map.on('layeradd', function (e){
		latLngs = e.layer._latlngs;
		//if (debug == true) console.log(e);
		//if (debug == true) console.log(latLngs);
	})

	map.on('popupopen', function(e){

		if (debug == true) console.log("map.on popupopen");

		//resizePopup(e.popup);

		if ($("#sidebar").css('display') == "block") {
			openSidebar(false);
		};

		// hide the results menu on small screens
		if ($("#map").height() < 645){
			// show the results list if it was shown before
			if ($("#results").is(':checked')){
				results_opened = true;
				$("#results").click();
			} else {
				results_opened = false;
			}
			// show the legend if it was shown before
			if ($("#legend").is(':checked')){
				legend_opened = true;
				$("#legend").click();
			} else {
				legend_opened = false;
			}
		}

		var uid = e.popup._source.feature.properties.uid;
		var uuid = e.popup._source.feature.properties.uuid;
		var category = e.popup._source.feature.properties.category;

		// set global variable so we know which popup is open
		openMarkerUID =  e.popup._source.feature.properties.uuid;

		// Initialize the jquery tabs
		//tabsInit(uid,category);

		// NEW NEW NEW
		var compoundSelect = document.getElementById('compoundSelect');
		var compound = $('#compoundSelect').children(":selected").attr("data-compound");
		var compound_period = $('#periodSelect').children(":selected").attr("data-period");
		var compound_period_text = $('#periodSelect').children(":selected").text();
		var limit = $("#limitSelect").children(":selected").attr("data-limit");
		var compound_longname = $('#compoundSelect').children(":selected").text();
		console.log("compound_longname: " + compound_longname);
		onChangeCompoundSelectMenu(uuid,uid,category,compound,compound_period,limit,compound_longname,compound_period_text);

		// Check if we have at least 2 options in the compoundSelect menu, if not blur it
		if ($('#compoundSelect').children().length < 2) {
			$('#compoundSelect').prop('disabled', 'disabled');
			var bg = "url(/img/down_arrow.png) no-repeat 96% rgb(220,220,220)";
			$("#compoundSelectOuter").css('background',bg);
			$("#compoundSelect").css('color','rgb(150,150,150)');
		};

		$(".GaugeMeter").gaugeMeter();

		// Initialize jQuery Selectmenu
		//$('.selectMenu').selectmenu();

		//destroy any old popups that might be attached
		//if (e.layer._popup != undefined){
		//	//alert("popup defined");
		//	e.layer.unbindPopup();
		//}
		//display a placeholder popup
		//alert(e.layer._latlng);
		//var pop = L.popup().setLatLng(e.layer._latlng).setContent('Loading...').openOn(map);

		//var url='/marker_popup.php?category=' + category + '&uid=' + uid;
		//var url='/img/air_quality.png';
		//request data and make a new popup when it's done
		//$.ajax({
		//	url: url,
		//	success: function (data){
				//close placeholder popup
		//		e.layer.closePopup();

				//attach the real popup and open it
		//		e.layer.bindPopup(data);
		//	}
		//});
		//e.layer.openPopup();


		//var url='/marker_popup.php?category=' + category + '&uid=' + uid;
		//$.get(url).done(function(data){
		//	popup.setContent(data);
		//	popup.update();
		//});

		// Highlight the marker in the resultList sidebar
		highlightResult(uuid);

		//////////////////////////////////////////////////
		// Draw a line between the marker and the center
		//
		//if (map.hasLayer(polyline)){
		//	map.removeLayer(polyline);
		//}
		//drawPolylineBetweenMarkers(e.popup._source);
		//////////////////////////////////////////////////

		if (debug == true) console.log('addressinput val: ' + $("#addressInput").val());
		if ($("#addressInput").val() == '') {
			if (debug == true) console.log("empty address field");
			if (debug == true) console.log('category: ' + e.popup._source.feature.properties.category);
			$("#addressInput").attr('data-lat', e.popup._source.feature.properties.lat);
			$("#addressInput").attr('data-lng', e.popup._source.feature.properties.lng);
			$("#addressInput").attr('data-category', e.popup._source.feature.properties.category);
			$("#categorySelect option[value=" + e.popup._source.feature.properties.category + "]").prop('selected',true);
			$("#addressInput").attr('data-radius', '5');
			$("#radiusSelect option[value='5']").prop('selected',true);
			$("#addressInput").val(e.popup._source.feature.properties.lat + ',' + e.popup._source.feature.properties.lng);
			//window.history.pushState('near', 'New Title', '/' + lang + '/map/near/' + e.popup._source.feature.properties.lat + ',' + e.popup._source.feature.properties.lng + '/' + '5' + '/' + e.popup._source.feature.properties.category);
		};

		//updatePopupNEW(uid,category,compound,compound_period);

		// This function with timeout is needed for re-calculating the popup's real height, so a nicescroll bar will be shown if height is too small!
		setTimeout(function() {

			var heightPopupContent = $(".iwindow").height() -  $(".iwindow .boxHeader").height() -  $(".iwindow .popUpSelectMenu").height() -10; // 29 equals hight of boxHeader; 20 equals padding
			//if (debug == true) console.log("heightPopupContent: " + heightPopupContent + ' (.iwindowHeight: ' + $(".iwindow").height() + '.iwindow .boxHeader: ' + $(".iwindow .boxHeader").height() + ' .iwindow#popUpSelectMenu: ' +  $(".iwindow .popUpSelectMenu").height() + ')');
			$("#tabs-popup").css("height",heightPopupContent);
			//$("#tabs-popup").css("max-width",700);
			//g.resize();
			tooltips();
		},500);

		// BUG: Hide the nicescroll bar to prevent multiple scrollbars
		$(".leaflet-popup div[id^='ascrail']").remove();
		$("#tabs-popup").niceScroll({horizrailenabled:false, autohidemode:false, cursorborder:'1px solid #ffffff', cursorcolor:'rgb(169,169,169)', cursorwidth: 4, railpadding: { top: 0, right: 0, left: 0, bottom: 0 }});
		$("#tabs-popup").getNiceScroll().resize();

		$( "#closePopup" )
		.button()
		.click(function() {
			$( document ).tooltip().remove();
			map.closePopup();
			window.history.pushState('none', 'New Title', '/' + lang + '/map/near/' + $("#addressInput").attr('data-lat') + ',' + $("#addressInput").attr('data-lng') + '/' + $("#addressInput").attr('data-radius') + '/' + $("#addressInput").attr('data-category'));
		});

	});

	map.on('popupclose', function(e){

		//resizePopup(e.popup);

		// set global variable to false so we know that no popup is open
		openMarkerUID = false;

		// remove highlight from resultlist
		$("ul.resultsListUL").children().removeClass("hovered");
		$("select#locationSelect option").removeAttr("selected");
	});

	map.on('click', function(e){

		if (($('#addressInput').attr('data-lat') != '') && ($('#addressInput').attr('data-radius') != undefined) && ($('#addressInput').attr('data-category') != undefined)) {
			window.history.pushState('none', 'New Title', '/' + lang + '/map/near/' + $("#addressInput").attr('data-lat') + ',' + $("#addressInput").attr('data-lng') + '/' + $("#addressInput").attr('data-radius') + '/' + $("#addressInput").attr('data-category'));
		}

		if (typeof oldMarker != 'undefined'){

			// Remove css border of previous highlighted marker
			$(oldMarker._icon).removeClass('selectedMarker');

		};
	});

	map.on('dragend', function(e){
		// Close the sidebar to prevent dragging / layers control minimap bug

		var center = map.getCenter();
		map.setView(center);

	});

	tooltips();
}

function resizePopup(popup){

	//var popup = e.getPopup(); // returns marker._popup
	//if (debug == true) console.log(popup._isOpen); // false

	getDimensions();
	getOffsets();
	popup.options.maxWidth = wmax;
	popup.options.minWidth = wmin;
	popup.options.maxHeight = hmax;
}

// currently unused!
function drawPolylineBetweenMarkers(marker){

	var latlngs = Array();

	if (map.hasLayer(loc_marker)){
		if (debug == true) console.log("Polyline Center (geoLoc): " + loc_marker.getLatLng())
		latlngs.push(loc_marker.getLatLng());
	} else if (map.hasLayer(polyBounds)) {
		if (debug == true) console.log(polyBounds._layers[0]);
		if (debug == true) console.log("Polyline Center (polybounds): " + polyBounds.LatLng)
		latlngs.push(polyBounds.LatLng);
	}
	latlngs.push(marker.getLatLng());

	polyline = L.polyline(latlngs, {color: 'red'});
	map.addLayer(polyline);
}

function highlightMarker(uuid){

	////////////////////////////////////////////////////////////////////////
	// This function highlights marker on map:
	// - find the marker from markerMap
	// - if marker is currently part of a markerCluster then spiderfy the cluster
	// - panTo marker on map
	// - add a css class that shows a border around the marker
	////////////////////////////////////////////////////////////////////////

	//markers.refreshClusters();
	//if (debug == true) console.log("highlightMarker: " +uid )
	var marker = markerMap[uuid];
	var markerParentID = marker.__parent._leaflet_id;
	var markerPosition = marker.getLatLng();

	// Check if this is a new marker or the same one
	if ((typeof oldMarker != 'undefined') && (oldMarker != marker)){

		// Remove css border of previous highlighted marker
		$(oldMarker._icon).removeClass('selectedMarker');

		// Close any open marker popups
		//map.closePopup();


		// ANIMATIONS BREAK THE MINIMAPS!!!! _resync is needed

		var sidebarOpen = $('#sidebar').css('display');

		// Set the offsets here instead of doing the math in the function below (this might trigger a minimaps bug)
		var leftOff = -offsetLeft/2;
		var topOff = -offsetTop/2
		var zoom = map.getZoom();

		var ltp = L.CRS.EPSG3857.latLngToPoint(markerPosition,zoom);

		// We need to pan so the marker is in the viewport. Otherwise, the check below (if marker is visible) will fail and produce a phantom cluster
		// Check if the highlighted marker is in the current viewport so we only need to pan if not; avoids unnecessary panning which feels hectic
		if (map.getBounds().contains(marker.getLatLng())){
			//if (debug == true) console.log("inside viewport");

		} else {
			//if (debug == true) console.log("oudside viewport");

			// if the sidebar is closed we can use panning with animations
			if (sidebarOpen == 'none'){

				var newltp = L.point(ltp.x + leftOff, ltp.y + topOff);
				if (debug == true) console.log('newltp: ' + newltp)
				var newMarkerPosition = L.CRS.EPSG3857.pointToLatLng(newltp, zoom );
				map.panTo(newMarkerPosition);

			// if the sidebar is open we pan without animations
			} else {
				//map.panTo(markerPosition,{animate:false},true);
				//map.panBy(new L.Point(leftOff,rightOff),{animate:false});
				//map.setView(map.getCenter());
				var leftOff = leftOff + 130;
				var newltp = L.point(ltp.x + leftOff, ltp.y + topOff);
				if (debug == true) console.log('newltp: ' + newltp);
				var newMarkerPosition = L.CRS.EPSG3857.pointToLatLng(newltp, zoom );
				map.panTo(newMarkerPosition,{animate:false, noMoveStart:true},true);
				//map.fire("moveend")
				//$(window).resize();
			}
		}
	}

	// if marker is part of a markercluster, spiderfy first
	if (!marker._icon){
		//if (debug == true) console.log('Spiderfying Marker: ' + markerParentID);
		marker.__parent.spiderfy();
	}

	$(marker._icon).addClass('selectedMarker');

	oldMarker = marker;
}

function clickMarker(uuid){

	if (debug == true) console.log('DEBUG(clickMarker) clicked marker: ' + uuid);

	var marker = markerMap[uuid];

	// if marker is part of a markercluster, spiderfy first before opneing popup
	if (!marker._icon) marker.__parent.spiderfy();

	//resizePopup(marker._popup);

	// Open the marker popup
	marker.openPopup(marker);

	updatePopupURL(uuid);

	//highlightResult(marker.feature.properties.uid);

	// Initialize the tabs for the popup; short timeout to wait for un-spiderfying
	//tabsInit(marker.feature.properties.uid, marker.feature.properties.category);

	// Leaflet 1.0 feature!
	//var pane = marker.getPane();
	//alert(pane);

}

// clear all markers, markerclusters, popups from previous searches
function clearLocations(){

	if (map.hasLayer(search_marker)){
		if (debug == true) console.log("has searchmarker");
		map.removeLayer(search_marker)
	};

	if (map.hasLayer(markers)){
		if (debug == true) console.log("has markers");
		map.removeLayer(markers);
	};

	if (map.hasLayer(layers)){
		//alert("has layers");
		map.removeLayer(layers);
	};

	if (map.hasLayer(polyBounds)){
		//alert("has bounds");
		map.removeLayer(polyBounds)
	};

	warning_counter = 0;
	dateWarningCounter = 0;

	// Clear the result menus
	//document.getElementById("ergebnisse").style.display = "none";
	document.getElementById("resultListOuter").style.display = "none";
	document.getElementById("resultList").style.display = "none";

	//document.getElementById("hide_results_button").style.display = "none";
	locationSelect.innerHTML = "";
	first_option = document.createElement("option");
	first_option.value = "none";
	first_option.innerHTML = "Zeige alle Ergebnisse:";
	locationSelect.appendChild(first_option);

	// Check if the results menu is shown or hidden; if shown, simulate click on it to hide it and thereby resize it properly
	if ($("#results").is(':checked')){
		$("#results").click();
	} else {
		// do nothing
	}
}

function clearLocMarker(){
	if (map.hasLayer(loc_marker)){
		map.removeLayer(loc_marker)
	};
	if (map.hasLayer(loc_circle)){
		map.removeLayer(loc_circle)
	};
}

function handleGeoJSON(){
	if (debug == true) console.log("handleGeoJSON");
};

function searchByPathname(){

	// if there is a path in the url, only search for this uid!
	if ((window.location.pathname) && (window.location.pathname != '/' + lang + '/map/')){

		//var that = parseURLPathName();
		//if (debug == true) console.log(that.path_location);

		// Split the url path into logical parts
		var pathArray = window.location.pathname.split( '/' );
		var path_language = pathArray[1];
		var path_doctype = pathArray[2];
		var path_searchtype = pathArray[3];

		if (path_searchtype === "station") {

			var path_uuid = pathArray[4];
			var searchURL = '/data.php?type=station&uuid=' + path_uuid + '&measurements=true&callback=handleGeoJSON';

			setTimeout (function(){
				clickMarker(path_uuid);
			},250);

			if (debug == true) console.log("DEBUG(searchByPathname): STATION(" + path_uuid + ")");

		} else if (path_searchtype == "near") {

			var path_location = pathArray[4];
			var path_location = path_location.split( ',' );
			var path_location_lat = path_location[0];
			var path_location_lng = path_location[1];
			//var setRadius = pathArray[5];
			var path_location_radius = pathArray[5];
			var path_location_category = pathArray[6];
			var setMulti = $(".chosen").chosen().val();
			var setAmount = $('#amountSelect').val();
			var searchURL = '/data.php?type=near&lat=' + path_location_lat + '&lng=' + path_location_lng + '&radius=' + path_location_radius + '&category=' + path_location_category + '&multi=' + setMulti + '&amount=' + setAmount + '&callback=handleGeoJSON';
			//alert('has location --> path_category: ' + path_category + ' / ' + path_location + ' / path_uid: ' +path_uid);

			if (debug == true) console.log("DEBUG(searchByPathname): NEAR(" + path_location_lat + " / " + path_location_lat + ")");
		}

		var returnedObject = {};
		returnedObject["searchURL"] = searchURL;
		return returnedObject;

	}
}

// Update the map from json
function searchLocationsNear(lat,lon,boundary){

	getDimensions();

	getOffsets();

	// Set initial limit to 7 days
	limit = 7; // GLOBAL

	// Create the resultItems array
	resultItems = []; // GLOBAL
	$('#resultsListUL').empty();

	// Get the search attributes from the frontend form and construct the searchURL for the request

	// If the user limited the search to points inside the search polygon ("Show only stations within boundary" / "Nur Stationen innerhalb Gebietsgrenze anzeigen" we minimize the radius so the PiP calculation is faster
	if ((typeof LLS != "") && (typeof bbRadius != "undefined") && (bbRadius != '') && ($('#limitToPolygon').prop('checked'))) {
		var setRadius = parseFloat(bbRadius).toFixed(1);
		if (debug == true) console.log('DEBUG(searchLocationsNear): LLS: ' + LLS + ' / bbRadius: ' + bbRadius);
	} else {
		var setRadius = document.getElementById('radiusSelect').value;
	}
	if (debug == true) console.log('DEBUG(searchLocationsNear): setRadius: ' + setRadius);

	var setAmount = document.getElementById('amountSelect').value;
	var setLocation = addressInput.value;
	var setAmount = $('#amountSelect').val();
	var setCategory = $('#categorySelect').val();
	var setMulti = $(".chosen").chosen().val();

	// Get the searchURL (returns an object with key "searchURL")
	var searchURL = searchByPathname();

	// Initiate the loading spinner
	var layer = L.geoJson(null).addTo(map);
	layer.fire('data:loading');

	// Request markers
	$.getJSON(searchURL.searchURL, function(data){

		// create a counter for the number of markers
		counter = 0;
		warning_counter = 0;

		geojson = L.geoJson(data, {
			// this adds the individual markers to the markerMap array which we need to build in order to later make the markers accessible when clicked from external links, such as the select menu that holds the search results
			pointToLayer: function (feature, latlng){
				marker = L.marker(latlng, {
					title: feature.properties.name
				});
				markerMap[feature.properties.uuid] = marker;
				return marker;
			},

			onEachFeature: function (feature, layer){

				// Create the marker popup
				createMarkerPopup(feature,layer);

			},

			filter: function (feature, layer) {

				if ((typeof LLS != 'undefined') && ($('#limitToPolygon').prop('checked'))) {

					var coordsToSwap = JSON.parse(JSON.stringify(feature.geometry.coordinates))
					var choords = swapLatLng(coordsToSwap);
					var inside = isMarkerInsidePolygon(L.marker(choords),LLS);
					return inside;
				} else {
					return true
				}

			}

		});

		// Create options in the results list
		createResultListItems();

		// fire the loaded event and hide the spinner
		layer.fire('data:loaded');

		// Markerclusterer
		markers = L.markerClusterGroup({
			//maxClusterRadius: 30,
			maxClusterRadius: function (zoom){
				return (zoom <= 13) ? 50 : 1; // radius in pixels
			},
			showCoverageOnHover: true,
			zoomToBoundsOnClick: true,
			riseOnHover: true,
			noHide: true,
			spiderfyOnMaxZoom: true,
			removeOutsideVisibleBounds: true,
			//disableClusteringAtZoom: "11", // prevents marker on the same lat/lng to uncluster
			animate: false,
			animateAddingMarkers: false
		});

		// Add the GeoJSON to the markers array
		geojson.addTo(markers);

		//markers.addTo(map);
		map.addLayer(markers);

		onMarkersCreate();

		if (counter == "0"){
			map.setView(new L.LatLng(lat, lon), 10);
			alertify.alert("Keine Mess-Stationen in <b>" + setRadius + "km</b> Umkreis von <b>" + setLocation + "</b> gefunden. Vergrößern Sie den Radius oder ändern Sie den Suchort.");
			return;
		}

		// Get the boundaries and center map to display all markers from search
		layers = L.featureGroup([markers]).addTo(map);

		// If we have a search location marker, add it to the layergroup
		if (map.hasLayer(search_marker)==true){
			if (debug == true) console.log("search_marker");
			layers.addLayer(search_marker);
		}

		// if nominatim gave back a polygon for the search, add it to the layer group
		if (map.hasLayer(polyBounds)==true){
			layers.addLayer(polyBounds);
		}

		var layersBounds = layers.getBounds();
		//var boundsZoom = map.getBoundsZoom(layersBounds);
		var layersBoundsOptions = {paddingTopLeft: [offsetLeft,offsetTop], paddingBottomRight: [offsetRight,offsetBottom]};

		// Small timeout needed in case we were setting the map view with the showBounds function before!
		//setTimeout(function(){
			map.fitBounds(layersBounds,layersBoundsOptions);
		//},250);

		//$(window).resize();

		// Wait till pan / zoom are finished so we have the real height of the map, then show the resultList (otherwise height might be too small!?!)
		setTimeout(function(){
			showResults();
		},250);

		// trigger piwik on open of marker info window
		piwikPush();

	});

};

function onMarkersCreate(){

	// When clicking on marker...
	markers.on('click', function (e){

		if (debug == true) console.log("markers.on click");

		var uuid = e.layer.feature.properties.uuid;
		var category = e.layer.feature.properties.category;
		updatePopupURL(uuid);

	});

	markers.on('mouseover', function(e){
		var uid = e.layer.feature.properties.uid;
		var uuid = e.layer.feature.properties.uuid;
		var category = e.layer.feature.properties.category;
		var marker = markerMap[uuid];
		$(marker._icon).addClass('selectedMarker');
		//$(marker._icon).animate({width: '48px'},{duration: 500,queue: false});
		//$(marker._icon).animate({height: '55px'},{duration: 500,queue: false});
	});

	markers.on('mouseout', function(e){
		var uid = e.layer.feature.properties.uid;
		var uuid = e.layer.feature.properties.uuid;
		var marker = markerMap[uuid];
		$(marker._icon).removeClass('selectedMarker');
		//$(marker._icon).animate({width: '32px'},{duration: 500,queue: false});
		//$(marker._icon).animate({height: '37px'},{duration: 500,queue: false});
	});

	markers.on('clusterclick', function (e){
		// a.layer is actually a cluster
		//if (debug == true) console.log(e.layer);
		var markerCount = e.layer.getAllChildMarkers().length;
		for (var i = 0; i < markerCount; i++){
			if (debug == true) console.log('marker._leaflet_id: ' + e.layer.getAllChildMarkers()[i]._leaflet_id);
		}
	});

};

function piwikPush(){
	var piwik_search_category = document.getElementById('categorySelect').value;
	if (piwik_search_category == ""){
		var piwik_search_category = "alle Kategorien";
	};
	var piwik_search_location = document.getElementById("addressInput").value;
	//_paq.push(['trackEvent', 'Search for:', piwik_search_category + ' at: ' + piwik_search_location ]);
	if (debug == true) console.log("piwik loaded: " + piwik_loaded)
	if (piwik_loaded == true){
		_paq.push(['trackSiteSearch', piwik_search_location , piwik_search_category , false ]);
	}
}
