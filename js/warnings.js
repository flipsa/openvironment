
function calculateAqi (compound_code, compound_period, compound_amount) {
	// see for info: https://en.wikipedia.org/wiki/Air_quality_index#Computing_the_AQI

	// Most of below code inspired by: https://github.com/rathishc24/aqi/blob/master/index.js

	// In that case, this function returns 0;
	// Needs more work!

	var breakpoints = {
		'co_8smw': ['0.0-4.4', '4.5-9.4', '9.5-12.4', '12.5-15.4', '15.5-30.4', '30.5-40.4', '40.5-50.4'],
		'no2_1smw': ['0-53', '54-100', '101-360', '361-649', '650-1249', '1250-1649', '1650-2049'],
		//'PM 2dot5': ['0-30', '31-60', '61-90', '91-120', '121-250'],
		'so2_1smw': ['0-35', '36-75', '76-185', '186-304'],
		'so2_1tmw': ['305-604', '605-804', '805-1004'],
		'pm10_1tmw': ['0-54', '55-154', '155-254', '255-354', '355-424', '425-504', '505-604'],
		'o3_1smw': ['125-164', '165-204', '205-404', '405-504', '505-604'],
		'o3_8smw': ['0-54', '55-70', '71-85', '86-105', '106-200']
	};

	var aqi = {
		'co_8smw': {
			'0.0': 0,
			'4.4': 50,
			'4.5': 51,
			'9.4': 100,
			'9.5': 101,
			'12.4': 150,
			'12.5': 151,
			'15.4': 200,
			'15.5': 201,
			'30.4': 300,
			'30.5': 301,
			'40.4': 400,
			'40.5': 401,
			'50.4': 500,
			'max': 501
		},
		'no2_1smw': {
			'0': 0,
			'53': 50,
			'54': 51,
			'100': 100,
			'101': 101,
			'360': 150,
			'361': 151,
			'649': 200,
			'650': 201,
			'1249': 300,
			'1250': 301,
			'1649': 400,
			'1650': 401,
			'2049': 500,
			'max': 501
		},
		'so2_1smw': {
			'0': 0,
			'35': 50,
			'36': 51,
			'75': 100,
			'76': 101,
			'185': 150,
			'186': 151,
			'304': 200
		},
		'so2_1tmw': {
			'305': 201,
			'604': 300,
			'605': 301,
			'804': 400,
			'805': 401,
			'max': 500
		},
		'pm10_1tmw': {
			'0': 0,
			'54': 50,
			'55': 51,
			'154': 100,
			'155': 101,
			'254': 200,
			'255': 201,
			'354': 300,
			'355': 301,
			'424': 400,
			'425': 401,
			'504': 500,
			'max': 501
		},
		'o3_1smw': {
			'125': 101,
			'164': 150,
			'165': 151,
			'204': 200,
			'205': 201,
			'404': 300,
			'405': 301,
			'504': 400,
			'505': 401,
			'max': 500,
		},
		'o3_8smw': {
			'0': 0,
			'54': 50,
			'55': 51,
			'70': 100,
			'71': 101,
			'85': 150,
			'86': 151,
			'105': 200,
			'106': 201,
			'200': 300
		}
	}

	function aqiW(aqi) {

		var aqiWord;
		if (aqi <= 500) {
			var aqiWord = lang_aqi_hazardous;
		}
		if (aqi <= 300) {
			var aqiWord = lang_aqi_very_unhealthy;
		}
		if (aqi <= 200) {
			var aqiWord = lang_aqi_unhealthy;
		}
		if (aqi <= 150) {
			var aqiWord = lang_aqi_unhealthy_for_some;
		}
		if (aqi <= 100) {
			var aqiWord = lang_aqi_moderate;
		}
		if (aqi <= 50) {
			var aqiWord = lang_aqi_good;
		}
		if (aqi == "n/a") {
			var aqiWord = lang_aqi_n_a;
		}

		return aqiWord;
	}

	function aqiColor(aqi) {

		var aqiColor;

		if (aqi <= 500) {
			var aqiColor = "#7E0023"; // maroon
		}
		if (aqi <= 300) {
			var aqiColor = "#8F3F97"; // purple
		}
		if (aqi <= 200) {
			var aqiColor = "#FF0000"; // red
		}
		if (aqi <= 150) {
			var aqiColor = "#FF7E00"; // orange
		}
		if (aqi <= 100) {
			var aqiColor = "#FFFF00"; // yellow
		}
		if (aqi <= 50) {
			var aqiColor = "#00E400"; // green
		}
		if (aqi == "n/a") {
			var aqiColor = "#000000"; // grey
		}

		return aqiColor;
	}

	var compound = compound_code + '_' + compound_period;
	var paramBreakpoints = breakpoints[compound];
	var paramAqi = aqi[compound];

	//var iP = 0;
	var iP = 'n/a';

	if (compound_code == "o3") {
		var compound_amount = compound_amount / 1.9957;
	} else if (compound_code == "no2") {
		var compound_amount = compound_amount / 1.9125;
	} else if (compound_code == "co") {
		var compound_amount = compound_amount / 1164.2;
	} else if (compound_code == "so2") {
		var compound_amount = compound_amount / 2.6609;
	}

	for (var i = 0; i < paramBreakpoints.length; i++) {
		var arrBreakpointRange = paramBreakpoints[i].split('-');
		var bpLow = parseFloat(arrBreakpointRange[0]);
		var bpHigh = parseFloat(arrBreakpointRange[1]);

		if (compound_amount >= bpLow && compound_amount <= bpHigh) {
			var iLow = paramAqi[arrBreakpointRange[0]];
			var iHigh = paramAqi[arrBreakpointRange[1]];
			iP = Math.round((((iHigh - iLow) / (bpHigh - bpLow)) * (compound_amount - bpLow)) + iLow);
			break;
		}
	}

	if (debug == true) console.log('compound: ' + compound + ' / ' + 'amount: ' + compound_amount + ' / AQI: ' +iP + ' / aqiW: ' + aqiW(iP) + ' / aqiColor: ' + aqiColor(iP))
	return {
		aqi: iP,
		aqiw: aqiW(iP),
		aqic: aqiColor(iP)
	};
};

function showWarnings(feature,layer,odl_warn){
	// Check if we need to show warnings if value is above threshold
	if (
			((feature.properties.category == "air_quality") && ( feature.properties.co_8smw >= 10000 || feature.properties.no2_1smw >= 200 || feature.properties.o3_1smw >= 120 || feature.properties.pm10_1tmw >= 50 || feature.properties.so2_1smw >= 350 ))
			||
			((feature.properties.category == "radiation") && (feature.properties.odl_1smw >= odl_warn ))
			||
			( (feature.properties.category == "pegel") && ( (feature.properties.w_stateMnwMhw == "low") || (feature.properties.w_stateMnwMhw == "high") || (feature.properties.w_stateNswHsw == "high") ) )
			||
			((feature.properties.category == "bw") && (feature.properties.bw_quality >= 3))
			||
			((feature.properties.category == "weather") && ((feature.properties.wind_speed >= 75) || (feature.properties.wind_peak >= 75) || (feature.properties.squall) && (feature.properties.squall.toLowerCase().indexOf("sturm") >= -1) || (feature.properties.squall) && (feature.properties.squall.toLowerCase().indexOf("schwere") >= -1) || (feature.properties.squall) && (feature.properties.squall.toLowerCase().indexOf("extrem") >= -1) || (feature.properties.squall) && (feature.properties.squall.toLowerCase().indexOf("orkan") >= -1) || (feature.properties.conditions) && (feature.properties.conditions.toLowerCase().indexOf("dauerregen") >= -1) || (feature.properties.conditions) && (feature.properties.conditions.toLowerCase().indexOf("starkregen") >= -1) || (feature.properties.conditions) && (feature.properties.conditions.toLowerCase().indexOf("eis") >= -1) || (feature.properties.conditions) && (feature.properties.conditions.toLowerCase().indexOf("hagel") >= -1) ))
		){
		warning_counter++;

		stationWarningColor = 'red';
		warningIcon = '<i class="icon-attention" style="color:' + stationWarningColor + ';"></i>';
		warningHTML = "&#9888;";
		warningName = warningName + ' (' + (warningHTML || '') + ')';
		//alert(feature.properties.squall);
	} else {
		stationWarningColor = '';
		warningIcon = '';
		warningName = warningName;
	};
}

function dateWarningStation(feature){
	var currentTime = new Date();
	//var currentTime = moment().format('YYYY-MM-DD HH:mm:ss');

	// The last_update value from mysql is a string in mysql DATETIME format (ISO) like 'YYYY-MM-DD HH:MM:SS'; we need to convert it to JS time first
	var jsDate = new Date(Date.parse(feature.properties.last_update.replace('-','/','g')));

	// Get the difference in minutes between now and the last_update date of the sensor
	var diffMins = Math.floor(( currentTime - jsDate) / (1000*60));
	if (debug == true) console.log('uid: ' + feature.properties.uid + ' / currentTime: ' + currentTime + ' / last_update(SQL): ' + feature.properties.last_update + ' / last_update(JS): ' + jsDate + ' --> diffMins: ' + diffMins);
	if (diffMins > 720) {
		dateWarningCounter++;
		dateWarningColor = "red";
		stationWarningColor = 'red';
		dateWarningIcon = '<i class="icon-back-in-time" style="color:' + dateWarningColor + '; margin-left:5px;"></i>';
		dateWarningHTML = '<span style="color:red;">&#9201;</span>';
		warningName = feature.properties.original_name + ' (' + (dateWarningHTML || '') + ')';
	} else {
		dateWarningColor = "";
		dateWarningIcon = "";
		warningName = feature.properties.original_name;
	}
}

function show_color(feature,compound_code,compound_period,compound_amount,compound_warning,odl_avg){
	if (feature.properties.category == "air_quality") {

		// Calculate AQI, but only if category is air_quality and period is either 1smw or 8smw
		if (compound_period == '1smw' || compound_period == '8smw' || compound_period == '1tmw') {
			var aqi = calculateAqi(compound_code,compound_period,compound_amount);
			return aqi.aqic;
		} else {
			return "#000000";
		}


	} else if (feature.properties.category == "radiation") {
		if ((compound_warning - compound_amount) <= 0) { return '#ff0000'}; // red
		if ((compound_warning - compound_amount) <= 0.01) { return '#FF7E00'}; // orange
		//if ((compound_warning - compound_amount) <= 0.01) { return '#ffbf00'};
		if ((compound_warning - compound_amount) >= 0.01) { return '#00E400'}; // green

	} else if (feature.properties.category == "pegel") {
		if (feature.properties.w_stateMnwMhw == "normal") {
			console.log("MnwMhw normal: " + feature.properties.w_stateMnwMhw);
			return '#00E400';
		} else if (feature.properties.w_stateMnwMhw == "low" || feature.properties.w_stateMnwMhw == "high") {
			console.log("MnwMhw not normal: " + feature.properties.w_stateMnwMhw);
			return '#ff0000'
		} else if (feature.properties.w_stateMnwMhw == "unknown") {
			console.log("MnwMhw not normal: " + feature.properties.w_stateMnwMhw);
			return '#2E86C1'
		}
	}
}
