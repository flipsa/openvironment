function getTimezone(){

	// Get the browser timezone or fall back to UTC default (Europe/London)
	if (moment.tz.guess()){
		userTimezone = moment.tz.guess(); // GLOBAL
		// Result Selectmenu

	} else {
		userTimezone = 'Europe/London'; // GLOBAL
	}
	var selectTimezone = $("#timezone").find("[name='" + userTimezone + "']");
	selectTimezone.prop('selected', true);
	
	// When user changes timezone manually
	timezoneSelect = document.getElementById("timezone");
	timezoneSelect.onchange = function(){
		//Get the id of the element clicked
		//var userTimezone = $('option:selected', this).attr("data-uuid");
		userTimezone = $('option:selected', this).attr('name'); // GLOBAL
		
		$(".timeAgo").map( function() {
			var time = $(this).attr('data-timeUTC');
			var newTime = userTime(time,userTimezone);
			console.log('new timeAgo: ' + newTime);
			$(this).attr('data-livestamp', newTime);
		});
	};	
};

function userTime(timestamp,userTimezone,format) {
	//timezoneOffset = '+1200';
	//var userTime = moment(timestamp).utcOffset(timezoneOffset).format('YYYY-MM-DD HH:mm');
	if (format == 'dayly') {
		var userTime = moment(timestamp).tz(userTimezone).format('YYYY-MM-DD');
	} else { 
		var userTime = moment(timestamp).tz(userTimezone).format('YYYY-MM-DD HH:mm');
	}
	return userTime;
}
