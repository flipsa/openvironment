console.log("lang.js - language was set to: " + lang)

if (lang == 'de') {

	moment.locale('de_at');

	lang_settings_unit_metric = "Metrisch";
	lang_settings_unit_imperial = "Imperial";
	lang_settings_timezone_local = "Lokal";
	lang_settings_timezone_utc = "UTC";
	lang_get_current_location = "Bestimme Ihre Position...";
	lang_current_location_unavailable = "Position nicht ermittelbar.";
	lang_curent_location_denied = "Positionbestimmung verweigert.";
	lang_curent_location_temporary_error = "Temporärer Fehler, bitte versuchen Sie es erneut.";
	lang_curent_location_timeout = "Timeout: Positionsbestimmung abgebrochen.";
	lang_curent_location_not_supported_in_browser = "Ihr Browser untestützt GeoLocation nicht. ";
	lang_unknown_address = "Unbekannte Adresse";
	lang_multiple_choices_please_select = "Mehrere Möglichkeiten. Bitte wählen Sie:";
	lang_no_stations_found = ''; // map.js line 1278

	lang_aqi = "Luftqualit&auml;tsindex";
	lang_aqi_hazardous = "Extrem ungesund";
	lang_aqi_very_unhealthy = "Sehr ungesund";
	lang_aqi_unhealthy = "Ungesund";
	lang_aqi_unhealthy_for_some = "Ungesund für sensible Gruppen";
	lang_aqi_moderate = "Moderat";
	lang_aqi_good = "Gut";
	lang_aqi_n_a = "n.v.";

	lang_sensors = "Sensoren";
	lang_last_update = "Letzte Aktualisierung";

	lang_warnings = "Warnungen";
	lang_outdated = "veraltet";
	lang_results = "Ergebnisse";

	lang_8smw = "8-Stunden Mittelwert";
	lang_8tmax = "8-Stunden Tagesmaximum";
	lang_1smw = "1-Stunden Mittelwert";
	lang_1tmax = "1-Stunden Tagesmaximum";
	lang_1tmw = "Tagesmittelwert";
	lang_recent = "Letzte &Auml;nderung";

	lang_week = "Woche";
	lang_month = "Monat";
	lang_year = "Jahr";

	lang_measurements_statistics = "Statistik";
	lang_average = "Durchschnitt";
	lang_threshold = "Grenzwert";
	lang_terrestrial = "Terrestrisch";
	lang_cosmic = "Kosmisch";
	lang_mnw_mhw = "MNW / MHW";
	lang_nsw_hsw = "NSW / HSW";

	lang_altitude = "m &uuml;NN";
	lang_start_date = "In Betrieb seit";
	lang_station_type = "Stationsart";
	lang_surrounding_area = "Umgebung";
	lang_ozone_class = "Ozon-Klasse";
	lang_waterbody_characteristic_measurements = "Kennzeichnende Wasserst&auml;nde";
	lang_water_longname = "Gew&auml;sser";
	lang_water_shortname = "Gew&auml;sser (Abk.)";
	lang_km = "Km";
	lang_original_uid = "Original UID";
	lang_uid_local = "Messstellen-UID (lokal)";
	lang_country = "Land";
	lang_state = "Bundesland";
	lang_city = "Stadt";
	lang_street = "Stra&szlig;e";
	lang_agency = "Messnetzbetreiber";
	lang_provider = "Datenquelle";

	lang_current_raw_data = "Aktuelle Daten";
	lang_current_value = "Messwert";
	lang_gauge_zero = "Pegelnullpunkt (PNP)";
	lang_gauge_zero_valid_from = "PNP Eichdatum";
	lang_timeseries = "Verlauf";
	lang_station_metadata = "Informationen &uuml;ber dieser Messstelle";
	lang_station_location = "Standort";
	lang_uid = "Datenprovider Messstellen-UID";
	lang_links_and_api = "Links & API";
	lang_station_without_current_data = "Messstelle (ohne Messwerte)";
	lang_station_with_current_data = "Messstelle (mit Messwerten)";
	lang_rss_only_warnings = "nur Warnungen";
	lang_rss_full = "alle Updates";

	lang_radiation_index = "Radiation Index";

	lang_close_popup = "Popup schließen";

	lang_o3_longname = "Ozon";
	lang_o3_shortname = "O₃";
	lang_o3_code = "o3";

	lang_no2_longname = "Stickstoffdioxid";
	lang_no2_shortname = "NO₂";
	lang_no2_code = "no2";

	lang_pm10_longname = "Feinstaub (PM10)";
	lang_pm10_shortname = "PM₁₀";
	lang_pm10_code = "pm10";

	lang_so2_longname = "Schwefeldioxid";
	lang_so2_shortname = "SO₂";
	lang_so2_code = "so2";

	lang_co_longname = "Kohlenmonoxid";
	lang_co_shortname = "CO";
	lang_co_code = "co";

	lang_trans_total = "Überschreitungen (Jahr)";

	lang_odl_longname = "Ortsdosisleistung";
	lang_odl_shortname = "ODL";
	lang_odl_code = "odl";

	lang_w_longname = "Wasserstand";
	lang_w_shortname = "w";
	lang_w_code = "w";

	lang_lt_longname = "Lufttemperatur";
	lang_lt_shortname = "Temp. (Luft)";
	lang_lt_code = "lt";

	lang_wt_longname = "Wassertemperatur";
	lang_wt_shortname = "Temp. (Wasser)";
	lang_wt_code = "wt";

	lang_q_longname = "Abfluss";
	lang_q_shortname = "Abfluss";
	lang_q_code = "q";

	lang_va_longname = "Fließgeschwindigkeit";
	lang_va_shortname = "Fließgeschw.";
	lang_va_code = "va";

	lang_dfh_longname = "Durchfahrtsh&ouml;he";
	lang_dfh_shortname = "DFH";
	lang_dfh_code = "dfh";

	lang_lf_longname = "Elektrische Leitfähigkeit";
	lang_lf_shortname = "Elekt. Leitfähigk.";
	lang_lf_code = "lf";

	lang_wg_longname = "Windgeschwindigkeit";
	lang_wg_shortname = "Wind (Geschw.)";
	lang_wg_code = "wg";

	lang_wr_longname = "Windrichtung";
	lang_wr_shortname = "Wind (Richt.)";
	lang_wr_code = "wr";

	lang_gru_longname = "Grundwasser";
	lang_gru_shortname = "Grundwasser";
	lang_gru_code = "gru";

	lang_hl_longname = "Luftfeuchtigkeit";
	lang_hl_shortname = "Luftfeuchte";
	lang_hl_code = "hl";

	lang_pl_longname = "Luftdruck";
	lang_pl_shortname = "Luftdruck";
	lang_pl_code = "pl";

	lang_n_longname = "Niederschlagsmenge";
	lang_n_shortname = "Niederschlagsmenge";
	lang_n_code = "n";

	lang_n_intensity_longname = "Niederschlagsintensit&auml;t";
	lang_n_intensity_shortname = "Niederschlagsintens.";
	lang_n_intensity_code = "n_intensity";

	lang_o2_longname = "Sauerstoffgehalt";
	lang_o2_shortname = "Sauerstoff";
	lang_o2_code = "o2";

	lang_ph_longname = "pH Wert";
	lang_ph_shortname = "pH";
	lang_ph_code = "ph";

	lang_cl_longname = "Chlorid";
	lang_cl_shortname = "Chlorid";
	lang_cl_code = "cl";

	lang_orp_longname = "Redoxspannung";
	lang_orp_shortname = "Redox";
	lang_orp_code = "orp";



} else if (lang == 'en') {

	moment.locale('en_us');

	lang_settings_unit_metric = "Metric";
	lang_settings_unit_imperial = "Imperial";
	lang_settings_timezone_local = "Local";
	lang_settings_timezone_utc = "UTC";

	lang_get_current_location = "Getting current location...";
	lang_current_location_unavailable = "Can't get location";
	lang_curent_location_denied = "Getting location denied by user!";
	lang_curent_location_temporary_error = "Temporary error, please try again later.";
	lang_curent_location_timeout = "Timeout getting location.";
	lang_curent_location_not_supported_in_browser = "Ihr Browser untestützt GeoLocation nicht. ";
	lang_unknown_address = "Unknown address";
	lang_multiple_choices_please_select = "Multiple choices, please select:";
	lang_no_stations_found = '';

	lang_aqi = "Air Quality Index";
	lang_aqi_hazardous = "Hazardous";
	lang_aqi_very_unhealthy = "Very Unhealthy";
	lang_aqi_unhealthy = "Unhealthy";
	lang_aqi_unhealthy_for_some = "Unhealthy for sensitive groups";
	lang_aqi_moderate = "Moderate";
	lang_aqi_good = "Good";
	lang_aqi_n_a = "n/a";

	lang_sensors = "Sensors";
	lang_last_update = "Last updated";

	lang_warnings = "warnings";
	lang_outdated = "outdated";
	lang_results = "results";

	lang_8smw = "8-hours average";
	lang_8tmax = "8-hours daily maximum";
	lang_1smw = "1-hour average";
	lang_1tmax = "1-hour daily maximum";
	lang_1tmw = "Daily average";
	lang_recent = "Latest";

	lang_week = "Week";
	lang_month = "Month";
	lang_year = "Year";

	lang_measurements_statistics = "Statistics";
	lang_average = "Average";
	lang_threshold = "Threshold";
	lang_terrestrial = "Terrestrial";
	lang_cosmic = "Cosmic";

	lang_mnw_mhw = "MLW / MHW";
	lang_nsw_hsw = "LSW / HSW";

	lang_altitude = "m a.s.l.";
	lang_start_date = "In use since";
	lang_station_type = "Type of station";
	lang_surrounding_area = "Surrounding area";
	lang_ozone_class = "Ozone classification";
	lang_waterbody_characteristic_measurements = "Characteristic water levels";
	lang_water_longname = "Water body";
	lang_water_shortname = "Water body (abbr)";
	lang_km = "Km";
	lang_original_uid = "Original UID";
	lang_uid_local = "Station ID (local)";
	lang_country = "Country";
	lang_state = "State";
	lang_city = "City";
	lang_street = "Street";
	lang_agency = "Network agency";
	lang_provider = "Data source";

	lang_current_raw_data = "Current data";
	lang_current_value = "Value";
	lang_gauge_zero = "Gauge datum";
	lang_gauge_zero_valid_from = "Gauge zero valid from";
	lang_timeseries = "Timeseries";
	lang_station_metadata = "Station meta data";
	lang_station_location = "Location";
	lang_uid = "Data source station ID";
	lang_links_and_api = "Links & API";
	lang_station_without_current_data = "Station meta data";
	lang_station_with_current_data = "Station meta data & measurements";
	lang_rss_only_warnings = "only warnings";
	lang_rss_full = "all updates";

	lang_radiation_index = "Radiation Index";

	lang_close_popup = "Close popup";

	lang_o3_longname = "Ozone";
	lang_o3_shortname = "O₃";
	lang_o3_code = "o3";

	lang_no2_longname = "Nitrous dioxide";
	lang_no2_shortname = "NO₂";
	lang_no2_code = "no2";

	lang_pm10_longname = "Fine dust (PM10)";
	lang_pm10_shortname = "PM₁₀";
	lang_pm10_code = "pm10";

	lang_so2_longname = "Sulfur dioxide";
	lang_so2_shortname = "SO₂";
	lang_so2_code = "so2";

	lang_co_longname = "Carbon monoxide";
	lang_co_shortname = "CO";
	lang_co_code = "co";

	lang_trans_total = "Transgressions (year)";

	lang_odl_longname = "Local dose rate";
	lang_odl_shortname = "LDR";
	lang_odl_code = "odl";

	lang_w_longname = "Water level";
	lang_w_shortname = "w";
	lang_w_code = "w";

	lang_lt_longname = "Air temperature";
	lang_lt_shortname = "Temp. (air)";
	lang_lt_code = "lt";

	lang_wt_longname = "Water temperature";
	lang_wt_shortname = "Temp. (water)";
	lang_wt_code = "wt";

	lang_q_longname = "Flow rate";
	lang_q_shortname = "Flow rate";
	lang_q_code = "q";

	lang_va_longname = "Flow velocity";
	lang_va_shortname = "Flow velocity";
	lang_va_code = "va";

	lang_dfh_longname = "Clearance height";
	lang_dfh_shortname = "Clearance height";
	lang_dfh_code = "dfh";

	lang_lf_longname = "Elektrische conductivity";
	lang_lf_shortname = "El. cond.";
	lang_lf_code = "lf";

	lang_wg_longname = "Wind speed";
	lang_wg_shortname = "Wind speed";
	lang_wg_code = "wg";

	lang_wr_longname = "Wind direction";
	lang_wr_shortname = "Wind (dir.)";
	lang_wr_code = "wr";

	lang_gru_longname = "Groundwater";
	lang_gru_shortname = "Groundwater";
	lang_gru_code = "gru";

	lang_hl_longname = "Humidity";
	lang_hl_shortname = "Humidity";
	lang_hl_code = "hl";

	lang_pl_longname = "Pressure";
	lang_pl_shortname = "Pressure";
	lang_pl_code = "pl";

	lang_n_longname = "Precipitation amount";
	lang_n_shortname = "Precipitation amount";
	lang_n_code = "n";

	lang_n_intensity_longname = "Precipitation intensity";
	lang_n_intensity_shortname = "Precipitation intensity";
	lang_n_intensity_code = "n_intensity";

	lang_o2_longname = "Oxygen level";
	lang_o2_shortname = "Oxygen";
	lang_o2_code = "o2";

	lang_ph_longname = "pH value";
	lang_ph_shortname = "pH";
	lang_ph_code = "ph";

	lang_cl_longname = "Chloride";
	lang_cl_shortname = "Chloride";
	lang_cl_code = "cl";

	lang_orp_longname = "Redox potential";
	lang_orp_shortname = "Redox pot.";
	lang_orp_code = "orp";
}
