function createMarkerPopup(feature,layer){

	// for each marker increment the counter
	counter++;

	if (feature.properties && feature.properties.category){

		// Set some global variables we need for other functions later as well
		//var name = feature.properties.original_name;
		var compounds = [];
		compoundsSelectMenu = [];

		// For station searches (!= near), the distance is 'undefined', so we need to set it to 0 here
		if (typeof feature.properties.distance == "undefined"){
			var distance = 0;
		} else {
			// If distance is defined, parse it as float and round it to one decimal
			//var distance = parseFloat(feature.properties.distance).toFixed(1);
			var distance = Math.round(parseFloat(feature.properties.distance) * 10) / 10;
		}

		if (feature.properties.category == 'air_quality'){

			// Get the ratios for the font cperiodsSelectMenu(compound)olor


			// O3
			if (feature.properties.o3_1smw != null){

				var compound_time = feature.properties.o3_1smw_time;
				var compound_amount = parseFloat(feature.properties.o3_1smw);
				var compound_unit = 'µg/m³';
				var compound_warning = 120;
				var compound_longname = lang_o3_longname;
				var compound_shortname = lang_o3_shortname;
				var compound_code = lang_o3_code;
				var compound_period= '8smw';
				var compound_ratio = show_ratio(bar_width, compound_warning, compound_amount);
				var compound_color = show_color(feature,compound_code,compound_period,compound_amount,compound_warning);
				var show_compound = show_compound_without_bar(compound_code, compound_time, compound_color, compound_amount, compound_unit, compound_warning, bar_width, compound_ratio);

				var selector_o3 = '<input type=checkbox id="2" checked onClick="change(this)" style="margin:0;padding:0;height:13px;vertical-align:bottom;"><label for="2">O₃ | </label>';

				var station_graph = show_graph(feature.properties.uid, feature.properties.category, compound_amount, compound_code, compound_period, g_container_w);
				var tabsHeaderCompound2 = showTabHeaderCompound(compound_code, compound_longname, compound_shortname);
				var tabsContentCompound2 = showTabContentCompound(compound_code, show_compound, station_graph);
				compounds.push('<span style="color:' + compound_color + ';">' + compound_longname + '</span>');
				compoundsSelectMenu.push('<option data-compound="' + compound_code + '">' + compound_longname + '</option>');
				//if (debug == true) console.log('uid: ' + feature.properties.uid + ' / amount: ' + compound_amount + ' / warning: ' + compound_warning + ' / color: ' + compound_color);
			};

			// NO2
			if (feature.properties.no2_1smw != null){

				var compound_time = feature.properties.no2_1smw_time;
				var compound_amount = parseFloat(feature.properties.no2_1smw);
				var compound_unit = 'µg/m³';
				var compound_warning = 200;
				var compound_longname = lang_no2_longname;
				var compound_shortname = lang_no2_shortname;
				var compound_code = lang_no2_code;
				var compound_period= '1smw';
				var compound_ratio = show_ratio(bar_width, compound_warning, compound_amount);
				var compound_color = show_color(feature,compound_code,compound_period,compound_amount,compound_warning);
				var show_compound = show_compound_without_bar(compound_code, compound_time, compound_color, compound_amount, compound_unit, compound_warning, bar_width, compound_ratio);

				var selector_no2 = '<input type=checkbox id="1" checked onClick="change(this)" style="margin:0;padding:0;height:13px;vertical-align:bottom;"><label for="1">NO₂ | </label>';

				var station_graph = show_graph(feature.properties.uid, feature.properties.category, compound_amount, compound_code, compound_period, g_container_w);
				var tabsHeaderCompound3 = showTabHeaderCompound(compound_code, compound_longname, compound_shortname);
				var tabsContentCompound3 = showTabContentCompound(compound_code, show_compound, station_graph);
				compounds.push('<span style="color:' + compound_color + ';">' + compound_longname + '</span>');
				compoundsSelectMenu.push('<option data-compound="' + compound_code + '">' + compound_longname + '</option>');
			};

			// PM10
			if (feature.properties.pm10_1tmw != null){

				//var compound_time = feature.properties.pm10_1tmw_time;
				var compound_amount = parseFloat(feature.properties.pm10_1tmw);
				//var compound_unit = 'µg/m³';
				var compound_warning = 50;
				var compound_longname = lang_pm10_longname;
				var compound_shortname = lang_pm10_shortname;
				var compound_code = lang_pm10_code;
				var compound_period= '1tmw';
				//var compound_ratio = show_ratio(bar_width, compound_warning, compound_amount);
				var compound_color = show_color(feature,compound_code,compound_period,compound_amount,compound_warning);
				//var show_compound = show_compound_without_bar(compound_code, compound_time, compound_color, compound_amount, compound_unit, compound_warning, bar_width, compound_ratio);

				var selector_pm10 = '<input type=checkbox id="3" checked onClick="change(this)" style="margin:0;padding:0;height:13px;vertical-align:bottom;"><label for="3">PM₁₀ | </label>';

				//var station_graph = show_graph(feature.properties.uid, feature.properties.category, compound_amount, compound_code, compound_period, g_container_w);
				//var tabsHeaderCompound1 = showTabHeaderCompound(compound_code, compound_longname, compound_shortname);
				//var tabsContentCompound1 = showTabContentCompound(compound_code, show_compound, station_graph);
				compounds.push('<span style="color:' + compound_color + ';">' + compound_longname + '</span>');
				compoundsSelectMenu.push('<option data-compound="' + compound_code + '">' + compound_longname + '</option>');
			};

			// CO
			if (feature.properties.co_8smw != null){

				var compound_time = feature.properties.co_8smw_time;
				var compound_amount = parseFloat(feature.properties.co_8smw);
				var compound_unit = '  g/m³';
				var compound_warning = 10000;
				var compound_longname = lang_co_longname;
				var compound_shortname = lang_co_shortname;
				var compound_code = lang_co_code;
				var compound_period = '8smw';
				var compound_ratio = show_ratio(bar_width, compound_warning, compound_amount);
				var compound_color = show_color(feature,compound_code,compound_period,compound_amount,compound_warning);
				var show_compound = show_compound_without_bar(compound_code, compound_time, compound_color, compound_amount, compound_unit, compound_warning, bar_width, compound_ratio);

				var selector_co = '<input type=checkbox id="0" checked onClick="change(this)" style="margin:0;padding:0;height:13px;vertical-align:bottom;"><label for="0">CO | </label>';

				var station_graph = show_graph(feature.properties.uid, feature.properties.category, compound_amount, compound_code, compound_period, g_container_w);
				var tabsHeaderCompound4 = showTabHeaderCompound(compound_code, compound_longname, compound_shortname);
				var tabsContentCompound4 = showTabContentCompound(compound_code, show_compound, station_graph);
				compounds.push('<span style="color:' + compound_color + ';">' + compound_longname + '</span>');
				compoundsSelectMenu.push('<option data-compound="' + compound_code + '">' + compound_longname + '</option>');
			};

			// SO2
			if (feature.properties.so2_1smw != null){

				var compound_time = feature.properties.so2_1smw_time;
				var compound_amount = parseFloat(feature.properties.so2_1smw);
				var compound_unit = 'µg/m³';
				var compound_warning = 350;
				var compound_longname = lang_so2_longname;
				var compound_shortname = lang_so2_shortname;
				var compound_code = lang_so2_code;
				var compound_period= '1smw';
				var compound_ratio = show_ratio(bar_width, compound_warning, compound_amount);
				var compound_color = show_color(feature,compound_code,compound_period,compound_amount,compound_warning);
				var show_compound = show_compound_without_bar(compound_code, compound_time, compound_color, compound_amount, compound_unit, compound_warning, bar_width, compound_ratio);

				var selector_so2 = '<input type=checkbox id="4" checked onClick="change(this)" style="margin:0;padding:0;height:13px;vertical-align:bottom;"><label for="4">SO₂ | </label>';

				var station_graph = show_graph(feature.properties.uid, feature.properties.category, compound_amount, compound_code, compound_period, g_container_w);
				var tabsHeaderCompound5 = showTabHeaderCompound(compound_code, compound_longname, compound_shortname);
				var tabsContentCompound5 = showTabContentCompound(compound_code, show_compound, station_graph);
				compounds.push('<span style="color:' + compound_color + ';">' + compound_longname + '</span>');
				compoundsSelectMenu.push('<option data-compound="' + compound_code + '">' + compound_longname + '</option>');
			};

			if (feature.properties.trans_total != null){
				var show_trans_total = '<tr><td class="tdL">' + lang_trans_total + ': </td><td class="tdR">' + feature.properties.trans_total + '</td></tr>';
			}

			var graphSelector = (selector_co || "") +
					(selector_no2 || "") +
					(selector_o3 || "") +
					(selector_pm10 || "") +
					(selector_so2 || "")
			;

			//var show_aqi = '<div style="max-width:200px; flex:1;  margin:10px 10px 0 0; background:#ffffff;"  class="round border">' +

			var aqi_rainbow =
				'<div id="aqiOuter" class="round border">' +
					'<h4>' + lang_aqi + '</h4>' +
					'<div id="aqiInner">' +
						//'<div id="aqiGaugeOuter">' +
						//	'<div id="aqiGauge"></div>' +
						//'</div>' +
						'<div id="aqiLegendOuter">' +
							'<div id="aqiLegend" class="border round">' +
								'<div class="aqi-good" data-aqi="#00E400">' + lang_aqi_good + '</div>' +
								'<div class="aqi-moderate" data-aqi="#FFFF00">' + lang_aqi_moderate + '</div>' +
								'<div class="aqi-unhealthy_some" data-aqi="#FF7E00">' + lang_aqi_unhealthy_for_some + '</div>' +
								'<div class="aqi-unhealthy" data-aqi="#FF0000">' + lang_aqi_unhealthy + '</div>' +
								'<div class="aqi-very_unhealthy" data-aqi="#8F3F97">' + lang_aqi_very_unhealthy + '</div>' +
								'<div class="aqi-hazardous" data-aqi="#7E0023">' + lang_aqi_hazardous + '</div>' +
							'</div>' +
						'</div>' +
						'<div class="clearfix"></div>' +
					'</div>' +
				'</div>'
			;

		} else if (feature.properties.category == "radiation"){

			if (feature.properties.odl_1smw != null){
				var odl_warn = (parseFloat(feature.properties.odl_avg_1smw) + 0.020);
				var odl_warn = odl_warn.toFixed(3);
				var compound_time = feature.properties.odl_time;
				var compound_amount = parseFloat(feature.properties.odl_1smw);
				var compound_unit = 'µSv/h';
				var compound_warning = odl_warn;
				var compound_longname = lang_odl_longname;
				var compound_shortname = lang_odl_shortname;
				var compound_code = lang_odl_code;
				var compound_period= '1smw';
				var compound_ratio = show_ratio(bar_width, compound_warning, compound_amount);
				var compound_color = show_color(feature,compound_code,compound_period,compound_amount,compound_warning);
				//alert("color: " + compound_color + " / amount: " + compound_amount + " / warn: " + compound_warning + " / ratio: " + compound_ratio);
				var show_compound = show_compound_without_bar(compound_code, compound_time, compound_color, compound_amount, compound_unit, compound_warning, bar_width, compound_ratio);

				var selector_odl = '<input type=checkbox id="0" checked onClick="change(this)" style="margin:0;padding:0;height:13px;vertical-align:bottom;"><label for="0">Gamma ODL | </label>';
				var graphSelector = (selector_odl || "");
				var station_graph = show_graph(feature.properties.uid, feature.properties.category, compound_amount, compound_code, compound_period, g_container_w);
				var tabsHeaderCompound1 = showTabHeaderCompound(compound_code, compound_longname, compound_shortname);
				var tabsContentCompound1 = showTabContentCompound(compound_code, show_compound, station_graph);
				compounds.push('<span style="color:' + compound_color + ';">' + compound_longname + '</span>');
				compoundsSelectMenu.push('<option data-compound="' + compound_code + '">' + compound_longname + '</option>');

				var compound_warning = odl_warn;
				var odl_avg = '<tr><td class="tdL">' + lang_average + ': </td><td class="tdR">' + feature.properties.odl_avg_1smw + ' ' + compound_unit + '</td></tr>';
				var odl_warning = '<tr><td class="tdL">' + lang_threshold + ': </td><td class="tdR">' + odl_warn + ' ' + compound_unit + '</td></tr>';
				var odl_ter = '<tr><td class="tdL">' + lang_terrestrial + ': </td><td class="tdR" style="color:' + compound_color + ';">' + feature.properties.odl_ter_1smw + ' ' + compound_unit + '</td></tr>';
				var odl_cos = '<tr><td class="tdL">' + lang_cosmic + ': </td><td class="tdR" style="color:' + compound_color + ';">' + feature.properties.odl_cos_1smw + ' ' + compound_unit + '</td></tr>';

				var radiation_statistics =
					(odl_avg || '') +
					(odl_warning || '')
				;
			};

		} else if (feature.properties.category == "weather"){

			if (feature.properties.pressure != null){
				var compound_time = feature.properties.sensor_time;
				var compound_color = 'black';
				var compound_amount = parseFloat(feature.properties.pressure);
				var compound_ratio = 0;
				var compound_unit = 'hPa';
				var compound_warning = '';
				var compound_longname = 'Luftdruck';
				var compound_shortname = 'Luftdruck';
				var compound_code = 'pressure';
				var compound_period= 'pressure';
				var show_pressure = '<div class="iw_block"><div class="iw_line"><div class="iw_label2_tab1">Luftdruck: </div>' + feature.properties.pressure + ' hPa</div></div>';
				var show_compound = show_pressure;
				var selector_pressure = '<input type=checkbox id="0" onClick="change(this)" style="margin:0;padding:0;height:13px;vertical-align:bottom;"><label for="0">Luftdruck | </label>';
				var station_graph = show_graph(feature.properties.uid, feature.properties.category, compound_amount, compound_code, compound_period, g_container_w);
				var tabsHeaderCompound1 = showTabHeaderCompound(compound_code, compound_longname, compound_shortname);
				var tabsContentCompound1 = showTabContentCompound(compound_code, show_compound, station_graph);
				compounds.push('<span style="color:' + compound_color + ';">' + compound_longname + '</span>');
				compoundsSelectMenu.push('<option data-compound="' + compound_code + '">' + compound_longname + '</option>');
			};

			if (feature.properties.temperature != null){
				var compound_time = feature.properties.sensor_time;
				var compound_color = 'black';
				var compound_amount = parseFloat(feature.properties.pressure);
				var compound_ratio = 0;
				var compound_unit = '°C';
				var compound_warning = '';
				var compound_longname = 'Temperatur';
				var compound_shortname = 'Temperatur';
				var compound_code = 'temperature';
				var compound_period= 'temperature';
				var show_temperature = '<div class="iw_block"><div class="iw_line"><div class="iw_label2_tab1">Temperatur: </div>' + feature.properties.temperature + '° C</div></div>';
				var show_compound = show_temperature;
				var selector_temperature = '<input type=checkbox id="1" checked onClick="change(this)" style="margin:0;padding:0;height:13px;vertical-align:bottom;"><label for="1">Temperatur | </label>';
				var station_graph = show_graph(feature.properties.uid, feature.properties.category, compound_amount, compound_code, compound_period, g_container_w);
				var tabsHeaderCompound2 = showTabHeaderCompound(compound_code, compound_longname, compound_shortname);
				var tabsContentCompound2 = showTabContentCompound(compound_code, show_compound, station_graph);
				compounds.push('<span style="color:' + compound_color + ';">' + compound_longname + '</span>');
				compoundsSelectMenu.push('<option data-compound="' + compound_code + '">' + compound_longname + '</option>');
			};

			if (feature.properties.precipitation != null){
				var compound_time = feature.properties.sensor_time;
				var compound_color = 'black';
				var compound_amount = parseFloat(feature.properties.pressure);
				var compound_ratio = 0;
				var compound_unit = 'L/m²';
				var compound_warning = '';
				var compound_longname = 'Niederschlag';
				var compound_shortname = 'Niederschlag';
				var compound_code = 'precipitation';
				var compound_period= 'precipitation';
				var show_precipitation = '<div class="iw_block"><div class="iw_line"><div class="iw_label2_tab1">Niederschlag: </div>' + feature.properties.precipitation + ' l/m²</div></div>';
				var show_compound = show_precipitation;
				var selector_precipitation = '<input type=checkbox id="2" checked onClick="change(this)" style="margin:0;padding:0;height:13px;vertical-align:bottom;"><label for="2">Niederschlag | </label>';
				var station_graph = show_graph(feature.properties.uid, feature.properties.category, compound_amount, compound_code, compound_period, g_container_w);
				var tabsHeaderCompound3 = showTabHeaderCompound(compound_code, compound_longname, compound_shortname);
				var tabsContentCompound3 = showTabContentCompound(compound_code, show_compound, station_graph);
				compounds.push('<span style="color:' + compound_color + ';">' + compound_longname + '</span>');
				compoundsSelectMenu.push('<option data-compound="' + compound_code + '">' + compound_longname + '</option>');
			};

			if (feature.properties.wind_direction != null){
				var show_wind_direction = '<div class="iw_block"><div class="iw_line"><div class="iw_label2_tab1">Windrichtung: </div>' + feature.properties.wind_direction + '</div></div>';
				var tabsHeaderCompound4a = '<li><a href="#tabsContentCompound4a">Windrichtung</a></li>';
				var tabsContentCompound4a = show_wind_direction;
			};

			if (feature.properties.wind_speed != null){
				var show_wind_speed = '<div class="iw_block"><div class="iw_line"><div class="iw_label2_tab1">Wingeschwindigkeit: </div>' + feature.properties.wind_speed + ' km/h</div></div>';
				var selector_wind_speed = '<input type=checkbox id="3" checked onClick="change(this)" style="margin:0;padding:0;height:13px;vertical-align:bottom;"><label for="3">Wingeschw. | </label>';
				var tabsHeaderCompound4b = '<li><a href="#tabsContentCompound4b">Windgeschw.</a></li>';
				var tabsContentCompound4b = show_wind_speed;
			};

			if (feature.properties.wind_peak != null){
				var show_wind_peak = '<div class="iw_block"><div class="iw_line"><div class="iw_label2_tab1">Windgesch. max: </div>' + feature.properties.wind_peak + ' km/h</div></div>';
				var selector_wind_peak = '<input type=checkbox id="4" checked onClick="change(this)" style="margin:0;padding:0;height:13px;vertical-align:bottom;"><label for="4">Windgesch. max | </label>';
				var tabsHeaderCompound4c = '<li><a href="#tabsContentCompound4c">Wind Max</a></li>';
				var tabsContentCompound4c = show_wind_peak;
			};

			if (feature.properties.conditions != null){
				var compound_time = feature.properties.sensor_time;
				var compound_color = 'black';
				var compound_amount = parseFloat(feature.properties.conditions);
				var compound_ratio = 0;
				var compound_unit = '';
				var compound_warning = '';
				var compound_longname = 'Wetterbedingungen';
				var compound_shortname = 'Bedingungen';
				var compound_code = 'conditions';
				var compound_period= 'conditions';
				var show_conditions = '<div class="iw_block"><div class="iw_line"><div class="iw_label2_tab1">Wetterbedingungen: </div>' + feature.properties.conditions + '</div></div>';
				var show_compound = show_conditions;
				var station_graph = show_graph(feature.properties.uid, feature.properties.category, compound_amount, compound_code, compound_period, g_container_w);
				var tabsHeaderCompound5 = showTabHeaderCompound(compound_code, compound_longname, compound_shortname);
				var tabsContentCompound5 = showTabContentCompound(compound_code, show_compound, station_graph);
				compounds.push('<span style="color:' + compound_color + ';">' + compound_longname + '</span>');
				compoundsSelectMenu.push('<option data-compound="' + compound_code + '">' + compound_longname + '</option>');
			};

			if (feature.properties.squall != null){
				var compound_time = feature.properties.sensor_time;
				var compound_color = 'black';
				var compound_amount = parseFloat(feature.properties.squall);
				var compound_ratio = 0;
				var compound_unit = '';
				var compound_warning = '';
				var compound_longname = 'B&ouml;en';
				var compound_shortname = 'B&ouml;en';
				var compound_code = 'squall';
				var compound_period= 'squall';
				var show_squall = '<div class="iw_block"><div class="iw_line"><div class="iw_label2_tab1">B&ouml;en: </div>' + feature.properties.squall + '</div></div>';
				var show_compound = show_squall;
				var station_graph = show_graph(feature.properties.uid, feature.properties.category, compound_amount, compound_code, compound_period, g_container_w);
				var tabsHeaderCompound6 = showTabHeaderCompound(compound_code, compound_longname, compound_shortname);
				var tabsContentCompound6 = showTabContentCompound(compound_code, show_compound, station_graph);
				compounds.push('<span style="color:' + compound_color + ';">' + compound_longname + '</span>');
			};

				var compound_time4 = feature.properties.sensor_time;
				var compound_color4 = 'black';
				var compound_amount4 = parseFloat(feature.properties.pressure);
				var compound_ratio4 = 0;
				var compound_unit4 = '';
				var compound_warning4 = '';
				var compound_longname4 = 'Wind';
				var compound_shortname4 = 'Wind';
				var compound_code4 = 'wind';
				var compound_period= 'wind';
				//var show_compound4 = show_wind;
				compounds.push('<span style="color:' + compound_color + ';">' + compound_longname + '</span>');
				compoundsSelectMenu.push('<option data-compound="' + compound_code + '">' + compound_longname + '</option>');

				var station_graph4 = '<div style="clear:both;"></div>' +
					'<div class="iw_box" >' +
						'<div class="iw_block" style="margin-bottom:0;">' +
							'<div class="iw_header"><div class="left50"><i class="icon-chart-line"></i> Verlauf</div><div class="right50"><span class="limit_active limit limit_7d" data-limit="7" onclick="setGraphlimit(7);">' + lang_week + '</span> / <span class="limit_default limit limit_30d" data-limit="30" onclick="setGraphlimit(30);">' + lang_month + '</span> / <span class="limit_default limit limit_365d" data-limit="365" onclick="setGraphlimit(365);">' + lang_year + '</span></div><div style="clear:both;"></div></div>' +
							'<div id="spinner' + compound_code + '" class="spinner_graph"><img src="/img/loading22.gif" alt="Loading gif" /></div>' +
							'<div id="g_container' + compound_code4 + '" class="g_container" style="width:' + g_container_w + '">' +
								'<div style="position:relative;">' +
									'<div id="g_labels_' + compound_code4 + '" class="g_labels" ></div>' +
									'<div id="graph_' + compound_code4 + '" class="graph" style="width:' + g_container_w + '"></div>' +
									//'<div id="g_selector" class="g_selector">' +
									//	"Zeige: " +
									//	graphSelector +
									//	"" +
									//'</div>' +
								'</div>' +
							'</div>' +
						'</div>' +
					'</div>' +
					'<div style="clear:both;"></div>'
				;
			var tabsHeaderCompound4 = '<li><a href="#tabsContentCompound4">Wind</li>';
			var tabsContentCompound4 = '<div id="tabsContentCompound4">' + (tabsContentCompound4a || "") +
										(tabsContentCompound4b || "") +
										(tabsContentCompound4c || "") +
										station_graph4 + '</div>';

			var graphSelector = (selector_pressure || "") +
						(selector_temperature || "") +
						(selector_precipitation || "") +
						(selector_wind_speed || "") +
						(selector_wind_peak || "")
			;

		} else if (feature.properties.category == "pegel"){

			if (feature.properties.w != null){
				var w_ratio = "0";

				var compound_time = feature.properties.w_timestamp;
				var compound_color = show_color(feature,compound_code,compound_period,compound_amount,compound_warning);
				var compound_amount = parseFloat(feature.properties.w);
				var compound_ratio = w_ratio;
				//var compound_unit = feature.properties.w_unit;
				var compound_unit = 'cm';
				var compound_warning = '';
				var compound_longname = lang_w_longname;
				var compound_shortname = lang_w_shortname;
				var compound_code = lang_w_code;

				var compound_period= 'w';
				var show_w = show_compound_without_bar(compound_code, compound_time, compound_color, compound_amount, compound_unit, compound_warning, bar_width, compound_ratio);
				var show_compound = show_w;
				var selector_w = '<input type=checkbox id="0" checked onClick="change(this)" style="margin:0;padding:0;height:13px;vertical-align:bottom;"><label for="0">Pegelstand | </label>';
				var station_graph = show_graph(feature.properties.uid, feature.properties.category, compound_amount, compound_code, compound_period, g_container_w);
				var tabsHeaderCompound1 = showTabHeaderCompound(compound_code, compound_longname, compound_shortname);
				var tabsContentCompound1 = showTabContentCompound(compound_code, show_compound, station_graph);
				compounds.push('<span style="color:' + compound_color + ';">' + compound_longname + '</span>');
				compoundsSelectMenu.push('<option data-compound="' + compound_code + '">' + compound_longname + '</option>');

				if (feature.properties.M_I_shortname != null){
					var M_I = '<tr><td class="tdL">' + feature.properties.M_I_shortname + ': </td><td class="tdR">' + parseFloat(feature.properties.M_I_value) + ' ' + feature.properties.M_I_unit + ' (' + feature.properties.M_I_validFrom + ')</td></tr>';
				};
				if (feature.properties.M_II_shortname != null){
					var M_II = '<tr><td class="tdL">' + feature.properties.M_II_shortname + ': </td><td class="tdR">' + parseFloat(feature.properties.M_II_value) + ' ' + feature.properties.M_II_unit + ' (' + feature.properties.M_II_validFrom + ')</td></tr>';
				};
				if (feature.properties.M_II_shortname != null){
					var M_III = '<tr><td class="tdL">' + feature.properties.M_III_shortname + ': </td><td class="tdR">' + parseFloat(feature.properties.M_III_value) + ' ' + feature.properties.M_III_unit + ' (' + feature.properties.M_III_validFrom + ')</td></tr>';
				};
				if (feature.properties.ZS_I_shortname != null){
					var ZS_I = '<tr><td class="tdL">' + feature.properties.ZS_I_shortname + ': </td><td class="tdR">' + parseFloat(feature.properties.ZS_I_value) + ' ' + feature.properties.ZS_I_unit + ' (' + feature.properties.ZS_I_validFrom + ')</td></tr>';
				};
				if (feature.properties.rnw_shortname != null){
					var rnw = '<tr><td class="tdL">' + feature.properties.rnw_shortname + ': </td><td class="tdR">' + parseFloat(feature.properties.rnw_value) + ' ' + feature.properties.rnw_unit + ' (' + feature.properties.rnw_validFrom + ')</td></tr>';
				};
				if (feature.properties.hsw_shortname != null){
					var hsw = '<tr><td class="tdL">' + feature.properties.hsw_shortname + ': </td><td class="tdR">' + parseFloat(feature.properties.hsw_value) + ' ' + feature.properties.hsw_unit + ' (' + feature.properties.hsw_validFrom + ')</td></tr>';
				};

				if (feature.properties.hhw_shortname != null){
					var hhw = '<tr><td class="tdL">' + feature.properties.hhw_shortname + ': </td><td class="tdR">' + parseFloat(feature.properties.hhw_value) + ' ' + feature.properties.hhw_unit + ' (' + feature.properties.hhw_date + ')</td></tr>';
				};
				if (feature.properties.nnw_shortname != null){
					var nnw = '<tr><td class="tdL">' + feature.properties.nnw_shortname + ': </td><td class="tdR">' + parseFloat(feature.properties.nnw_value) + ' ' + feature.properties.nnw_unit + ' (' + feature.properties.nnw_date + ')</td></tr>';
				};
				if (feature.properties.mw_shortname != null){
					var mw = '<tr><td class="tdL">' + feature.properties.mw_shortname + ': </td><td class="tdR">' + parseFloat(feature.properties.mw_value) + ' ' + feature.properties.mw_unit + ' (' + feature.properties.mw_timespanStart + ' - ' + feature.properties.mw_timespanEnd + ')</td></tr>';
				};

				if (feature.properties.mnw_shortname != null){
					var mnw = '<tr><td class="tdL">' + feature.properties.mnw_shortname + ': </td><td class="tdR">' + parseFloat(feature.properties.mnw_value) + ' ' + feature.properties.mnw_unit + ' (' + feature.properties.mnw_timespanStart + ' - ' + feature.properties.mnw_timespanEnd + ')</td></tr>';
				};
				if (feature.properties.mhw_shortname != null){
					var mhw = '<tr><td class="tdL">' + feature.properties.mhw_shortname + ': </td><td class="tdR">' + parseFloat(feature.properties.mhw_value) + ' ' + feature.properties.mhw_unit + ' (' + feature.properties.mhw_timespanStart + ' - ' + feature.properties.mhw_timespanEnd + ')</td></tr>';
				};
				if (feature.properties.mtnw_shortname != null){
					var mtnw = '<tr><td class="tdL">' + feature.properties.mtnw_shortname + ': </td><td class="tdR">' + parseFloat(feature.properties.mtnw_value) + ' ' + feature.properties.mtnw_unit + ' (' + feature.properties.mtnw_timespanStart + ' - ' + feature.properties.mtnw_timespanEnd + ')</td></tr>';
				};
				if (feature.properties.mthw_shortname != null){
					var mthw = '<tr><td class="tdL">' + feature.properties.mthw_shortname + ': </td><td class="tdR">' + parseFloat(feature.properties.mthw_value) + ' ' + feature.properties.mthw_unit + ' (' + feature.properties.mthw_timespanStart + ' - ' + feature.properties.mthw_timespanEnd + ')</td></tr>';
				};

				if (feature.properties.hthw_shortname != null){
					var hthw = '<tr><td class="tdL">' + feature.properties.hthw_shortname + ': </td><td class="tdR">' + parseFloat(feature.properties.hthw_value) + ' ' + feature.properties.hthw_unit + ' (' + feature.properties.hthw_date + ')</td></tr>';
				};
				if (feature.properties.ntnw_shortname != null){
					var ntnw = '<tr><td class="tdL">' + feature.properties.ntnw_shortname + ': </td><td class="tdR">' + parseFloat(feature.properties.ntnw_value) + ' ' + feature.properties.ntnw_unit + ' (' + feature.properties.ntnw_date +  ')</td></tr>';
				};

				if (feature.properties.w_gaugeZero_value != null){
					var w_gaugeZero_value = '<tr><td class="tdL">' + lang_gauge_zero + ': </td><td class="tdR">' + parseFloat(feature.properties.w_gaugeZero_value) + ' ' + feature.properties.w_gaugeZero_unit + '</td></tr>';
				};
				if (feature.properties.w_gaugeZero_validFrom != null){
					var w_gaugeZero_validFrom = '<tr><td class="tdL">' + lang_gauge_zero_valid_from + ': </td><td class="tdR">' + feature.properties.w_gaugeZero_validFrom + '</td></tr>';
				};

				var waterways_statistics =
					// Waterways Statistics
					(mw || '') +
					(mnw || '') +
					(mhw || '') +
					(hhw || '') +
					(nnw || '') +
					(mtnw || '') +
					(mthw || '') +
					(hthw || '') +
					(ntnw || '') +
					(hsw || '') +
					(M_I || '') +
					(M_II || '') +
					(M_III || '') +
					(ZS_I || '') +
					(rnw || '')
				;
			}

			if (feature.properties.lt != null){
				var lt_color = "";
				var lt_ratio = "0";

				var compound_time = feature.properties.lt_timestamp;
				var compound_color = lt_color;
				var compound_amount = parseFloat(feature.properties.lt);
				var compound_ratio = lt_ratio;
				//var compound_unit = feature.properties.lt_unit;
				var compound_unit = '° C';
				var compound_warning = '';
				var compound_longname = lang_lt_longname;
				var compound_shortname = lang_lt_shortname;
				var compound_code = lang_lt_code;

				//var compound_period= 'lt';
				var show_lt = show_compound_without_bar(compound_code, compound_time, compound_color, compound_amount, compound_unit, compound_warning, bar_width, compound_ratio);
				var show_compound = show_lt;
				var selector_lt = '<input type=checkbox id="1" checked onClick="change(this)" style="margin:0;padding:0;height:13px;vertical-align:bottom;"><label for="1">Lufttemperatur | </label>';
				var station_graph = show_graph(feature.properties.uid, feature.properties.category, compound_amount, compound_code, compound_period, g_container_w);
				var tabsHeaderCompound2 = showTabHeaderCompound(compound_code, compound_longname, compound_shortname);
				var tabsContentCompound2 = showTabContentCompound(compound_code, show_compound, station_graph);
				compounds.push('<span style="color:' + compound_color + ';">' + compound_longname + '</span>');
				compoundsSelectMenu.push('<option data-compound="' + compound_code + '">' + compound_longname + '</option>');
			}

			if (feature.properties.wt != null){
				var wt_color = "";
				var wt_ratio = "0";

				var compound_time = feature.properties.wt_timestamp;
				var compound_color = wt_color;
				var compound_amount = parseFloat(feature.properties.wt);
				var compound_ratio = wt_ratio;
				var compound_unit = feature.properties.wt_unit;
				var compound_unit = '° C';
				var compound_warning = '';
				var compound_longname = lang_wt_longname;
				var compound_shortname = lang_wt_shortname;
				var compound_code = lang_wt_code;
				var compound_period= 'wt';
				var show_wt = show_compound_without_bar(compound_code, compound_time, compound_color, compound_amount, compound_unit, compound_warning, bar_width, compound_ratio);
				var show_compound = show_wt;
				var selector_wt = '<input type=checkbox id="2" checked onClick="change(this)" style="margin:0;padding:0;height:13px;vertical-align:bottom;"><label for="2">Wassertemperatur | </label>';
				var station_graph = show_graph(feature.properties.uid, feature.properties.category, compound_amount, compound_code, compound_period, g_container_w);
				var tabsHeaderCompound3 = showTabHeaderCompound(compound_code, compound_longname, compound_shortname);
				var tabsContentCompound3 = showTabContentCompound(compound_code, show_compound, station_graph);
				compounds.push('<span style="color:' + compound_color + ';">' + compound_longname + '</span>');
				compoundsSelectMenu.push('<option data-compound="' + compound_code + '">' + compound_longname + '</option>');
			}

			if (feature.properties.q != null){
				var q_color = "";
				var q_ratio = "0";

				var compound_time = feature.properties.q_timestamp;
				var compound_color = q_color;
				var compound_amount = parseFloat(feature.properties.q);
				var compound_ratio = q_ratio;
				//var compound_unit = feature.properties.q_unit;
				var compound_unit = 'm³/s';
				var compound_warning = '';
				var compound_longname = lang_q_longname;
				var compound_shortname = lang_q_shortname;
				var compound_code = lang_q_code;
				var compound_period= 'q';
				var show_q = show_compound_without_bar(compound_code, compound_time, compound_color, compound_amount, compound_unit, compound_warning, bar_width, compound_ratio);
				var show_compound = show_q;
				var selector_q = '<input type=checkbox id="3" checked onClick="change(this)" style="margin:0;padding:0;height:13px;vertical-align:bottom;"><label for="3">Abfluss | </label>';
				var station_graph = show_graph(feature.properties.uid, feature.properties.category, compound_amount, compound_code, compound_period, g_container_w);
				var tabsHeaderCompound4 = showTabHeaderCompound(compound_code, compound_longname, compound_shortname);
				var tabsContentCompound4 = showTabContentCompound(compound_code, show_compound, station_graph);
				compounds.push('<span style="color:' + compound_color + ';">' + compound_longname + '</span>');
				compoundsSelectMenu.push('<option data-compound="' + compound_code + '">' + compound_longname + '</option>');
			}

			if (feature.properties.va != null){
				var va_color = "";
				var va_ratio = "0";

				var compound_time = feature.properties.va_timestamp;
				var compound_color = va_color;
				var compound_amount = parseFloat(feature.properties.va);
				var compound_ratio = va_ratio;
				var compound_unit = feature.properties.va_unit;
				var compound_unit = 'm/s';
				var compound_warning = '';
				var compound_longname = lang_va_longname;
				var compound_shortname = lang_va_shortname;
				var compound_code = lang_va_code;
				var compound_period= 'va';
				var show_va = show_compound_without_bar(compound_code, compound_time, compound_color, compound_amount, compound_unit, compound_warning, bar_width, compound_ratio);
				var show_compound = show_va;
				var selector_va = '<input type=checkbox id="4" checked onClick="change(this)" style="margin:0;padding:0;height:13px;vertical-align:bottom;"><label for="4">Fliessgeschwindigkeit | </label>';
				var station_graph = show_graph(feature.properties.uid, feature.properties.category, compound_amount, compound_code, compound_period, g_container_w);
				var tabsHeaderCompound5 = showTabHeaderCompound(compound_code, compound_longname, compound_shortname);
				var tabsContentCompound5 = showTabContentCompound(compound_code, show_compound, station_graph);
				compounds.push('<span style="color:' + compound_color + ';">' + compound_longname + '</span>');
				compoundsSelectMenu.push('<option data-compound="' + compound_code + '">' + compound_longname + '</option>');
			}

			if (feature.properties.dfh != null){
				var dfh_color = "";
				var dfh_ratio = "0";

				var compound_time = feature.properties.dfh_timestamp;
				var compound_color = dfh_color;
				var compound_amount = parseFloat(feature.properties.dfh);
				var compound_ratio = dfh_ratio;
				//var compound_unit = feature.properties.dfh_unit;
				var compound_unit = 'cm';
				var compound_warning = '';
				var compound_longname = lang_dfh_longname;
				var compound_shortname = lang_dfh_shortname;
				var compound_code = lang_dfh_code;
				var compound_period= 'dfh';
				var show_dfh = show_compound_without_bar(compound_code, compound_time, compound_color, compound_amount, compound_unit, compound_warning, bar_width, compound_ratio);
				var show_compound = show_dfh;
				var selector_dfh = '<input type=checkbox id="5" checked onClick="change(this)" style="margin:0;padding:0;height:13px;vertical-align:bottom;"><label for="5">Durchfahrtsh&ouml;he | </label>';
				var station_graph = show_graph(feature.properties.uid, feature.properties.category, compound_amount, compound_code, compound_period, g_container_w);
				var tabsHeaderCompound6 = showTabHeaderCompound(compound_code, compound_longname, compound_shortname);
				var tabsContentCompound6 = showTabContentCompound(compound_code, show_compound, station_graph);
				compounds.push('<span style="color:' + compound_color + ';">' + compound_longname + '</span>');
				compoundsSelectMenu.push('<option data-compound="' + compound_code + '">' + compound_longname + '</option>');
			}

			if (feature.properties.lf != null){
				var lf_color = "";
				var lf_ratio = "0";

				var compound_time = feature.properties.lf_timestamp;
				var compound_color = lf_color;
				var compound_amount = parseFloat(feature.properties.lf);
				var compound_ratio = lf_ratio;
				//var compound_unit = feature.properties.lf_unit;
				var compound_unit = 'µS/cm';
				var compound_warning = '';
				var compound_longname = lang_lf_longname;
				var compound_shortname = lang_lf_shortname;
				var compound_code = lang_lf_code;
				var compound_period= 'lf';
				var show_lf = show_compound_without_bar(compound_code, compound_time, compound_color, compound_amount, compound_unit, compound_warning, bar_width, compound_ratio);
				var show_compound = show_lf;
				var selector_lf = '<input type=checkbox id="6" checked onClick="change(this)" style="margin:0;padding:0;height:13px;vertical-align:bottom;"><label for="6">Elektr. Leitf&auml;higkeit | </label>';
				var station_graph = show_graph(feature.properties.uid, feature.properties.category, compound_amount, compound_code, compound_period, g_container_w);
				var tabsHeaderCompound7 = showTabHeaderCompound(compound_code, compound_longname, compound_shortname);
				var tabsContentCompound7 = showTabContentCompound(compound_code, show_compound, station_graph);
				compounds.push('<span style="color:' + compound_color + ';">' + compound_longname + '</span>');
				compoundsSelectMenu.push('<option data-compound="' + compound_code + '">' + compound_longname + '</option>');
			}

			if (feature.properties.wg != null){
				var wg_color = "";
				var wg_ratio = "0";

				var compound_time = feature.properties.wg_timestamp;
				var compound_color = wg_color;
				var compound_amount = parseFloat(feature.properties.wg);
				var compound_ratio = wg_ratio;
				//var compound_unit = feature.properties.wg_unit;
				var compound_unit = 'm/s';
				var compound_warning = '';
				var compound_longname = lang_wg_longname;
				var compound_shortname = lang_wg_shortname;
				var compound_code = lang_wg_code;
				var compound_period= 'wg';
				var show_wg = show_compound_without_bar(compound_code, compound_time, compound_color, compound_amount, compound_unit, compound_warning, bar_width, compound_ratio);
				var show_compound = show_wg;
				var selector_wg = '<input type=checkbox id="7" checked onClick="change(this)" style="margin:0;padding:0;height:13px;vertical-align:bottom;"><label for="7">Windgeschwindigkeit | </label>';
				var station_graph = show_graph(feature.properties.uid, feature.properties.category, compound_amount, compound_code, compound_period, g_container_w);
				var tabsHeaderCompound8 = showTabHeaderCompound(compound_code, compound_longname, compound_shortname);
				var tabsContentCompound8 = showTabContentCompound(compound_code, show_compound, station_graph);
				compounds.push('<span style="color:' + compound_color + ';">' + compound_longname + '</span>');
				compoundsSelectMenu.push('<option data-compound="' + compound_code + '">' + compound_longname + '</option>');
			}

			if (feature.properties.wr != null){
				var wr_color = "";
				var wr_ratio = "0";

				var compound_time = feature.properties.wr_timestamp;
				var compound_color = wr_color;
				var compound_amount = parseFloat(feature.properties.wr);
				var compound_ratio = wr_ratio;
				//var compound_unit = feature.properties.wr_unit;
				var compound_unit = '°';
				var compound_warning = '';
				var compound_longname = lang_wr_longname;
				var compound_shortname = lang_wr_shortname;
				var compound_code = lang_wr_code;
				var compound_period= 'wr';
				var show_wr = show_compound_without_bar(compound_code, compound_time, compound_color, compound_amount, compound_unit, compound_warning, bar_width, compound_ratio);
				var show_compound = show_wr;
				var selector_wr = '<input type=checkbox id="8" checked onClick="change(this)" style="margin:0;padding:0;height:13px;vertical-align:bottom;"><label for="8">Windrichtung | </label>';
				var station_graph = show_graph(feature.properties.uid, feature.properties.category, compound_amount, compound_code, compound_period, g_container_w);
				var tabsHeaderCompound9 = showTabHeaderCompound(compound_code, compound_longname, compound_shortname);
				var tabsContentCompound9 = showTabContentCompound(compound_code, show_compound, station_graph);
				compounds.push('<span style="color:' + compound_color + ';">' + compound_longname + '</span>');
				compoundsSelectMenu.push('<option data-compound="' + compound_code + '">' + compound_longname + '</option>');
			}

			if (feature.properties.gru != null){
				var gru_color = "";
				var gru_ratio = "0";

				var compound_time = feature.properties.gru_timestamp;
				var compound_color = gru_color;
				var compound_amount = parseFloat(feature.properties.gru);
				var compound_ratio = gru_ratio;
				//var compound_unit = feature.properties.gru_unit;
				var compound_unit = 'm+NHN';
				var compound_warning = '';
				var compound_longname = lang_gru_longname;
				var compound_shortname = lang_gru_shortname;
				var compound_code = lang_gru_code;
				var compound_period= 'gru';
				var show_gru = show_compound_without_bar(compound_code, compound_time, compound_color, compound_amount, compound_unit, compound_warning, bar_width, compound_ratio);
				var show_compound = show_gru;
				var selector_gru = '<input type=checkbox id="9" checked onClick="change(this)" style="margin:0;padding:0;height:13px;vertical-align:bottom;"><label for="9">Grundwasser | </label>';
				var station_graph = show_graph(feature.properties.uid, feature.properties.category, compound_amount, compound_code, compound_period, g_container_w);
				var tabsHeaderCompound10 = showTabHeaderCompound(compound_code, compound_longname, compound_shortname);
				var tabsContentCompound10 = showTabContentCompound(compound_code, show_compound, station_graph);
				compounds.push('<span style="color:' + compound_color + ';">' + compound_longname + '</span>');
				compoundsSelectMenu.push('<option data-compound="' + compound_code + '">' + compound_longname + '</option>');
			}

			if (feature.properties.hl != null){
				var hl_color = "";
				var hl_ratio = "0";

				var compound_time = feature.properties.hl_timestamp;
				var compound_color = hl_color;
				var compound_amount = parseFloat(feature.properties.hl);
				var compound_ratio = hl_ratio;
				//var compound_unit = feature.properties.hl_unit;
				var compound_unit = '%';
				var compound_warning = '';
				var compound_longname = lang_hl_longname;
				var compound_shortname = lang_hl_shortname;
				var compound_code = lang_hl_code;
				var compound_period= 'hl';
				var show_hl = show_compound_without_bar(compound_code, compound_time, compound_color, compound_amount, compound_unit, compound_warning, bar_width, compound_ratio);
				var show_compound = show_hl;
				var selector_hl = '<input type=checkbox id="10" checked onClick="change(this)" style="margin:0;padding:0;height:13px;vertical-align:bottom;"><label for="10">Luftfeuchte | </label>';
				var station_graph = show_graph(feature.properties.uid, feature.properties.category, compound_amount, compound_code, compound_period, g_container_w);
				var tabsHeaderCompound11 = showTabHeaderCompound(compound_code, compound_longname, compound_shortname);
				var tabsContentCompound11 = showTabContentCompound(compound_code, show_compound, station_graph);
				compounds.push('<span style="color:' + compound_color + ';">' + compound_longname + '</span>');
				compoundsSelectMenu.push('<option data-compound="' + compound_code + '">' + compound_longname + '</option>');
			}

			if (feature.properties.pl != null){
				var pl_color = "";
				var pl_ratio = "0";

				var compound_time = feature.properties.pl_timestamp;
				var compound_color = pl_color;
				var compound_amount = parseFloat(feature.properties.pl);
				var compound_ratio = pl_ratio;
				//var compound_unit = feature.properties.pl_unit;
				var compound_unit = 'hPa';
				var compound_warning = '';
				var compound_longname = lang_pl_longname;
				var compound_shortname = lang_pl_shortname;
				var compound_code = lang_pl_code;
				var compound_period= 'pl';
				var show_pl = show_compound_without_bar(compound_code, compound_time, compound_color, compound_amount, compound_unit, compound_warning, bar_width, compound_ratio);
				var show_compound = show_pl;
				var selector_pl = '<input type=checkbox id="11" checked onClick="change(this)" style="margin:0;padding:0;height:13px;vertical-align:bottom;"><label for="11">Luftdruck | </label>';
				var station_graph = show_graph(feature.properties.uid, feature.properties.category, compound_amount, compound_code, compound_period, g_container_w);
				var tabsHeaderCompound12 = showTabHeaderCompound(compound_code, compound_longname, compound_shortname);
				var tabsContentCompound12 = showTabContentCompound(compound_code, show_compound, station_graph);
				compounds.push('<span style="color:' + compound_color + ';">' + compound_longname + '</span>');
				compoundsSelectMenu.push('<option data-compound="' + compound_code + '">' + compound_longname + '</option>');
			}

			if (feature.properties.n != null){
				var n_color = "";
				var n_ratio = "0";

				var compound_time = feature.properties.n_timestamp;
				var compound_color = n_color;
				var compound_amount = parseFloat(feature.properties.n);
				var compound_ratio = n_ratio;
				var compound_unit = feature.properties.n_unit;
				var compound_warning = '';
				var compound_longname = lang_n_longname;
				var compound_shortname = lang_n_shortname;
				var compound_code = lang_n_code;
				var compound_period= 'n';
				var show_n = show_compound_without_bar(compound_code, compound_time, compound_color, compound_amount, compound_unit, compound_warning, bar_width, compound_ratio);
				var show_compound = show_n;
				var selector_n = '<input type=checkbox id="12" checked onClick="change(this)" style="margin:0;padding:0;height:13px;vertical-align:bottom;"><label for="12">Niederschlag | </label>';
				var station_graph = show_graph(feature.properties.uid, feature.properties.category, compound_amount, compound_code, compound_period, g_container_w);
				var tabsHeaderCompound13 = showTabHeaderCompound(compound_code, compound_longname, compound_shortname);
				var tabsContentCompound13 = showTabContentCompound(compound_code, show_compound, station_graph);
				compounds.push('<span style="color:' + compound_color + ';">' + compound_longname + '</span>');
				compoundsSelectMenu.push('<option data-compound="' + compound_code + '">' + compound_longname + '</option>');
			}

			if (feature.properties.n_intensity != null){
				var n_intensity_color = "";
				var n_intensity_ratio = "0";

				var compound_time = feature.properties.n_intensity_timestamp;
				var compound_color = n_intensity_color;
				var compound_amount = parseFloat(feature.properties.n_intensity);
				var compound_ratio = n_intensity_ratio;
				var compound_unit = feature.properties.n_intensity_unit;
				var compound_warning = '';
				var compound_longname = lang_n_intensity_longname;
				var compound_shortname = lang_n_intensity_shortname;
				var compound_code = lang_n_intensity_code;
				var compound_period= 'n_intensity';
				var show_n_intensity = show_compound_without_bar(compound_code, compound_time, compound_color, compound_amount, compound_unit, compound_warning, bar_width, compound_ratio);
				var show_compound = show_n_intensity;
				var selector_n_intensity = '<input type=checkbox id="13" checked onClick="change(this)" style="margin:0;padding:0;height:13px;vertical-align:bottom;"><label for="12">Niederschlag | </label>';
				var station_graph = show_graph(feature.properties.uid, feature.properties.category, compound_amount, compound_code, compound_period, g_container_w);
				var tabsHeaderCompound14 = showTabHeaderCompound(compound_code, compound_longname, compound_shortname);
				var tabsContentCompound14 = showTabContentCompound(compound_code, show_compound, station_graph);
				compounds.push('<span style="color:' + compound_color + ';">' + compound_longname + '</span>');
				compoundsSelectMenu.push('<option data-compound="' + compound_code + '">' + compound_longname + '</option>');
			}

			if (feature.properties.o2 != null){
				var o2_color = "";
				var o2_ratio = "0";

				var compound_time = feature.properties.o2_timestamp;
				var compound_color = o2_color;
				var compound_amount = parseFloat(feature.properties.o2);
				var compound_ratio = o2_ratio;
				//var compound_unit = feature.properties.o2_unit;
				var compound_unit = 'mg/l';
				var compound_warning = '';
				var compound_longname = lang_o2_longname;
				var compound_shortname = lang_o2_shortname;
				var compound_code = lang_o2_code;
				var compound_period= 'o2';
				var show_o2 = show_compound_without_bar(compound_code, compound_time, compound_color, compound_amount, compound_unit, compound_warning, bar_width, compound_ratio);
				var show_compound = show_o2;
				var selector_o2 = '<input type=checkbox id="12" checked onClick="change(this)" style="margin:0;padding:0;height:13px;vertical-align:bottom;"><label for="12">Niederschlag | </label>';
				var station_graph = show_graph(feature.properties.uid, feature.properties.category, compound_amount, compound_code, compound_period, g_container_w);
				var tabsHeaderCompound14 = showTabHeaderCompound(compound_code, compound_longname, compound_shortname);
				var tabsContentCompound14 = showTabContentCompound(compound_code, show_compound, station_graph);
				compounds.push('<span style="color:' + compound_color + ';">' + compound_longname + '</span>');
				compoundsSelectMenu.push('<option data-compound="' + compound_code + '">' + compound_longname + '</option>');
			}

			if (feature.properties.ph != null){
				var ph_color = "";
				var ph_ratio = "0";

				var compound_time = feature.properties.ph_timestamp;
				var compound_color = ph_color;
				var compound_amount = parseFloat(feature.properties.ph);
				var compound_ratio = ph_ratio;
				//var compound_unit = feature.properties.ph_unit;
				var compound_unit = '';
				var compound_warning = '';
				var compound_longname = lang_ph_longname;
				var compound_shortname = lang_ph_shortname;
				var compound_code = lang_ph_code;
				var compound_period= 'ph';
				var show_ph = show_compound_without_bar(compound_code, compound_time, compound_color, compound_amount, compound_unit, compound_warning, bar_width, compound_ratio);
				var show_compound = show_ph;
				var selector_ph = '<input type=checkbox id="12" checked onClick="change(this)" style="margin:0;padding:0;height:13px;vertical-align:bottom;"><label for="12">Niederschlag | </label>';
				var station_graph = show_graph(feature.properties.uid, feature.properties.category, compound_amount, compound_code, compound_period, g_container_w);
				var tabsHeaderCompound15 = showTabHeaderCompound(compound_code, compound_longname, compound_shortname);
				var tabsContentCompound15 = showTabContentCompound(compound_code, show_compound, station_graph);
				compounds.push('<span style="color:' + compound_color + ';">' + compound_longname + '</span>');
				compoundsSelectMenu.push('<option data-compound="' + compound_code + '">' + compound_longname + '</option>');
			}

			if (feature.properties.cl != null){
				var cl_color = "";
				var cl_ratio = "0";

				var compound_time = feature.properties.cl_timestamp;
				var compound_color = cl_color;
				var compound_amount = parseFloat(feature.properties.cl);
				var compound_ratio = cl_ratio;
				//var compound_unit = feature.properties.cl_unit;
				var compound_unit = '';
				var compound_warning = '';
				var compound_longname = lang_cl_longname;
				var compound_shortname = lang_cl_shortname;
				var compound_code = lang_cl_code;
				var compound_period= 'cl';
				var show_cl = show_compound_without_bar(compound_code, compound_time, compound_color, compound_amount, compound_unit, compound_warning, bar_width, compound_ratio);
				var show_compound = show_cl;
				var selector_cl = '<input type=checkbox id="12" checked onClick="change(this)" style="margin:0;padding:0;height:13px;vertical-align:bottom;"><label for="16">Chlorid | </label>';
				var station_graph = show_graph(feature.properties.uid, feature.properties.category, compound_amount, compound_code, compound_period, g_container_w);
				var tabsHeaderCompound16 = showTabHeaderCompound(compound_code, compound_longname, compound_shortname);
				var tabsContentCompound16 = showTabContentCompound(compound_code, show_compound, station_graph);
				compounds.push('<span style="color:' + compound_color + ';">' + compound_longname + '</span>');
				compoundsSelectMenu.push('<option data-compound="' + compound_code + '">' + compound_longname + '</option>');
			}

			if (feature.properties.orp != null){
				var orp_color = "";
				var orp_ratio = "0";

				var compound_time = feature.properties.orp_timestamp;
				var compound_color = orp_color;
				var compound_amount = parseFloat(feature.properties.orp);
				var compound_ratio = orp_ratio;
				//var compound_unit = feature.properties.orp_unit;
				var compound_unit = '';
				var compound_warning = '';
				var compound_longname = lang_orp_longname;
				var compound_shortname = lang_orp_shortname;
				var compound_code = lang_orp_code;
				var compound_period= 'orp';
				var show_orp = show_compound_without_bar(compound_code, compound_time, compound_color, compound_amount, compound_unit, compound_warning, bar_width, compound_ratio);
				var show_compound = show_orp;
				var selector_orp = '<input type=checkbox id="12" checked onClick="change(this)" style="margin:0;padding:0;height:13px;vertical-align:bottom;"><label for="17">Redoxspannung | </label>';
				var station_graph = show_graph(feature.properties.uid, feature.properties.category, compound_amount, compound_code, compound_period, g_container_w);
				var tabsHeaderCompound17 = showTabHeaderCompound(compound_code, compound_longname, compound_shortname);
				var tabsContentCompound17 = showTabContentCompound(compound_code, show_compound, station_graph);
				compounds.push('<span style="color:' + compound_color + ';">' + compound_longname + '</span>');
				compoundsSelectMenu.push('<option data-compound="' + compound_code + '">' + compound_longname + '</option>');
			}

			var graphSelector = (selector_w || "") +
						(selector_lt || "") +
						(selector_wt || "") +
						(selector_q || "") +
						(selector_va || "") +
						(selector_dfh || "") +
						(selector_lf || "") +
						(selector_wg || "") +
						(selector_wr || "") +
						(selector_gru || "") +
						(selector_hl || "") +
						(selector_pl || "") +
						(selector_n || "") +
						(selector_n_intensity || "") +
						(selector_o2 || "") +
						(selector_ph || "") +
						(selector_cl || "") +
						(selector_orp || "")
			;

		} else if (feature.properties.category == "bw"){

			if (feature.properties.bw_quality != null){

				var compound_time = feature.properties.sensor_time;
				//var compound_color = bw_quality_color;
				var compound_amount = parseFloat(feature.properties.bw_quality);
				var compound_unit = '';
				var compound_warning = '4';
				var compound_longname = 'Gew&auml;sserqualit&auml;t';
				var compound_shortname = '';
				var compound_code = 'bw_quality';
				var compound_period= 'bw_quality';
				var compound_ratio = show_ratio(bar_width, compound_warning, compound_amount);
				var compound_color = show_color(feature,compound_code,compound_period,compound_amount,compound_warning);


				var show_bw_quality = show_compound_with_bar(compound_code, compound_time, compound_color, compound_amount, compound_unit, compound_warning, bar_width, compound_ratio);
				var show_compound = show_bw_quality;


				var selector_bw_quality = '<input type=checkbox id="0" checked onClick="change(this)" style="margin:0;padding:0;height:13px;vertical-align:bottom;"><label for="0">Gew&auml;sserqualit&auml;t | </label>';
				var station_graph = show_graph(feature.properties.uid, feature.properties.category, compound_amount, compound_code, compound_period, g_container_w);
				var tabsHeaderCompound1 = showTabHeaderCompound(compound_code, compound_longname, compound_shortname);
				var tabsContentCompound1 = showTabContentCompound(compound_code, show_compound, station_graph);
				compounds.push('<span style="color:' + compound_color + ';">' + compound_longname + '</span>');
				compoundsSelectMenu.push('<option data-compound="' + compound_code + '">' + compound_longname + '</option>');
			};

			var graphSelector = (selector_bw_quality || "")
			;

		} else if (feature.properties.category == "eprtr"){

			if (feature.properties.eprtr != null){

				var compound_time = feature.properties.time;
				var compound_color = 'black';
				var compound_amount = parseFloat(feature.properties.eprtr);
				var compound_ratio = '0';
				var compound_unit = '';
				var compound_warning = '';
				var compound_longname = 'EPRTR';
				var compound_shortname = '';
				var compound_code = 'eprtr';
				var compound_period= 'eprtr';
				var show_eprtr = show_compound_with_bar(compound_code, compound_time, compound_color, compound_amount, compound_unit, compound_warning, bar_width, compound_ratio);
				var show_compound = show_eprtr;
				var selector_eprtr = '<input type=checkbox id="0" checked onClick="change(this)" style="margin:0;padding:0;height:13px;vertical-align:bottom;"><label for="0">EPRTR | </label>';
				var station_graph = show_graph(feature.properties.uid, feature.properties.category, compound_amount, compound_code, compound_period, g_container_w);
				var tabsHeaderCompound1 = showTabHeaderCompound(compound_code, compound_longname, compound_shortname);
				var tabsContentCompound1 = showTabContentCompound(compound_code, show_compound, station_graph);
				compounds.push('<span style="color:' + compound_color + ';">' + compound_longname + '</span>');
				compoundsSelectMenu.push('<option data-compound="' + compound_code + '">' + compound_longname + '</option>');
			};

			var graphSelector = (selector_eprtr || "")
			;

		} else if (feature.properties.category == "energy"){

		}

		layer.setIcon(new L.Icon({
			iconUrl: "/img/" + feature.properties.category + ".png",
			iconSize: [32, 37],
			iconAnchor: [16, 37],
		}));


		function show_compound_with_bar(compound_code, compound_time, compound_color, compound_amount, compound_unit, compound_warning, bar_width, compound_ratio){
			//return function(input){
				return '<div  id="box_' + compound_code + '_' + compound_period + '"><div class="iw_block"><div class="iw_header"><div class="left50"><i class="icon-clock"></i><span class="time_' + compound_code + '"> ' + compound_time + '</span></div><div class="right50"><div class="periodSelect"><select id="periodSelect_' + compound_code + '"></select></div></div><div style="clear:both;"></div></div>' +
				'<div class="iw_line"><div class="left50"><font class="amount_' + compound_code + '" style="font-size:200%;color:' + compound_color + '">' + compound_amount + '</font> ' + compound_unit + '</div><div class="smaller" style="float:right; width:50%; margin-top:15px; text-align:right;">[Grenzwert: ' + compound_warning + ' ' + compound_unit +']</div><div style="clear:both;"></div></div><div style="background:rgb(169,169,169);"><div class="bar_graph" style="max-width:' + bar_width + 'px; width:' + compound_ratio + 'px;"></div></div></div></div>';
			//}
		}

		function show_compound_without_bar(compound_code, compound_time, compound_color, compound_amount, compound_unit, compound_warning, compound_ratio){
			return '<div class="iw_block"><div class="iw_header"><div class="left50"><i class="icon-clock"></i><span class="time_' + compound_code + '"> ' + compound_time + '</span></div><div class="right50"><div class="periodSelect"><select id="periodSelect_' + compound_code + '"></select></div></div><div style="clear:both;"></div></div>' +
			'<div class="iw_line"><div class="left50"><font class="amount_' + compound_code + '" style="font-size:200%;color:' + compound_color + '">' + compound_amount + '</font> ' + compound_unit + '</div><div class="smaller" style="float:right; width:50%; margin-top:15px; text-align:right;">[Grenzwert: '+ compound_warning + ' ' + compound_unit +']</div><div style="clear:both;"></div></div></div>';
		}

		function show_limit_selector(uid, category, compound_amount, compound_code, compound_period, g_container_w){
				//return '<span class="limit_active limit limit_7d" data-limit="7" data-uid="' + uid + '" data-category="' + category + '" data-compound="' + compound_code + '">Woche</span> / <span class="limit_default limit limit_30d" data-limit="30" data-uid="' + uid + '" data-category="' + category + '" data-compound="' + compound_code + '">Monat</span> / <span class="limit_default limit limit_365d" data-limit="365" data-uid="' + uid + '" data-category="' + category + '" data-compound="' + compound_code + '">Jahr</span>';
				return '<div class="limitSelect"><select id="limitSelect_' + compound_code + '"><option data-limit="7" data-uid="' + uid + '" data-category="' + category + '" data-compound="' + compound_code + '">' + lang_week + '</option><option data-limit="30" data-uid="' + uid + '" data-category="' + category + '" data-compound="' + compound_code + '">' + lang_month + '</option><option data-limit="365" data-uid="' + uid + '" data-category="' + category + '" data-compound="' + compound_code + '">' + lang_year + '</option></select></div>';
		}

		function show_graph(uid, category, compound_amount, compound_code, compound_period, g_container_w){
			return 	'<div style="clear:both;"></div>' +
					'<div class="iw_box" >' +
						'<div class="iw_block" style="margin-bottom:0;">' +
							'<div class="iw_header"><div class="left50"><i class="icon-chart-line"></i> Verlauf</div><div class="right50">' + show_limit_selector(uid, category, compound_amount, compound_code, compound_period, g_container_w) + '</div><div style="clear:both;"></div></div>' +
							'<div id="spinner' + compound_code + '" class="spinner_graph"><img src="/img/loading22.gif" alt="Loading gif" /></div>' +
							'<div id="g_container' + compound_code + '" class="g_container" style="width:' + g_container_w + '">' +
								'<div style="position:relative;">' +
									'<div id="g_labels_' + compound_code + '" class="g_labels" ></div>' +
									'<div id="graph_' + compound_code + '" class="graph" style="width:' + g_container_w + '"></div>' +
									//'<div id="g_selector" class="g_selector">' +
									//	"Zeige: " +
									//	graphSelector +
									//	"" +
									//'</div>' +
								'</div>' +
							'</div>' +
						'</div>' +
					'</div>' +
					'<div style="clear:both;"></div>';
		}

		function show_ratio(bar_width, compound_warning, compound_amount){
			return ( bar_width / ( compound_warning / compound_amount ) );
		}

		function showTabHeaderCompound(compound_code, compound_longname, compound_shortname){
			return '<li><a href="#tabsContentCompound' + compound_code + '">' + compound_longname + '</a></li>';
		}

		function showTabContentCompound(compound_code, show_compound, station_graph){
			return '<div id="tabsContentCompound' + compound_code + '" data-compound="' + compound_code + '">' + show_compound + station_graph + '</div>';
		}

		// Check which station info variables are set / not null
		if (feature.properties.altitude != null){
			var show_altitude = '(' + feature.properties.altitude + lang_altitude + ')';
		}

		if (feature.properties.start_date != null){
			var show_start_date = '<tr><td class="tdL">' + lang_start_date + ': </td><td class="tdR">' + feature.properties.start_date + '</td></tr>';
		}

		if (feature.properties.type != null){
			var airq_type = '<tr><td class="tdL">' + lang_station_type + ': </td><td class="tdR">' + feature.properties.type + '</td></tr>';
		}

		if (feature.properties.area != null){
			var airq_area = '<tr><td class="tdL">' + lang_surrounding_area + ': </td><td class="tdR">' + feature.properties.area_subcat + '</td></tr>';
		}

		if ((feature.properties.ozone_class != null) && (feature.properties.ozone_class != 'unknown')){
			var airq_ozone_class = '<tr><td class="tdL">' + lang_ozone_class + ': </td><td class="tdR">' + feature.properties.ozone_class + '</td></tr>';
		}

		if ((feature.properties.water_longname != null) && (feature.properties.water_longname != 'unknown')){
			var waterways_name = '<tr><td class="tdL">' + lang_water_longname + ': </td><td class="tdR">' + feature.properties.water_longname + ' (' + feature.properties.km + ' km)</td></tr>';
		}

		if ((feature.properties.km != null) && (feature.properties.km != 'unknown')){
			var waterways_km = '<tr><td class="tdL">' + lang_km + ': </td><td class="tdR">' + feature.properties.km + '</td></tr>';
		}

		if (feature.properties.uuid != null){
			var uuid = '<tr><td class="tdL">UUID: </td><td class="tdR">' + feature.properties.uuid + '</td></tr>';
		}

		if (feature.properties.uid != null){
			var uid = '<tr><td class="tdL">' + lang_original_uid + ': </td><td class="tdR">' + feature.properties.uid + '</td></tr>';
		}

		if (feature.properties.uid_local != null){
			var uid_local = '<tr><td class="tdL">' + lang_uid_local + ': </td><td class="tdR">' + feature.properties.uid_local + '</td></tr>';
		}

		if (feature.properties.country != null){
			var country = '<tr><td class="tdL">' + lang_country + ': </td><td class="tdR">' + feature.properties.country + '</td></tr>';
		}

		if (feature.properties.state != null){
			var state = '<tr><td class="tdL">' + lang_state + ': </td><td class="tdR">' + feature.properties.state + '</td></tr>';
		}

		if (feature.properties.city != null){
			var city = '<tr><td class="tdL">' + lang_city + ': </td><td class="tdR">' + feature.properties.city + '</td></tr>';
		}

		if (feature.properties.street != null){
			var street = '<tr><td class="tdL">' + lang_street + ': </td><td class="tdR">' + feature.properties.street + '</td></tr>';
		}

		if (feature.properties.agency_link != null){
			var agency_link = '<a href="' + feature.properties.agency_link + '" target="_blank"></a>';
		}

		if (feature.properties.agency != null){
			var agency = '<tr><td class="tdL">' + lang_agency + ': </td><td class="tdR">' + (agency_link || "") + feature.properties.agency + '</td></tr>';
		}

		if (feature.properties.provider_link != null){
			var provider_link = '<a href="' + feature.properties.provider_link + '" target="_blank"</a>';
		}

		if (feature.properties.provider != null){
			var provider = '<tr><td class="tdL">' + lang_provider + ': </td><td class="tdR">' + (provider_link || "") + feature.properties.provider + '</td></tr>';
		}

		if (feature.properties.last_update != null){
			var last_update = '<tr><td class="tdL">' + lang_last_update + ': </td><td class="tdR">' + feature.properties.last_update + '</td></tr>';
		}

		//if (debug == true) console.log('compoundsSelectMenu.length: ' + compoundsSelectMenu.length)

		popupNEW = L.popup();
		popupNEW.setContent(
			'<div class="iwindow link">' +

				'<div class="boxHeader">' +
					'<h3>' + feature.properties.original_name + '</h3>' +
					//'<div>(Entfernung: ' + feature.properties.distance + ' km)</div>' +
					'<button id="closePopup" class="boxButton"><i class="icon-cancel"></i></button>' +
				'</div>' +

				'<div class="popUpSelectMenu">' +

					'<div id="compoundSelectOuter" class="marginTR">' +
						'<select id="compoundSelect" class="selectMenu">' + compoundsSelectMenu.join('') + '</select>' +
					'</div>' +

					'<div id="periodSelectOuter" class="marginTR">' +
						'<select id="periodSelect" class="selectMenu"></select>' +
					'</div>' +

					'<div id="limitSelectOuter" class="marginTR">' +
						'<select id="limitSelect" class="selectMenu">' +
							'<option data-limit="7">' + lang_week + '</option>' +
							'<option data-limit="30">' + lang_month + '</option>' +
							'<option data-limit="365">' + lang_year + '</option>' +
						'</select>' +
					'</div>' +

				'</div>' +

				'<div id="tabs-popup" class="popupContent">' +

					//'<ul id="tabsPopupUL">' +
					//	'<li><a href="#station_data_neu" title="Aktuelle Messwerte"><i class="icon-chart-line"></i></a></li>' +
					//	'<li><a href="#stationInfo" title="Über diese Messstation"><i class="icon-info"></i></a></li>' +
					//'</ul>' +

					//'<div id="stationInfo">' +
					//	station_info +
					//'</div>' +

					'<div id="station_data_neu">' +

					'<div>' +

						'<div id="currentDataOuter">' +

							'<div id="currentRawDataOuter" class="round border marginBR">' +

								'<h4><span class="compound_longname">' + compound_longname + '</span> (<span class="compound_period">' + compound_period +'</span>)</h4>' +

								'<div style="background:#ffffff;padding:10px;">' +

									'<table class="current_data_table" style="width:100%;">' +
										'<tr>' +
											'<td class="tdL">' + lang_last_update + ': </td><td class="tdR"><span class="time"></span></td>' +
										'</tr>' +
										'<tr>' +
											'<td class="tdL">' + lang_current_value + ': </td><td class="tdR"><span class="amount"></span> <span class="unit"></span></td>' +
										'</tr>' +
										//(show_aqi || '') +
										(odl_ter || '') +
										(odl_cos || '') +
										//'<tr><td>Trend: </td><td><span class="trend"></span></td></tr>' +
									'</table>' +
									'<table style="width:100%;" class="additional_measurements"></table>' +

								'</div>' +

							'</div>' +

							'<div style="flex:1; min-width:300px; margin:0 10px 10px 0; background:#ffffff;" class="round border">' +
								'<h4>' + lang_measurements_statistics + '</h4>' +
								'<div style="background:#ffffff;padding:10px;">' +
									'<table style="width:100%; border:none;" class="statistics">' +
										(radiation_statistics || '') +
										(waterways_statistics || '') +
										(show_trans_total || '') +
									'</table>' +
								'</div>' +
							'</div>' +



							//(show_aqi || "") +

							//(waterways_station_info || '') +


							'<div id="graphOuter" class="round border marginBR">' +

								'<h4>' + lang_timeseries + '</h4>' +

								'<div id="spinner" class="spinner_graph"><img src="/img/loading22.gif" alt="Loading gif" /></div>' +

								'<div id="g_container" class="g_container">' +

									'<div id="g_labels" class="g_labels" ></div>' +

									'<div id="graph" class="graph"></div>' +
									//'<div id="g_selector" class="g_selector">' +
									//	"Zeige: " +
									//	graphSelector +699px
									//	"" +
									//'</div>' +
								'</div>' +

							'</div>' +

						'</div>' +

					'</div>' +

				'</div>' +

				'<div id="stationInfoOuter" style="padding: 0 0 0 10px;">' +

					'<div id="stationMetaDataOuter" class="round border marginBR">' +
						'<h4>' + lang_station_metadata + '</h4>' +
						'<div style="background:#ffffff;padding:10px;">' +
							'<table style="width:100%; border:none;">' +

								'<tr><td class="tdL">' + lang_station_location + ': </td><td class="tdR">' + feature.properties.lat + " / " + feature.properties.lng + ' ' + (show_altitude || '') + '</td></tr>' +
								//(show_start_date || '') +
								(airq_type || '') +
								(airq_area || '') +
								(airq_ozone_class || '') +
								(waterways_name || '') +
								(waterways_km || '') +
								(w_gaugeZero_value || '') +
								(w_gaugeZero_validFrom || '') +
								//(uuid || '') +

								//(country || '') +
								//(state || '') +
								//(city || '') +
								//(street || '') +
								(agency || '') +
								(uid_local || '') +
								(provider || '') +
								'<tr><td class="tdL">' + lang_uid+ ': </td><td class="tdR">' + feature.properties.uid + '</td></tr>' +
								//(last_update || '') +

							'</table>' +
							'<table style="width:100%;" class="additional_station_info"></table>' +
						'</div>' +
					'</div>' +

					'<div id="stationAPIInfo" class="round border marginBR">' +
						'<h4>' + lang_links_and_api + '</h4>' +
						'<div style="background:#ffffff;padding:10px;">' +
							'<table style="width:100%;">' +
								'<tr><td class="tdL">Link (HTML): </td><td class="tdR"><a href="/' + lang + '/map/station/' + feature.properties.uuid + '" target="_blank">OUD ' + feature.properties.uuid + '</a></td></tr>' +
								'<tr><td class="tdL">API (GeoJSON): </td><td class="tdR"><a href="/api.php?key=12345&type=station&category=' + feature.properties.category + '&uuid=' + feature.properties.uuid + '&measurements=false" target="_blank">' + lang_station_without_current_data + '</a></td></tr>' +
								'<tr><td class="tdL">API (GeoJSON): </td><td class="tdR"><a href="/api.php?key=12345&type=station&category=' + feature.properties.category + '&uuid=' + feature.properties.uuid + '&measurements=true" target="_blank">' + lang_station_with_current_data + '</a></td></tr>' +
								'<tr><td class="tdL">Subscribe (RSS): </td><td class="tdR"><a type="application/rss+xml" href="/rss.php?uid=' + feature.properties.uid + '&amp;category=' + feature.properties.category + '&amp;get=warn" target="_blank">' + lang_rss_only_warnings + '</a> / <a type="application/rss+xml" href="/rss.php?uid=' + feature.properties.uid + '&amp;category=' + feature.properties.category + '&amp;get=all" target="_blank">' + lang_rss_full + '</a></td></tr>' +
							'</table>' +
						'</div>' +

					'</div>' +

					'<div style="clear:both;"></div>' +

				'</div>' +

			'</div>' +

		'</div>'
		)

		layer.bindPopup(popupNEW, {offset:new L.Point(0, -30), closeButton:false, autoPanPaddingTopLeft: [offsetLeft,offsetTop], autoPanPaddingBottomRight: [offsetRight,offsetBottom], minWidth: wmin, maxWidth: wmax, maxHeight: hmax});

	};

	// Check if the station has not been updated in a while
	dateWarningStation(feature);

	// Show warnings if there are any
	showWarnings(feature,layer,odl_warn);

	// Build the array for the resultListItems in the sidebar
	createResultListItemsArray(feature,distance,compounds);

	// Create options in the results select menu
	createOption(feature, distance, stationWarningColor);

	//createCompoundsOption(feature,compound_longname);

}

function periodsSelectMenuNEW(compound){
	// Populate the periodSelect menu with options

	//alert("periodsSelectMenu: " + compound);
	// Get the periods for this compound
	//compoundPeriods(compound);

	var compoud_periods_name = [];
	var compoud_periods = [];

	// Air-Quality
	if (compound === "co"){
		compound_periods=["8smw","8tmax"];
		compound_periods_name=[lang_8smw, lang_8tmax];
	} else if (compound === "no2"){
		compound_periods=["1smw","1tmax"];
		compound_periods_name=[lang_1smw, lang_1tmax];
		compound_periods_selected="1smw";
	} else if (compound === "o3"){
		compound_periods=["1smw","8smw","1tmax","8tmax"];
		compound_periods_name=[lang_1smw, lang_8smw, lang_1tmax, lang_8tmax];
		compound_periods_selected="8smw";
	} else if (compound === "pm10"){
		compound_periods=["1tmw"];
		compound_periods_name=[lang_1tmw];
		compound_periods_selected="1tmw";
	} else 	if (compound === "so2"){
		compound_periods=["1smw","1tmax","1tmw"];
		compound_periods_name=[lang_1smw, lang_1tmax, lang_1tmw];
		compound_periods_selected="1smw";

	// Waterways / Pegel
	} else 	if ((compound == "w") || (compound == "lt") || (compound == "wt") || (compound == "q") || (compound == "va") || (compound == "dfh") || (compound == "lf") || (compound == "wg") || (compound == "wr") || (compound == "gru") || (compound == "hl") || (compound == "pl") || (compound == "n") || (compound == "n_intensity") || (compound == "o2") || (compound == "ph") || (compound == "cl") || (compound == "orp")){
		compound_periods=["recent"];
		compound_periods_name=[lang_recent];
		compound_periods_selected="recent";
	// Catch-All
	} else {
		compound_periods=["1smw"];
		compound_periods_name=[lang_1smw];
		compound_periods_selected="1smw";
	}

	//alert(compound_periods_name);
	// Clear the select menu from previous options

	$('#periodSelect').empty();
	$('#periodSelect').removeAttr('disabled');
	document.getElementById('periodSelectOuter').style.removeProperty('background');
	$("#periodSelect").css('color','#2d2d2d');

	// Get array length, cycle through array to get periods and period names
	var arrayLength = compound_periods.length;
	for (var i = 0; i < arrayLength; i++){
		var period = compound_periods[i];
		var period_name = compound_periods_name[i];
		//alert(period + period_name);
		// Create the options
		var periodSelect = document.getElementById("periodSelect");

		option = document.createElement("option");
		option.value = period;
		//option.setAttribute("data-class", category );
		option.setAttribute("data-period", period);

		//option.setAttribute("style", "color:" + stationWarningColor + "");
		option.innerHTML = period_name;
		periodSelect.appendChild(option);
	}

	// If there is only one element to select, we disable and grey out the menu
	if (arrayLength < 2) {
		$('#periodSelect').prop('disabled', 'disabled');
		var bg = "url(/img/down_arrow.png) no-repeat 96% rgb(220,220,220)";
		$("#periodSelectOuter").css('background',bg);
		$("#periodSelect").css('color','rgb(150,150,150)');
	};

	$('#periodSelect option[value=' + compound_periods_selected + ']').prop('selected',true);
}

function tabsInit(uid,category){

	//alert(uid + category);
	// TABS-POPUP
	$( "#tabs-popup" ).tabs({
		activate: function(event, ui) {
			// Resize nicescroll
			$("#tabs-popup").getNiceScroll().resize();
			$("#tabs-popup").getNiceScroll().show();

		},
		heightStyle: "content"
	});
	$( ".tabs-min" ).tabs({
		create: function(event, ui){

			// Trigger a window resize so the tab content is shown properly
			//$(window).resize();

			var compound = $(this).children("[data-compound]:first").attr('data-compound');

			periodsSelectMenu(compound);
			var compound_period = $("#periodSelect_" + compound).children("[data-period]:first").attr('data-period');
			show_graph(uid,category,compound,compound_period,limit);
			onChangeGraphPeriod(uid,category,compound,compound_period);
			onChangeGraphLimit(uid,category,compound,compound_period,limit);

			// Get number of tabs for setting dynamic width
			//var numTabs = $('.tabs-min li').length;
			//var tabWidth = (wmin - (numTabs * 4))  / numTabs; // using 98 because sometimes using 100 will cause last element to get "bumped" down below the first element.
			//tabPX = tabWidth + "px";
			//alert("CREATED - uid: " + uid + " / category: " + category + " / compound: " + compound + " / compound_period: " + compound_period + " / limit: " + limit);
		},
		beforeLoad: function(event, ui){
			// before AJAX tabs are loaded
			//alert("before load");
			ui.jqXHR.fail(function(){
			  ui.panel.html(
				"Couldn't load this tab. We'll try to fix this as soon as possible. " +
				"If this wouldn't be a demo." );
			});
		},
		beforeActivate: function(event, ui){
			//alert("before activate");
			//
		},
		//heightStyle: "content"
		activate: function(event, ui){

			// Trigger a window resize so the tab content is shown properly
			//$(window).resize();

			// triggers after a non-AJAX tabs has loaded / after animation is complete
			var compound = ui.newPanel.attr('data-compound');
			//alert(compound);
			periodsSelectMenu(compound);
			var compound_period = $("#periodSelect_" + compound).children("[data-period]:first").attr('data-period');
			//alert(compound_period);
			updatePopup(uid,category,compound,compound_period);
			onChangeGraphPeriod(uid,category,compound,compound_period);
			onChangeGraphLimit(uid,category,compound,compound_period,limit);

		},
		load: function(event, ui){
			//alert("load");
			// triggers after a AJAX tabs loads
		},
		//show: { effect: "fade", duration: 250 },
		//hide: { effect: "fade", duration: 250 },
		heightStyle: "content"
	});

	//$('.tabs-min li').each(function (){
    //    $(this).css("width", tabPX);
    //    $(this).css("display", "inline-block");
    //});
};

function onChangeCompoundSelectMenu(uuid,uid,category,compound,compound_longname,compound_period){

	periodsSelectMenuNEW(compound);
	onChangeGraphPeriodNEW(uuid,uid,category,compound,compound_period);
	onChangeGraphLimitNEW(uuid,uid,category,compound,compound_period,limit);

	var compound_period = $("#periodSelect").children(":selected").attr("data-period");
	var compound_period_text = $('#periodSelect').children(":selected").text();
	var compound_longname = $("#compoundSelect").children(":selected").text();
	updatePopupNEW(uuid,uid,category,compound,compound_period,limit,compound_longname,compound_period_text);

	document.getElementById('compoundSelect').onchange = function(e){

		var compound = $(this).children(":selected").attr("data-compound");
		periodsSelectMenuNEW(compound);
		var limit = $("#limitSelect").children(":selected").attr("data-limit");
		var compound_period = $("#periodSelect").children(":selected").attr("data-period");
		var compound_period_text = $('#periodSelect').children(":selected").text();
		var compound_longname = $(this).children(":selected").text();
		updatePopupNEW(uuid,uid,category,compound,compound_period,limit,compound_longname,compound_period_text);
	};
}

function aqiGauge(aqi){
	$(".GaugeMeter").gaugeMeter().empty().remove();
	$('<div>', { class: 'GaugeMeter', style: 'margin:0 auto; height:130px;' }).appendTo('#aqiGauge');
	$('#aqiLegend').children().fadeTo( aqi.aqi * 2, 0.4);
	$('#aqiLegend').find('[data-aqi="' + aqi.aqic + '"]').fadeTo( aqi.aqi * 2 , 1);
	$('.GaugeMeter').attr('data-percent', '0');
	$('.GaugeMeter').attr('data-style', 'Semi');
	$('.GaugeMeter').attr('data-total', '500');
	$('.GaugeMeter').attr('data-used', aqi.aqi);
	$('.GaugeMeter').attr('data-text', aqi.aqi);
	$('.GaugeMeter').attr('data-append', '/500');
	$('.GaugeMeter').attr('data-size', '220');
	$('.GaugeMeter').attr('data-width', '10');
	$('.GaugeMeter').attr('data-color', aqi.aqic);
	//$('.GaugeMeter').attr('data-animate_gauge_colors', '1');
	$('.GaugeMeter').attr('data-animate_text_colors', '1');
	//$('.GaugeMeter').attr('data-label', aqi.aqiw);
	//$('.GaugeMeter').attr('data-append', aqi);

	$(".GaugeMeter").gaugeMeter();
}

function updatePopupNEW(uuid,uid,category,compound,compound_period,limit,compound_longname,compound_period_text){

	// Get values from JSON (FIXME: this is the 2nd time we are calling the json feed. unnecessary!)
	//searchURL = '/json.php?category=' + category + '&type=station&uid=' + uid + '&callback=handleGeoJSON';
	//var searchURL = '/data.php?type=station&category=' + category + '&uid=' + uid + '&measurements=true&callback=handleGeoJSON';
	var searchURL = '/data.php?type=station&uuid=' + uuid + '&measurements=true&callback=handleGeoJSON';

	//alert(searchURL);
	$.getJSON(searchURL, function(data){
		geojson = L.geoJson(data, {
			onEachFeature: function (feature, layer){
				var amount = parseFloat(feature.properties[compound + '_' + compound_period]);
				var time = feature.properties[compound + '_' + compound_period + '_time'];

				//var unit = feature.properties[compound + '_unit'];

				var compound_color = show_color(feature,compound,compound_period,amount,compound_warning);

				if (feature.properties.category == 'air_quality'){

					// To calculate AQI values, we need to convert most of our values (based on mass) to ppm or ppb
					// See: https://uk-air.defra.gov.uk/assets/documents/reports/cat06/0502160851_Conversion_Factors_Between_ppb_and.pdf

					// O3
					if (compound == "o3"){
						//var amount_tmp = amount / 1.9957;
						var compound_unit = 'µg/m³';
						var compound_warning = 120;
					};

					// NO2
					if (compound == "no2"){
						//var amount_tmp = amount / 1.9125;
						var compound_unit = 'µg/m³';
						var compound_warning = 200;
					};

					// PM10
					if (compound == "pm10"){
						//var amount_tmp = amount;
						var compound_unit = 'µg/m³';
						var compound_warning = 50;

					};

					// CO
					if (compound == "co"){
						//var amount_tmp = amount / 1164.2;
						var compound_unit = 'µg/m³';
						var compound_warning = 10000;
					};

					// SO2
					if (compound == "so2"){
						//var amount_tmp = amount / 2.6609;
						var compound_unit = 'µg/m³';
						var compound_warning = 350;
					};

					// Calculate AQI, but only if category is air_quality and period is either 1smw or 8smw
					if ((compound_period == '1smw') || (compound_period == '8smw') || (compound_period == '1tmw')) {

						var aqi = calculateAqi(compound, compound_period, amount);
						var aqiw = '<tr><td class="tdL">AQI: </td> <td class="tdR" style="color:' + compound_color + ';">' + aqi.aqiw + '</td></tr>';
						//aqiGauge(aqi);
						//show_aqi;

					} else {
						var aqi = '';
						var aqiw = '<tr><td class="tdL">AQI: </td> <td class="tdR">' + lang_aqi_n_a + '</td></tr>';
						var compound_colour = "#000000";
					}

				} else if (feature.properties.category == "radiation"){

					if (compound == "odl"){
						var odl_warn = (parseFloat(feature.properties.odl_avg_1smw) + 0.020);
						var odl_warn = odl_warn.toFixed(3);
						var compound_unit = 'µSv/h';
						var compound_warning = odl_warn;
						//var odl_avg = '<tr><td class="tdL">' + lang_average + ': </td><td class="tdR">' + feature.properties.odl_avg_1smw + ' ' + compound_unit + '</td></tr>';
						//var odl_warn = '<tr><td class="tdL">' + lang_threshold + ': </td><td class="tdR">' + odl_warn + ' ' + compound_unit + '</td></tr>';
						//var odl_ter = '<tr><td class="tdL">' + lang_terrestrial + ': </td><td class="tdR">' + feature.properties.odl_ter_1smw + ' ' + compound_unit + '</td></tr>';
						//var odl_cos = '<tr><td class="tdL">' + lang_cosmic + ': </td><td class="tdR">' + feature.properties.odl_cos_1smw + ' ' + compound_unit + '</td></tr>';
					};

				} else if (feature.properties.category == "weather"){

					if (compound == "pressure"){
						var compound_unit = 'hPa';
						var compound_warning = '';
					};

					if (compound == "temperature"){
						var compound_unit = '°C';
						var compound_warning = '';
					};

					if (compound == "precipitation"){
						var compound_unit = 'L/m²';
						var compound_warning = '';
					};

					if (compound == "wind_direction"){
					};

					if (compound == "wind_speed"){
					};

					if (compound == "wind_peak"){
					};

					if (compound == "conditions"){
						var compound_unit = '';
						var compound_warning = '';
					};

					if (compound == "squall"){
						var compound_unit = '';
						var compound_warning = '';
					};

				} else if (feature.properties.category == "pegel"){

					//alert(compound)
					var amount = parseFloat(feature.properties[compound]);
					var time = feature.properties[compound + '_timestamp'];
					var trend = feature.properties[compound + '_trend'];

					if (compound == "w"){
						var compound_unit = 'cm';
						var compound_warning = '';

						var w_stateMnwMhw = '<tr><td class="tdL">' + lang_mnw_mhw + ' <!-- <i class="icon-help-circled help-tooltip" title="' + lang_mnw_mhw + '"></i> --> </td><td class="tdR" style="color:' + compound_color + ';">' + feature.properties.w_stateMnwMhw + '</td></tr>';
						if (feature.properties.w_stateNswHsw != 'unknown'){
							var w_stateNswHsw = '<tr><td class="tdL">' + lang_nsw_hsw + ': </td><td class="tdR" style="color:' + compound_color + ';">' + feature.properties.w_stateNswHsw + '</td></tr>';
						}

					}

					if (compound == "lt"){
						var compound_unit = '° C';
						var compound_warning = '';
					}

					if (compound == "wt"){
						var compound_unit = '° C';
						var compound_warning = '';
					}

					if (compound == "q"){
						var compound_unit = 'm³/s';
						var compound_warning = '';
					}

					if (compound == "va"){
						var compound_unit = 'm/s';
						var compound_warning = '';
					}

					if (compound == "dfh"){
						var compound_unit = 'cm';
						var compound_warning = '';
					}

					if (compound == "lf"){
						var compound_unit = 'µS/cm';
						var compound_warning = '';
					}

					if (compound == "wg"){
						var compound_unit = 'm/s';
						var compound_warning = '';
					}

					if (compound == "wr"){
						var compound_unit = '°';
						var compound_warning = '';
					}

					if (compound == "gru"){
						var compound_unit = 'm+NHN';
						var compound_warning = '';
					}

					if (compound == "hl"){
						var compound_unit = '%';
						var compound_warning = '';
					}

					if (compound == "pl"){
						var compound_unit = 'hPa';
						var compound_warning = '';
					}

					if (compound == "n"){
						var compound_unit = 'mm';
						var compound_warning = '';
					}

					if (compound == "n_intensity"){
						var compound_unit = 'mm/h';
						var compound_warning = '';
					}

					if (compound == "o2"){
						var compound_unit = 'mg/l';
						var compound_warning = '';
					}

					if (compound == "ph"){
						var compound_unit = '';
						var compound_warning = '';
					}

					if (compound == "cl"){
						var compound_unit = 'mg/l';
						var compound_warning = '';
					}

					if (compound == "orp"){
						var compound_unit = 'mV';
						var compound_warning = '';
					}

				} else if (feature.properties.category == "bw"){

					if (compound == "bw_quality"){

						var compound_unit = '';
						var compound_warning = '4';
					};

				} else if (feature.properties.category == "eprtr"){

					if (compound == "eprtr"){

						var compound_unit = '';
						var compound_warning = '';
					};

				} else if (feature.properties.category == "energy"){

				}

				// Set the values

				if ((compound_period == '1tmw') || (compound_period == '1tmax')) {
					var localTime = userTime(time,userTimezone,'dayly');

				} else {
					var localTime = userTime(time,userTimezone,'hourly');
				}

				$('.time').html(' ' + localTime );
				if (debug == true) console.log('setting time according to user timezone: ' + localTime + 'period: ' + compound_period);

				//compound_warning=10;

				//$('.amount_' + compound).css('color', compound_color);
				$('.amount').html(amount);
				//$('.amount').css('color', compound_color);

				$('.unit').html(compound_unit);
				//$('.unit').css('color', compound_color);

				//$('.trend').html(trend);
				$('.compound_longname').html(compound_longname);
				$('.compound_period').html(compound_period_text);

				$('.additional_measurements').html(
					//(odl_ter || '') +
					//(odl_cos || '') +
					(w_stateMnwMhw || '') +
					(w_stateNswHsw || '') +
					(aqiw || '')
				);
				$('.additional_station_info').html(

				);

			}
		});
	});

	// Update the graph!
	//updateGraph(compound_period,limit);
	var limit = $("#limitSelect").children(":selected").attr("data-limit");
	//alert(uid + category + compound + compound_period + limit);

	show_graphNEW(uuid,uid,category,compound,compound_period,limit);

	// Needed with CSS flexbox layout, otherwise graph is rendered too small
	//g.resize();

	setTimeout(function(){

	},250);

}

function updatePopupURL(uuid){
	// Change URL Hash
	//window.location.hash = uid;
	window.history.pushState('station', 'New Title', ('/' + lang + '/map/station/' + uuid ));

	// Push Piwik event
	if (piwik_loaded == true){
		_paq.push(['trackEvent', 'Marker', + uuid ]);
	};
}
