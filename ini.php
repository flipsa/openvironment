<?php

	$DEBUG = true;

	if ($DEBUG == true){
		ini_set('display_errors',1);
		error_reporting(E_ALL);
	}

	//ini_set("session.use_cookies",0);
	//ini_set("session.use_only_cookies",0);
	//ini_set("session.use_trans_sid",1);
	
	//session_start();
	
	
	// Set the language 
	// First see if we already have a URL string for language, e.g. /en or /de
	$URI = explode('/', $_SERVER['REQUEST_URI']);
	$LANG = $URI[1];
	
	//$LANG = "en";
	
	if ($LANG != NULL){
		
		// Check if language is allowed / known
		if (($LANG === "de") || ($LANG === "en")){

			//setcookie("LANG", $URI[1], time() + (86400 * 365), "/");
			$_SESSION['LANG'] = $URI[1];
			
			if ($LANG === "en") {
				//setcookie("UNITS", "imperial", time() + (86400 * 365), "/");
				$_SESSION['UNITS'] = "imperial";
				
				//setcookie("DISTANCE_UNIT", "mi.", time() + (86400 * 365), "/");
				$_SESSION['DISTANCE_UNIT'] = "mi.";
			} else {
				//setcookie("UNITS", "metric", time() + (86400 * 365), "/");
				$_SESSION['UNITS'] = "metric";
				
				//setcookie("DISTANCE_UNIT", "km", time() + (86400 * 365), "/");
				$_SESSION['DISTANCE_UNIT'] = "km";
			};

		} else {
			//echo 'Setting default language to "de"';
			header('Location: https://'.$_SERVER['HTTP_HOST'] . '/de/map' );
			//setcookie("LANG", "de", time() + (86400 * 365), "/");
			$_SESSION['LANG'] = "de";
			
			//setcookie("UNITS", "metric", time() + (86400 * 365), "/");
			$_SESSION['UNITS'] = "metric";
			
			//setcookie("DISTANCE_UNIT", "km", time() + (86400 * 365), "/");
			$_SESSION['DISTANCE_UNIT'] = "km";
		}
		
	} else if (isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])){
		
		$LANG = substr(locale_accept_from_http($_SERVER['HTTP_ACCEPT_LANGUAGE']), 0, 2);
		header('Location: https://'.$_SERVER['HTTP_HOST'] . '/' . $LANG . '/map' );
		//echo 'HTTP_ACCEPT_LANGUAGE: ' . $LANG;
		//setcookie("LANG", $LANG, time() + (86400 * 365), "/");
		$_SESSION['LANG'] = $LANG;
		
		if ($LANG === "en") {
			//setcookie("UNITS", "imperial", time() + (86400 * 365), "/");
			$_SESSION['UNITS'] = "imperial";
			
			//setcookie("DISTANCE_UNIT", "mi.", time() + (86400 * 365), "/");
			$_SESSION['DISTANCE_UNIT'] = "mi.";
		} else {
			//setcookie("UNITS", "metric", time() + (86400 * 365), "/");
			$_SESSION['UNITS'] = "metric";
			
			//setcookie("DISTANCE_UNIT", "km", time() + (86400 * 365), "/");
			$_SESSION['DISTANCE_UNIT'] = "km";
		};
	} 
	
?>
