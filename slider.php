					<div id="sliderContainer">
						<div class='slider'>
							<div class='slide' style="background:url(/img/slider/air_quality2-960.jpg) no-repeat;">
								<div class='slidecontent'>
									<h1>Wie schadstoffbelastet ist die Luft in meiner Stadt?</h1>
									<h2></h2>
								</div>
							</div>
							<div class='slide' style="background:url(/img/slider/bathing2-960.jpg) no-repeat;">
								<div class='slidecontent'>
									<h1>Wie sauber sind unsere Badeseen und Strandbäder?</h1>
									<h2></h2>
								</div>
							</div>
							<div class='slide' style="background:url(/img/slider/radiation2-960.jpg) no-repeat;">
								<div class='slidecontent'>
									<h1>Wie hoch ist die Strahlenbelastung in meiner Gegend?</h1>
									<h2></h2>
								</div>
							</div>
							<div class='slide' style="background:url(/img/slider/waterlevel-960.jpg) no-repeat;">
								<div class='slidecontent'>
									<h1>Wie ist der aktuelle Pegelstand von Flüssen und Meeren?</h1>
									<h2></h2>
								</div>
							</div>
							
						</div>
					</div>
