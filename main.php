<?php
	require_once("constants.php");
	require_once("db_stats.php");
?>

		<div id="header" style="display:none;">

			<div id="headline">
				<a href="/<? echo $LANG ?>/map/">
					<h1><? echo $HEADLINE ?> <span id="beta">{beta}</span></h1>
					<h2 style="display:none;"><? echo $SUBHEADLINE ?></h2>
				</a>
			</div>

		</div>

		<? require_once("leftBar.php"); ?>


		<div id="outer">

			<div id="map"></div>

			<!--<div id="sidebar"><?php //require_once("highlights_airq.php"); ?></div>-->

			<div id="panel">

				<div>
					<input id="fullscreen" class="panelCheckbox" type="checkbox" />
					<label id="fullscreenLabel" class="locateLabel tooltip-left" for="fullscreen" title="<? echo $FULLSCREEN ?>" ><i class="icon-resize-full" ></i></label>
				</div>

				<div>
					<input id="layersMenu" class="panelCheckbox" type="checkbox" />
					<label id="layersMenuLabel" class="locateLabel tooltip-left" for="layersMenu" title="<? echo $LAYERS_MENU ?>" ><i class="icon-map" ></i></label>
				</div>

				<div>
					<input id="legend" class="panelCheckbox" type="checkbox" />
					<label id="legendLabel" class="locateLabel tooltip-left" for="legend" title="<? echo $LEGEND ?>" ><i class="icon-info" ></i></label>
				</div>

				<div>
					<input id="settings" class="panelCheckbox" type="checkbox" />
					<label id="settingsLabel" class="locateLabel tooltip-left" for="settings" title="<? echo $SETTINGS ?>"><i class="icon-cog" ></i></label>
				</div>

				<div>
					<input id="help" class="panelCheckbox" type="checkbox" />
					<label id="helpLabel" class="locateLabel tooltip-left" for="help" title="<? echo $HELP ?>"><i class="icon-help" ></i></label>
				</div>
			</div>

			<div id="sidebar" class="clearfix">

				<div id="layersBar" class="clearfix panelElement round border">

					<div class="boxHeader">

						<h3><? echo $MAP_LAYER ?></h3>

						<button class="closeSidebar boxButton" title="" alt=""><i class="icon-cancel"></i></button>

					</div>

					<div class="scroll panelElementInner clearfix">
						<div id="layersMinimap"></div>
					</div>

				</div>

				<div id="legendBar" class="clearfix panelElement round border">

					<div class="boxHeader">

						<h3><? echo $LEGEND ?></h3>

						<button class="closeSidebar boxButton" title="" alt=""><i class="icon-cancel"></i></button>

					</div>

					<div class="scroll panelElementInner clearfix">

						<div id="luftqualitaet" class="tabs2-content">
							<h5><img src="/img/air_quality.png" class="tabs2-header" alt="Luftschadstoffe" />Luftqualit&auml;t</h5>
							<p><b><? echo $STATIONS ?>:</b> <? echo $rows_air; ?> </p>
							<p><b><? echo $SENSORS ?>:</b> Kohlenmonoxid (CO), Stickstoffdioxid (NO₂), Ozon (O₃), Feinstaub (PM₁₀), Schwefeldioxid (SO₂)</p>
							<p><b><? echo $INTERVAL ?>:</b> stündlich von 07:30 - 22:30 </p>
							<p><b><? echo $WARNINGS ?>:</b> &Uuml;berschreitung gesetzl. Grenzwerte</p>
							<p><b><? echo $DATA_SOURCE ?>:</b> <a href="http://www.umweltbundesamt.de/daten/luftbelastung/aktuelle-luftdaten" target="_blank"> Umweltbundesamt</a></p>
						</div>

						<!-- <div id="wetter" class="tabs2-content">
							<h5><img src="/img/weather.png" class="tabs2-header" alt="Wetterdaten" />Wetter</h5>
							<p><b><? echo $STATIONS ?>:</b> <? echo $rows_weather; ?> </p>
							<p><b><? echo $SENSORS ?>:</b> Temperatur, Luftdruck, Niederschlag, Windrichtung und Geschwindigkeit, Wetterbedingungen</p>
							<p><b><? echo $INTERVAL ?>:</b> Stündlich (zur vollen Stunde)</p>
							<p><b><? echo $WARNINGS ?>:</b> bei <a href="http://de.wikipedia.org/wiki/Unwetter" target="_blank">Unwetter</a></p>
							<p><b><? echo $DATA_SOURCE ?>: </b><a href="http://www.dwd.de" target="_blank">Deutscher Wetterdienst</a></p>
						</div> -->

						<div id="gamma-odl" class="tabs2-content">
							<h5><img src="/img/radiation.png" class="tabs2-header" alt="Radioaktivit&auml;t / Gamma Ortsdosisleistung" />Radioaktivit&auml;t</h5>
							<p><b><? echo $STATIONS ?>:</b> <? echo $rows_radiation; ?> </p>
							<p><b><? echo $SENSORS ?>: </b> Gamma Ortsdosisleistung (24h Mittelwert)</p>
							<p><b><? echo $INTERVAL ?>:</b> 4x / Tag (5 / 11 / 17 / 23 Uhr)</p>
							<p><b><? echo $WARNINGS ?>:</b> 0.02µSv/h über dem Durchschnittswert f&uuml;r die jeweilige Station</p>
							<p><b><? echo $DATA_SOURCE ?>:</b> <a href="http://odlinfo.bfs.de/" target="_blank">Bundesamt für Strahlenschutz</a></p>
						</div>

						<div id="pegel" class="tabs2-content">
							<h5><img src="/img/pegel.png" class="tabs2-header" alt="Pegelstände und andere Gew&auml;sserinformationen" />Pegelst&auml;nde</h5>
							<p><b><? echo $STATIONS ?>:</b> <? echo $rows_waterways; ?> </p>
							<p><b><? echo $SENSORS ?>:</b> Pegelst&auml;nde, Flie&szlig;geschwindigkeit, Abfluss, Durchfahrtsh&ouml;hen, vereinzelt meteorologische Daten</p> 		
							<p><b><? echo $INTERVAL ?>:</b> St&uuml;ndlich (bei &Auml;nderung)</p>
							<p><b><? echo $WARNINGS ?>:</b> noch nicht implementiert</p>
							<p><b><? echo $DATA_SOURCE ?>:</b> <a href="https://www.pegelonline.wsv.de/gast/start" target="_blank">Pegel Online / WSV</a></p>
						</div>

						<!--
						<div id="gewaesser" class="tabs2-content">
							<h5><img src="/img/bw.png" class="tabs2-header" alt="Badegewässerqualität" />Badegew&auml;sser</h5>
							<p><b><? echo $STATIONS ?>:</b> <? echo $rows_bw_quality; ?> </p>
							<p><b><? echo $SENSORS ?>:</b>Badegew&auml;sserqualit&auml;t (m&ouml;gliche Werte: Exzellent, Gut, Ausreichend, Mangelhaft, Geschlossen, Dauerhaft Geschlossen, Neu (keine Bewertung))</p>
							<p><b><? echo $INTERVAL ?>:</b>j&auml;hrlich (aktuell: Mai 2014)</p>
							<p><b><? echo $WARNINGS ?>:</b> bei Zustand "schlecht" (4)</p>
							<p><b><? echo $DATA_SOURCE ?>:</b><a href="http://www.eea.europa.eu/data-and-maps/data/bw-water-directive-status-of-bw-water-5" target="_blank"> Europ&auml;ische Umweltbeh&ouml;rde / EEA</a></p>
						</div>
						-->
					</div>
				</div>

				<div id="settingsBar" class="clearfix panelElement round border">

					<div class="boxHeader">

						<h3><? echo $SETTINGS ?></h3>

						<button class="closeSidebar boxButton" title="" alt=""><i class="icon-cancel"></i></button>

					</div>

					<div class="scroll panelElementInner clearfix">
						<div style="padding:5px 10px 0 10px ; border-bottom:1px solid rgb(169,169,169);">
							<div><? echo $LANGUAGE ?>:</div>
							<a href="/de/map/"><img src="/img/flag_de.png" class="langSelect" style="<? if ($LANG == 'de') { $opacity = '1.0'; } else { $opacity = '0.3'; }; ?> opacity:<? echo $opacity ?>;"></a>
							<a href="/en/map/"><img src="/img/flag_en.png" class="langSelect" style="<? if ($LANG == 'en') { $opacity = '1.0'; } else { $opacity = '0.3'; }; ?> opacity:<? echo $opacity ?>;"></a>
						</div>

						<div style="padding:10px; border-bottom:1px solid rgb(169,169,169);">
							<div style="margin-bottom:10px;"><? echo $UNITS ?>:</div>
							<div id="unitToggle" class="toggle toggle-modern" style="width:100% !important;height:30px;"></div>
						</div>

						<div style="padding:10px; border-bottom:1px solid rgb(169,169,169);">
							<div style="margin-bottom:10px;"><? echo $TIMEZONE ?>:</div>
							<? require_once("tz.php"); ?>
						</div>
					</div>

				</div>

			</div>




		</div> <!-- outer -->

		<!--<div id="footer">
			<div id="share" style="float:left;margin-left:10px;" >
				<div class="socialshareprivacy"></div>
				<div style="clear:both;"></div>
			</div>
		</div>-->

		<div id="tabs1" class="tabs" style=""> <!-- Start tabs -->
			<div id="tab_select1" style="display:none;" >
				<ul id="menuSelect">
					<li><a href="#menu" class="tooltip-left" style="padding:0;" title="<? echo $MENU ?>"><i class="icon-menu"></i></a></li>
				</ul>
			</div> <!-- Tab_select -->




			<div id="menu" class="tab1-content">

				<div id="tabs2" class="tabs" style="display:flex;">

				<ul style="margin:0 auto; padding:10px 0;">
					<li><a href="#ueber"><? echo $ABOUT ?></a></li>
					<li><a href="#api"><? echo $API ?></a></li>
					<li><a href="#credits"><? echo $CREDITS ?></a></li>
					<li><a href="#impressum"><? echo $IMPRINT ?></a></li>
				</ul>

				<div id="ueber" class="spacer scroll">

					<? //require_once('slider.php'); ?>

					<div class="column">

						<div class="claims">

							<h1><? echo $ABOUT_HEADLINE_INTRO ?></h1>
							<p><? echo $ABOUT_PARAGRAPH_INTRO ?></p>
							<ul>
								<li><? echo $AIR_QUALITY ?></li>
								<li><? echo $RADIATION ?></li>
								<li><? echo $WATERWAYS ?></li>
								<li><? echo $WEATHER ?> (soon!)</li>
								<li><? echo $BATHING ?> (soon!)</li>
								<li><? echo $INDUSTRIAL_POLLUTION ?> (soon!)</li>
							</ul>

							<h1><? echo $ABOUT_HEADLINE_OPEN_DATA ?></h1>

							<p><? echo $ABOUT_PARAGRAPH_OPEN_DATA ?></p>

							<h1><? echo $ABOUT_HEADLINE_YOUR_HELP ?></h1>

							<p><? echo $ABOUT_PARAGRAPH_YOUR_HELP ?></p>

							<div class="claim-links">
								<div><span><? echo $ABOUT_YOUR_HELP_CODE ?>:</span><a href="https://gitlab.com/flipsa/openvironment" target="_blank"><i class="awesome-gitlab claim-icon-small"></i></a></div>
								<div><span><? echo $ABOUT_YOUR_HELP_DATASETS ?>:</span><a href="mailto:info@openvironment.org"><i class="awesome-attach claim-icon-small"></i></a></div>
								<div><span><? echo $ABOUT_YOUR_HELP_CONTACT ?>:</span><a href="https://twitter.com/Umweltdaten" target="_blank"><i class="awesome-twitter claim-icon-small"></i></a></div>
							</div>



						</div>


						<div class="clearfix"></div>

					</div>

				</div>

				<div id="api" class="spacer scroll">

					<div class="column">

						<div class="claims">
							<h1><? echo $API_HEADLINE_INTRO ?></h1>
							<p><? echo $API_PARAGRAPH_INTRO ?></p>

							<h1><? echo $API_HEADLINE_SEARCH_AREA ?></h1>
							<p><? echo $API_PARAGRAPH_SEARCH_AREA ?></p>

							<h1><? echo $API_HEADLINE_SEARCH_STATION ?></h1>
							<p><? echo $API_PARAGRAPH_SEARCH_STATION ?></p>

							<h1><? echo $API_HEADLINE_TERMS ?></h1>
							<p><? echo $API_PARAGRAPH_TERMS ?></p>

						</div>

					</div>
				</div>

				<div id="credits" class="spacer scroll">
					<div class="column">
						<div class="claims">

							<h1><? echo $CREDITS_HEADLINE_DATA ?></h1>
							<p><? echo $CREDITS_PARAGRAPH_DATA ?></p>

							<ul>
								<li>Umweltbundesamt</li>
								<li>Pegel-Online / WSV</li>
								<li>Bundesamt f&uuml;r Strahlenschutz</li>
							</ul>

							<h1><? echo $CREDITS_HEADLINE_CODE ?></h1>
							<p><? echo $CREDITS_PARAGRAPH_CODE ?></p>

							<ul>
								<li><a href="https://github.com/alertifyjs/" target="_blank" title="">AlertifyJS</a> (<a href="http://www.opensource.org/licenses/MIT" target="_blank" title="MIT License">MIT</a>)</li>
								<li><a href="http://www.dygraphs.com" target="_blank">Dygraphs</a> (<a href="http://www.opensource.org/licenses/MIT" target="_blank" title="MIT License">MIT</a>)</li>
								<li><a href="https://usablica.github.io/intro.js/" target="_blank" title="">IntroJS</a> ( <a href="http://www.opensource.org/licenses/MIT" target="_blank" title="MIT License">MIT</a>)</li>
								<li><a href="http://jquery.com/" target="_blank" title="jQuery / jQueryUI">jQuery / jQueryUI</a> (<a href="http://www.opensource.org/licenses/MIT" target="_blank" title="MIT License">MIT</a>)</li>
								<li><a href="https://github.com/inuyaksa/jquery.nicescroll" target="_blank">jQuery.Nicescroll </a> (<a href="http://www.opensource.org/licenses/MIT" target="_blank" title="MIT License">MIT</a>)</li>
								<li><a href="http://leafletjs.com/" target="_blank" title="Leaflet">Leaflet</a> (<a href="http://opensource.org/licenses/BSD-2-Clause" target="_blank" title="">BSD 2-Clause</a>)</li>
								<li><a href="https://github.com/makinacorpus/Leaflet.Spin" target="_blank">Leaflet.Spin </a> (<a href="http://www.opensource.org/licenses/MIT" target="_blank" title="MIT License">MIT</a>)</li>
								<li><a href="http://mapicons.nicolasmollet.com/" target="_blank" >Map Icons</a> (<a href="http://creativecommons.org/licenses/by-sa/3.0/" target="_blank">CC BY-SA 3.0</a>)</li>
								<li><a href="http://piwik.org/" target="_blank">Piwik</a> (<a href="https://gnu.org/licenses/gpl.html" target="_blank">GPLv3</a>)</li>
								<li><a href="https://github.com/sindresorhus/screenfull.js/" target="_blank">screenfull.js</a> (<a href="http://www.opensource.org/licenses/MIT" target="_blank" title="MIT License">MIT</a>)</li>
								<li><a href="https://fgnass.github.io/spin.js/" target="_blank">SpinJS</a> (<a href="http://www.opensource.org/licenses/MIT" target="_blank" title="MIT License">MIT</a>)</li>
							</ul>
						</div>
					</div>

				</div>

				<div id="impressum" class="spacer scroll">
					<div class="column">
						<div class="claims">

							<h1><? echo $IMPRINT_HEADLINE_INTRO ?></h1>
							<p><? echo $IMPRINT_PARAGRAPH_INTRO ?></p>

							<h1><? echo $IMPRINT_HEADLINE_CONTACT ?></h1>

							<div class="claim-links" style="margin:40px 0;">
								<div style="font-size:40px;"><span><? echo $IMPRINT_EMAIL ?>:</span><a href="mailto:info@openvironment.org"><i class="awesome-mail claim-icon-small"></i></a></div>
								<div style="font-size:40px;"><span><? echo $IMPRINT_TWITTER ?>:</span><a href="https://twitter.com/Pol_Hackonomist" target="_blank"><i class="awesome-twitter claim-icon-small"></i></a></div>
								<div style="font-size:40px;"><span><? echo $IMPRINT_GIT ?>:</span><a href="https://gitlab.com/flipsa" target="_blank"><i class="awesome-gitlab claim-icon-small"></i></a></div>
								<div style="font-size:40px;"><span><? echo $IMPRINT_LINKEDIN ?>:</span><a href="https://www.linkedin.com/profile/view?id=50429486" target="_blank"><i class="awesome-linkedin claim-icon-small"></i></a></div>
							</div>
							<p id="imprint_footer"><br /><? echo $IMPRINT_FOOTER ?></p>
						</div>
					</div>
				</div>

			</div>


			</div>


		</div> <!-- End tabs1 -->

		<div id="spinnerFull">
			<div id="spinner_content">
				<img src="/img/loading22.gif" alt="Loading gif" />
			</div>
		</div>
