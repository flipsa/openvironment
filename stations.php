<?php

session_start();

if ($DEBUG = true){
	ini_set('display_errors',1);
	error_reporting(E_ALL);
}

// Get credentials for database
require_once("db_connect.php");

// pick up the PHP session, so every time we do a search, the session lifetime is prolonged
//session_start();

// Get the unit to use from cookie
if (isset($_SESSION['UNITS'])) {
	$UNITS = $_SESSION['UNITS'];

	if ($UNITS == 'metric') {
		$DISTANCE_UNIT = 111.045;
	} else {
		$DISTANCE_UNIT = 69;
	};

} else {
	// Use the metric system
	$DISTANCE_UNIT = 111.045;
};



//echo $DISTANCE_UNIT;

// Get parameters for area or station search from URL
if (isset($_GET["type"])) {$type = $_GET["type"];};
if (isset($_GET["uuid"])) {$uuid = $_GET["uuid"];};
if (isset($_GET["uid"])) {$uid = $_GET["uid"];};
if (isset($_GET["lat"])) {$lat = $_GET["lat"];};
if (isset($_GET["lng"])) {$lng = $_GET["lng"];};
if (isset($_GET["category"])) {$category = $_GET["category"];};
if (isset($_GET["multi"])) {$multi = $_GET["multi"];};
if (isset($_GET["radius"])) {$radius = $_GET["radius"];};
if (isset($_GET["measurements"])) {$measurements = $_GET["measurements"];};

if (isset($_GET["amount"])) {
	$amount = $_GET["amount"];
	if ($_GET["amount"] == "") {
		$amount = 10000;
	}
};

// Get parameters for timeline search from URL
if (isset($_GET["compound"])) {$compound=$_GET["compound"];};

if (isset($_GET["period"])) {$period=$_GET["period"];};

if ( (isset($_GET["compound"])) && (isset($_GET["period"])) ) {
	$comp_period=$compound . '_' . $period;
};

// Set the default limit to 7 days
if (isset($_GET["limit"])) {
	$limit=$_GET["limit"];
	if ($limit == "") {
		$limit = 7;
	};
};

# Connect to MySQL database
$conn = new PDO("mysql:host=$server;dbname=$database;charset=utf8",$username,$password);
$conn->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

// If we are searching for stations around a lat-lng point OR if we are getting recent data for one specific station...
if ((isset($type)) && (($type == 'near') || ($type == 'station'))) {

	// if searching for stations near a lat/lng point (i.e. area search)
	if ((isset($type)) && ($type == 'near')) {

		$all_categories = "SELECT res.*, sair.type, sair.area, sair.area_subcat, sair.ozone_class, co_8smw, co_8smw_time, no2_1smw, no2_1smw_time, o3_1smw, o3_1smw_time, pm10_1tmw, pm10_1tmw_time, so2_1smw, so2_1smw_time, odl_1smw, odl_cos_1smw, odl_ter_1smw, odl_avg_1smw, odl_1smw_time, swat.km, swat.water_shortname, swat.water_longname, swat.w_gaugeZero_unit, swat.w_gaugeZero_value, swat.w_gaugeZero_validFrom, swat.M_I_shortname, swat.M_I_longname, swat.M_I_unit, swat.M_I_value, swat.M_I_validFrom, swat.M_II_shortname, swat.M_II_longname, swat.M_II_unit, swat.M_II_value, swat.M_II_validFrom, swat.M_III_shortname, swat.M_III_longname, swat.M_III_unit, swat.M_III_value, swat.M_III_validFrom, swat.mw_shortname, swat.mw_longname, swat.mw_unit, swat.mw_value, swat.mw_timespanStart, swat.mw_timespanEnd, swat.mnw_shortname, swat.mnw_longname, swat.mnw_unit, swat.mnw_value, swat.mnw_timespanStart, swat.mnw_timespanEnd, swat.mhw_shortname, swat.mhw_longname, swat.mhw_unit, swat.mhw_value, swat.mhw_timespanStart, swat.mhw_timespanEnd, swat.hhw_shortname, swat.hhw_longname, swat.hhw_unit, swat.hhw_value, swat.hhw_date, swat.nnw_shortname, swat.nnw_longname, swat.nnw_unit, swat.nnw_value, swat.nnw_date, swat.hsw_shortname, swat.hsw_longname, swat.hsw_unit, swat.hsw_value, swat.hsw_validFrom, swat.hthw_shortname, swat.hthw_longname, swat.hthw_unit, swat.hthw_value, swat.hthw_timespanStart, swat.hthw_timespanEnd, swat.hthw_date, swat.ntnw_shortname, swat.ntnw_longname, swat.ntnw_unit, swat.ntnw_value, swat.ntnw_timespanStart, swat.ntnw_timespanEnd, swat.ntnw_date, swat.mtnw_shortname, swat.mtnw_longname, swat.mtnw_unit, swat.mtnw_value, swat.mtnw_timespanStart, swat.mtnw_timespanEnd, swat.mthw_shortname, swat.mthw_longname, swat.mthw_unit, swat.mthw_value, swat.mthw_timespanStart, swat.mthw_timespanEnd, swat.ZS_I_shortname, swat.ZS_I_longname, swat.ZS_I_unit, swat.ZS_I_value, swat.ZS_I_validFrom, swat.rnw_shortname, swat.rnw_longname, swat.rnw_unit, swat.rnw_value, swat.rnw_validFrom, w_timestamp, w, w_trend, w_stateMnwMhw, w_stateNswHsw, lt_timestamp, lt, lt_trend, wt_timestamp, wt, wt_trend, q_timestamp, q, q_trend, va_timestamp, va, va_trend, dfh_timestamp, dfh, dfh_trend, lf_timestamp, lf, lf_trend, wg_timestamp, wg, wg_trend, wr_timestamp, wr, wr_trend,  gru_timestamp, gru, gru_trend, hl_timestamp, hl, hl_trend, pl_timestamp, pl, pl_trend, n_timestamp, n, n_trend,  n_intensity_timestamp, n_intensity, n_intensity_trend, o2_timestamp, o2, o2_trend, ph_timestamp, ph, ph_trend, cl_timestamp, cl, cl_trend, orp_timestamp, orp, orp_trend FROM (SELECT * FROM (SELECT s.*, p.radius, p.distance_unit * ROUND(DEGREES(ACOS(COS(RADIANS(p.latpoint)) * COS(RADIANS(s.lat)) * COS(RADIANS(p.longpoint - s.lng)) + SIN(RADIANS(p.latpoint)) * SIN(RADIANS(s.lat)))),3) AS distance FROM stations AS s JOIN (SELECT (SELECT :lat)  AS latpoint, (SELECT :lng) AS longpoint, (SELECT :radius) AS radius, $DISTANCE_UNIT AS distance_unit ) AS p ON 1=1 WHERE s.lat BETWEEN p.latpoint - (p.radius / p.distance_unit) AND p.latpoint  + (p.radius / p.distance_unit) AND s.lng BETWEEN p.longpoint - (p.radius / (p.distance_unit * COS(RADIANS(p.latpoint)))) AND p.longpoint + (p.radius / (p.distance_unit * COS(RADIANS(p.latpoint))))) AS d WHERE category like :category AND op_state='active' AND distance <= radius ORDER BY distance LIMIT :amount) as res LEFT JOIN air_current air ON res.uuid=air.uuid LEFT JOIN radiation_current rad ON res.uuid=rad.uuid LEFT JOIN waterways_current water ON res.uuid=water.uuid LEFT JOIN stations_air sair ON res.uuid=sair.uuid LEFT JOIN stations_radiation srad ON res.uuid=srad.uuid LEFT JOIN stations_waterways swat ON res.uuid=swat.uuid";

		// If category is not set / getting all categories
		if (($category == "") || ($category == "all")){

			// If we do a simple all categories search
			if ($multi == "null") {
				$sql = $conn->prepare($all_categories);

			// If we do a multi selection search
			} else {

				$multi_exploded = explode(',', $multi);
				$multi_length = sizeof($multi_exploded);

				//foreach ($multi_exploded as $compound) {
				for ($i = 0; $i <= $multi_length -1; $i++) {

					if ($multi_length == 1) {

						$sql_arg = $multi_exploded[$i] . " IS NOT NULL";

					} else {
						if ($i == 0) {
							$sql_arg = "$multi_exploded[$i] IS NOT NULL";
						} else {
							$sql_arg = $sql_arg . " OR $multi_exploded[$i] IS NOT NULL";
						}
					}
				}

				$sql = $conn->prepare("SELECT * FROM ($all_categories) AS multi WHERE $sql_arg");

			}

		} else {
			// fetch the data
			if (($category == "air") || ($category == "air_quality")) {
				$table = 'air_current';
				$table_station_extended = 'LEFT JOIN stations_air sex ON res.uuid=sex.uuid';

			} else if ($category == "radiation") {
				$table = 'radiation_current';
				//$table_station_extended = 'LEFT JOIN stations_radiation sex ON res.uuid=sex.uuid';
				$table_station_extended = '';

			} else if (($category == "waterways") || ($category == "pegel")) {
				$table = 'waterways_current';
				$table_station_extended = 'LEFT JOIN stations_waterways sex ON res.uuid=sex.uuid';
			} else {
				return;
			}
			$sql = $conn->prepare("SELECT * FROM (SELECT * FROM (SELECT s.*, p.radius, p.distance_unit * ROUND(DEGREES(ACOS(COS(RADIANS(p.latpoint)) * COS(RADIANS(s.lat)) * COS(RADIANS(p.longpoint - s.lng)) + SIN(RADIANS(p.latpoint)) * SIN(RADIANS(s.lat)))),3) AS distance FROM stations AS s JOIN (SELECT (SELECT :lat)  AS latpoint, (SELECT :lng) AS longpoint, (SELECT :radius) AS radius, $DISTANCE_UNIT AS distance_unit ) AS p ON 1=1 WHERE s.lat BETWEEN p.latpoint - (p.radius / p.distance_unit) AND p.latpoint  + (p.radius / p.distance_unit) AND s.lng BETWEEN p.longpoint - (p.radius / (p.distance_unit * COS(RADIANS(p.latpoint)))) AND p.longpoint + (p.radius / (p.distance_unit * COS(RADIANS(p.latpoint))))) AS d WHERE category like :category AND op_state='active' AND distance <= radius ORDER BY distance LIMIT :amount) as res LEFT JOIN $table t ON res.uuid=t.uuid $table_station_extended");
		}

		// Execute our prepared statement with the values from the URL
		if ($category == "all") {
			$category = "";
		};

		$sql->execute(array(
			':lat' => $lat,
			':lng' => $lng,
			':category' => '%'.$category.'%',
			':radius' => $radius,
			':amount' => $amount
		));

	}

	// If were are getting recent data for a specific station
	if ((isset($type)) && ($type == "station")) {

		if (!isset($uuid)){
			get_uuid_from_uid($conn, $uid, $category);
		}

		if (!isset($category)){
			get_category_from_uuid($conn, $uuid);
		}

		if (($category == "air") || ($category == "air_quality")) {
			$table = 'air_current';
			$table_station_extended = 'stations_air';
		} else if ($category == "radiation") {
			$table = 'radiation_current';
			$table_station_extended = 'stations_radiation';
		} else if (($category == "waterways") || ($category == "pegel")) {
			$table = 'waterways_current';
			$table_station_extended = 'stations_waterways';
		} else {
			return;
		}

		//get_category_from_uuid($conn, $uuid);
		//$table = $cat . '_current';

		// Incluse current measurements or not?
		if ($measurements == "true") {
			$station_data = "SELECT *, t.uuid FROM (SELECT * FROM stations WHERE `uuid`= :uuid AND op_state='active') as stations LEFT JOIN $table AS t ON stations.uuid=t.uuid LEFT JOIN $table_station_extended sex ON stations.uuid=sex.uuid";
		} else {
			$station_data = "SELECT * FROM (SELECT * FROM stations WHERE `uuid`= :uuid AND op_state='active') as stations LEFT JOIN $table_station_extended sex ON stations.uuid=sex.uuid";
		}
		// Prepare the PDO statement
		$sql = $conn->prepare($station_data);

		// Execute the PDO statement
		$sql->execute(array(
			':uuid' => $uuid
		));
	}


	/**
	* If bbox variable is set, only return records that are within the bounding box
	* bbox should be a string in the form of 'southwest_lng,southwest_lat,northeast_lng,northeast_lat'
	* Leaflet: map.getBounds().pad(0.05).toBBoxString()
	*/
	if (isset($_GET['bbox']) || isset($_POST['bbox'])) {
		$bbox = explode(',', $_GET['bbox']);
		$sql = $sql . ' WHERE res.lng <= ' . $bbox[2] . ' AND res.lng >= ' . $bbox[0] . ' AND res.lat <= ' . $bbox[3] . ' AND res.lat >= ' . $bbox[1];
	}

	// If there is an error with the query...
	if (!$sql) {
		echo "\nPDO::errorInfo():\n";
		print_r($sql->errorInfo());
		exit;
	}

	# Build GeoJSON feature collection array
	$geojson = array(
	   'type'      => 'FeatureCollection',
	   'features'  => array()
	);

	# Loop through rows to build feature arrays
	while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
		$properties = $row;

		# Remove x and y fields from properties (optional)
		//unset($properties['x']);
		//unset($properties['y']);
		$feature = array(
			'type' => 'Feature',
			'geometry' => array(
				'type' => 'Point',
				'coordinates' => array(
					$row['lng'],
					$row['lat']
				)
			),
			'properties' => $properties
		);
		# Add feature arrays to feature collection array
		array_push($geojson['features'], $feature);
	}

	// Return routing result
	header("Content-Type:application/json",true);
	echo json_encode($geojson);
}

// TIMELINES
if ((isset($type)) && ($type == 'timeline')) {

	//get_category_from_uuid($conn, $uuid);
	//get_uid_from_uuid($conn, $uuid);

	$output = fopen('php://output', 'w');

	// fetch the data
	if (($category == "air") || ($category == "air_quality")) {
		$table = 'air_timeline_'.$compound;
	} else if ($category == "radiation") {
		$table = 'radiation_timeline';
	} else if (($category == "waterways") || ($category == "pegel")) {
		$table = 'waterways_timeline_'.$compound;
	} else {
		return;
	}

	// Prepare statement
	$sql = $conn->prepare("SELECT sensor_time, $comp_period FROM $table WHERE $comp_period IS NOT NULL AND sensor_time > NOW() - INTERVAL :limit DAY AND uid=:uid ORDER BY sensor_time");

	// Execute statement
	$sql->execute(array(
		//':comp_period' => $comp_period,
		':limit' => $limit,
		':uid' => $uid
	));

	// Create and fill array from PDO query
	$header = array();
	// loop over the rows, outputting them
	while ($row = $sql->fetch(PDO::FETCH_ASSOC)) {
		if(empty($header)){
			//$header = array_keys($row); // get the columnnames
			//fputcsv($output, $header); // put them in csv
		}

		// enclosure has to be set to " " (= no enclosure), otherwise dygraphs (or rather javascript's date function) won't work
		fputcsv($output, $row, $delimiter=",", $enclosure=" ");

	}

}

// HELPER FUNCTIONS FOR TRANSITION FROM UID+CATEGORY TO UUID

function get_category_from_uuid($conn, $uuid) {
	// Prepare PDO statement
	$get_cat = $conn->prepare("SELECT category FROM stations WHERE uuid= :uuid");

	// Execute PDO statement
	$get_cat->execute(array(
		//':table' => 'air_current',
		':uuid' => $uuid
	));

	// If execution fails...
	if (!$get_cat) {
		echo "\nPDO::errorInfo():\n";
		print_r($get_cat->errorInfo());
		exit;
	}

	// Get category from lookup of uuid in the stations table
	while ($row = $get_cat->fetch(PDO::FETCH_ASSOC)) {
		global $category;
		$category = $row['category'];
	}
}

function get_uid_from_uuid($conn, $uuid) {
	// Prepare PDO statement
	$get_uid = $conn->prepare("SELECT uid FROM stations WHERE uuid= :uuid");

	// Execute PDO statement
	$get_uid->execute(array(
		':uuid' => $uuid
	));

	// If execution fails...
	if (!$get_uid) {
		echo "\nPDO::errorInfo():\n";
		print_r($get_uid->errorInfo());
		exit;
	}

	// Get category from lookup of uuid in the stations table
	while ($row = $get_uid->fetch(PDO::FETCH_ASSOC)) {
		global $uid;
		$uid = $row['uid'];
	}
}

function get_uuid_from_uid($conn, $uid, $category) {
	// Prepare PDO statement
	$get_uuid = $conn->prepare("SELECT uuid FROM stations WHERE uid= :uid and category= :category");

	// Execute PDO statement
	$get_uuid->execute(array(
		':uid' => $uid,
		':category' => $category
	));

	// If execution fails...
	if (!$get_uuid) {
		echo "\nPDO::errorInfo():\n";
		print_r($get_uuid->errorInfo());
		exit;
	}

	// Get category from lookup of uuid in the stations table
	while ($row = $get_uuid->fetch(PDO::FETCH_ASSOC)) {
		global $uuid;
		$uuid = $row['uuid'];
	}
}

$conn = NULL;

?>
