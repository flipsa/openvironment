<?php

/**
 * GitHub webhook handler template.
 *
 * @see  https://developer.github.com/webhooks/
 * @author  Miloslav Hůla (https://github.com/milo)
 */

# For hook secret
require_once("db_connect.php");

$LOCAL_ROOT         = "/var/www/";
$LOCAL_REPO_NAME    = "offene-umweltdaten.de";
$LOCAL_REPO         = "{$LOCAL_ROOT}/{$LOCAL_REPO_NAME}";
$REMOTE_REPO        = "git@gitlab.com:flipsa/openvironment.git";
//$BRANCH             = "master";

set_error_handler(function($severity, $message, $file, $line) {
	throw new \ErrorException($message, 0, $severity, $file, $line);
});

set_exception_handler(function($e) {
	header('HTTP/1.1 500 Internal Server Error');
	echo "Error on line {$e->getLine()}: " . htmlSpecialChars($e->getMessage());
	die();
});

if (!isset($_SERVER['CONTENT_TYPE'])) {
        throw new \Exception("Missing HTTP 'Content-Type' header.");
} elseif (!isset($_SERVER['HTTP_X_GITLAB_EVENT'])) {
        throw new \Exception("Missing HTTP 'X-GitLab-Event' header.");
};

$rawPost = NULL;
if ($GIT_WEBHOOK_SECRET !== NULL) {
	if (!isset($_SERVER['HTTP_X_GITLAB_TOKEN'])) {
		throw new \Exception("Missing GitLab Secret Token");
	} else {
		$remoteSecret = $_SERVER['HTTP_X_GITLAB_TOKEN'];
		if ($remoteSecret !== $GIT_WEBHOOK_SECRET) {
			throw new \Exception('Hook secret does not match.');
		};
	};
};

if (!isset($_SERVER['CONTENT_TYPE'])) {
	throw new \Exception("Missing HTTP 'Content-Type' header.");
} elseif (!isset($_SERVER['HTTP_X_GITLAB_EVENT'])) {
	throw new \Exception("Missing HTTP 'X-GitLab-Event' header.");
};

switch ($_SERVER['CONTENT_TYPE']) {
	case 'application/json':
		$json = $rawPost ?: file_get_contents('php://input');
		break;

	case 'application/x-www-form-urlencoded':
		$json = $_POST['payload'];
		break;

	default:
		throw new \Exception("Unsupported content type: $_SERVER[HTTP_CONTENT_TYPE]");
}

# Payload structure depends on triggered event
# https://developer.github.com/v3/activity/events/types/
$payload = json_decode($json);

switch (strtolower($_SERVER['HTTP_X_GITLAB_EVENT'])) {
	case 'ping':
		echo 'pong';
		break;

	case 'push hook':
		shell_exec("cd {$LOCAL_REPO} && git pull && echo $(date) - Automated Pull from GitLab Webhook >> {$LOCAL_ROOT}/{$LOCAL_REPO_NAME}/git.log");
		break;

	case 'create':
		shell_exec("cd {$LOCAL_ROOT} && git clone {$REMOTE_REPO} && echo $(date) - Automated Clone from GitLab Webhook >> {$LOCAL_ROOT}/{$LOCAL_REPO_NAME}/git.log");
		break;

	default:
		header('HTTP/1.0 404 Not Found');
		echo "Event:$_SERVER[HTTP_X_GITHUB_EVENT] Payload:\n";
		print_r($payload); # For debug only. Can be found in GitHub hook log.
		die();
}
