# Select database
use ud_luft;
set session collation_database=utf8_unicode_ci;
set session character_set_database=utf8;

# Truncate all tables 
truncate table bw_DE;


# Populate table
load data local infile '/var/www/offene-umweltdaten.de/scrape/bathing_water/ud_data/DE_BW2013.csv' into table bw_DE fields terminated by ';' enclosed by '' lines terminated by '\n' (@dummy, uid, @dummy, @dummy, @dummy, @dummy, name, @dummy, lng, lat, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy,  @dummy, @dummy, @dummy, @dummy, @dummy,  @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, bw_quality_2011, bw_quality_2012, bw_quality_2013, @dummy);


# Update category and icon
DELETE FROM `bw_DE` WHERE name IS NULL;
DELETE FROM `bw_DE` WHERE lat IS NULL;
DELETE FROM `bw_DE` WHERE lng IS NULL;
DELETE FROM `bw_DE` WHERE bw_quality_2013 AND bw_quality_2012 AND bw_quality_2011 IS NULL;
DELETE FROM `bw_DE` WHERE bw_quality_2013='' AND bw_quality_2011='' AND bw_quality_2012='';
DELETE FROM `bw_DE` WHERE lng="0";
DELETE FROM `bw_DE` WHERE lat="0";
UPDATE bw_DE SET category='bw_quality';
UPDATE bw_DE SET icon='/img/bw.png';
#UPDATE bw_DE SET bw_quality_2011='Exzellent' WHERE bw_quality_2012='excellent';
#UPDATE bw_DE SET bw_quality_2011='Gut' WHERE bw_quality_2011='good';
#UPDATE bw_DE SET bw_quality_2011='Ausreichend' WHERE bw_quality_2011='sufficient';
#UPDATE bw_DE SET bw_quality_2011='Mangelhaft' WHERE bw_quality_2011='poor';
#UPDATE bw_DE SET bw_quality_2011='Geschlossen' WHERE bw_quality_2011='closed';
#UPDATE bw_DE SET bw_quality_2011='Dauerhaft geschlossen' WHERE bw_quality_2011='permanently closed';
#UPDATE bw_DE SET bw_quality_2011='Neu - Keine Bewertung in 2011' WHERE bw_quality_2011='new';
#UPDATE bw_DE SET bw_quality_2011='Nicht genug Proben' WHERE bw_quality_2011='insufficiently sampled';
#UPDATE bw_DE SET bw_quality_2011='Erneuert - Keine Bewertung in 2011' WHERE bw_quality_2011='changes';
