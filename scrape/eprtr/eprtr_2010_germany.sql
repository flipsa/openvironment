# Select database
use ud_luft;

#set session collation_database=utf8_unicode_ci;
#set session character_set_database=utf8;

# Truncate all tables 
truncate table eprtr_2010_germany;

# Table create statements with timestamps / For future versions

# Populate airq_stations table
load data local infile '/var/www/offene-umweltdaten.de/scrape/eprtr/ud_data/eprtr_2010_germany.csv' into table eprtr_2010_germany fields terminated by '\t' lines terminated by '\n' (start_date, country, uid, name, name_proper, @dummy, @dummy, @dummy, @dummy, lat, lng, @dummy, @dummy, @dummy, @dummy, @dummy, var1, var2, var3, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy, @dummy);


#UPDATE air_quality_germany,airq_so2 SET air_quality_germany.so2=airq_so2.SO2 WHERE air_quality_germany.uid=airq_so2.station_code;

# Delete inactive stations and stations 
#DELETE FROM `air_quality_germany` WHERE end_date<>'';
#DELETE FROM `air_quality_germany` WHERE co IS NULL and no2 IS NULL and o3 IS NULL and pm10 IS NULL and so2 IS NULL;
UPDATE eprtr_2010_germany SET category='eprtr';
UPDATE eprtr_2010_germany SET icon='/img/eprtr.png';

