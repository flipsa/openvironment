CREATE TABLE IF NOT EXISTS `pegel_w` (
  `uid` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `uid2` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `uid3` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `category` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'pegel',
  `icon` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '/img/river.png',
  `name` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name_proper` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `km` decimal(6,2) DEFAULT NULL,
  `agency` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lng` decimal(19,14) NOT NULL,
  `lat` decimal(19,14) NOT NULL,
  `water_shortname` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `water_longname` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `w_shortname` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `w_longname` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `w_unit` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `w_equidistance` int(3) DEFAULT NULL,
  `w_timestamp` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `w` decimal(6,1) DEFAULT NULL,
  `w_trend` int(2) DEFAULT NULL,
  `w_stateMnwMhw` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `w_stateNswHsw` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `w_gaugeZero_unit` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `w_gaugeZero_value` varchar(6) COLLATE utf8_unicode_ci DEFAULT NULL,
  `w_gaugeZero_validFrom` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lt_shortname` varchar(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lt_longname` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lt_unit` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lt_timestamp` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lt` decimal(6,1) DEFAULT NULL,
  `lt_trend` int(2) DEFAULT NULL,
  `wt_shortname` varchar(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `wt_longname` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `wt_unit` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `wt_timestamp` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `wt` decimal(6,1) DEFAULT NULL,
  `wt_trend` int(2) DEFAULT NULL,
  `q_shortname` varchar(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `q_longname` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `q_unit` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `q_timestamp` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `q` decimal(6,1) DEFAULT NULL,
  `q_trend` int(2) DEFAULT NULL,
  `va_shortname` varchar(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `va_longname` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `va_unit` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `va_timestamp` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `va` decimal(6,1) DEFAULT NULL,
  `va_trend` int(2) DEFAULT NULL,
  `dfh_shortname` varchar(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dfh_longname` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dfh_unit` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dfh_timestamp` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dfh` decimal(6,1) DEFAULT NULL,
  `dfh_trend` int(2) DEFAULT NULL,
  `lf_shortname` varchar(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lf_longname` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lf_unit` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lf_timestamp` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lf` decimal(6,1) DEFAULT NULL,
  `lf_trend` int(2) DEFAULT NULL,
  `wg_shortname` varchar(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `wg_longname` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `wg_unit` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `wg_timestamp` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `wg` decimal(6,1) DEFAULT NULL,
  `wg_trend` int(2) DEFAULT NULL,
  `wr_shortname` varchar(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `wr_longname` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `wr_unit` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `wr_timestamp` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `wr` decimal(6,1) DEFAULT NULL,
  `wr_trend` int(2) DEFAULT NULL,
  `gru_shortname` varchar(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gru_longname` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gru_unit` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gru_timestamp` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gru` decimal(6,1) DEFAULT NULL,
  `gru_trend` int(2) DEFAULT NULL,
  `hl_shortname` varchar(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hl_longname` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hl_unit` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hl_timestamp` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hl` decimal(6,1) DEFAULT NULL,
  `hl_trend` int(2) DEFAULT NULL,
  `pl_shortname` varchar(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pl_longname` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pl_unit` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pl_timestamp` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pl` decimal(6,1) DEFAULT NULL,
  `pl_trend` int(2) DEFAULT NULL,
  `n_shortname` varchar(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `n_longname` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `n_unit` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `n_timestamp` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `n` decimal(6,1) DEFAULT NULL,
  `n_trend` int(2) DEFAULT NULL,
  `o2_shortname` varchar(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `o2_longname` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `o2_unit` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `o2_timestamp` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `o2` decimal(6,1) DEFAULT NULL,
  `o2_trend` int(2) DEFAULT NULL,
  `ph_shortname` varchar(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ph_longname` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ph_unit` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ph_timestamp` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ph` decimal(6,1) DEFAULT NULL,
  `ph_trend` int(2) DEFAULT NULL,
  `cl_shortname` varchar(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cl_longname` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cl_unit` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cl_timestamp` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cl` decimal(7,2) DEFAULT NULL,
  `cl_trend` int(2) DEFAULT NULL,
  `orp_shortname` varchar(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `orp_longname` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `orp_unit` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `orp_timestamp` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  `orp` decimal(6,1) DEFAULT NULL,
  `orp_trend` int(2) DEFAULT NULL,  
  `op_state` varchar(8) COLLATE utf8_unicode_ci DEFAULT 'active',
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  UNIQUE KEY `uid_2` (`uid`),
  KEY `lng` (`lng`,`lat`),
  KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `waterways_current` (
  `uuid` INT NOT NULL,
  `uid` varchar(36) COLLATE utf8_unicode_ci DEFAULT NULL,
  `km` decimal(6,2) DEFAULT NULL,
  `water_shortname` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `water_longname` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `w_timestamp` datetime DEFAULT NULL,
  `w` decimal(6,1) DEFAULT NULL,
  `w_trend` int(2) DEFAULT NULL,
  `lt_timestamp` datetime DEFAULT NULL,
  `lt` decimal(6,1) DEFAULT NULL,
  `lt_trend` int(2) DEFAULT NULL,
  `wt_timestamp` datetime DEFAULT NULL,
  `wt` decimal(6,1) DEFAULT NULL,
  `wt_trend` int(2) DEFAULT NULL,
  `q_timestamp` datetime DEFAULT NULL,
  `q` decimal(6,1) DEFAULT NULL,
  `q_trend` int(2) DEFAULT NULL,
  `va_timestamp` datetime DEFAULT NULL,
  `va` decimal(6,1) DEFAULT NULL,
  `va_trend` int(2) DEFAULT NULL,
  `dfh_timestamp` datetime DEFAULT NULL,
  `dfh` decimal(6,1) DEFAULT NULL,
  `dfh_trend` int(2) DEFAULT NULL,
  `lf_timestamp` datetime DEFAULT NULL,
  `lf` decimal(6,1) DEFAULT NULL,
  `lf_trend` int(2) DEFAULT NULL,
  `wg_timestamp` datetime DEFAULT NULL,
  `wg` decimal(6,1) DEFAULT NULL,
  `wg_trend` int(2) DEFAULT NULL,
  `wr_timestamp` datetime DEFAULT NULL,
  `wr` decimal(6,1) DEFAULT NULL,
  `wr_trend` int(2) DEFAULT NULL,
  `gru_timestamp` datetime DEFAULT NULL,
  `gru` decimal(6,1) DEFAULT NULL,
  `gru_trend` int(2) DEFAULT NULL,
  `hl_timestamp` datetime DEFAULT NULL,
  `hl` decimal(6,1) DEFAULT NULL,
  `hl_trend` int(2) DEFAULT NULL,
  `pl_timestamp` datetime DEFAULT NULL,
  `pl` decimal(6,1) DEFAULT NULL,
  `pl_trend` int(2) DEFAULT NULL,
  `n_timestamp` datetime DEFAULT NULL,
  `n` decimal(6,1) DEFAULT NULL,
  `n_trend` int(2) DEFAULT NULL,
  `o2_timestamp` datetime DEFAULT NULL,
  `o2` decimal(6,1) DEFAULT NULL,
  `o2_trend` int(2) DEFAULT NULL,
  `ph_timestamp` datetime DEFAULT NULL,
  `ph` decimal(6,1) DEFAULT NULL,
  `ph_trend` int(2) DEFAULT NULL,
  `cl_timestamp` datetime DEFAULT NULL,
  `cl` decimal(7,2) DEFAULT NULL,
  `cl_trend` int(2) DEFAULT NULL,
  `orp_timestamp` datetime DEFAULT NULL,
  `orp` decimal(6,1) DEFAULT NULL,
  `orp_trend` int(2) DEFAULT NULL,
  `last_mod` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `uuid` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



CREATE TABLE IF NOT EXISTS `waterways_timeline` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` varchar(36) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sensor_time` datetime NOT NULL,
  `w` decimal(6,1) DEFAULT NULL,
  `lt` decimal(6,1) DEFAULT NULL,
  `wt` decimal(6,1) DEFAULT NULL,
  `q` decimal(6,1) DEFAULT NULL,
  `va` decimal(6,1) DEFAULT NULL,
  `dfh` decimal(6,1) DEFAULT NULL,
  `lf` decimal(6,1) DEFAULT NULL,
  `wg` decimal(6,1) DEFAULT NULL,
  `wr` decimal(6,1) DEFAULT NULL,
  `gru` decimal(6,1) DEFAULT NULL,
  `hl` decimal(6,1) DEFAULT NULL,
  `pl` decimal(6,1) DEFAULT NULL,
  `n` decimal(6,1) DEFAULT NULL,
  `o2` decimal(6,1) DEFAULT NULL,
  `ph` decimal(6,1) DEFAULT NULL,
  `cl` decimal(7,2) DEFAULT NULL,
  `orp` decimal(6,1) DEFAULT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid_2` (`uid`,`sensor_time`)
) ENGINE=InnoDB AUTO_INCREMENT=222965284 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `waterways_timeline_w` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` varchar(36) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sensor_time` datetime NOT NULL,
  `w_recent` decimal(6,1) DEFAULT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid_2` (`uid`,`sensor_time`)
) ENGINE=InnoDB AUTO_INCREMENT=222965284 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `waterways_timeline_lt` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` varchar(36) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sensor_time` datetime NOT NULL,
  `lt_recent` decimal(6,1) DEFAULT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid_2` (`uid`,`sensor_time`)
) ENGINE=InnoDB AUTO_INCREMENT=222965284 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `waterways_timeline_wt` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` varchar(36) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sensor_time` datetime NOT NULL,
  `wt_recent` decimal(6,1) DEFAULT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid_2` (`uid`,`sensor_time`)
) ENGINE=InnoDB AUTO_INCREMENT=222965284 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `waterways_timeline_q` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` varchar(36) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sensor_time` datetime NOT NULL,
  `q_recent` decimal(6,1) DEFAULT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid_2` (`uid`,`sensor_time`)
) ENGINE=InnoDB AUTO_INCREMENT=222965284 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `waterways_timeline_va` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` varchar(36) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sensor_time` datetime NOT NULL,
  `va_recent` decimal(6,1) DEFAULT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid_2` (`uid`,`sensor_time`)
) ENGINE=InnoDB AUTO_INCREMENT=222965284 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `waterways_timeline_dfh` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` varchar(36) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sensor_time` datetime NOT NULL,
  `dfh_recent` decimal(6,1) DEFAULT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid_2` (`uid`,`sensor_time`)
) ENGINE=InnoDB AUTO_INCREMENT=222965284 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `waterways_timeline_lf` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` varchar(36) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sensor_time` datetime NOT NULL,
  `lf_recent` decimal(6,1) DEFAULT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid_2` (`uid`,`sensor_time`)
) ENGINE=InnoDB AUTO_INCREMENT=222965284 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `waterways_timeline_wg` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` varchar(36) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sensor_time` datetime NOT NULL,
  `wg_recent` decimal(6,1) DEFAULT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid_2` (`uid`,`sensor_time`)
) ENGINE=InnoDB AUTO_INCREMENT=222965284 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `waterways_timeline_wr` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` varchar(36) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sensor_time` datetime NOT NULL,
  `wr_recent` decimal(6,1) DEFAULT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid_2` (`uid`,`sensor_time`)
) ENGINE=InnoDB AUTO_INCREMENT=222965284 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `waterways_timeline_gru` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` varchar(36) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sensor_time` datetime NOT NULL,
  `gru_recent` decimal(6,1) DEFAULT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid_2` (`uid`,`sensor_time`)
) ENGINE=InnoDB AUTO_INCREMENT=222965284 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `waterways_timeline_hl` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` varchar(36) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sensor_time` datetime NOT NULL,
  `hl_recent` decimal(6,1) DEFAULT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid_2` (`uid`,`sensor_time`)
) ENGINE=InnoDB AUTO_INCREMENT=222965284 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `waterways_timeline_pl` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` varchar(36) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sensor_time` datetime NOT NULL,
  `pl_recent` decimal(6,1) DEFAULT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid_2` (`uid`,`sensor_time`)
) ENGINE=InnoDB AUTO_INCREMENT=222965284 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `waterways_timeline_n` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` varchar(36) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sensor_time` datetime NOT NULL,
  `n_recent` decimal(6,1) DEFAULT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid_2` (`uid`,`sensor_time`)
) ENGINE=InnoDB AUTO_INCREMENT=222965284 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `waterways_timeline_o2` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` varchar(36) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sensor_time` datetime NOT NULL,
  `o2_recent` decimal(6,1) DEFAULT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid_2` (`uid`,`sensor_time`)
) ENGINE=InnoDB AUTO_INCREMENT=222965284 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `waterways_timeline_ph` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` varchar(36) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sensor_time` datetime NOT NULL,
  `ph_recent` decimal(6,1) DEFAULT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid_2` (`uid`,`sensor_time`)
) ENGINE=InnoDB AUTO_INCREMENT=222965284 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `waterways_timeline_cl` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` varchar(36) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sensor_time` datetime NOT NULL,
  `cl_recent` decimal(7,2) DEFAULT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid_2` (`uid`,`sensor_time`)
) ENGINE=InnoDB AUTO_INCREMENT=222965284 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `waterways_timeline_orp` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` varchar(36) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sensor_time` datetime NOT NULL,
  `orp_recent` decimal(6,1) DEFAULT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid_2` (`uid`,`sensor_time`)
) ENGINE=InnoDB AUTO_INCREMENT=222965284 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
