<?php

	ini_set('display_errors',1);  
	error_reporting(E_ALL);

	require_once("../../db_connect.php");

	// Opens a connection to a mySQL server
	$connection=mysqli_connect('localhost', $username, $password);

	if (!$connection) {
		die("Not connected : " . mysqli_error($connection));
	}

	// THE MAGIC LINE THAT SOLVES ALL CHARSET TROUBLE!!1!11!!! 
	mysqli_set_charset($connection,'utf8');

	// Set the active mySQL database
	$db_selected = mysqli_select_db($connection,$database);

	if (!$db_selected) {
		die ("Can\'t use db : " . mysqli_error($connection));
	}

	$json_file = file_get_contents('https://www.pegelonline.wsv.de/webservices/rest-api/v2/stations.json?includeTimeseries=true&includeCurrentMeasurement=true&includeCharacteristicValues=true');

	// truncate the table
	mysqli_query($connection,"TRUNCATE TABLE `pegel_w`");
	mysqli_query($connection,"TRUNCATE TABLE `waterways_current`");

	function sensor_type($connection){
		
		global $data, $sensor, $i, $j;

		// set variables which every sensor should provide
		if (isset($data[$i]->uuid)) { $uuid = $data[$i]->uuid; };
		if (isset($data[$i]->number)) { $number = $data[$i]->number; };
		if (isset($data[$i]->shortname)) { $shortname = $data[$i]->shortname; };
		if (isset($data[$i]->longname)) { $longname = $data[$i]->longname; };
		if (isset($data[$i]->km)) { $km = $data[$i]->km; };
		if (isset($data[$i]->agency)) { $agency = $data[$i]->agency; };
		if (isset($data[$i]->longitude)) { $longitude = $data[$i]->longitude; };
		if (isset($data[$i]->latitude)) { $latitude = $data[$i]->latitude; };
		if (isset($data[$i]->water->shortname)) { $water_shortname = $data[$i]->water->shortname; };
		if (isset($data[$i]->water->longname)) { $water_longname = $data[$i]->water->longname; };
		
		$sql="INSERT INTO `pegel_w` (`uid`, `uid2`, `country`, `name`, `name_proper`, `km`, `agency`, `provider`, `provider_link`, `lng`, `lat`, `water_shortname`, `water_longname`, `sensor_time`) VALUES ('$uuid', '$number', 'de', '$shortname', '$longname', '$km', '$agency', 'Pegel Online / WSV', 'https://www.pegelonline.wsv.de/gast/stammdaten?pegelnr=$number', '$longitude', '$latitude', '$water_shortname', '$water_longname', '0000-00-00 00:00:00')";
		
		mysqli_query($connection,$sql);
		
		// set variables according to sensor type
		if ($sensor == "W") {
			if (isset($data[$i]->timeseries[$j]->shortname)) { $w_shortname = $data[$i]->timeseries[$j]->shortname; };
			if (isset($data[$i]->timeseries[$j]->longname)) { $w_longname = $data[$i]->timeseries[$j]->longname; };
			if (isset($data[$i]->timeseries[$j]->unit)) { $w_unit = $data[$i]->timeseries[$j]->unit; };
			//if (isset($data[$i]->timeseries[$j]->currentMeasurement->timestamp)) { $w_timestamp` = $data[$i]->timeseries[$j]->currentMeasurement->timestamp; };
			if (isset($data[$i]->timeseries[$j]->currentMeasurement->timestamp)) { $datetime = $data[$i]->timeseries[$j]->currentMeasurement->timestamp; $w_timestamp = date("Y-m-d H:i:s", strtotime($datetime)); };
			if (isset($data[$i]->timeseries[$j]->currentMeasurement->value)) { $w = $data[$i]->timeseries[$j]->currentMeasurement->value; };
			if (isset($data[$i]->timeseries[$j]->currentMeasurement->trend)) { $w_trend = $data[$i]->timeseries[$j]->currentMeasurement->trend; };
			if (isset($data[$i]->timeseries[$j]->currentMeasurement->stateMnwMhw)) { $w_stateMnwMhw = $data[$i]->timeseries[$j]->currentMeasurement->stateMnwMhw; };
			if (isset($data[$i]->timeseries[$j]->currentMeasurement->stateNswHsw)) { $w_stateNswHsw = $data[$i]->timeseries[$j]->currentMeasurement->stateNswHsw; };
			if (isset($data[$i]->timeseries[$j]->gaugeZero->unit)) {
				$w_gaugeZero_unit = $data[$i]->timeseries[$j]->gaugeZero->unit;
			} else {
				$w_gaugeZero_unit = "---";
			};
			if (isset($data[$i]->timeseries[$j]->gaugeZero->value)) {
				$w_gaugeZero = $data[$i]->timeseries[$j]->gaugeZero->value;
			} else {
				$w_gaugeZero = "---";
			};
			if (isset($data[$i]->timeseries[$j]->gaugeZero->validFrom)) {
				$w_gaugeZero_validFrom = $data[$i]->timeseries[$j]->gaugeZero->validFrom;
			} else {
				$w_gaugeZero_validFrom = "---";
			};
			
			// get characteristic values
			if (isset($data[$i]->timeseries[$j]->characteristicValues)) {
				// Cycle through array of possible characteristics
				for($k=0; $k<count($data[$i]->timeseries[$j]->characteristicValues); $k++){
					// set $sensor to value of shortname so we can identify it in the sensor_type function
					$characteristic = $data[$i]->timeseries[$j]->characteristicValues[$k]->shortname;
					if ($characteristic == "M_I") {
						if (isset($data[$i]->timeseries[$j]->characteristicValues[$k]->shortname)) { $M_I_shortname = $data[$i]->timeseries[$j]->characteristicValues[$k]->shortname; };
						if (isset($data[$i]->timeseries[$j]->characteristicValues[$k]->longname)) { $M_I_longname = $data[$i]->timeseries[$j]->characteristicValues[$k]->longname; };
						if (isset($data[$i]->timeseries[$j]->characteristicValues[$k]->unit)) { $M_I_unit = $data[$i]->timeseries[$j]->characteristicValues[$k]->unit; };
						if (isset($data[$i]->timeseries[$j]->characteristicValues[$k]->value)) { $M_I_value = $data[$i]->timeseries[$j]->characteristicValues[$k]->value; };
						if (isset($data[$i]->timeseries[$j]->characteristicValues[$k]->validFrom)) { 
							$datetime = array($data[$i]->timeseries[$j]->characteristicValues[$k]->validFrom); 
							$M_I_timestamp = date("Y-m-d", strtotime($datetime[0])); 
						};
						
						$sql="UPDATE `pegel_w` SET `M_I_shortname` =  '$M_I_shortname', `M_I_longname` =  '$M_I_longname', `M_I_unit` = '$M_I_unit', `M_I_validFrom` = '$M_I_timestamp', `M_I_value` = '$M_I_value' WHERE `uid` = '$uuid'";

						mysqli_query($connection,$sql);
					};
					if ($characteristic == "M_II") {
						if (isset($data[$i]->timeseries[$j]->characteristicValues[$k]->shortname)) { $M_II_shortname = $data[$i]->timeseries[$j]->characteristicValues[$k]->shortname; };
						if (isset($data[$i]->timeseries[$j]->characteristicValues[$k]->longname)) { $M_II_longname = $data[$i]->timeseries[$j]->characteristicValues[$k]->longname; };
						if (isset($data[$i]->timeseries[$j]->characteristicValues[$k]->unit)) { $M_II_unit = $data[$i]->timeseries[$j]->characteristicValues[$k]->unit; };
						if (isset($data[$i]->timeseries[$j]->characteristicValues[$k]->value)) { $M_II_value = $data[$i]->timeseries[$j]->characteristicValues[$k]->value; };
						if (isset($data[$i]->timeseries[$j]->characteristicValues[$k]->validFrom)) { $datetime = $data[$i]->timeseries[$j]->characteristicValues[$k]->validFrom; $M_II_timestamp = date("Y-m-d", strtotime($datetime)); };
						
						$sql="UPDATE `pegel_w` SET `M_II_shortname` =  '$M_II_shortname', `M_II_longname` =  '$M_II_longname', `M_II_unit` = '$M_II_unit', `M_II_validFrom` = '$M_II_timestamp', `M_II_value` = '$M_II_value' WHERE `uid` = '$uuid'";

						mysqli_query($connection,$sql);
					};
					if ($characteristic == "M_III") {
						if (isset($data[$i]->timeseries[$j]->characteristicValues[$k]->shortname)) { $M_III_shortname = $data[$i]->timeseries[$j]->characteristicValues[$k]->shortname; };
						if (isset($data[$i]->timeseries[$j]->characteristicValues[$k]->longname)) { $M_III_longname = $data[$i]->timeseries[$j]->characteristicValues[$k]->longname; };
						if (isset($data[$i]->timeseries[$j]->characteristicValues[$k]->unit)) { $M_III_unit = $data[$i]->timeseries[$j]->characteristicValues[$k]->unit; };
						if (isset($data[$i]->timeseries[$j]->characteristicValues[$k]->value)) { $M_III_value = $data[$i]->timeseries[$j]->characteristicValues[$k]->value; };
						if (isset($data[$i]->timeseries[$j]->characteristicValues[$k]->validFrom)) { $datetime = $data[$i]->timeseries[$j]->characteristicValues[$k]->validFrom; $M_III_timestamp = date("Y-m-d", strtotime($datetime)); };
						
						$sql="UPDATE `pegel_w` SET `M_III_shortname` =  '$M_III_shortname', `M_III_longname` =  '$M_III_longname', `M_III_unit` = '$M_III_unit', `M_III_validFrom` = '$M_III_timestamp', `M_III_value` = '$M_III_value' WHERE `uid` = '$uuid'";

						mysqli_query($connection,$sql);
					};	
					if ($characteristic == "HSW") {
						if (isset($data[$i]->timeseries[$j]->characteristicValues[$k]->shortname)) { $HSW_shortname = $data[$i]->timeseries[$j]->characteristicValues[$k]->shortname; };
						if (isset($data[$i]->timeseries[$j]->characteristicValues[$k]->longname)) { $HSW_longname = $data[$i]->timeseries[$j]->characteristicValues[$k]->longname; };
						if (isset($data[$i]->timeseries[$j]->characteristicValues[$k]->unit)) { $HSW_unit = $data[$i]->timeseries[$j]->characteristicValues[$k]->unit; };
						if (isset($data[$i]->timeseries[$j]->characteristicValues[$k]->value)) { $HSW_value = $data[$i]->timeseries[$j]->characteristicValues[$k]->value; };
						if (isset($data[$i]->timeseries[$j]->characteristicValues[$k]->validFrom)) { $datetime = $data[$i]->timeseries[$j]->characteristicValues[$k]->validFrom; $HSW_timestamp = date("Y-m-d", strtotime($datetime)); };
						
						$sql="UPDATE `pegel_w` SET `HSW_shortname` =  '$HSW_shortname', `HSW_longname` =  '$HSW_longname', `HSW_unit` = '$HSW_unit', `HSW_validFrom` = '$HSW_timestamp', `HSW_value` = '$HSW_value' WHERE `uid` = '$uuid'";

						mysqli_query($connection,$sql);
					};	
					if ($characteristic == "RNW") {
						if (isset($data[$i]->timeseries[$j]->characteristicValues[$k]->shortname)) { $RNW_shortname = $data[$i]->timeseries[$j]->characteristicValues[$k]->shortname; };
						if (isset($data[$i]->timeseries[$j]->characteristicValues[$k]->longname)) { $RNW_longname = $data[$i]->timeseries[$j]->characteristicValues[$k]->longname; };
						if (isset($data[$i]->timeseries[$j]->characteristicValues[$k]->unit)) { $RNW_unit = $data[$i]->timeseries[$j]->characteristicValues[$k]->unit; };
						if (isset($data[$i]->timeseries[$j]->characteristicValues[$k]->value)) { $RNW_value = $data[$i]->timeseries[$j]->characteristicValues[$k]->value; };
						if (isset($data[$i]->timeseries[$j]->characteristicValues[$k]->validFrom)) { $datetime = $data[$i]->timeseries[$j]->characteristicValues[$k]->validFrom; $RNW_timestamp = date("Y-m-d", strtotime($datetime)); };
						
						$sql="UPDATE `pegel_w` SET `RNW_shortname` =  '$RNW_shortname', `RNW_longname` =  '$RNW_longname', `RNW_unit` = '$RNW_unit', `RNW_validFrom` = '$RNW_timestamp', `RNW_value` = '$RNW_value' WHERE `uid` = '$uuid'";

						mysqli_query($connection,$sql);
					};	
					if ($characteristic == "ZS_I") {
						if (isset($data[$i]->timeseries[$j]->characteristicValues[$k]->shortname)) { $ZS_I_shortname = $data[$i]->timeseries[$j]->characteristicValues[$k]->shortname; };
						if (isset($data[$i]->timeseries[$j]->characteristicValues[$k]->longname)) { $ZS_I_longname = $data[$i]->timeseries[$j]->characteristicValues[$k]->longname; };
						if (isset($data[$i]->timeseries[$j]->characteristicValues[$k]->unit)) { $ZS_I_unit = $data[$i]->timeseries[$j]->characteristicValues[$k]->unit; };
						if (isset($data[$i]->timeseries[$j]->characteristicValues[$k]->value)) { $ZS_I_value = $data[$i]->timeseries[$j]->characteristicValues[$k]->value; };
						if (isset($data[$i]->timeseries[$j]->characteristicValues[$k]->validFrom)) { $datetime = $data[$i]->timeseries[$j]->characteristicValues[$k]->validFrom; $ZS_I_timestamp = date("Y-m-d", strtotime($datetime)); };
						
						$sql="UPDATE `pegel_w` SET `ZS_I_shortname` =  '$ZS_I_shortname', `ZS_I_longname` =  '$ZS_I_longname', `ZS_I_unit` = '$ZS_I_unit', `ZS_I_validFrom` = '$ZS_I_timestamp', `ZS_I_value` = '$ZS_I_value' WHERE `uid` = '$uuid'";

						mysqli_query($connection,$sql);
					};									
					if ($characteristic == "HHW") {
						if (isset($data[$i]->timeseries[$j]->characteristicValues[$k]->shortname)) { $HHW_shortname = $data[$i]->timeseries[$j]->characteristicValues[$k]->shortname; };
						if (isset($data[$i]->timeseries[$j]->characteristicValues[$k]->longname)) { $HHW_longname = $data[$i]->timeseries[$j]->characteristicValues[$k]->longname; };
						if (isset($data[$i]->timeseries[$j]->characteristicValues[$k]->unit)) { $HHW_unit = $data[$i]->timeseries[$j]->characteristicValues[$k]->unit; };
						if (isset($data[$i]->timeseries[$j]->characteristicValues[$k]->value)) { $HHW_value = $data[$i]->timeseries[$j]->characteristicValues[$k]->value; };
						if (isset($data[$i]->timeseries[$j]->characteristicValues[$k]->occurrences)) { $datetime = $data[$i]->timeseries[$j]->characteristicValues[$k]->occurrences[0]; $HHW_timestamp = date("Y-m-d", strtotime($datetime)); };
						
						$sql="UPDATE `pegel_w` SET `HHW_shortname` =  '$HHW_shortname', `HHW_longname` =  '$HHW_longname', `HHW_unit` = '$HHW_unit', `HHW_date` = '$HHW_timestamp', `HHW_value` = '$HHW_value' WHERE `uid` = '$uuid'";

						mysqli_query($connection,$sql);
					};	
					if ($characteristic == "NNW") {
						if (isset($data[$i]->timeseries[$j]->characteristicValues[$k]->shortname)) { $NNW_shortname = $data[$i]->timeseries[$j]->characteristicValues[$k]->shortname; };
						if (isset($data[$i]->timeseries[$j]->characteristicValues[$k]->longname)) { $NNW_longname = $data[$i]->timeseries[$j]->characteristicValues[$k]->longname; };
						if (isset($data[$i]->timeseries[$j]->characteristicValues[$k]->unit)) { $NNW_unit = $data[$i]->timeseries[$j]->characteristicValues[$k]->unit; };
						if (isset($data[$i]->timeseries[$j]->characteristicValues[$k]->value)) { $NNW_value = $data[$i]->timeseries[$j]->characteristicValues[$k]->value; };
						if (isset($data[$i]->timeseries[$j]->characteristicValues[$k]->occurrences)) { $datetime = $data[$i]->timeseries[$j]->characteristicValues[$k]->occurrences[0]; $NNW_timestamp = date("Y-m-d", strtotime($datetime)); };
						
						$sql="UPDATE `pegel_w` SET `NNW_shortname` =  '$NNW_shortname', `NNW_longname` =  '$NNW_longname', `NNW_unit` = '$NNW_unit', `NNW_date` = '$NNW_timestamp', `NNW_value` = '$NNW_value' WHERE `uid` = '$uuid'";

						mysqli_query($connection,$sql);
					};										
					if ($characteristic == "MW") {
						if (isset($data[$i]->timeseries[$j]->characteristicValues[$k]->shortname)) { $MW_shortname = $data[$i]->timeseries[$j]->characteristicValues[$k]->shortname; };
						if (isset($data[$i]->timeseries[$j]->characteristicValues[$k]->longname)) { $MW_longname = $data[$i]->timeseries[$j]->characteristicValues[$k]->longname; };
						if (isset($data[$i]->timeseries[$j]->characteristicValues[$k]->unit)) { $MW_unit = $data[$i]->timeseries[$j]->characteristicValues[$k]->unit; };
						if (isset($data[$i]->timeseries[$j]->characteristicValues[$k]->value)) { $MW_value = $data[$i]->timeseries[$j]->characteristicValues[$k]->value; };
						if (isset($data[$i]->timeseries[$j]->characteristicValues[$k]->timespanStart)) { $datetime = $data[$i]->timeseries[$j]->characteristicValues[$k]->timespanStart; $MW_timespanStart = date("Y-m-d H:i:s", strtotime($datetime)); };
						if (isset($data[$i]->timeseries[$j]->characteristicValues[$k]->timespanEnd)) { $datetime = $data[$i]->timeseries[$j]->characteristicValues[$k]->timespanEnd; $MW_timespanEnd = date("Y-m-d H:i:s", strtotime($datetime)); };
						
						$sql="UPDATE `pegel_w` SET `mw_shortname` =  '$MW_shortname', `mw_longname` =  '$MW_longname', `mw_unit` = '$MW_unit', `mw_timespanStart` = '$MW_timespanStart', `mw_timespanEnd` = '$MW_timespanEnd', `mw_value` = '$MW_value' WHERE `uid` = '$uuid'";

						mysqli_query($connection,$sql);
					};
					if ($characteristic == "MNW") {
						if (isset($data[$i]->timeseries[$j]->characteristicValues[$k]->shortname)) { $MNW_shortname = $data[$i]->timeseries[$j]->characteristicValues[$k]->shortname; };
						if (isset($data[$i]->timeseries[$j]->characteristicValues[$k]->longname)) { $MNW_longname = $data[$i]->timeseries[$j]->characteristicValues[$k]->longname; };
						if (isset($data[$i]->timeseries[$j]->characteristicValues[$k]->unit)) { $MNW_unit = $data[$i]->timeseries[$j]->characteristicValues[$k]->unit; };
						if (isset($data[$i]->timeseries[$j]->characteristicValues[$k]->value)) { $MNW_value = $data[$i]->timeseries[$j]->characteristicValues[$k]->value; };
						if (isset($data[$i]->timeseries[$j]->characteristicValues[$k]->timespanStart)) { $datetime = $data[$i]->timeseries[$j]->characteristicValues[$k]->timespanStart; $MNW_timespanStart = date("Y-m-d H:i:s", strtotime($datetime)); };
						if (isset($data[$i]->timeseries[$j]->characteristicValues[$k]->timespanEnd)) { $datetime = $data[$i]->timeseries[$j]->characteristicValues[$k]->timespanEnd; $MNW_timespanEnd = date("Y-m-d H:i:s", strtotime($datetime)); };
						
						$sql="UPDATE `pegel_w` SET `mnw_shortname` =  '$MNW_shortname', `mnw_longname` =  '$MNW_longname', `mnw_unit` = '$MNW_unit', `mnw_timespanStart` = '$MNW_timespanStart', `mnw_timespanEnd` = '$MNW_timespanEnd', `mnw_value` = '$MNW_value' WHERE `uid` = '$uuid'";

						mysqli_query($connection,$sql);
					};
					if ($characteristic == "MHW") {
						if (isset($data[$i]->timeseries[$j]->characteristicValues[$k]->shortname)) { $MHW_shortname = $data[$i]->timeseries[$j]->characteristicValues[$k]->shortname; };
						if (isset($data[$i]->timeseries[$j]->characteristicValues[$k]->longname)) { $MHW_longname = $data[$i]->timeseries[$j]->characteristicValues[$k]->longname; };
						if (isset($data[$i]->timeseries[$j]->characteristicValues[$k]->unit)) { $MHW_unit = $data[$i]->timeseries[$j]->characteristicValues[$k]->unit; };
						if (isset($data[$i]->timeseries[$j]->characteristicValues[$k]->value)) { $MHW_value = $data[$i]->timeseries[$j]->characteristicValues[$k]->value; };
						if (isset($data[$i]->timeseries[$j]->characteristicValues[$k]->timespanStart)) { $datetime = $data[$i]->timeseries[$j]->characteristicValues[$k]->timespanStart; $MHW_timespanStart = date("Y-m-d H:i:s", strtotime($datetime)); };
						if (isset($data[$i]->timeseries[$j]->characteristicValues[$k]->timespanEnd)) { $datetime = $data[$i]->timeseries[$j]->characteristicValues[$k]->timespanEnd; $MHW_timespanEnd = date("Y-m-d H:i:s", strtotime($datetime)); };
						
						$sql="UPDATE `pegel_w` SET `mhw_shortname` =  '$MHW_shortname', `mhw_longname` =  '$MHW_longname', `mhw_unit` = '$MHW_unit', `mhw_timespanStart` = '$MHW_timespanStart', `mhw_timespanEnd` = '$MHW_timespanEnd', `mhw_value` = '$MHW_value' WHERE `uid` = '$uuid'";

						mysqli_query($connection,$sql);
					};		
					if ($characteristic == "MTnw") {
						if (isset($data[$i]->timeseries[$j]->characteristicValues[$k]->shortname)) { $MTnw_shortname = $data[$i]->timeseries[$j]->characteristicValues[$k]->shortname; };
						if (isset($data[$i]->timeseries[$j]->characteristicValues[$k]->longname)) { $MTnw_longname = $data[$i]->timeseries[$j]->characteristicValues[$k]->longname; };
						if (isset($data[$i]->timeseries[$j]->characteristicValues[$k]->unit)) { $MTnw_unit = $data[$i]->timeseries[$j]->characteristicValues[$k]->unit; };
						if (isset($data[$i]->timeseries[$j]->characteristicValues[$k]->value)) { $MTnw_value = $data[$i]->timeseries[$j]->characteristicValues[$k]->value; };
						if (isset($data[$i]->timeseries[$j]->characteristicValues[$k]->timespanStart)) { $datetime = $data[$i]->timeseries[$j]->characteristicValues[$k]->timespanStart; $MTnw_timespanStart = date("Y-m-d H:i:s", strtotime($datetime)); };
						if (isset($data[$i]->timeseries[$j]->characteristicValues[$k]->timespanEnd)) { $datetime = $data[$i]->timeseries[$j]->characteristicValues[$k]->timespanEnd; $MTnw_timespanEnd = date("Y-m-d H:i:s", strtotime($datetime)); };
						
						$sql="UPDATE `pegel_w` SET `MTnw_shortname` =  '$MTnw_shortname', `MTnw_longname` =  '$MTnw_longname', `MTnw_unit` = '$MTnw_unit', `MTnw_timespanStart` = '$MTnw_timespanStart', `MTnw_timespanEnd` = '$MTnw_timespanEnd', `MTnw_value` = '$MTnw_value' WHERE `uid` = '$uuid'";

						mysqli_query($connection,$sql);
					};	
					if ($characteristic == "MThw") {
						if (isset($data[$i]->timeseries[$j]->characteristicValues[$k]->shortname)) { $MThw_shortname = $data[$i]->timeseries[$j]->characteristicValues[$k]->shortname; };
						if (isset($data[$i]->timeseries[$j]->characteristicValues[$k]->longname)) { $MThw_longname = $data[$i]->timeseries[$j]->characteristicValues[$k]->longname; };
						if (isset($data[$i]->timeseries[$j]->characteristicValues[$k]->unit)) { $MThw_unit = $data[$i]->timeseries[$j]->characteristicValues[$k]->unit; };
						if (isset($data[$i]->timeseries[$j]->characteristicValues[$k]->value)) { $MThw_value = $data[$i]->timeseries[$j]->characteristicValues[$k]->value; };
						if (isset($data[$i]->timeseries[$j]->characteristicValues[$k]->timespanStart)) { $datetime = $data[$i]->timeseries[$j]->characteristicValues[$k]->timespanStart; $MThw_timespanStart = date("Y-m-d H:i:s", strtotime($datetime)); };
						if (isset($data[$i]->timeseries[$j]->characteristicValues[$k]->timespanEnd)) { $datetime = $data[$i]->timeseries[$j]->characteristicValues[$k]->timespanEnd; $MThw_timespanEnd = date("Y-m-d H:i:s", strtotime($datetime)); };
						
						$sql="UPDATE `pegel_w` SET `MThw_shortname` =  '$MThw_shortname', `MThw_longname` =  '$MThw_longname', `MThw_unit` = '$MThw_unit', `MThw_timespanStart` = '$MThw_timespanStart', `MThw_timespanEnd` = '$MThw_timespanEnd', `MThw_value` = '$MThw_value' WHERE `uid` = '$uuid'";

						mysqli_query($connection,$sql);
					};
					if ($characteristic == "NTnw") {
						if (isset($data[$i]->timeseries[$j]->characteristicValues[$k]->shortname)) { $NTnw_shortname = $data[$i]->timeseries[$j]->characteristicValues[$k]->shortname; };
						if (isset($data[$i]->timeseries[$j]->characteristicValues[$k]->longname)) { $NTnw_longname = $data[$i]->timeseries[$j]->characteristicValues[$k]->longname; };
						if (isset($data[$i]->timeseries[$j]->characteristicValues[$k]->unit)) { $NTnw_unit = $data[$i]->timeseries[$j]->characteristicValues[$k]->unit; };
						if (isset($data[$i]->timeseries[$j]->characteristicValues[$k]->value)) { $NTnw_value = $data[$i]->timeseries[$j]->characteristicValues[$k]->value; };
						if (isset($data[$i]->timeseries[$j]->characteristicValues[$k]->timespanStart)) { $datetime = $data[$i]->timeseries[$j]->characteristicValues[$k]->timespanStart; $NTnw_timespanStart = date("Y-m-d H:i:s", strtotime($datetime)); };
						if (isset($data[$i]->timeseries[$j]->characteristicValues[$k]->timespanEnd)) { $datetime = $data[$i]->timeseries[$j]->characteristicValues[$k]->timespanEnd; $NTnw_timespanEnd = date("Y-m-d H:i:s", strtotime($datetime)); };
						if (isset($data[$i]->timeseries[$j]->characteristicValues[$k]->occurrences)) { $datetime = $data[$i]->timeseries[$j]->characteristicValues[$k]->occurrences[0]; $NTnw_timestamp = date("Y-m-d", strtotime($datetime)); };
						
						$sql="UPDATE `pegel_w` SET `NTnw_shortname` =  '$NTnw_shortname', `NTnw_longname` =  '$NTnw_longname', `NTnw_unit` = '$NTnw_unit', `NTnw_timespanStart` = '$NTnw_timespanStart', `NTnw_timespanEnd` = '$NTnw_timespanEnd', `NTnw_date` = '$NTnw_timestamp', `NTnw_value` = '$NTnw_value' WHERE `uid` = '$uuid'";

						mysqli_query($connection,$sql);
					};
					if ($characteristic == "HThw") {
						if (isset($data[$i]->timeseries[$j]->characteristicValues[$k]->shortname)) { $HThw_shortname = $data[$i]->timeseries[$j]->characteristicValues[$k]->shortname; };
						if (isset($data[$i]->timeseries[$j]->characteristicValues[$k]->longname)) { $HThw_longname = $data[$i]->timeseries[$j]->characteristicValues[$k]->longname; };
						if (isset($data[$i]->timeseries[$j]->characteristicValues[$k]->unit)) { $HThw_unit = $data[$i]->timeseries[$j]->characteristicValues[$k]->unit; };
						if (isset($data[$i]->timeseries[$j]->characteristicValues[$k]->value)) { $HThw_value = $data[$i]->timeseries[$j]->characteristicValues[$k]->value; };
						if (isset($data[$i]->timeseries[$j]->characteristicValues[$k]->timespanStart)) { $datetime = $data[$i]->timeseries[$j]->characteristicValues[$k]->timespanStart; $HThw_timespanStart = date("Y-m-d H:i:s", strtotime($datetime)); };
						if (isset($data[$i]->timeseries[$j]->characteristicValues[$k]->timespanEnd)) { $datetime = $data[$i]->timeseries[$j]->characteristicValues[$k]->timespanEnd; $HThw_timespanEnd = date("Y-m-d H:i:s", strtotime($datetime)); };
						if (isset($data[$i]->timeseries[$j]->characteristicValues[$k]->occurrences)) { $datetime = $data[$i]->timeseries[$j]->characteristicValues[$k]->occurrences[0]; $HThw_timestamp = date("Y-m-d", strtotime($datetime)); };
						
						$sql="UPDATE `pegel_w` SET `HThw_shortname` =  '$HThw_shortname', `HThw_longname` =  '$HThw_longname', `HThw_unit` = '$HThw_unit', `HThw_timespanStart` = '$HThw_timespanStart', `HThw_timespanEnd` = '$HThw_timespanEnd', `HThw_date` = '$HThw_timestamp', `HThw_value` = '$HThw_value' WHERE `uid` = '$uuid'";

						mysqli_query($connection,$sql);
					
					};												
				};
			};
			
			if (isset($data[$i]->timeseries[$j]->comment)) {
				$comment_short = $data[$i]->timeseries[$j]->comment->shortDescription;
				$comment_long = $data[$i]->timeseries[$j]->comment->longDescription;
			} else {
				$comment_long = null;
			};
			
			$sql="UPDATE `pegel_w` SET `w_shortname` = '$w_shortname', `w_longname` = '$w_longname', `w_unit` = '$w_unit', `w_timestamp` = '$w_timestamp', `w` = '$w', `w_trend` = '$w_trend', `w_stateMnwMhw` = '$w_stateMnwMhw', `w_stateNswHsw` = '$w_stateNswHsw', `w_gaugeZero_unit` = '$w_gaugeZero_unit', `w_gaugeZero_value` = '$w_gaugeZero', `w_gaugeZero_validFrom` = '$w_gaugeZero_validFrom', `w_comment`= '$comment_long', `sensor_time` = CASE WHEN `w_timestamp` > `sensor_time` THEN `w_timestamp` ELSE `sensor_time` END  WHERE `uid` = '$uuid'";

			mysqli_query($connection,$sql);
			
		};
		if ($sensor == "LT") {
			if (isset($data[$i]->timeseries[$j]->shortname)) { $lt_shortname = $data[$i]->timeseries[$j]->shortname; };
			if (isset($data[$i]->timeseries[$j]->longname)) { $lt_longname = $data[$i]->timeseries[$j]->longname; };
			if (isset($data[$i]->timeseries[$j]->unit)) { $lt_unit = $data[$i]->timeseries[$j]->unit; };
			if (isset($data[$i]->timeseries[$j]->currentMeasurement->timestamp)) { $datetime = $data[$i]->timeseries[$j]->currentMeasurement->timestamp; $lt_timestamp = date("Y-m-d H:i:s", strtotime($datetime)); };
			if (isset($data[$i]->timeseries[$j]->currentMeasurement->value)) { $lt = $data[$i]->timeseries[$j]->currentMeasurement->value; };
			if (isset($data[$i]->timeseries[$j]->currentMeasurement->trend)) { $lt_trend = $data[$i]->timeseries[$j]->currentMeasurement->trend; };
			if (isset($data[$i]->timeseries[$j]->comment)) {
				$comment_short = $data[$i]->timeseries[$j]->comment->shortDescription;
				$comment_long = $data[$i]->timeseries[$j]->comment->longDescription;
			} else {
				$comment_long = null;
			};
			
			$sql="UPDATE `pegel_w` SET `lt_shortname` =  '$lt_shortname', `lt_longname` =  '$lt_longname', `lt_unit` = '$lt_unit', `lt_timestamp` = '$lt_timestamp', `lt` = '$lt', `lt_trend` = '$lt_trend', `lt_comment`= '$comment_long', `sensor_time` = CASE WHEN `lt_timestamp` > `sensor_time` THEN `lt_timestamp` ELSE `sensor_time` END WHERE `uid` = '$uuid'";

			mysqli_query($connection,$sql);
			
		};
		if ($sensor == "WT") {
			if (isset($data[$i]->timeseries[$j]->shortname)) { $wt_shortname = $data[$i]->timeseries[$j]->shortname; };
			if (isset($data[$i]->timeseries[$j]->longname)) { $wt_longname = $data[$i]->timeseries[$j]->longname; };
			if (isset($data[$i]->timeseries[$j]->unit)) { $wt_unit = $data[$i]->timeseries[$j]->unit; };
			if (isset($data[$i]->timeseries[$j]->currentMeasurement->timestamp)) { $datetime = $data[$i]->timeseries[$j]->currentMeasurement->timestamp; $wt_timestamp = date("Y-m-d H:i:s", strtotime($datetime)); };
			if (isset($data[$i]->timeseries[$j]->currentMeasurement->value)) { $wt = $data[$i]->timeseries[$j]->currentMeasurement->value; };
			if (isset($data[$i]->timeseries[$j]->currentMeasurement->trend)) { $wt_trend = $data[$i]->timeseries[$j]->currentMeasurement->trend; };
			if (isset($data[$i]->timeseries[$j]->comment)) {
				$comment_short = $data[$i]->timeseries[$j]->comment->shortDescription;
				$comment_long = $data[$i]->timeseries[$j]->comment->longDescription;
			} else {
				$comment_long = null;
			};
			
			$sql="UPDATE `pegel_w` SET `wt_shortname` =  '$wt_shortname', `wt_longname` =  '$wt_longname', `wt_unit` = '$wt_unit', `wt_timestamp` = '$wt_timestamp', `wt` = '$wt', `wt_trend` = '$wt_trend', `wt_comment`= '$comment_long', `sensor_time` = CASE WHEN `wt_timestamp` > `sensor_time` THEN `wt_timestamp` ELSE `sensor_time` END WHERE `uid` = '$uuid'";

			mysqli_query($connection,$sql);

		};
		if ($sensor == "Q") {
			if (isset($data[$i]->timeseries[$j]->shortname)) { $q_shortname = $data[$i]->timeseries[$j]->shortname; };
			if (isset($data[$i]->timeseries[$j]->longname)) { $q_longname = $data[$i]->timeseries[$j]->longname; };
			if (isset($data[$i]->timeseries[$j]->unit)) { $q_unit = $data[$i]->timeseries[$j]->unit; };
			if (isset($data[$i]->timeseries[$j]->currentMeasurement->timestamp)) { $datetime = $data[$i]->timeseries[$j]->currentMeasurement->timestamp; $q_timestamp = date("Y-m-d H:i:s", strtotime($datetime)); };
			if (isset($data[$i]->timeseries[$j]->currentMeasurement->value)) { $q = $data[$i]->timeseries[$j]->currentMeasurement->value; };
			if (isset($data[$i]->timeseries[$j]->currentMeasurement->trend)) { $q_trend = $data[$i]->timeseries[$j]->currentMeasurement->trend; };
			if (isset($data[$i]->timeseries[$j]->comment)) {
				$comment_short = $data[$i]->timeseries[$j]->comment->shortDescription;
				$comment_long = $data[$i]->timeseries[$j]->comment->longDescription;
			} else {
				$comment_long = null;
			};
			
			$sql="UPDATE `pegel_w` SET `q_shortname` =  '$q_shortname', `q_longname` =  '$q_longname', `q_unit` = '$q_unit', `q_timestamp` = '$q_timestamp', `q` = '$q', `q_trend` = '$q_trend', `q_comment`= '$comment_long', `sensor_time` = CASE WHEN `q_timestamp` > `sensor_time` THEN `q_timestamp` ELSE `sensor_time` END WHERE `uid` = '$uuid'";

			mysqli_query($connection,$sql);

		};
		if ($sensor == "VA") {
			if (isset($data[$i]->timeseries[$j]->shortname)) { $va_shortname = $data[$i]->timeseries[$j]->shortname; };
			if (isset($data[$i]->timeseries[$j]->longname)) { $va_longname = $data[$i]->timeseries[$j]->longname; };
			if (isset($data[$i]->timeseries[$j]->unit)) { $va_unit = $data[$i]->timeseries[$j]->unit; };
			if (isset($data[$i]->timeseries[$j]->currentMeasurement->timestamp)) { $datetime = $data[$i]->timeseries[$j]->currentMeasurement->timestamp; $va_timestamp = date("Y-m-d H:i:s", strtotime($datetime)); };
			if (isset($data[$i]->timeseries[$j]->currentMeasurement->value)) { $va = $data[$i]->timeseries[$j]->currentMeasurement->value; };
			if (isset($data[$i]->timeseries[$j]->currentMeasurement->trend)) { $va_trend = $data[$i]->timeseries[$j]->currentMeasurement->trend; };
			if (isset($data[$i]->timeseries[$j]->comment)) {
				$comment_short = $data[$i]->timeseries[$j]->comment->shortDescription;
				$comment_long = $data[$i]->timeseries[$j]->comment->longDescription;
			} else {
				$comment_long = null;
			};
			
			$sql="UPDATE `pegel_w` SET `va_shortname` =  '$va_shortname', `va_longname` =  '$va_longname', `va_unit` = '$va_unit', `va_timestamp` = '$va_timestamp', `va` = '$va', `va_trend` = '$va_trend', `va_comment`= '$comment_long', `sensor_time` = CASE WHEN `va_timestamp` > `sensor_time` THEN `va_timestamp` ELSE `sensor_time` END WHERE `uid` = '$uuid'";

			mysqli_query($connection,$sql);

		};
		if ($sensor == "DFH") {
			if (isset($data[$i]->timeseries[$j]->shortname)) { $dfh_shortname = $data[$i]->timeseries[$j]->shortname; };
			if (isset($data[$i]->timeseries[$j]->longname)) { $dfh_longname = $data[$i]->timeseries[$j]->longname; };
			if (isset($data[$i]->timeseries[$j]->unit)) { $dfh_unit = $data[$i]->timeseries[$j]->unit; };
			if (isset($data[$i]->timeseries[$j]->currentMeasurement->timestamp)) { $datetime = $data[$i]->timeseries[$j]->currentMeasurement->timestamp; $dfh_timestamp = date("Y-m-d H:i:s", strtotime($datetime)); };
			if (isset($data[$i]->timeseries[$j]->currentMeasurement->value)) { $dfh = $data[$i]->timeseries[$j]->currentMeasurement->value; };
			if (isset($data[$i]->timeseries[$j]->currentMeasurement->trend)) { $dfh_trend = $data[$i]->timeseries[$j]->currentMeasurement->trend; };
			if (isset($data[$i]->timeseries[$j]->comment)) {
				$comment_short = $data[$i]->timeseries[$j]->comment->shortDescription;
				$comment_long = $data[$i]->timeseries[$j]->comment->longDescription;
			} else {
				$comment_long = null;
			};
			
			$sql="UPDATE `pegel_w` SET `dfh_shortname` =  '$dfh_shortname', `dfh_longname` =  '$dfh_longname', `dfh_unit` = '$dfh_unit', `dfh_timestamp` = '$dfh_timestamp', `dfh` = '$dfh', `dfh_trend` = '$dfh_trend', `dfh_comment`= '$comment_long', `sensor_time` = CASE WHEN `dfh_timestamp` > `sensor_time` THEN `dfh_timestamp` ELSE `sensor_time` END WHERE `uid` = '$uuid'";

			mysqli_query($connection,$sql);

		};
		if ($sensor == "LF") {
			if (isset($data[$i]->timeseries[$j]->shortname)) { $lf_shortname = $data[$i]->timeseries[$j]->shortname; };
			if (isset($data[$i]->timeseries[$j]->longname)) { $lf_longname = $data[$i]->timeseries[$j]->longname; };
			if (isset($data[$i]->timeseries[$j]->unit)) { $lf_unit = $data[$i]->timeseries[$j]->unit; };
			if (isset($data[$i]->timeseries[$j]->currentMeasurement->timestamp)) { $datetime = $data[$i]->timeseries[$j]->currentMeasurement->timestamp; $lf_timestamp = date("Y-m-d H:i:s", strtotime($datetime)); };
			if (isset($data[$i]->timeseries[$j]->currentMeasurement->value)) { $lf = $data[$i]->timeseries[$j]->currentMeasurement->value; };
			if (isset($data[$i]->timeseries[$j]->currentMeasurement->trend)) { $lf_trend = $data[$i]->timeseries[$j]->currentMeasurement->trend; };
			if (isset($data[$i]->timeseries[$j]->comment)) {
				$comment_short = $data[$i]->timeseries[$j]->comment->shortDescription;
				$comment_long = $data[$i]->timeseries[$j]->comment->longDescription;
			} else {
				$comment_long = null;
			};
			
			$sql="UPDATE `pegel_w` SET `lf_shortname` =  '$lf_shortname', `lf_longname` =  '$lf_longname', `lf_unit` = '$lf_unit', `lf_timestamp` = '$lf_timestamp', `lf` = '$lf', `lf_trend` = '$lf_trend', `lf_comment`= '$comment_long', `sensor_time` = CASE WHEN `lf_timestamp` > `sensor_time` THEN `lf_timestamp` ELSE `sensor_time` END WHERE `uid` = '$uuid'";

			mysqli_query($connection,$sql);

		};
		if ($sensor == "WG") {
			if (isset($data[$i]->timeseries[$j]->shortname)) { $wg_shortname = $data[$i]->timeseries[$j]->shortname; };
			if (isset($data[$i]->timeseries[$j]->longname)) { $wg_longname = $data[$i]->timeseries[$j]->longname; };
			if (isset($data[$i]->timeseries[$j]->unit)) { $wg_unit = $data[$i]->timeseries[$j]->unit; };
			if (isset($data[$i]->timeseries[$j]->currentMeasurement->timestamp)) { $datetime = $data[$i]->timeseries[$j]->currentMeasurement->timestamp; $wg_timestamp = date("Y-m-d H:i:s", strtotime($datetime)); };
			if (isset($data[$i]->timeseries[$j]->currentMeasurement->value)) { $wg = $data[$i]->timeseries[$j]->currentMeasurement->value; };
			if (isset($data[$i]->timeseries[$j]->currentMeasurement->trend)) { $wg_trend = $data[$i]->timeseries[$j]->currentMeasurement->trend; };
			if (isset($data[$i]->timeseries[$j]->comment)) {
				$comment_short = $data[$i]->timeseries[$j]->comment->shortDescription;
				$comment_long = $data[$i]->timeseries[$j]->comment->longDescription;
			} else {
				$comment_long = null;
			};
			
			$sql="UPDATE `pegel_w` SET `wg_shortname` =  '$wg_shortname', `wg_longname` =  '$wg_longname', `wg_unit` = '$wg_unit', `wg_timestamp` = '$wg_timestamp', `wg` = '$wg', `wg_trend` = '$wg_trend', `wg_comment`= '$comment_long', `sensor_time` = CASE WHEN `wg_timestamp` > `sensor_time` THEN `wg_timestamp` ELSE `sensor_time` END WHERE `uid` = '$uuid'";

			mysqli_query($connection,$sql);

		};
		if ($sensor == "WR") {
			if (isset($data[$i]->timeseries[$j]->shortname)) { $wr_shortname = $data[$i]->timeseries[$j]->shortname; };
			if (isset($data[$i]->timeseries[$j]->longname)) { $wr_longname = $data[$i]->timeseries[$j]->longname; };
			if (isset($data[$i]->timeseries[$j]->unit)) { $wr_unit = $data[$i]->timeseries[$j]->unit; };
			if (isset($data[$i]->timeseries[$j]->currentMeasurement->timestamp)) { $datetime = $data[$i]->timeseries[$j]->currentMeasurement->timestamp; $wr_timestamp = date("Y-m-d H:i:s", strtotime($datetime)); };
			if (isset($data[$i]->timeseries[$j]->currentMeasurement->value)) { $wr = $data[$i]->timeseries[$j]->currentMeasurement->value; };
			if (isset($data[$i]->timeseries[$j]->currentMeasurement->trend)) { $wr_trend = $data[$i]->timeseries[$j]->currentMeasurement->trend; };
			if (isset($data[$i]->timeseries[$j]->comment)) {
				$comment_short = $data[$i]->timeseries[$j]->comment->shortDescription;
				$comment_long = $data[$i]->timeseries[$j]->comment->longDescription;
			} else {
				$comment_long = null;
			};
			
			$sql="UPDATE `pegel_w` SET `wr_shortname` =  '$wr_shortname', `wr_longname` =  '$wr_longname', `wr_unit` = '$wr_unit', `wr_timestamp` = '$wr_timestamp', `wr` = '$wr', `wr_trend` = '$wr_trend', `wr_comment`= '$comment_long', `sensor_time` = CASE WHEN `wr_timestamp` > `sensor_time` THEN `wr_timestamp` ELSE `sensor_time` END WHERE `uid` = '$uuid'";

			mysqli_query($connection,$sql);

		};
		if ($sensor == "GRU") {
			if (isset($data[$i]->timeseries[$j]->shortname)) { $gru_shortname = $data[$i]->timeseries[$j]->shortname; };
			if (isset($data[$i]->timeseries[$j]->longname)) { $gru_longname = $data[$i]->timeseries[$j]->longname; };
			if (isset($data[$i]->timeseries[$j]->unit)) { $gru_unit = $data[$i]->timeseries[$j]->unit; };
			if (isset($data[$i]->timeseries[$j]->currentMeasurement->timestamp)) { $datetime = $data[$i]->timeseries[$j]->currentMeasurement->timestamp; $gru_timestamp = date("Y-m-d H:i:s", strtotime($datetime)); };
			if (isset($data[$i]->timeseries[$j]->currentMeasurement->value)) { $gru = $data[$i]->timeseries[$j]->currentMeasurement->value; };
			if (isset($data[$i]->timeseries[$j]->currentMeasurement->trend)) { $gru_trend = $data[$i]->timeseries[$j]->currentMeasurement->trend; };
			if (isset($data[$i]->timeseries[$j]->comment)) {
				$comment_short = $data[$i]->timeseries[$j]->comment->shortDescription;
				$comment_long = $data[$i]->timeseries[$j]->comment->longDescription;
			} else {
				$comment_long = null;
			};
			
			$sql="UPDATE `pegel_w` SET `gru_shortname` =  '$gru_shortname', `gru_longname` =  '$gru_longname', `gru_unit` = '$gru_unit', `gru_timestamp` = '$gru_timestamp', `gru` = '$gru', `gru_trend` = '$gru_trend', `gru_comment`= '$comment_long', `sensor_time` = CASE WHEN `gru_timestamp` > `sensor_time` THEN `gru_timestamp` ELSE `sensor_time` END WHERE `uid` = '$uuid'";

			mysqli_query($connection,$sql);

		};
		if ($sensor == "HL") {
			if (isset($data[$i]->timeseries[$j]->shortname)) { $hl_shortname = $data[$i]->timeseries[$j]->shortname; };
			if (isset($data[$i]->timeseries[$j]->longname)) { $hl_longname = $data[$i]->timeseries[$j]->longname; };
			if (isset($data[$i]->timeseries[$j]->unit)) { $hl_unit = $data[$i]->timeseries[$j]->unit; };
			if (isset($data[$i]->timeseries[$j]->currentMeasurement->timestamp)) { $datetime = $data[$i]->timeseries[$j]->currentMeasurement->timestamp; $hl_timestamp = date("Y-m-d H:i:s", strtotime($datetime)); };
			if (isset($data[$i]->timeseries[$j]->currentMeasurement->value)) { $hl = $data[$i]->timeseries[$j]->currentMeasurement->value; };
			if (isset($data[$i]->timeseries[$j]->currentMeasurement->trend)) { $hl_trend = $data[$i]->timeseries[$j]->currentMeasurement->trend; };
			if (isset($data[$i]->timeseries[$j]->comment)) {
				$comment_short = $data[$i]->timeseries[$j]->comment->shortDescription;
				$comment_long = $data[$i]->timeseries[$j]->comment->longDescription;
			} else {
				$comment_long = null;
			};
			
			$sql="UPDATE `pegel_w` SET `hl_shortname` =  '$hl_shortname', `hl_longname` =  '$hl_longname', `hl_unit` = '$hl_unit', `hl_timestamp` = '$hl_timestamp', `hl` = '$hl', `hl_trend` = '$hl_trend', `hl_comment`= '$comment_long', `sensor_time` = CASE WHEN `hl_timestamp` > `sensor_time` THEN `hl_timestamp` ELSE `sensor_time` END WHERE `uid` = '$uuid'";

			mysqli_query($connection,$sql);

		};
		if ($sensor == "PL") {
			if (isset($data[$i]->timeseries[$j]->shortname)) { $pl_shortname = $data[$i]->timeseries[$j]->shortname; };
			if (isset($data[$i]->timeseries[$j]->longname)) { $pl_longname = $data[$i]->timeseries[$j]->longname; };
			if (isset($data[$i]->timeseries[$j]->unit)) { $pl_unit = $data[$i]->timeseries[$j]->unit; };
			if (isset($data[$i]->timeseries[$j]->currentMeasurement->timestamp)) { $datetime = $data[$i]->timeseries[$j]->currentMeasurement->timestamp; $pl_timestamp = date("Y-m-d H:i:s", strtotime($datetime)); };
			if (isset($data[$i]->timeseries[$j]->currentMeasurement->value)) { $pl = $data[$i]->timeseries[$j]->currentMeasurement->value; };
			if (isset($data[$i]->timeseries[$j]->currentMeasurement->trend)) { $pl_trend = $data[$i]->timeseries[$j]->currentMeasurement->trend; };
			if (isset($data[$i]->timeseries[$j]->comment)) {
				$comment_short = $data[$i]->timeseries[$j]->comment->shortDescription;
				$comment_long = $data[$i]->timeseries[$j]->comment->longDescription;
			} else {
				$comment_long = null;
			};
			
			$sql="UPDATE `pegel_w` SET `pl_shortname` =  '$pl_shortname', `pl_longname` =  '$pl_longname', `pl_unit` = '$pl_unit', `pl_timestamp` = '$pl_timestamp', `pl` = '$pl', `pl_trend` = '$pl_trend', `pl_comment`= '$comment_long', `sensor_time` = CASE WHEN `pl_timestamp` > `sensor_time` THEN `pl_timestamp` ELSE `sensor_time` END WHERE `uid` = '$uuid'";

			mysqli_query($connection,$sql);

		};
		if ($sensor == "NIEDERSCHLAG") {
			if (isset($data[$i]->timeseries[$j]->shortname)) { $n_shortname = $data[$i]->timeseries[$j]->shortname; };
			if (isset($data[$i]->timeseries[$j]->longname)) { $n_longname = $data[$i]->timeseries[$j]->longname; };
			if (isset($data[$i]->timeseries[$j]->unit)) { $n_unit = $data[$i]->timeseries[$j]->unit; };
			if (isset($data[$i]->timeseries[$j]->currentMeasurement->timestamp)) { $datetime = $data[$i]->timeseries[$j]->currentMeasurement->timestamp; $n_timestamp = date("Y-m-d H:i:s", strtotime($datetime)); };
			if (isset($data[$i]->timeseries[$j]->currentMeasurement->value)) { $n = $data[$i]->timeseries[$j]->currentMeasurement->value; };
			if (isset($data[$i]->timeseries[$j]->currentMeasurement->trend)) { $n_trend = $data[$i]->timeseries[$j]->currentMeasurement->trend; };
			if (isset($data[$i]->timeseries[$j]->comment)) {
				$comment_short = $data[$i]->timeseries[$j]->comment->shortDescription;
				$comment_long = $data[$i]->timeseries[$j]->comment->longDescription;
			} else {
				$comment_long = null;
			};
			
			$sql="UPDATE `pegel_w` SET `n_shortname` =  '$n_shortname', `n_longname` =  '$n_longname', `n_unit` = '$n_unit', `n_timestamp` = '$n_timestamp', `n` = '$n', `n_trend` = '$n_trend', `n_comment`= '$comment_long', `sensor_time` = CASE WHEN `n_timestamp` > `sensor_time` THEN `n_timestamp` ELSE `sensor_time` END WHERE `uid` = '$uuid'";

			mysqli_query($connection,$sql);

		};
		if ($sensor == "NIEDERSCHLAGSINTENSITÄT") {
			if (isset($data[$i]->timeseries[$j]->shortname)) { $n_intensity_shortname = $data[$i]->timeseries[$j]->shortname; };
			if (isset($data[$i]->timeseries[$j]->longname)) { $n_intensity_longname = $data[$i]->timeseries[$j]->longname; };
			if (isset($data[$i]->timeseries[$j]->unit)) { $n_intensity_unit = $data[$i]->timeseries[$j]->unit; };
			if (isset($data[$i]->timeseries[$j]->currentMeasurement->timestamp)) { $datetime = $data[$i]->timeseries[$j]->currentMeasurement->timestamp; $n_intensity_timestamp = date("Y-m-d H:i:s", strtotime($datetime)); };
			if (isset($data[$i]->timeseries[$j]->currentMeasurement->value)) { $n_intensity = $data[$i]->timeseries[$j]->currentMeasurement->value; };
			if (isset($data[$i]->timeseries[$j]->currentMeasurement->trend)) { $n_intensity_trend = $data[$i]->timeseries[$j]->currentMeasurement->trend; };
			if (isset($data[$i]->timeseries[$j]->comment)) {
				$comment_short = $data[$i]->timeseries[$j]->comment->shortDescription;
				$comment_long = $data[$i]->timeseries[$j]->comment->longDescription;
			} else {
				$comment_long = null;
			};
			
			$sql="UPDATE `pegel_w` SET `n_intensity_shortname` =  '$n_intensity_shortname', `n_intensity_longname` =  '$n_intensity_longname', `n_intensity_unit` = '$n_intensity_unit', `n_intensity_timestamp` = '$n_intensity_timestamp', `n_intensity` = '$n_intensity', `n_intensity_trend` = '$n_intensity_trend', `n_intensity_comment`= '$comment_long', `sensor_time` = CASE WHEN `n_intensity_timestamp` > `sensor_time` THEN `n_intensity_timestamp` ELSE `sensor_time` END WHERE `uid` = '$uuid'";

			mysqli_query($connection,$sql);

		};
		if ($sensor == "O2") {
			if (isset($data[$i]->timeseries[$j]->shortname)) { $o2_shortname = $data[$i]->timeseries[$j]->shortname; };
			if (isset($data[$i]->timeseries[$j]->longname)) { $o2_longname = $data[$i]->timeseries[$j]->longname; };
			if (isset($data[$i]->timeseries[$j]->unit)) { $o2_unit = $data[$i]->timeseries[$j]->unit; };
			if (isset($data[$i]->timeseries[$j]->currentMeasurement->timestamp)) { $datetime = $data[$i]->timeseries[$j]->currentMeasurement->timestamp; $o2_timestamp = date("Y-m-d H:i:s", strtotime($datetime)); };
			if (isset($data[$i]->timeseries[$j]->currentMeasurement->value)) { $o2 = $data[$i]->timeseries[$j]->currentMeasurement->value; };
			if (isset($data[$i]->timeseries[$j]->currentMeasurement->trend)) { $o2_trend = $data[$i]->timeseries[$j]->currentMeasurement->trend; };
			if (isset($data[$i]->timeseries[$j]->comment)) {
				$comment_short = $data[$i]->timeseries[$j]->comment->shortDescription;
				$comment_long = $data[$i]->timeseries[$j]->comment->longDescription;
			} else {
				$comment_long = null;
			};
			
			$sql="UPDATE `pegel_w` SET `o2_shortname` =  '$o2_shortname', `o2_longname` =  '$o2_longname', `o2_unit` = '$o2_unit', `o2_timestamp` = '$o2_timestamp', `o2` = '$o2', `o2_trend` = '$o2_trend', `o2_comment`= '$comment_long', `sensor_time` = CASE WHEN `o2_timestamp` > `sensor_time` THEN `o2_timestamp` ELSE `sensor_time` END WHERE `uid` = '$uuid'";

			mysqli_query($connection,$sql);

		};
		if ($sensor == "PH") {
			if (isset($data[$i]->timeseries[$j]->shortname)) { $ph_shortname = $data[$i]->timeseries[$j]->shortname; };
			if (isset($data[$i]->timeseries[$j]->longname)) { $ph_longname = $data[$i]->timeseries[$j]->longname; };
			if (isset($data[$i]->timeseries[$j]->unit)) { $ph_unit = $data[$i]->timeseries[$j]->unit; };
			if (isset($data[$i]->timeseries[$j]->currentMeasurement->timestamp)) { $datetime = $data[$i]->timeseries[$j]->currentMeasurement->timestamp; $ph_timestamp = date("Y-m-d H:i:s", strtotime($datetime)); };
			if (isset($data[$i]->timeseries[$j]->currentMeasurement->value)) { $ph = $data[$i]->timeseries[$j]->currentMeasurement->value; };
			if (isset($data[$i]->timeseries[$j]->currentMeasurement->trend)) { $ph_trend = $data[$i]->timeseries[$j]->currentMeasurement->trend; };
			if (isset($data[$i]->timeseries[$j]->comment)) {
				$comment_short = $data[$i]->timeseries[$j]->comment->shortDescription;
				$comment_long = $data[$i]->timeseries[$j]->comment->longDescription;
			} else {
				$comment_long = null;
			};
			
			$sql="UPDATE `pegel_w` SET `ph_shortname` =  '$ph_shortname', `ph_longname` =  '$ph_longname', `ph_unit` = '$ph_unit', `ph_timestamp` = '$ph_timestamp', `ph` = '$ph', `ph_trend` = '$ph_trend', `ph_comment`= '$comment_long', `sensor_time` = CASE WHEN `ph_timestamp` > `sensor_time` THEN `ph_timestamp` ELSE `sensor_time` END WHERE `uid` = '$uuid'";

			mysqli_query($connection,$sql);

		};
		if ($sensor == "CL") {
			if (isset($data[$i]->timeseries[$j]->shortname)) { $cl_shortname = $data[$i]->timeseries[$j]->shortname; };
			if (isset($data[$i]->timeseries[$j]->longname)) { $cl_longname = $data[$i]->timeseries[$j]->longname; };
			if (isset($data[$i]->timeseries[$j]->unit)) { $cl_unit = $data[$i]->timeseries[$j]->unit; };
			if (isset($data[$i]->timeseries[$j]->currentMeasurement->timestamp)) { $datetime = $data[$i]->timeseries[$j]->currentMeasurement->timestamp; $cl_timestamp = date("Y-m-d H:i:s", strtotime($datetime)); };
			if (isset($data[$i]->timeseries[$j]->currentMeasurement->value)) { $cl = $data[$i]->timeseries[$j]->currentMeasurement->value; };
			if (isset($data[$i]->timeseries[$j]->currentMeasurement->trend)) { $cl_trend = $data[$i]->timeseries[$j]->currentMeasurement->trend; };
			if (isset($data[$i]->timeseries[$j]->comment)) {
				$comment_short = $data[$i]->timeseries[$j]->comment->shortDescription;
				$comment_long = $data[$i]->timeseries[$j]->comment->longDescription;
			} else {
				$comment_long = null;
			};
			
			$sql="UPDATE `pegel_w` SET `cl_shortname` =  '$cl_shortname', `cl_longname` =  '$cl_longname', `cl_unit` = '$cl_unit', `cl_timestamp` = '$cl_timestamp', `cl` = '$cl', `cl_trend` = '$cl_trend', `cl_comment`= '$comment_long', `sensor_time` = CASE WHEN `cl_timestamp` > `sensor_time` THEN `cl_timestamp` ELSE `sensor_time` END WHERE `uid` = '$uuid'";

			mysqli_query($connection,$sql);

		};
		if ($sensor == "ORP") {
			if (isset($data[$i]->timeseries[$j]->shortname)) { $orp_shortname = $data[$i]->timeseries[$j]->shortname; };
			if (isset($data[$i]->timeseries[$j]->longname)) { $orp_longname = $data[$i]->timeseries[$j]->longname; };
			if (isset($data[$i]->timeseries[$j]->unit)) { $orp_unit = $data[$i]->timeseries[$j]->unit; };
			if (isset($data[$i]->timeseries[$j]->currentMeasurement->timestamp)) { $datetime = $data[$i]->timeseries[$j]->currentMeasurement->timestamp; $orp_timestamp = date("Y-m-d H:i:s", strtotime($datetime)); };
			if (isset($data[$i]->timeseries[$j]->currentMeasurement->value)) { $orp = $data[$i]->timeseries[$j]->currentMeasurement->value; };
			if (isset($data[$i]->timeseries[$j]->currentMeasurement->trend)) { $orp_trend = $data[$i]->timeseries[$j]->currentMeasurement->trend; };
			if (isset($data[$i]->timeseries[$j]->comment)) {
				$comment_short = $data[$i]->timeseries[$j]->comment->shortDescription;
				$comment_long = $data[$i]->timeseries[$j]->comment->longDescription;
			} else {
				$comment_long = null;
			};
			
			$sql="UPDATE `pegel_w` SET `orp_shortname` =  '$orp_shortname', `orp_longname` =  '$orp_longname', `orp_unit` = '$orp_unit', `orp_timestamp` = '$orp_timestamp', `orp` = '$orp', `orp_trend` = '$orp_trend', `orp_comment`= '$comment_long', `sensor_time` = CASE WHEN `orp_timestamp` > `sensor_time` THEN `orp_timestamp` ELSE `sensor_time` END WHERE `uid` = '$uuid'";

			mysqli_query($connection,$sql);

		};
	}

	if(!function_exists('json_decode')) die('Your host does not support json');

	$data = json_decode($json_file);

	// check for elements with empty longitude and drop them from array
	// seems checking for longitude is enough to catch all sensors which don't provide the minimum data necessary...
	for($i=0; $i<count($data); $i++) {
		if (!isset($data[$i]->longitude)) {
			// if longitude is not set we can not map it, so we drop it
			unset($data[$i]);
		}
	}
	// correct array offsets after unsetting it before
	array_splice($data, 0,0);

	// cycle trough the remaining elements in the array
	for($i=0; $i<count($data); $i++) {
		// check if we have at least one object in the timeseries array
		if (count($data[$i]->timeseries) > 0) {
			// cycle through timeseries array and call the sensor_type function which assigns the proper variable names
			for($j=0; $j<count($data[$i]->timeseries); $j++){
				// set $sensor to value of shortname so we can identify it in the sensor_type function
				$sensor = $data[$i]->timeseries[$j]->shortname;
				sensor_type($connection);
			};
			
			// Update the timeseries table for each station
			$uid = $data[$i]->uuid;
			$array = array("w", "lt", "wt", "q", "va", "dfh", "lf", "wg", "wr", "gru", "hl", "pl", "n", "n_intensity", "o2", "ph", "cl", "orp");
			foreach($array as $k) {		
				
				$sql="insert into waterways_timeline_$k select null, uid, " . $k . "_timestamp, $k, null from pegel_w where uid='$uid' AND op_state='active' ON DUPLICATE KEY UPDATE waterways_timeline_$k." . $k . "_recent=pegel_w.$k";

				mysqli_query($connection,$sql);
				//$sqla="delete from  waterways_timeline_$k where `sensor_time`='0000-00-00 00:00:00'";
				//mysql_query($sqla);
			};
			
			
		};

	};

	// Insert / update the stations table
	$sql="INSERT INTO stations SELECT null,uid,uid2,category,lat,lng,null,country,null,null,null,null,null,name,null,null,null,null,agency,agency_code,agency_link,provider,provider_code,provider_link,null,op_state,null,null FROM pegel_w ON DUPLICATE KEY UPDATE lat=pegel_w.lat, lng=pegel_w.lng, country=pegel_w.country, original_name=pegel_w.name_proper, agency=pegel_w.agency, agency_code=pegel_w.agency_code, agency_link=pegel_w.agency_link, provider=pegel_w.provider, provider_code=pegel_w.provider_code, provider_link=pegel_w.provider_link, op_state=pegel_w.op_state, last_update=pegel_w.sensor_time;";

	mysqli_query($connection,$sql);

	$sql="UPDATE `pegel_w`, `stations` SET `pegel_w`.`uuid`=`stations`.`uuid` WHERE `pegel_w`.`uid`=`stations`.`uid` AND `pegel_w`.`category`=`stations`.`category`;";

	mysqli_query($connection,$sql);

	$sql="INSERT INTO stations_waterways SELECT uuid, km, water_shortname, water_longname, w_gaugeZero_unit, w_gaugeZero_value, w_gaugeZero_validFrom, M_I_shortname, M_I_longname, M_I_unit, M_I_value, M_I_validFrom, M_II_shortname, M_II_longname, M_II_unit, M_II_value, M_II_validFrom, M_III_shortname, M_III_longname, M_III_unit, M_III_value, M_III_validFrom, mw_shortname, mw_longname, mw_unit, mw_value, mw_timespanStart, mw_timespanEnd, mnw_shortname, mnw_longname, mnw_unit, mnw_value, mnw_timespanStart, mnw_timespanEnd, mhw_shortname, mhw_longname, mhw_unit, mhw_value, mhw_timespanStart, mhw_timespanEnd, hhw_shortname, hhw_longname, hhw_unit, hhw_value, hhw_date, nnw_shortname, nnw_longname, nnw_unit, nnw_value, nnw_date, hsw_shortname, hsw_longname, hsw_unit, hsw_value, hsw_validFrom, hthw_shortname, hthw_longname, hthw_unit, hthw_value, hthw_timespanStart, hthw_timespanEnd, hthw_date, ntnw_shortname, ntnw_longname, ntnw_unit, ntnw_value, ntnw_timespanStart, ntnw_timespanEnd, ntnw_date, mtnw_shortname, mtnw_longname, mtnw_unit, mtnw_value, mtnw_timespanStart, mtnw_timespanEnd, mthw_shortname, mthw_longname, mthw_unit, mthw_value, mthw_timespanStart, mthw_timespanEnd, ZS_I_shortname, ZS_I_longname, ZS_I_unit, ZS_I_value, ZS_I_validFrom, rnw_shortname, rnw_longname, rnw_unit, rnw_value, rnw_validFrom, null FROM pegel_w ON DUPLICATE KEY UPDATE uuid=pegel_w.uuid, w_gaugeZero_unit=pegel_w.w_gaugeZero_unit, w_gaugeZero_value=pegel_w.w_gaugeZero_value, w_gaugeZero_validFrom=pegel_w.w_gaugeZero_validFrom, M_I_shortname=pegel_w.M_I_shortname, M_I_longname=pegel_w.M_I_longname, M_I_unit=pegel_w.M_I_unit, M_I_value=pegel_w.M_I_value, M_I_validFrom=pegel_w.M_I_validFrom, M_II_shortname=pegel_w.M_II_shortname, M_II_longname=pegel_w.M_II_longname, M_II_unit=pegel_w.M_II_unit, M_II_value=pegel_w.M_II_value, M_II_validFrom=pegel_w.M_II_validFrom, M_III_shortname=pegel_w.M_III_shortname, M_III_longname=pegel_w.M_III_longname, M_III_unit=pegel_w. M_III_unit, M_III_value=pegel_w.M_III_value, M_III_validFrom=pegel_w.M_III_validFrom, mw_shortname=pegel_w.mw_shortname, mw_longname=pegel_w.mw_longname, mw_unit=pegel_w.mw_unit, mw_value=pegel_w.mw_value, mw_timespanStart=pegel_w.mw_timespanStart, mw_timespanEnd=pegel_w.mw_timespanEnd, mnw_shortname=pegel_w.mnw_shortname, mnw_longname=pegel_w.mnw_longname, mnw_unit=pegel_w.mnw_unit, mnw_value=pegel_w.mnw_value, mnw_timespanStart=pegel_w.mnw_timespanStart, mnw_timespanEnd=pegel_w.mnw_timespanEnd, mhw_shortname=pegel_w.mhw_shortname, mhw_longname=pegel_w.mhw_longname, mhw_unit=pegel_w.mhw_unit, mhw_value=pegel_w.mhw_value, mhw_timespanStart=pegel_w.mhw_timespanStart, mhw_timespanEnd=pegel_w.mhw_timespanEnd, hhw_shortname=pegel_w.hhw_shortname, hhw_longname=pegel_w.hhw_longname, hhw_unit=pegel_w.hhw_unit, hhw_value=pegel_w.hhw_value, hhw_date=pegel_w.hhw_date, nnw_shortname=pegel_w.nnw_shortname, nnw_longname=pegel_w.nnw_longname, nnw_unit=pegel_w.nnw_unit, nnw_value=pegel_w.nnw_value, nnw_date=pegel_w.nnw_date, hsw_shortname=pegel_w.hsw_shortname, hsw_longname=pegel_w.hsw_longname, hsw_unit=pegel_w.hsw_unit, hsw_value=pegel_w.hsw_value, hsw_validFrom=pegel_w.hsw_validFrom, hthw_shortname=pegel_w.hthw_shortname, hthw_longname=pegel_w.hthw_longname, hthw_unit=pegel_w.hthw_unit, hthw_value=pegel_w.hthw_value, hthw_timespanStart=pegel_w.hthw_timespanStart, hthw_timespanEnd=pegel_w.hthw_timespanEnd, hthw_date=pegel_w.hthw_date, ntnw_shortname=pegel_w.ntnw_shortname, ntnw_longname=pegel_w.ntnw_longname, ntnw_unit=pegel_w.ntnw_unit, ntnw_value=pegel_w.ntnw_value, ntnw_timespanStart=pegel_w.ntnw_timespanStart, ntnw_timespanEnd=pegel_w.ntnw_timespanEnd, ntnw_date=pegel_w.ntnw_date, mtnw_shortname=pegel_w.mtnw_shortname, mtnw_longname=pegel_w.mtnw_longname, mtnw_unit=pegel_w.mtnw_unit, mtnw_value=pegel_w.mtnw_value, mtnw_timespanStart=pegel_w.mtnw_timespanStart, mtnw_timespanEnd=pegel_w.mtnw_timespanEnd, mthw_shortname=pegel_w.mthw_shortname, mthw_longname=pegel_w.mthw_longname, mthw_unit=pegel_w.mthw_unit, mthw_value=pegel_w.mthw_value, mthw_timespanStart=pegel_w.mthw_timespanStart, mthw_timespanEnd=pegel_w.mthw_timespanEnd, ZS_I_shortname=pegel_w.ZS_I_shortname, ZS_I_longname=pegel_w.ZS_I_longname, ZS_I_unit=pegel_w.ZS_I_unit, ZS_I_value=pegel_w.ZS_I_value, ZS_I_validFrom=pegel_w.ZS_I_validFrom, rnw_shortname=pegel_w.rnw_shortname, rnw_longname=pegel_w.rnw_longname, rnw_unit=pegel_w.rnw_unit, rnw_value=pegel_w.rnw_value, rnw_validFrom=pegel_w.rnw_validFrom;";

	mysqli_query($connection,$sql);

	$sql="INSERT INTO `waterways_current` SELECT `uuid`, `uid`, `km`, `water_shortname`, `water_longname`, `w_timestamp`, `w`, `w_trend`, `w_stateMnwMhw`, `w_stateNswHsw`, `lt_timestamp`, `lt`, `lt_trend`, `wt_timestamp`, `wt`, `wt_trend`, `q_timestamp`, `q`, `q_trend`, `va_timestamp`, `va`, `va_trend`, `dfh_timestamp`, `dfh`, `dfh_trend`, `lf_timestamp`, `lf`, `lf_trend`, `wg_timestamp`, `wg`, `wg_trend`, `wr_timestamp`, `wr`, `wr_trend`, `gru_timestamp`, `gru`, `gru_trend`, `hl_timestamp`, `hl`, `hl_trend`, `pl_timestamp`, `pl`, `pl_trend`, `n_timestamp`, `n`, `n_trend`, `n_intensity_timestamp`, `n_intensity`, `n_intensity_trend`, `o2_timestamp`, `o2`, `o2_trend`, `ph_timestamp`, `ph`, `ph_trend`, `cl_timestamp`, `cl`, `cl_trend`, `orp_timestamp`, `orp`, `orp_trend`, null FROM `pegel_w` WHERE `op_state`='active' 

	ON DUPLICATE KEY UPDATE 

	`waterways_current`.`uuid`=`pegel_w`.`uuid`, `waterways_current`.`uid`=`pegel_w`.`uid`, 

	`waterways_current`.`w`=CASE WHEN `pegel_w`.`w_timestamp` > `waterways_current`.`w_timestamp` THEN `pegel_w`.`w` ELSE `waterways_current`.`w` END, 

	`waterways_current`.`w_timestamp`=CASE WHEN `pegel_w`.`w_timestamp` > `waterways_current`.`w_timestamp` THEN `pegel_w`.`w_timestamp` ELSE `waterways_current`.`w_timestamp` END, 

	`waterways_current`.`w_stateMnwMhw`=CASE WHEN `pegel_w`.`w_timestamp` > `waterways_current`.`w_timestamp` THEN `pegel_w`.`w_stateMnwMhw` ELSE `waterways_current`.`w_stateMnwMhw` END,

	`waterways_current`.`w_stateNswHsw`=CASE WHEN `pegel_w`.`w_timestamp` > `waterways_current`.`w_timestamp` THEN `pegel_w`.`w_stateNswHsw` ELSE `waterways_current`.`w_stateNswHsw` END,

	`waterways_current`.`lt`=CASE WHEN `pegel_w`.`lt_timestamp` > `waterways_current`.`lt_timestamp` THEN `pegel_w`.`lt` ELSE `waterways_current`.`lt` END,

	`waterways_current`.`lt_timestamp`=CASE WHEN `pegel_w`.`lt_timestamp` > `waterways_current`.`lt_timestamp` THEN `pegel_w`.`lt_timestamp` ELSE `waterways_current`.`lt_timestamp` END, 

	`waterways_current`.`wt`=CASE WHEN `pegel_w`.`wt_timestamp` > `waterways_current`.`wt_timestamp` THEN `pegel_w`.`wt` ELSE `waterways_current`.`wt` END, 

	`waterways_current`.`wt_timestamp`=CASE WHEN `pegel_w`.`wt_timestamp` > `waterways_current`.`wt_timestamp` THEN `pegel_w`.`wt_timestamp` ELSE `waterways_current`.`wt_timestamp` END, 

	`waterways_current`.`q`=CASE WHEN `pegel_w`.`q_timestamp` > `waterways_current`.`q_timestamp` THEN `pegel_w`.`q` ELSE `waterways_current`.`q` END, 

	`waterways_current`.`q_timestamp`=CASE WHEN  `pegel_w`.`q_timestamp` > `waterways_current`.`q_timestamp` THEN `pegel_w`.`q_timestamp` ELSE `waterways_current`.`q_timestamp` END, 

	`waterways_current`.`va`=CASE WHEN `pegel_w`.`va_timestamp` > `waterways_current`.`va_timestamp` THEN `pegel_w`.`va` ELSE `waterways_current`.`va` END, 

	`waterways_current`.`va_timestamp`=CASE WHEN  `pegel_w`.`va_timestamp` > `waterways_current`.`va_timestamp` THEN `pegel_w`.`va_timestamp` ELSE `waterways_current`.`va_timestamp` END, 

	`waterways_current`.`dfh`=CASE WHEN `pegel_w`.`dfh_timestamp` > `waterways_current`.`dfh_timestamp` THEN `pegel_w`.`dfh` ELSE `waterways_current`.`dfh` END, 

	`waterways_current`.`dfh_timestamp`=CASE WHEN `pegel_w`.`dfh_timestamp` > `waterways_current`.`dfh_timestamp` THEN `pegel_w`.`dfh_timestamp` ELSE `waterways_current`.`dfh_timestamp` END, 

	`waterways_current`.`lf`=CASE WHEN `pegel_w`.`lf_timestamp` > `waterways_current`.`lf_timestamp` THEN `pegel_w`.`lf` ELSE `waterways_current`.`lf` END, 

	`waterways_current`.`lf_timestamp`=CASE WHEN  `pegel_w`.`lf_timestamp` > `waterways_current`.`lf_timestamp` THEN `pegel_w`.`lf_timestamp` ELSE `waterways_current`.`lf_timestamp` END, 

	`waterways_current`.`wg`=CASE WHEN `pegel_w`.`wg_timestamp` > `waterways_current`.`wg_timestamp` THEN `pegel_w`.`wg` ELSE `waterways_current`.`wg` END, 

	`waterways_current`.`wg_timestamp`=CASE WHEN `pegel_w`.`wg_timestamp` > `waterways_current`.`wg_timestamp` THEN `pegel_w`.`wg_timestamp` ELSE `waterways_current`.`wg_timestamp` END, 

	`waterways_current`.`wr`=CASE WHEN `pegel_w`.`wr_timestamp` > `waterways_current`.`wr_timestamp` THEN `pegel_w`.`wr` ELSE `waterways_current`.`wr` END, 

	`waterways_current`.`wr_timestamp`=CASE WHEN `pegel_w`.`wr_timestamp` > `waterways_current`.`wr_timestamp` THEN `pegel_w`.`wr_timestamp` ELSE `waterways_current`.`wr_timestamp` END, 

	`waterways_current`.`gru`=CASE WHEN `pegel_w`.`gru_timestamp` > `waterways_current`.`gru_timestamp` THEN `pegel_w`.`gru` ELSE `waterways_current`.`gru` END, 

	`waterways_current`.`gru_timestamp`=CASE WHEN `pegel_w`.`gru_timestamp` > `waterways_current`.`gru_timestamp` THEN `pegel_w`.`gru_timestamp` ELSE `waterways_current`.`gru_timestamp` END, 

	`waterways_current`.`hl`=CASE WHEN `pegel_w`.`hl_timestamp` > `waterways_current`.`hl_timestamp` THEN `pegel_w`.`hl` ELSE `waterways_current`.`hl` END, 

	`waterways_current`.`hl_timestamp`=CASE WHEN `pegel_w`.`hl_timestamp` > `waterways_current`.`hl_timestamp` THEN `pegel_w`.`hl_timestamp` ELSE `waterways_current`.`hl_timestamp` END, 

	`waterways_current`.`pl`=CASE WHEN `pegel_w`.`pl_timestamp` > `waterways_current`.`pl_timestamp` THEN `pegel_w`.`pl` ELSE `waterways_current`.`pl` END, 

	`waterways_current`.`pl_timestamp`=CASE WHEN `pegel_w`.`pl_timestamp` > `waterways_current`.`pl_timestamp` THEN `pegel_w`.`pl_timestamp` ELSE `waterways_current`.`pl_timestamp` END, 

	`waterways_current`.`n`=CASE WHEN `pegel_w`.`n_timestamp` > `waterways_current`.`n_timestamp` THEN `pegel_w`.`n` ELSE `waterways_current`.`n` END, 

	`waterways_current`.`n_timestamp`=CASE WHEN `pegel_w`.`n_timestamp` > `waterways_current`.`n_timestamp` THEN `pegel_w`.`n_timestamp` ELSE `waterways_current`.`n_timestamp` END, 

	`waterways_current`.`n_intensity`=CASE WHEN `pegel_w`.`n_intensity_timestamp` > `waterways_current`.`n_intensity_timestamp` THEN `pegel_w`.`n_intensity` ELSE `waterways_current`.`n_intensity` END, 

	`waterways_current`.`n_intensity_timestamp`=CASE WHEN `pegel_w`.`n_intensity_timestamp` > `waterways_current`.`n_intensity_timestamp` THEN `pegel_w`.`n_intensity_timestamp` ELSE `waterways_current`.`n_intensity_timestamp` END, 

	`waterways_current`.`o2`=CASE WHEN `pegel_w`.`o2_timestamp` > `waterways_current`.`o2_timestamp` THEN `pegel_w`.`o2` ELSE `waterways_current`.`o2` END, 

	`waterways_current`.`o2_timestamp`=CASE WHEN `pegel_w`.`o2_timestamp` > `waterways_current`.`o2_timestamp` THEN `pegel_w`.`o2_timestamp` ELSE `waterways_current`.`o2_timestamp` END, 

	`waterways_current`.`ph`=CASE WHEN `pegel_w`.`ph_timestamp` > `waterways_current`.`ph_timestamp` THEN `pegel_w`.`ph` ELSE `waterways_current`.`ph` END, 

	`waterways_current`.`ph_timestamp`=CASE WHEN `pegel_w`.`ph_timestamp` > `waterways_current`.`ph_timestamp` THEN `pegel_w`.`ph_timestamp` ELSE `waterways_current`.`ph_timestamp` END, 

	`waterways_current`.`cl`=CASE WHEN `pegel_w`.`cl_timestamp` > `waterways_current`.`cl_timestamp` THEN `pegel_w`.`cl` ELSE `waterways_current`.`cl` END, 

	`waterways_current`.`cl_timestamp`=CASE WHEN `pegel_w`.`cl_timestamp` > `waterways_current`.`cl_timestamp` THEN `pegel_w`.`cl_timestamp` ELSE `waterways_current`.`cl_timestamp` END, 

	`waterways_current`.`orp`=CASE WHEN `pegel_w`.`orp_timestamp` > `waterways_current`.`orp_timestamp` THEN `pegel_w`.`orp` ELSE `waterways_current`.`orp` END, 

	`waterways_current`.`orp_timestamp`=CASE WHEN `pegel_w`.`orp_timestamp` > `waterways_current`.`orp_timestamp` THEN `pegel_w`.`orp_timestamp` ELSE `waterways_current`.`orp_timestamp` END, 

	`waterways_current`.`w_trend`=CASE WHEN `pegel_w`.`w_timestamp` > `waterways_current`.`w_timestamp` THEN `pegel_w`.`w_trend` ELSE `waterways_current`.`w_trend` END, 

	`waterways_current`.`lt_trend`=CASE WHEN `pegel_w`.`lt_timestamp` > `waterways_current`.`lt_timestamp` THEN `pegel_w`.`lt_trend` ELSE `waterways_current`.`lt_trend` END, 

	`waterways_current`.`wt_trend`=CASE WHEN `pegel_w`.`wt_timestamp` > `waterways_current`.`wt_timestamp` THEN `pegel_w`.`wt_trend` ELSE `waterways_current`.`wt_trend` END, 

	`waterways_current`.`q_trend`=CASE WHEN `pegel_w`.`q_timestamp` > `waterways_current`.`q_timestamp` THEN `pegel_w`.`q_trend` ELSE `waterways_current`.`q_trend` END, 

	`waterways_current`.`va_trend`=CASE WHEN `pegel_w`.`va_timestamp` > `waterways_current`.`va_timestamp` THEN `pegel_w`.`va_trend` ELSE `waterways_current`.`va_trend` END, 

	`waterways_current`.`dfh_trend`=CASE WHEN `pegel_w`.`dfh_timestamp` > `waterways_current`.`dfh_timestamp` THEN `pegel_w`.`dfh_trend` ELSE `waterways_current`.`dfh_trend` END, 

	`waterways_current`.`lf_trend`=CASE WHEN `pegel_w`.`lf_timestamp` > `waterways_current`.`lf_timestamp` THEN `pegel_w`.`lf_trend` ELSE `waterways_current`.`lf_trend` END, 

	`waterways_current`.`wg_trend`=CASE WHEN `pegel_w`.`wg_timestamp` > `waterways_current`.`wg_timestamp` THEN `pegel_w`.`wg_trend` ELSE `waterways_current`.`wg_trend` END, 

	`waterways_current`.`wr_trend`=CASE WHEN `pegel_w`.`wr_timestamp` > `waterways_current`.`wr_timestamp` THEN `pegel_w`.`wr_trend` ELSE `waterways_current`.`wr_trend` END, 

	`waterways_current`.`gru_trend`=CASE WHEN `pegel_w`.`gru_timestamp` > `waterways_current`.`gru_timestamp` THEN `pegel_w`.`gru_trend` ELSE `waterways_current`.`gru_trend` END, 

	`waterways_current`.`hl_trend`=CASE WHEN `pegel_w`.`hl_timestamp` > `waterways_current`.`hl_timestamp` THEN `pegel_w`.`hl_trend` ELSE `waterways_current`.`hl_trend` END, 

	`waterways_current`.`pl_trend`=CASE WHEN `pegel_w`.`pl_timestamp` > `waterways_current`.`pl_timestamp` THEN `pegel_w`.`pl_trend` ELSE `waterways_current`.`pl_trend` END, 

	`waterways_current`.`n_trend`=CASE WHEN `pegel_w`.`n_timestamp` > `waterways_current`.`n_timestamp` THEN `pegel_w`.`n_trend` ELSE `waterways_current`.`n_trend` END, 

	`waterways_current`.`n_intensity_trend`=CASE WHEN `pegel_w`.`n_intensity_timestamp` > `waterways_current`.`n_intensity_timestamp` THEN `pegel_w`.`n_intensity_trend` ELSE `waterways_current`.`n_intensity_trend` END, 

	`waterways_current`.`o2_trend`=CASE WHEN `pegel_w`.`o2_timestamp` > `waterways_current`.`o2_timestamp` THEN `pegel_w`.`o2_trend` ELSE `waterways_current`.`o2_trend` END, 

	`waterways_current`.`ph_trend`=CASE WHEN `pegel_w`.`ph_timestamp` > `waterways_current`.`ph_timestamp` THEN `pegel_w`.`ph_trend` ELSE `waterways_current`.`ph_trend` END,

	`waterways_current`.`cl_trend`=CASE WHEN `pegel_w`.`cl_timestamp` > `waterways_current`.`cl_timestamp` THEN `pegel_w`.`cl_trend` ELSE `waterways_current`.`cl_trend` END,

	`waterways_current`.`orp_trend`=CASE WHEN `pegel_w`.`orp_timestamp` > `waterways_current`.`orp_timestamp` THEN `pegel_w`.`orp_trend` ELSE `waterways_current`.`orp_trend` END;";

	mysqli_query($connection,$sql);

?>
