# Select database
use ud_luft;

# needs not be set here!
#set session collation_database=utf8_unicode_ci;
#set session character_set_database=utf8;


CREATE TABLE IF NOT EXISTS `air_transgressions` (
  `uuid` int NOT NULL,
  `uid` varchar(36) COLLATE utf8_unicode_ci DEFAULT NULL,
  `trans_total` int(3),
  `trans_method` varchar(1) COLLATE utf8_unicode_ci DEFAULT NULL,
  `annual_first`  datetime NOT NULL,
  `annual_last`  datetime NOT NULL,
  `trans_jan` int(2),
  `trans_feb` int(2),
  `trans_mar` int(2),
  `trans_apr` int(2),
  `trans_may` int(2),
  `trans_jun` int(2),
  `trans_jul` int(2),
  `trans_aug` int(2),
  `trans_sep` int(2),
  `trans_oct` int(2),
  `trans_nov` int(2),
  `trans_dec` int(2),
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

# Truncate all tables
truncate table air_transgressions;

# Populate air_transgressions table
load data local infile '/tmp/envDB/air/DE/transgressions.csv' into table air_transgressions fields terminated by ';' lines terminated by '\n' (uid,@dummy, trans_total, trans_method, annual_first, annual_last, trans_jan, trans_feb, trans_mar, trans_apr, trans_may, trans_jun, trans_jul, trans_aug, trans_sep, trans_oct, trans_nov, trans_dec);


# Update with proper uuid from stations table
UPDATE `air_transgressions`, `stations` SET `air_transgressions`.`uuid`=`stations`.`uuid` WHERE `air_transgressions`.`uid`=`stations`.`uid`;



