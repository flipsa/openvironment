use ud_luft;
set session collation_database=utf8_unicode_ci;
set session character_set_database=utf8;

# Load stations into temporary stations table;
TRUNCATE TABLE `air_quality_germany`; load data local infile '/tmp/envDB/air/DE/stations.csv' into table `air_quality_germany`  fields terminated by ';' enclosed by '\"' lines terminated by '\r\n' (@dummy, @dummy, uid, name, start_date, end_date, lat, @dummy, lng, @dummy, altitude, var1, var2, var3, var4); DELETE FROM `air_quality_germany` WHERE uid IS NULL;

# Delete inactive stations and stations which neither measure co, no2, o3, pm10, so2
UPDATE `air_quality_germany` SET `op_state`='active';
UPDATE `air_quality_germany` SET `end_date`=NULL WHERE end_date='';
UPDATE `air_quality_germany` SET `op_state`='inactive' WHERE end_date IS NOT NULL OR end_date<>'';

UPDATE `air_quality_germany` SET `category`='air_quality';
UPDATE `air_quality_germany` SET `icon`='/img/air_quality.png';
UPDATE `air_quality_germany` SET `country`='de';
UPDATE `air_quality_germany` SET `state`=SUBSTRING(`uid`,3,2);
UPDATE `air_quality_germany` SET `provider`='Umweltbundesamt';
UPDATE `air_quality_germany` SET `provider_code`='UBA';
UPDATE `air_quality_germany` SET `provider_link`='http://www.umweltbundesamt.de/daten/luftbelastung/aktuelle-luftdaten';


# Copy stations from temporary table to permanent stations table. Update if station already exists
INSERT INTO stations SELECT null,uid,uid2,category,lat,lng,altitude,country,null,null,null,null,null,name,null,null,start_date,end_date,null,null,null,null,null,null,null,op_state,null,null FROM `air_quality_germany` ON DUPLICATE KEY UPDATE `original_name`=`air_quality_germany`.`name`, `country`=`air_quality_germany`.`country`, `state`=`air_quality_germany`.`state`, `agency`=`air_quality_germany`.`agency`, `agency_code`=`air_quality_germany`.`agency_code`, `agency_link`=`air_quality_germany`.`agency_link`, `provider`=`air_quality_germany`.`provider`, `provider_code`=`air_quality_germany`.`provider_code`, `provider_link`=`air_quality_germany`.`provider_link`, `end_date`=COALESCE( `air_quality_germany`.`end_date`, `stations`.`end_date`), `op_state`=`air_quality_germany`.`op_state`, `last_update`=`air_quality_germany`.`sensor_time`;
