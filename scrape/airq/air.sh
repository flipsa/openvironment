#!/bin/bash

# Get DB credentials
. /var/www/offene-umweltdaten.de/scrape/settings

DATE=$(date +%Y%m%d);
DATE_YESTERDAY=$(date +%Y%m%d -d "1 days ago");
DAY=$(date +%d);
#DAY="01"
YEAR=$(date +%Y);

TIME=$(date +"%H");
#TIME="07";

cleanUmlauts() {
	echo "REPLACING UMLAUTS"
	$(sed -i 's/Ã/ß/g' ""$AIR_TEMP_DIR"/DE/stations-"$DATE"".csv)
	$(sed -i 's/Ãœ/Ü/g' ""$AIR_TEMP_DIR"/DE/stations-"$DATE"".csv)
	$(sed -i 's/Ã¼/ü/g' ""$AIR_TEMP_DIR"/DE/stations-"$DATE"".csv)
	$(sed -i 's/Ã–/Ö/g' ""$AIR_TEMP_DIR"/DE/stations-"$DATE"".csv)
	$(sed -i 's/Ã¶/ö/g' ""$AIR_TEMP_DIR"/DE/stations-"$DATE"".csv)
	$(sed -i 's/Ã„/Ä/g' ""$AIR_TEMP_DIR"/DE/stations-"$DATE"".csv)
	$(sed -i 's/Ã¤/ä/g' ""$AIR_TEMP_DIR"/DE/stations-"$DATE"".csv)
}

correctNames() {
	$(sed -i 's/B\ /Berlin\ /g' ""$AIR_TEMP_DIR"/DE/stations-"$DATE"".csv)
}

updateStations() {
	if [ "$DAY" = "01" ]; then

		echo "First of month! Downloading new station list..."
		wget http://www.env-it.de/stationen/public/download.do?event=euMetaStation -nc -o ""$AIR_LOG_DIR"/DE_Stations.log" -O ""$AIR_TEMP_DIR"/DE/stations-"$DATE"-temp".csv > /dev/null 2>&1

		# check the wget_log if the file was not retrieved because it already exists. if so, skip further processing!
		if grep -iq "not retrieving" ""$AIR_LOG_DIR"/DE_Stations.log"; then

			echo "Station File already up to date!"

		else
			# copy the temp file
			$(iconv --from-code=ISO-8859-1 --to-code=UTF-8 ""$AIR_TEMP_DIR"/DE/stations-"$DATE"-temp".csv > ""$AIR_TEMP_DIR"/DE/stations-"$DATE"".csv)
			rm ""$AIR_TEMP_DIR"/DE/stations-"$DATE"-temp".csv
			#cp ""$AIR_TEMP_DIR"/DE/stations-"$DATE"-temp".csv ""$AIR_TEMP_DIR"/DE/stations-"$DATE"".csv

			# clean umlauts
			cleanUmlauts

			# delete the first 2 lines
			$(sed -i '1,2d' ""$AIR_TEMP_DIR"/DE/stations-"$DATE"".csv)

			# Correct common problems
			correctNames

			# Rename file so mysql script can process further
			mv ""$AIR_TEMP_DIR"/DE/stations-"$DATE".csv" ""$AIR_TEMP_DIR"/DE/stations.csv"

			# Process data
			mysql "$DB" < "$AIR_SQL_DE_STATIONS" -u"$DB_USER" -p"$DB_PW" -h"$DB_HOST"

		fi
	fi
}

transgressions(){
	echo "Downloading transgression list..."
	wget "https://www.umweltbundesamt.de/uaq/csv/transgressions/data?uaq_pollutant=PM10&uaq_transgressions_year="$YEAR"" -nc -o ""$AIR_LOG_DIR"/DE_Transgressions.log" -O ""$AIR_TEMP_DIR"/DE/transgressions".csv > /dev/null 2>&1

	# Delete the first line from the file
	$(sed -i '1,2d' ""$AIR_TEMP_DIR"/DE/transgressions".csv)

	mysql "$DB" < "$AIR_SQL_DE_TRANSGRESSIONS" -u"$DB_USER" -p"$DB_PW" -h"$DB_HOST"
}

if [ "$TIME" = "07" ]; then

	HOURS="00 01 02 03 04 05 06"
	HOURS_YESTERDAY="00 01 02 03 04 05 06 07 08 09 10 11 12 13 14 15 16 17 18 19 20 21 22 23"

	updateStations
	transgressions

	echo "It's 7am - Downloading data from last night ..."

	for HOUR_Y in $HOURS_YESTERDAY; do
		echo "Downloading data for "$DATE_YESTERDAY" - "$HOUR_Y":00:00"
		sh /var/www/offene-umweltdaten.de/scrape/airq/DE_AirQuality.sh -d "$DATE_YESTERDAY" -t "$HOUR_Y" #> /dev/null 2>&1
	done
	for HOUR in $HOURS; do
		echo "Downloading data for "$DATE" - "$HOUR":00:00"
		sh /var/www/offene-umweltdaten.de/scrape/airq/DE_AirQuality.sh -d "$DATE" -t "$HOUR" #> /dev/null 2>&1
	done
	#curl "http://"$SERVER"/update_stations.php?category=air_quality_germany&action=pm10"
fi

if  [ "$TIME" = "08" ] || [ "$TIME" = "09" ] || [ "$TIME" = "10" ] || [ "$TIME" = "11" ] || [ "$TIME" = "12" ] || [ "$TIME" = "13" ] || [ "$TIME" = "14" ] || [ "$TIME" = "15" ] || [ "$TIME" = "16" ] || [ "$TIME" = "17" ] || [ "$TIME" = "18" ] || [ "$TIME" = "19" ] || [ "$TIME" = "20" ] || [ "$TIME" = "21" ] || [ "$TIME" = "22" ] || [ "$TIME" = "23" ]; then
	echo "It's "$TIME" o clock, trying to get most recent data."
	echo "Downloading data for "$DATE" - $(expr $HOUR + 1)0:00"
	sh /var/www/offene-umweltdaten.de/scrape/airq/DE_AirQuality.sh #> /dev/null 2>&1
else
	echo "It's "$TIME" o clock. There won't be any new data. Aborting!"
fi
