# Select database
use ud_luft;
set session collation_database=utf8_unicode_ci;
set session character_set_database=utf8;


# create air_quality_germany table
CREATE TABLE IF NOT EXISTS `air_quality_germany` (
  `uuid` int DEFAULT NULL,
  `uid` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `uid2` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `uid3` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `category` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `icon` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name_proper` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `start_date` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `end_date` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lat` float(10,7) DEFAULT NULL,
  `lng` float(10,7) DEFAULT NULL,
  `altitude` varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var1` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var2` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var3` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var4` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var5` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `co_8smw` decimal(5,1) DEFAULT NULL,
  `co_8smw_time` varchar(18) COLLATE utf8_unicode_ci DEFAULT NULL,
  `co_8tmax` decimal(5,1) DEFAULT NULL,
  `co_8tmax_time` varchar(18) COLLATE utf8_unicode_ci DEFAULT NULL,
  `no2_1smw` decimal(5,1) DEFAULT NULL,
  `no2_1smw_time` varchar(18) COLLATE utf8_unicode_ci DEFAULT NULL,
  `no2_1tmax` decimal(5,1) DEFAULT NULL,
  `no2_1tmax_time` varchar(18) COLLATE utf8_unicode_ci DEFAULT NULL,
  `o3_1smw` decimal(5,1) DEFAULT NULL,
  `o3_1smw_time` varchar(18) COLLATE utf8_unicode_ci DEFAULT NULL,
  `o3_8smw` decimal(5,1) DEFAULT NULL,
  `o3_8smw_time` varchar(18) COLLATE utf8_unicode_ci DEFAULT NULL,
  `o3_1tmax` decimal(5,1) DEFAULT NULL,
  `o3_1tmax_time` varchar(18) COLLATE utf8_unicode_ci DEFAULT NULL,
  `o3_8tmax` decimal(5,1) DEFAULT NULL,
  `o3_8tmax_time` varchar(18) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pm10_1tmw` decimal(5,1) DEFAULT NULL,
  `pm10_1tmw_time` varchar(18) COLLATE utf8_unicode_ci DEFAULT NULL,
  `so2_1smw` decimal(5,1) DEFAULT NULL,
  `so2_1smw_time` varchar(18) COLLATE utf8_unicode_ci DEFAULT NULL,
  `so2_1tmax` decimal(5,1) DEFAULT NULL,
  `so2_1tmax_time` varchar(18) COLLATE utf8_unicode_ci DEFAULT NULL,
  `so2_1tmw` decimal(5,1) DEFAULT NULL,
  `so2_1tmw_time` varchar(18) COLLATE utf8_unicode_ci DEFAULT NULL,
  `op_state` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'active',
  `sensor_time` DATETIME NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `lat` (`lat`,`lng`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


# Populate airq_stations table
# UNCOMMENT IF WE START OVER WITH AN EMPTY DB (currently the station file on disk is not regularly updated; how to check if new version?)
#TRUNCATE air_quality_germany;
#load data local infile '/tmp/envDB/air/DE/stations.csv' into table air_quality_germany CHARACTER SET UTF8 fields terminated by ';' enclosed by '"' lines terminated by '\n' (@dummy, @dummy, uid, name, start_date, end_date, lat, @dummy, lng, @dummy, altitude, var1, var2, var3, var4);


# Table create statements for the temporary tables 

CREATE TABLE IF NOT EXISTS `airq_co_8smw` (
  `station_code` varchar(7) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `station_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `co_8smw` decimal(5,1) DEFAULT NULL,
  `time` varchar(18) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`station_code`),
  KEY `station_code` (`station_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `airq_co_8tmax` (
  `station_code` varchar(7) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `station_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `co_8tmax` decimal(5,1) DEFAULT NULL,
  `time` varchar(18) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`station_code`),
  KEY `station_code` (`station_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `airq_no2_1smw` (
  `station_code` varchar(7) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `station_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `no2_1smw` decimal(5,1) DEFAULT NULL,
  `time` varchar(18) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`station_code`),
  KEY `station_code` (`station_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `airq_no2_1tmax` (
  `station_code` varchar(7) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `station_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `no2_1tmax` decimal(5,1) DEFAULT NULL,
  `time` varchar(18) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`station_code`),
  KEY `station_code` (`station_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `airq_o3_1smw` (
  `station_code` varchar(7) COLLATE utf8_unicode_ci DEFAULT NULL,
  `station_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `o3_1smw` decimal(5,1) DEFAULT NULL,
  `time` varchar(18) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`station_code`),
  KEY `station_code` (`station_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `airq_o3_1tmax` (
  `station_code` varchar(7) COLLATE utf8_unicode_ci DEFAULT NULL,
  `station_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `o3_1tmax` decimal(5,1) DEFAULT NULL,
  `time` varchar(18) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`station_code`),
  KEY `station_code` (`station_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `airq_o3_8smw` (
  `station_code` varchar(7) COLLATE utf8_unicode_ci DEFAULT NULL,
  `station_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `o3_8smw` decimal(5,1) DEFAULT NULL,
  `time` varchar(18) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`station_code`),
  KEY `station_code` (`station_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `airq_o3_8tmax` (
  `station_code` varchar(7) COLLATE utf8_unicode_ci DEFAULT NULL,
  `station_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `o3_8tmax` decimal(5,1) DEFAULT NULL,
  `time` varchar(18) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`station_code`),
  KEY `station_code` (`station_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


CREATE TABLE IF NOT EXISTS `airq_pm10_1tmw` (
  `station_code` varchar(7) COLLATE utf8_unicode_ci DEFAULT NULL,
  `station_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `pm10_1tmw` decimal(5,1) DEFAULT NULL,
  `time` varchar(18) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`station_code`),
  KEY `station_code` (`station_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `airq_so2_1smw` (
  `station_code` varchar(7) COLLATE utf8_unicode_ci DEFAULT NULL,
  `station_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `so2_1smw` decimal(5,1) DEFAULT NULL,
  `time` varchar(18) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`station_code`),
  KEY `station_code` (`station_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `airq_so2_1tmw` (
  `station_code` varchar(7) COLLATE utf8_unicode_ci DEFAULT NULL,
  `station_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `so2_1tmw` decimal(5,1) DEFAULT NULL,
  `time` varchar(18) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`station_code`),
  KEY `station_code` (`station_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `airq_so2_1tmax` (
  `station_code` varchar(7) COLLATE utf8_unicode_ci DEFAULT NULL,
  `station_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `so2_1tmax` decimal(5,1) DEFAULT NULL,
  `time` varchar(18) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`station_code`),
  KEY `station_code` (`station_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

# CREATE THE TIMESERIES TABLES 

CREATE TABLE IF NOT EXISTS `air_timeline_co` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `uid` varchar(7) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `co_8smw` decimal(5,1) DEFAULT NULL,
  `co_8tmax` decimal(5,1) DEFAULT NULL,
  `sensor_time` DATETIME DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid_time` (`uid`,`sensor_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


CREATE TABLE IF NOT EXISTS `air_timeline_no2` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `uid` varchar(7) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `no2_1smw` decimal(5,1) DEFAULT NULL,
  `no2_1tmax` decimal(5,1) DEFAULT NULL,
  `sensor_time` DATETIME DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid_time` (`uid`,`sensor_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `air_timeline_o3` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `uid` varchar(7) COLLATE utf8_unicode_ci DEFAULT NULL,
  `o3_1smw` decimal(5,1) DEFAULT NULL,
  `o3_8smw` decimal(5,1) DEFAULT NULL,
  `o3_1tmax` decimal(5,1) DEFAULT NULL,
  `o3_8tmax` decimal(5,1) DEFAULT NULL,
  `sensor_time` DATETIME NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid_time` (`uid`,`sensor_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `air_timeline_pm10` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `uid` varchar(7) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pm10_1tmw` decimal(5,1) DEFAULT NULL,
  `sensor_time` DATETIME NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid_time` (`uid`,`sensor_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `air_timeline_so2` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `uid` varchar(7) COLLATE utf8_unicode_ci DEFAULT NULL,
  `so2_1smw` decimal(5,1) DEFAULT NULL,
  `so2_1tmax` decimal(5,1) DEFAULT NULL,
  `so2_1tmw` decimal(5,1) DEFAULT NULL,
  `sensor_time` DATETIME NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uid_time` (`uid`,`sensor_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


# Truncate all temporary tables (just in case we forgot to drop them before)
truncate table airq_co_8smw;
truncate table airq_co_8tmax;

truncate table airq_no2_1smw;
truncate table airq_no2_1tmax;

truncate table airq_o3_1smw;
truncate table airq_o3_1tmax;
truncate table airq_o3_8smw;
truncate table airq_o3_8tmax;

truncate table airq_pm10_1tmw;

truncate table airq_so2_1smw;
truncate table airq_so2_1tmax;
truncate table airq_so2_1tmw;


# Populate the temporary emission tables with the current data from the csv files; the second operation will set time to null if the value was null. This means, e.g. that if we download data too early from UBA and end up with csv files with empty values, we also set the time to NULL. This is important, because only then the later COALESCE function to update the air_quality_germany table will fail (which it should, as otherwise we change time but show old values).

# CO 8SMW
load data local infile '/tmp/envDB/air/DE/CO-8SMW-clean.csv' into table airq_co_8smw fields terminated by ';' enclosed by '"' lines terminated by '\n' (station_code, station_name, @dummy, @dummy, @dummy, @dummy, @co_8smw, time) SET co_8smw = nullif(@co_8smw,'');
update airq_co_8smw set time=NULL where co_8smw IS NULL;

# load local date infile does not have on duplicate key routine, so we emulate the behaviour. 
# see: https://stackoverflow.com/questions/15271202/mysql-load-data-infile-with-on-duplicate-key-update
load data local infile '/tmp/envDB/air/DE/CO-8SMW_MAX-clean.csv' into table airq_co_8tmax fields terminated by ';' enclosed by '"' lines terminated by '\n' (station_code, station_name, @dummy, @dummy, @dummy, @dummy, @co_8tmax, time) SET co_8tmax = nullif(@co_8tmax,'');
update airq_co_8tmax set time=NULL where co_8tmax IS NULL;

# NO2
load data local infile '/tmp/envDB/air/DE/NO2-1SMW-clean.csv' into table airq_no2_1smw fields terminated by ';' enclosed by '"' lines terminated by '\n' (station_code, station_name, @dummy, @dummy, @dummy, @dummy, @no2_1smw, time) SET no2_1smw = nullif(@no2_1smw,'');
update airq_no2_1smw set time=NULL where no2_1smw IS NULL;

load data local infile '/tmp/envDB/air/DE/NO2-1SMW_MAX-clean.csv' into table airq_no2_1tmax fields terminated by ';' enclosed by '"' lines terminated by '\n' (station_code, station_name, @dummy, @dummy, @dummy, @dummy, @no2_1tmax, time) SET no2_1tmax = nullif(@no2_1tmax,'');
update airq_no2_1tmax set time=NULL where no2_1tmax IS NULL;

# O3
load data local infile '/tmp/envDB/air/DE/O3-1SMW-clean.csv' into table airq_o3_1smw fields terminated by ';' enclosed by '"' lines terminated by '\n' (station_code, station_name, @dummy, @dummy, @dummy, @dummy, @o3_1smw, time) SET o3_1smw = nullif(@o3_1smw,'');
update airq_o3_1smw set time=NULL where o3_1smw IS NULL;

# O3
load data local infile '/tmp/envDB/air/DE/O3-8SMW-clean.csv' into table airq_o3_8smw fields terminated by ';' enclosed by '"' lines terminated by '\n' (station_code, station_name, @dummy, @dummy, @dummy, @dummy, @o3_8smw, time) SET o3_8smw = nullif(@o3_8smw,'');
update airq_o3_8smw set time=NULL where o3_8smw IS NULL;

# O3
load data local infile '/tmp/envDB/air/DE/O3-1SMW_MAX-clean.csv' into table airq_o3_1tmax fields terminated by ';' enclosed by '"' lines terminated by '\n' (station_code, station_name, @dummy, @dummy, @dummy, @dummy, @o3_1tmax, time) SET o3_1tmax = nullif(@o3_1tmax,'');
update airq_o3_1tmax set time=NULL where o3_1tmax IS NULL;

# O3
load data local infile '/tmp/envDB/air/DE/O3-8SMW_MAX-clean.csv' into table airq_o3_8tmax fields terminated by ';' enclosed by '"' lines terminated by '\n' (station_code, station_name, @dummy, @dummy, @dummy, @dummy, @o3_8tmax, time) SET o3_8tmax = nullif(@o3_8tmax,'');
update airq_o3_8tmax set time=NULL where o3_8tmax IS NULL;

# PM10
load data local infile '/tmp/envDB/air/DE/PM10-1TMW-clean.csv' into table airq_pm10_1tmw fields terminated by ';' enclosed by '"' lines terminated by '\n' (station_code, station_name, @dummy, @dummy, @dummy, @dummy, @pm10_1tmw, time) SET pm10_1tmw = nullif(@pm10_1tmw,'');
update airq_pm10_1tmw set time=NULL where pm10_1tmw IS NULL;

# SO2
load data local infile '/tmp/envDB/air/DE/SO2-1SMW-clean.csv' into table airq_so2_1smw fields terminated by ';' enclosed by '"' lines terminated by '\n' (station_code, station_name, @dummy, @dummy, @dummy, @dummy, @so2_1smw, time) SET so2_1smw = nullif(@so2_1smw,'');
update airq_so2_1smw set time=NULL where so2_1smw IS NULL;

load data local infile '/tmp/envDB/air/DE/SO2-1SMW_MAX-clean.csv' into table airq_so2_1tmax fields terminated by ';' enclosed by '"' lines terminated by '\n' (station_code, station_name, @dummy, @dummy, @dummy, @dummy, @so2_1tmax, time) SET so2_1tmax = nullif(@so2_1tmax,'');
update airq_so2_1tmax set time=NULL where so2_1tmax IS NULL;

load data local infile '/tmp/envDB/air/DE/SO2-1TMW-clean.csv' into table airq_so2_1tmw fields terminated by ';' enclosed by '"' lines terminated by '\n' (station_code, station_name, @dummy, @dummy, @dummy, @dummy, @so2_1tmw, time) SET so2_1tmw = nullif(@so2_1tmw,'');
update airq_so2_1tmw set time=NULL where so2_1tmw IS NULL;


#####

# Update air_qaulity_germany from the temp tables; use coalesce to only update if we have new, non-empty values!

UPDATE air_quality_germany,airq_pm10_1tmw SET air_quality_germany.pm10_1tmw=COALESCE(airq_pm10_1tmw.pm10_1tmw, air_quality_germany.pm10_1tmw), air_quality_germany.pm10_1tmw_time=COALESCE(airq_pm10_1tmw.time, air_quality_germany.pm10_1tmw_time), air_quality_germany.sensor_time=CASE WHEN airq_pm10_1tmw.time > air_quality_germany.sensor_time THEN airq_pm10_1tmw.time ELSE air_quality_germany.sensor_time END WHERE air_quality_germany.uid=airq_pm10_1tmw.station_code;

UPDATE air_quality_germany,airq_co_8smw SET air_quality_germany.co_8smw=COALESCE(airq_co_8smw.co_8smw, air_quality_germany.co_8smw), air_quality_germany.co_8smw_time=COALESCE(airq_co_8smw.time, air_quality_germany.co_8smw_time), air_quality_germany.sensor_time=CASE WHEN airq_co_8smw.time > air_quality_germany.sensor_time THEN airq_co_8smw.time ELSE air_quality_germany.sensor_time END WHERE air_quality_germany.uid=airq_co_8smw.station_code;

UPDATE air_quality_germany,airq_co_8tmax SET air_quality_germany.co_8tmax=COALESCE(airq_co_8tmax.co_8tmax, air_quality_germany.co_8tmax), air_quality_germany.co_8tmax_time=COALESCE(airq_co_8tmax.time, air_quality_germany.co_8tmax_time), air_quality_germany.sensor_time=CASE WHEN airq_co_8tmax.time > air_quality_germany.sensor_time THEN airq_co_8tmax.time ELSE air_quality_germany.sensor_time END WHERE air_quality_germany.uid=airq_co_8tmax.station_code;

UPDATE air_quality_germany,airq_no2_1smw SET air_quality_germany.no2_1smw=COALESCE(airq_no2_1smw.no2_1smw, air_quality_germany.no2_1smw), air_quality_germany.no2_1smw_time=COALESCE(airq_no2_1smw.time, air_quality_germany.no2_1smw_time), air_quality_germany.sensor_time=CASE WHEN airq_no2_1smw.time > air_quality_germany.sensor_time THEN airq_no2_1smw.time ELSE air_quality_germany.sensor_time END WHERE air_quality_germany.uid=airq_no2_1smw.station_code;

UPDATE air_quality_germany,airq_no2_1tmax SET air_quality_germany.no2_1tmax=COALESCE(airq_no2_1tmax.no2_1tmax, air_quality_germany.no2_1tmax), air_quality_germany.no2_1tmax_time=COALESCE(airq_no2_1tmax.time, air_quality_germany.no2_1tmax_time), air_quality_germany.sensor_time=CASE WHEN airq_no2_1tmax.time > air_quality_germany.sensor_time THEN airq_no2_1tmax.time ELSE air_quality_germany.sensor_time END WHERE air_quality_germany.uid=airq_no2_1tmax.station_code;

UPDATE air_quality_germany,airq_o3_1smw SET air_quality_germany.o3_1smw=COALESCE(airq_o3_1smw.o3_1smw, air_quality_germany.o3_1smw), air_quality_germany.o3_1smw_time=COALESCE(airq_o3_1smw.time, air_quality_germany.o3_1smw_time), air_quality_germany.sensor_time=CASE WHEN airq_o3_1smw.time > air_quality_germany.sensor_time THEN airq_o3_1smw.time ELSE air_quality_germany.sensor_time END WHERE air_quality_germany.uid=airq_o3_1smw.station_code;

UPDATE air_quality_germany,airq_o3_8smw SET air_quality_germany.o3_8smw=COALESCE(airq_o3_8smw.o3_8smw, air_quality_germany.o3_8smw), air_quality_germany.o3_8smw_time=COALESCE(airq_o3_8smw.time, air_quality_germany.o3_8smw_time), air_quality_germany.sensor_time=CASE WHEN airq_o3_8smw.time > air_quality_germany.sensor_time THEN airq_o3_8smw.time ELSE air_quality_germany.sensor_time END WHERE air_quality_germany.uid=airq_o3_8smw.station_code;

UPDATE air_quality_germany,airq_o3_1tmax SET air_quality_germany.o3_1tmax=COALESCE(airq_o3_1tmax.o3_1tmax, air_quality_germany.o3_1tmax), air_quality_germany.o3_1tmax_time=COALESCE(airq_o3_1tmax.time, air_quality_germany.o3_1tmax_time), air_quality_germany.sensor_time=CASE WHEN airq_o3_1tmax.time > air_quality_germany.sensor_time THEN airq_o3_1tmax.time ELSE air_quality_germany.sensor_time END WHERE air_quality_germany.uid=airq_o3_1tmax.station_code;

UPDATE air_quality_germany,airq_o3_8tmax SET air_quality_germany.o3_8tmax=COALESCE(airq_o3_8tmax.o3_8tmax, air_quality_germany.o3_8tmax), air_quality_germany.o3_8tmax_time=COALESCE(airq_o3_8tmax.time, air_quality_germany.o3_8tmax_time), air_quality_germany.sensor_time=CASE WHEN airq_o3_8tmax.time > air_quality_germany.sensor_time THEN airq_o3_8tmax.time ELSE air_quality_germany.sensor_time END WHERE air_quality_germany.uid=airq_o3_8tmax.station_code;

UPDATE air_quality_germany,airq_so2_1smw SET air_quality_germany.so2_1smw=COALESCE(airq_so2_1smw.so2_1smw, air_quality_germany.so2_1smw), air_quality_germany.so2_1smw_time=COALESCE(airq_so2_1smw.time, air_quality_germany.so2_1smw_time), air_quality_germany.sensor_time=CASE WHEN airq_so2_1smw.time > air_quality_germany.sensor_time THEN airq_so2_1smw.time ELSE air_quality_germany.sensor_time END WHERE air_quality_germany.uid=airq_so2_1smw.station_code;

UPDATE air_quality_germany,airq_so2_1tmax SET air_quality_germany.so2_1tmax=COALESCE(airq_so2_1tmax.so2_1tmax, air_quality_germany.so2_1tmax), air_quality_germany.so2_1tmax_time=COALESCE(airq_so2_1tmax.time, air_quality_germany.so2_1tmax_time), air_quality_germany.sensor_time=CASE WHEN airq_so2_1tmax.time > air_quality_germany.sensor_time THEN airq_so2_1tmax.time ELSE air_quality_germany.sensor_time END WHERE air_quality_germany.uid=airq_so2_1tmax.station_code;

UPDATE air_quality_germany,airq_so2_1tmw SET air_quality_germany.so2_1tmw=COALESCE(airq_so2_1tmw.so2_1tmw, air_quality_germany.so2_1tmw), air_quality_germany.so2_1tmw_time=COALESCE(airq_so2_1tmw.time, air_quality_germany.so2_1tmw_time), air_quality_germany.sensor_time=CASE WHEN airq_so2_1tmw.time > air_quality_germany.sensor_time THEN airq_so2_1tmw.time ELSE air_quality_germany.sensor_time END WHERE air_quality_germany.uid=airq_so2_1tmw.station_code;

#####

# Update with proper uuid from stations table
UPDATE `air_quality_germany`, `stations` SET `air_quality_germany`.`uuid`=`stations`.`uuid` WHERE `air_quality_germany`.`uid`=`stations`.`uid` AND `air_quality_germany`.`category`=`stations`.`category`;

INSERT INTO `stations_air` SELECT uuid, var1, var2, var3, var4 FROM `air_quality_germany` ON DUPLICATE KEY UPDATE uuid=`air_quality_germany`.uuid, type=`air_quality_germany`.var1, area=`air_quality_germany`.var2, area_subcat=`air_quality_germany`.var3, ozone_class=`air_quality_germany`.var4;

# Set inactive stations and stations which neither measure co, no2, o3, pm10, so2
UPDATE `air_quality_germany` SET op_state='inactive' WHERE `air_quality_germany`.co_8smw IS NULL and `air_quality_germany`.co_8tmax IS NULL and `air_quality_germany`.no2_1smw IS NULL and `air_quality_germany`.no2_1tmax IS NULL and `air_quality_germany`.o3_1smw IS NULL and `air_quality_germany`.o3_8smw IS NULL and `air_quality_germany`.o3_1tmax IS NULL and `air_quality_germany`.o3_8tmax IS NULL and `air_quality_germany`.pm10_1tmw IS NULL and `air_quality_germany`.so2_1smw IS NULL and `air_quality_germany`.so2_1tmax IS NULL and `air_quality_germany`.so2_1tmw IS NULL;



# delete all temporary tables
DROP TABLE IF EXISTS `airq_co_8smw`;
DROP TABLE IF EXISTS `airq_co_8tmax`;
DROP TABLE IF EXISTS `airq_no2_1smw`;
DROP TABLE IF EXISTS `airq_no2_1tmax`;
DROP TABLE IF EXISTS `airq_o3_1smw`;
DROP TABLE IF EXISTS `airq_o3_8smw`;
DROP TABLE IF EXISTS `airq_o3_1tmax`;
DROP TABLE IF EXISTS `airq_o3_8tmax`;
DROP TABLE IF EXISTS `airq_pm10_1tmw`;
DROP TABLE IF EXISTS `airq_so2_1smw`;
DROP TABLE IF EXISTS `airq_so2_1tmax`;
DROP TABLE IF EXISTS `airq_so2_1tmw`;

#####

# create air_current table
CREATE TABLE IF NOT EXISTS `air_current` (
  `uuid` int NOT NULL,
  `uid` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `co_8smw` decimal(5,1) DEFAULT NULL,
  `co_8smw_time` DATETIME DEFAULT NULL,
  `co_8tmax` decimal(5,1) DEFAULT NULL,
  `co_8tmax_time` DATETIME DEFAULT NULL,
  `no2_1smw` decimal(5,1) DEFAULT NULL,
  `no2_1smw_time` DATETIME DEFAULT NULL,
  `no2_1tmax` decimal(5,1) DEFAULT NULL,
  `no2_1tmax_time` DATETIME DEFAULT NULL,
  `o3_1smw` decimal(5,1) DEFAULT NULL,
  `o3_1smw_time` DATETIME DEFAULT NULL,
  `o3_8smw` decimal(5,1) DEFAULT NULL,
  `o3_8smw_time` DATETIME DEFAULT NULL,
  `o3_1tmax` decimal(5,1) DEFAULT NULL,
  `o3_1tmax_time` DATETIME DEFAULT NULL,
  `o3_8tmax` decimal(5,1) DEFAULT NULL,
  `o3_8tmax_time` DATETIME DEFAULT NULL,
  `pm10_1tmw` decimal(5,1) DEFAULT NULL,
  `pm10_1tmw_time` DATETIME DEFAULT NULL,
  `so2_1smw` decimal(5,1) DEFAULT NULL,
  `so2_1smw_time` DATETIME DEFAULT NULL,
  `so2_1tmax` decimal(5,1) DEFAULT NULL,
  `so2_1tmax_time` DATETIME DEFAULT NULL,
  `so2_1tmw` decimal(5,1) DEFAULT NULL,
  `so2_1tmw_time` DATETIME DEFAULT NULL,
  `last_mod` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

# update air_current
INSERT INTO `air_current` SELECT `uuid`, `uid`, `co_8smw`, `co_8smw_time`, `co_8tmax`, `co_8tmax_time`, `no2_1smw`, `no2_1smw_time`, `no2_1tmax`, `no2_1tmax_time`, `o3_1smw`, `o3_1smw_time`, `o3_8smw`, `o3_8smw_time`, `o3_1tmax`, `o3_1tmax_time`, `o3_8tmax`, `o3_8tmax_time`, `pm10_1tmw`, `pm10_1tmw_time`, `so2_1smw`, `so2_1smw_time`, `so2_1tmax`, `so2_1tmax_time`, `so2_1tmw`, `so2_1tmw_time`, null FROM `air_quality_germany` WHERE `op_state`='active' ON DUPLICATE KEY UPDATE `air_current`.`uuid`=`air_quality_germany`.`uuid`, `air_current`.`uid`=`air_quality_germany`.`uid`, `air_current`.`co_8smw`=CASE WHEN `air_quality_germany`.`co_8smw_time` > `air_current`.`co_8smw_time` THEN `air_quality_germany`.`co_8smw` ELSE `air_current`.`co_8smw` END, `air_current`.`co_8smw_time`=CASE WHEN `air_quality_germany`.`co_8smw_time` > `air_current`.`co_8smw_time` THEN `air_quality_germany`.`co_8smw_time` ELSE `air_current`.`co_8smw_time` END, `air_current`.`co_8tmax`=CASE WHEN `air_quality_germany`.`co_8tmax_time` > `air_current`.`co_8tmax_time` THEN `air_quality_germany`.`co_8tmax` ELSE `air_current`.`co_8tmax` END, `air_current`.`co_8tmax_time`=CASE WHEN `air_quality_germany`.`co_8tmax_time` > `air_current`.`co_8tmax_time` THEN `air_quality_germany`.`co_8tmax_time` ELSE `air_current`.`co_8tmax_time` END, `air_current`.`no2_1smw`=CASE WHEN `air_quality_germany`.`no2_1smw_time` > `air_current`.`no2_1smw_time` THEN `air_quality_germany`.`no2_1smw` ELSE `air_current`.`no2_1smw` END, `air_current`.`no2_1smw_time`=CASE WHEN `air_quality_germany`.`no2_1smw_time` > `air_current`.`no2_1smw_time` THEN `air_quality_germany`.`no2_1smw_time` ELSE `air_current`.`no2_1smw_time` END, `air_current`.`no2_1tmax`=CASE WHEN `air_quality_germany`.`no2_1tmax_time` > `air_current`.`no2_1tmax_time` THEN `air_quality_germany`.`no2_1tmax` ELSE `air_current`.`no2_1tmax` END, `air_current`.`no2_1tmax_time`=CASE WHEN  `air_quality_germany`.`no2_1tmax_time` > `air_current`.`no2_1tmax_time` THEN `air_quality_germany`.`no2_1tmax_time` ELSE `air_current`.`no2_1tmax_time` END, `air_current`.`o3_1smw`=CASE WHEN `air_quality_germany`.`o3_1smw_time` > `air_current`.`o3_1smw_time` THEN `air_quality_germany`.`o3_1smw` ELSE `air_current`.`o3_1smw` END, `air_current`.`o3_1smw_time`=CASE WHEN  `air_quality_germany`.`o3_1smw_time` > `air_current`.`o3_1smw_time` THEN `air_quality_germany`.`o3_1smw_time` ELSE `air_current`.`o3_1smw_time` END, `air_current`.`o3_8smw`=CASE WHEN `air_quality_germany`.`o3_8smw_time` > `air_current`.`o3_8smw_time` THEN `air_quality_germany`.`o3_8smw` ELSE `air_current`.`o3_8smw` END, `air_current`.`o3_8smw_time`=CASE WHEN `air_quality_germany`.`o3_8smw_time` > `air_current`.`o3_8smw_time` THEN `air_quality_germany`.`o3_8smw_time` ELSE `air_current`.`o3_8smw_time` END, `air_current`.`o3_1tmax`=CASE WHEN `air_quality_germany`.`o3_1tmax_time` > `air_current`.`o3_1tmax_time` THEN `air_quality_germany`.`o3_1tmax` ELSE `air_current`.`o3_1tmax` END, `air_current`.`o3_1tmax_time`=CASE WHEN  `air_quality_germany`.`o3_1tmax_time` > `air_current`.`o3_1tmax_time` THEN `air_quality_germany`.`o3_1tmax_time` ELSE `air_current`.`o3_1tmax_time` END, `air_current`.`o3_8tmax`=CASE WHEN `air_quality_germany`.`o3_8tmax_time` > `air_current`.`o3_8tmax_time` THEN `air_quality_germany`.`o3_8tmax` ELSE `air_current`.`o3_8tmax` END, `air_current`.`o3_8tmax_time`=CASE WHEN `air_quality_germany`.`o3_8tmax_time` > `air_current`.`o3_8tmax_time` THEN `air_quality_germany`.`o3_8tmax_time` ELSE `air_current`.`o3_8tmax_time` END, `air_current`.`pm10_1tmw`=CASE WHEN `air_quality_germany`.`pm10_1tmw_time` > `air_current`.`pm10_1tmw_time` THEN `air_quality_germany`.`pm10_1tmw` ELSE `air_current`.`pm10_1tmw` END, `air_current`.`pm10_1tmw_time`=CASE WHEN `air_quality_germany`.`pm10_1tmw_time` > `air_current`.`pm10_1tmw_time` THEN `air_quality_germany`.`pm10_1tmw_time` ELSE `air_current`.`pm10_1tmw_time` END, `air_current`.`so2_1smw`=CASE WHEN `air_quality_germany`.`so2_1smw_time` > `air_current`.`so2_1smw_time` THEN `air_quality_germany`.`so2_1smw` ELSE `air_current`.`so2_1smw` END, `air_current`.`so2_1smw_time`=CASE WHEN `air_quality_germany`.`so2_1smw_time` > `air_current`.`so2_1smw_time` THEN `air_quality_germany`.`so2_1smw_time` ELSE `air_current`.`so2_1smw_time` END, `air_current`.`so2_1tmax`=CASE WHEN `air_quality_germany`.`so2_1tmax_time` > `air_current`.`so2_1tmax_time` THEN `air_quality_germany`.`so2_1tmax` ELSE `air_current`.`so2_1tmax` END, `air_current`.`so2_1tmax_time`=CASE WHEN `air_quality_germany`.`so2_1tmax_time` > `air_current`.`so2_1tmax_time` THEN `air_quality_germany`.`so2_1tmax_time` ELSE `air_current`.`so2_1tmax_time` END, `air_current`.`so2_1tmw`=CASE WHEN `air_quality_germany`.`so2_1tmw_time` > `air_current`.`so2_1tmw_time` THEN `air_quality_germany`.`so2_1tmw` ELSE `air_current`.`so2_1tmw` END, `air_current`.`so2_1tmw_time`=CASE WHEN `air_quality_germany`.`so2_1tmw_time` > `air_current`.`so2_1tmw_time` THEN `air_quality_germany`.`so2_1tmw_time` ELSE `air_current`.`so2_1tmw_time` END;

# update active stations
UPDATE `stations`, `air_quality_germany` SET `stations`.op_state='inactive' WHERE `stations`.uuid=`air_quality_germany`.uuid AND `air_quality_germany`.co_8smw IS NULL and `air_quality_germany`.co_8tmax IS NULL and `air_quality_germany`.no2_1smw IS NULL and `air_quality_germany`.no2_1tmax IS NULL and `air_quality_germany`.o3_1smw IS NULL and `air_quality_germany`.o3_8smw IS NULL and `air_quality_germany`.o3_1tmax IS NULL and `air_quality_germany`.o3_8tmax IS NULL and `air_quality_germany`.pm10_1tmw IS NULL and `air_quality_germany`.so2_1smw IS NULL and `air_quality_germany`.so2_1tmax IS NULL and `air_quality_germany`.so2_1tmw IS NULL;

UPDATE `stations`, `air_quality_germany` SET `stations`.`last_update`=CASE WHEN `air_quality_germany`.`sensor_time` > `stations`.`last_update` THEN `air_quality_germany`.`sensor_time` ELSE `stations`.`last_update` END WHERE `stations`.`uuid`=`air_quality_germany`.`uuid`;
