#!/bin/bash

# This script scrapes air quality / polluion data from the website of the Umweltbundesamt (UBA)
# This script can take up to five arguments, all are optional. if no arguments are specified, the script tries to get most recent data for all compounds for all states
#
# optional: -c the pollutant compound [CO,NO2,O3,PM10,SO2]
# optional: -p the measurement period [1TMW, 1SMW_MAX, 8SMW_MAX, 1SMW, 8SMW] <-- not all pollutants allow all periods
# optional: -d the date in format YYYYMMDD (if missing, then current date will be used)
# optional: -t the time in format HH [1-24] (if missing, then the previous hour will be used)
# optional: -s the state [BW BY BE BB HB HH HE MV NI NW RP SL SN ST SH TH UB] <-- individual states have more recent data than if we just get all states at once!
#
# Notice:
# UBA publishes data at certain times (plus 5-10minutes in practice!):
# at 07:30 they release data from the previous night (data for 23-06)
# starting at 08:30 they release data for the previous hour (so at 08:30 we get data for 07:00)
# 23:30 is the last data published (for 22:00)
# Copyleft: I hereby release this script to the public domain. I'd still be happy to hear from you if you use it, but just out of curiosity :)
# Bugs / Contributions: If you find bugs or want to contribute code, see my github account at: https://github.com/flipsa

usage() { echo "Usage: $0 [-c <CO|NO2|O3|PM10|SO2>] [-p <1TMW|1SMW_MAX|8SMW_MAX|1SMW|8SMW>] [-d <YYYYMMDD>] [-t <1|...|24>] [-s <BW|BY|BE|BB|HB|HH|HE|MV|NI|NW|RP|SL|SN|ST|SH|TH|UB>]" 1>&2; exit 1; }

# get the command line arguments: all valuers are required, except the state (optional; if omitted then data for Germany as a whole is downloaded)
# FIXME: return better usage instructions such as allowed combinations of pollutant (COMP) and data-type (PERIOD)
while getopts :c:p:d:t:s: o
do
    case "${o}" in
        c)
            c=${OPTARG}
#            if [ "$c" = "CO" ] || [ "$c" = "NO2" ] || [ "$c" = "O3" ] || [ "$c" = "PM10" ] || [ "$c" = "SO2" ]
#	    then echo $usage
#	    fi
            ;;
        p)
            p=${OPTARG}

#	    if [ "$c" = "CO" ]; then
#		if [ "$p" = "8SMW" ] || [ "$p" = "8SMW_MAX" ]
#			then echo $usage
#	    	fi
#	    fi

#            if [ "$c" = "NO2" ]; then
#                if [ "$p" = "1SMW" ] || [ "$p" = "1SMW_MAX" ]
#		then echo $usage
#		fi
#            fi

#            if [ "$c" = "O3" ]; then
#                if [ "$p" = "1SMW_MAX" ] || [ "$p" = "8SMW_MAX" ] || [ "$p" = "1SMW" ] || [ "$p" = "8SMW" ]
#		then echo $usage
#		fi
#            fi

#            if [ "$c" = "PM10" ]; then
#                if [ "$p" = "1TMW" ]
#		then echo $usage
#		fi
#            fi

#            if [ "$c" = "SO3" ]; then
#		if [ "$p" = "1TMW" ] || [ "$p" = "1SMW" ] || [ "$p" = "1SMW_MAX" ]
#		then echo $usage
#		fi
#            fi
            ;;

	d)
	    d=${OPTARG}
	    ;;

        t)
            t=${OPTARG}
#            ((t == 1 || t == 2 || t == 3 || t == 4 || t == 5 || t == 6 || t == 7 || t == 8 || t == 9 || t == 10 || t == 11 || t == 12 || t == 13 || t == 14 || t == 15 || t == 16 || t == 17 || t == 18 || t == 19 || t == 20 || t == 21 || t == 22 || t == 23 || t == 24)) || usage
            ;;

	s)
	    s=${OPTARG}
#            ((s == BW || s == BY || s == BE || s == BB || s == HB || s == HH || s == HE || s == MV || s == NI || s == NW || s == RP || s == SL || s == SN || s == ST || s == SH || s == TH || s == UB)) || usage
	    ;;
        *)
            usage
            ;;
    esac
done
shift $((OPTIND-1))

#if [ -z "${c}" ] || [ -z "${p}" ]; then
#    usage
#fi


# Get settings like paths, credentials, etc...
. /var/www/offene-umweltdaten.de/scrape/settings

# Set some variables for the download script
STATES="BE BB BW BY HB HH HE MV NI NW RP SL SN ST SH TH UB"
COMPS="PM10 CO O3 SO2 NO2"
#URL="http://www.umweltbundesamt.de/luftdaten/stations/locations.csv?"
# UBA changed their backend some time in march / april 2017
URL="https://www.umweltbundesamt.de/uaq/csv/stations/data?"

logger(){
	# $1 is MSG
	# $2 is log level
	if [ "$VERBOSE" -gt $2 ]; then
		echo "$1"
	fi

	if [ "$LOGGING" -gt $2 ]; then
		echo $(date +"%Y-%m-%d %H:%M:%S") - $1 >> ""$AIR_LOG_DIR"/DE_AirQuality.log"
	fi
}

get_args(){
	# compound [CO,NO2,O3,PM10,SO2]
	COMP=${c}

	# period
	PERIOD=${p}

	# time [1,2, ..., 13, ..., 24]
	TIME=${t}

	# date in YYMMDD format
	DATE=${d}

	# state
	STATE=${s}

	#echo "arguments are: state ->""$STATE"", PERIOD -> ""$PERIOD"", TIME -> ""$TIME"", DATE -> ""$DATE"", COMP -> ""$COMP""";
}

get_manual_timestamp(){
# if date and time are given, set timestamp
if [ "$DATE" != "" ] && [ "$TIME" != "" ]; then
	YEAR=${DATE%????}
	DAY=${DATE#??????}
	MON=${DATE#$YEAR}
	MON=${MON%$DAY}
	MAN_DATE=$(gawk -v t="$DATE""$TIME"0000 'BEGIN {gsub(/../, "& ", t); sub(/ /,"",t); print mktime(t)}')
	TIMESTAMP=$(date +"%Y-%m-%d %H:%M" --date=@$MAN_DATE)
	DAYSTAMP=$(date +"%Y-%m-%d" --date=@$MAN_DATE)
fi
}

get_period(){
	if [ "$COMP" = "PM10" ]; then
			PERIODS="1TMW";
	elif [ "$COMP" = "CO" ]; then
			PERIODS="8SMW 8SMW_MAX";
	elif [ "$COMP" = "O3" ]; then
			PERIODS="1SMW 1SMW_MAX 8SMW 8SMW_MAX";
	elif [ "$COMP" = "SO2" ]; then
			PERIODS="1SMW 1TMW 1SMW_MAX";
	elif [ "$COMP" = "NO2" ]; then
			PERIODS="1SMW 1SMW_MAX";
	fi;
}

get_time(){
	# if time is not provided, get the most recent data; for fully automatic mode, this must happen before getting the date, because the date depends on the time.
	TIME=$(date +"%H" -d "1 hour ago");

	if [ "$TIME" = "0" ]; then
		#TIME="24";
		DATE=$(date +%Y%m%d)
	fi;

	if [ "$TIME" > "23" ]; then
		DATE=$(date +%Y%m%d -d "1 day ago");
	fi;

	logger "No time specified. Using: $DATE - $TIME" 1
}

# If no date was given at runtime, figure out what the most recent data for this compound and period is, and set date, time and the timestamp accordingly!
get_date(){

	# if time is not provided, get the most recent data
	if [ "${t}" = "" ]; then

		TIME=$(date +"%H" -d "1 hour ago");
		logger "No time specified, using current time minus 1 hour: $TIME" 1

	fi

	# changed after uba new api 2017
	#if [ "$TIME" = "0" ]; then
	#	TIME="24";
	#fi;

	# date is not provided, so set date dependent of period
	DATE=$(date +%Y%m%d);
	logger "No date specified, taking today's date: $DATE" 1

	if [ "$PERIOD" = "1TMW" ] || [ "$PERIOD" = "1SMW_MAX" ] || [ "$PERIOD" = "8SMW_MAX" ]; then
		logger "PROBLEM: "$DATE" "$TIME":00:00" 1
		if [ "$TIME" = "23" ] || [ "$TIME" = "24" ] || [ "$TIME" = "01" ] || [ "$TIME" = "02" ] || [ "$TIME" = "03" ] || [ "$TIME" = "04" ] || [ "$TIME" = "05" ] || [ "$TIME" = "06" ]; then
			DATE=$(date +%Y%m%d -d "2 days ago");
			#if [ "$TIMESTAMP" = "" ]; then
				TIMESTAMP=$(date +"%Y-%m-%d" -d "2 days ago");
				logger "Timestamp: $TIMESTAMP" 1
			#fi
		else
			logger "if TIME between 07(=09) and 22(=24) we get yesterday's data. hour does not matter for these periods." 1
			DATE=$(date +%Y%m%d -d "1 day ago");
			#if [ "$TIMESTAMP" = "" ]; then
				TIMESTAMP=$(date +"%Y-%m-%d" -d "1 day ago");
				logger "Timestamp: $TIMESTAMP" 1
			#fi
		fi;
		# Calculate epoch from our date ad time
		EPOCH_END=$(date "+%s" -d "$DATE $TIME:00:00")
		#let EPOCH_START=""$EPOCH_END"-86400"
		EPOCH_START=$(($EPOCH_END-86400))
	fi;

	if [ "$PERIOD" = "1SMW" ]; then
		if [ "$TIME" = "23" ] || [ "$TIME" = "24" ] || [ "$TIME" = "01" ] || [ "$TIME" = "02" ] || [ "$TIME" = "03" ] || [ "$TIME" = "04" ] || [ "$TIME" = "05" ] || [ "$TIME" = "06" ]; then
			logger "For 1SMW values, if TIME is between 23 and 08 then we have to set the time to previous day at 22:00 when last results are published until 8 in the morning" 1
			DATE=$(date +%Y%m%d -d "1 day ago");
			#if [ "$TIMESTAMP" = "" ]; then
				TIMESTAMP=$(date +"%Y-%m-%d 21:00" -d "1 day ago");
				logger "Timestamp: $TIMESTAMP" 1
			#fi
			TIME="21";
		else
			logger "For 1SMW values of current date, last published value is for same day 2hours before." 1
			MIN=$(date +"%M");
			if [ $MIN -gt 49 ]; then
				TIME=$(date +"%H" -d "1 hours ago");
				#echo """$TIMESTAMP"""
				#if [ "$TIMESTAMP" = "" ]; then
					TIMESTAMP=$(date +"%Y-%m-%d %H:00" -d "1 hour ago");
					logger "Timestamp: $TIMESTAMP" 1
				#fi
			else
				TIME=$(date +"%H" -d "2 hours ago");
				#if [ "$TIMESTAMP" = "" ]; then
					TIMESTAMP=$(date +"%Y-%m-%d %H:00" -d "2 hours ago");
					logger "Timestamp: $TIMESTAMP" 1
				#fi
			fi;
		fi;
                # Calculate epoch from our date ad time
                EPOCH_END=$(date "+%s" -d "$DATE $TIME:00:00")
                #let EPOCH_START=""$EPOCH_END"-3600"
		EPOCH_START=$(($EPOCH_END-3600))
	fi;

	if [ "$PERIOD" = "8SMW" ]; then
		if [ "$TIME" = "23" ] || [ "$TIME" = "24" ] || [ "$TIME" = "01" ] || [ "$TIME" = "02" ] || [ "$TIME" = "03" ] || [ "$TIME" = "04" ] || [ "$TIME" = "05" ] || [ "$TIME" = "06" ]; then
			DATE=$(date +%Y%m%d -d "1 day ago");
			#if [ "$TIMESTAMP" = "" ]; then
				TIMESTAMP=$(date +"%Y-%m-%d 22:00" -d "1 day ago");
				logger "Timestamp: $TIMESTAMP" 1
			#fi
			TIME="22";
		else
			MIN=$(date +"%M");
			if [ $MIN -gt 49 ]; then
				TIME=$(date +"%H" -d "1 hours ago");
			#	if [ "$TIMESTAMP" = "" ]; then
					TIMESTAMP=$(date +"%Y-%m-%d %H:00" -d "1 hour ago");
				logger "Timestamp: $TIMESTAMP" 1
			#	fi
			else
				TIME=$(date +"%H" -d "2 hours ago");
			#	if [ "$TIMESTAMP" = "" ]; then
					TIMESTAMP=$(date +"%Y-%m-%d %H:00" -d "2 hours ago");
				logger "Timestamp: $TIMESTAMP" 1
			#	fi
			fi;
		fi;
                # Calculate epoch from our date ad time
                EPOCH_END=$(date "+%s" -d "$DATE $TIME:00:00")
                #let EPOCH_START=""$EPOCH_END"-3600"
		EPOCH_START=$(($EPOCH_END-3600))
	fi;
}

get_state(){
	for PERIOD in $PERIODS; do
	logger "Period: $PERIOD" 1
	if [ -z "${d}" ]; then
		get_date
	else
		get_manual_timestamp
	fi
	# if state is not specified, download data for all states
	# NOTICE: the UBA website is kaputt - it returns more recent data if we specify a state than if we omit it!
	#for STATE in $STATES; do
	#	if [ "$PERIOD" = "1SMW_MAX" ] || [ "$PERIOD" = "1TMW" ] || [ "$PERIOD" = "8SMW_MAX" ] && [ "$TIME" != '08' ] ; then
	#		# if we download daily data sets, only do it once in the morning
	#		continue
	#	else
	#		logger "Downloading $COMP for period $PERIOD for state: $STATE on date: $TIMESTAMP" 1
	#		download
	#	fi
	#done
	STATE=''

	if [ "$PERIOD" = "1SMW_MAX" ] || [ "$PERIOD" = "1TMW" ] || [ "$PERIOD" = "8SMW_MAX" ] && [ "$TIME" != '07' ] ; then
		# if we download daily data sets, only do it once in the morning
		logger "it's "$TIME" o clock - continuing" 1
		continue
	else
		logger "Downloading $COMP for period $PERIOD for state: $STATE on date: $TIMESTAMP" 1
		download
	fi

	if [ "$PERIOD" = "1SMW_MAX" ] || [ "$PERIOD" = "1TMW" ] || [ "$PERIOD" = "8SMW_MAX" ] && [ "$TIME" != '07' ] ; then
		logger "It's $DATE - $TIME:00 - Skipping $PERIOD for $COMP" 1
	else
		# Add newline at end of file
		find . \( -name "*.csv" \) -print | xargs sed -i -e '$G'

		# Concatenate all state files into one file with all states combined
		cat "$COMP"-"$PERIOD"*-clean".csv" >> $COMP"-"$PERIOD"-nl".csv

		# Delete empty lines from file (due to bad encoding, grep thinks files are binary, hence the "-a" option)
		grep -av "^$" $COMP"-"$PERIOD"-nl".csv > $COMP"-"$PERIOD"-clean".csv

		#cut -f1,3,4 -d ';' CO-8SMW-clean.csv > CO-left.csv
		#cut -f1,3,4 -d ';' CO-8SMW_MAX-clean.csv > CO-right.csv
		#join -j1 -t';' CO-left.csv CO-right.csv

		#mv -f $COMP"-"$PERIOD"-clean".csv ""$AIR_TEMP_DIR"/DE/"
		#rm *.csv
		rm $COMP"-"$PERIOD"-nl".csv
	fi
	done
}

download(){

	OUTFILE=""$COMP"-"$PERIOD"-"$DATE"-"$TIME"-"$STATE""
	# make sure we set the time interval correctly (1day for daily data, 1hour for hourly data)
	if [ "$PERIOD" = "1SMW_MAX" ] || [ "$PERIOD" = "1TMW" ] || [ "$PERIOD" = "8SMW_MAX" ]; then
                EPOCH_END=$(date "+%s" -d "$DATE $TIME:00:00")
                EPOCH_START=$(($EPOCH_END-86400))
	else
                EPOCH_END=$(date "+%s" -d "$DATE $TIME:00:00 + 1 hour")
                EPOCH_START=$(($EPOCH_END-3600))
	fi


	if [ "$VERBOSE" -gt 2 ]; then
		logger "Saving download to $OUTFILE" 1 | tee -a ""$AIR_LOG_DIR"/DE_AirQuality.log"
		#wget ""$URL"pollutant="$COMP"&data_type="$PERIOD"&date="$DATE"&hour="$TIME"&statename=&state="$STATE"" -O ""$AIR_TEMP_DIR"/DE/"$OUTFILE"-temp".csv
		wget ""$URL"pollutant[]="$COMP"&scope[]="$PERIOD"&group[]=pollutant&range[]="$EPOCH_START","$EPOCH_END"" -O ""$AIR_TEMP_DIR"/DE/"$OUTFILE"-temp".csv
	else
		#wget ""$URL"pollutant="$COMP"&data_type="$PERIOD"&date="$DATE"&hour="$TIME"&statename=&state="$STATE"" -O ""$AIR_TEMP_DIR"/DE/"$OUTFILE"-temp".csv > /dev/null 2>&1
		wget ""$URL"pollutant[]="$COMP"&scope[]="$PERIOD"&group[]=pollutant&range[]="$EPOCH_START","$EPOCH_END"" -O ""$AIR_TEMP_DIR"/DE/"$OUTFILE"-temp".csv > /dev/null 2>&1
	fi
	process
}

process(){

	logger "Processing file: $OUTFILE-temp.csv" 1
	#$(iconv --from-code=ISO-8859-1 --to-code=UTF-8 ""$OUTFILE"-temp1".csv > ""$OUTFILE"-temp".csv)
	# Replace wrongly encoded Umlauts
	#echo "REPLACING UMLAUTS"
	$(sed -i 's/Ã/ß/g' ""$AIR_TEMP_DIR"/DE/"$OUTFILE"-temp".csv)
	$(sed -i 's/Ãœ/Ü/g' ""$AIR_TEMP_DIR"/DE/"$OUTFILE"-temp".csv)
	$(sed -i 's/Ã¼/ü/g' ""$AIR_TEMP_DIR"/DE/"$OUTFILE"-temp".csv)
	$(sed -i 's/Ã–/Ö/g' ""$AIR_TEMP_DIR"/DE/"$OUTFILE"-temp".csv)
	$(sed -i 's/Ã¶/ö/g' ""$AIR_TEMP_DIR"/DE/"$OUTFILE"-temp".csv)
	$(sed -i 's/Ã„/Ä/g' ""$AIR_TEMP_DIR"/DE/"$OUTFILE"-temp".csv)
	$(sed -i 's/Ã¤/ä/g' ""$AIR_TEMP_DIR"/DE/"$OUTFILE"-temp".csv)
	# Delete the first line from the file
	$(sed -i '1d' ""$AIR_TEMP_DIR"/DE/"$OUTFILE"-temp".csv)
	$(sed -i 's/B\ /Berlin\ /g' ""$AIR_TEMP_DIR"/DE/"$OUTFILE"-temp".csv)

	#$(sed -i 's/-/\ -\ /g' ""$OUTFILE"-temp1".csv)
	#$(sed -i 's/\//\ /g' ""$OUTFILE"-temp1".csv)

	# If we are in manual mode and processing daily data, set the timestamp to only show the date but not the time. Rest is handled by the database (composite UNIQUE KEY on date and compound)
	if [ "$PERIOD" = "1TMW" ] || [ "$PERIOD" = "1SMW_MAX" ] || [ "$PERIOD" = "8SMW_MAX" ] && [ "$DAYSTAMP" != "" ]; then
		logger "Correcting timestamp for $COMP $PERIOD data to $DAYSTAMP" 1
		TIMESTAMP=$DAYSTAMP
	fi
	$(sed 's/$/;'"$TIMESTAMP"'/g' ""$AIR_TEMP_DIR"/DE/"$OUTFILE"-temp".csv > ""$AIR_TEMP_DIR"/DE/"$OUTFILE"-clean".csv)
	rm ""$AIR_TEMP_DIR"/DE/"$OUTFILE"-temp".csv
}

update_db(){

	logger "Updating database" 0

	mysql "$DB" < "$AIR_SQL_DE" -u"$DB_USER" -p"$DB_PW" -h"$DB_HOST"
	if [ "$TIME" = "07" ]; then
		#echo "updating pm10"

		mysql -h"$DB_HOST" -u"$DB_USER" -p"$DB_PW" -D"$DB" -e "INSERT INTO air_timeline_co SELECT null,uid,co_8smw,null,co_8smw_time FROM air_quality_germany WHERE co_8smw IS NOT NULL ON DUPLICATE KEY UPDATE air_timeline_co.co_8smw=air_quality_germany.co_8smw; \
INSERT INTO air_timeline_co SELECT null,uid,null,co_8tmax,co_8tmax_time FROM air_quality_germany WHERE co_8tmax IS NOT NULL ON DUPLICATE KEY UPDATE air_timeline_co.co_8tmax=air_quality_germany.co_8tmax;INSERT INTO air_timeline_no2 SELECT null,uid,no2_1smw,null,no2_1smw_time FROM air_quality_germany WHERE no2_1smw IS NOT NULL ON DUPLICATE KEY UPDATE air_timeline_no2.no2_1smw=air_quality_germany.no2_1smw;INSERT INTO air_timeline_no2 SELECT null,uid,null,no2_1tmax,no2_1tmax_time FROM air_quality_germany WHERE no2_1tmax IS NOT NULL ON DUPLICATE KEY UPDATE air_timeline_no2.no2_1tmax=air_quality_germany.no2_1tmax; \
INSERT INTO air_timeline_o3 SELECT null,uid,o3_1smw,null,null,null,o3_1smw_time FROM air_quality_germany WHERE o3_1smw IS NOT NULL ON DUPLICATE KEY UPDATE air_timeline_o3.o3_1smw=air_quality_germany.o3_1smw;INSERT INTO air_timeline_o3 SELECT null,uid,null,o3_8smw,null,null,o3_8smw_time FROM air_quality_germany WHERE o3_8smw IS NOT NULL ON DUPLICATE KEY UPDATE air_timeline_o3.o3_8smw=air_quality_germany.o3_8smw;INSERT INTO air_timeline_o3 SELECT null,uid,null,null,o3_1tmax,null,o3_1tmax_time FROM air_quality_germany WHERE o3_1tmax IS NOT NULL ON DUPLICATE KEY UPDATE air_timeline_o3.o3_1tmax=air_quality_germany.o3_1tmax;INSERT INTO air_timeline_o3 SELECT null,uid,null,null,null,o3_8tmax,o3_8tmax_time FROM air_quality_germany WHERE o3_8tmax IS NOT NULL ON DUPLICATE KEY UPDATE air_timeline_o3.o3_8tmax=air_quality_germany.o3_8tmax; \
INSERT INTO air_timeline_pm10 SELECT null,uid,pm10_1tmw,pm10_1tmw_time FROM air_quality_germany WHERE pm10_1tmw IS NOT NULL ON DUPLICATE KEY UPDATE air_timeline_pm10.pm10_1tmw=air_quality_germany.pm10_1tmw; \
INSERT INTO air_timeline_so2 SELECT null,uid,so2_1smw,null,null,so2_1smw_time FROM air_quality_germany WHERE so2_1smw IS NOT NULL ON DUPLICATE KEY UPDATE air_timeline_so2.so2_1smw=air_quality_germany.so2_1smw;INSERT INTO air_timeline_so2 SELECT null,uid,null,so2_1tmax,null,so2_1tmax_time FROM air_quality_germany WHERE so2_1tmax IS NOT NULL ON DUPLICATE KEY UPDATE air_timeline_so2.so2_1tmax=air_quality_germany.so2_1tmax;INSERT INTO air_timeline_so2 SELECT null,uid,null,null,so2_1tmw,so2_1tmw_time FROM air_quality_germany WHERE so2_1tmw IS NOT NULL ON DUPLICATE KEY UPDATE air_timeline_so2.so2_1tmw=air_quality_germany.so2_1tmw;"

	else
		#echo "not updating pm10"
		#mysql -h"$DB_HOST" -uwp_ud_user1 -pwp0815 -Dud_luft -e "insert into air_timeline select null,uid,sensor_time,co,no2,o3,null,so2,null from air_quality_germany where op_state='active' ON DUPLICATE KEY UPDATE air_timeline.co=air_quality_germany.co, air_timeline.no2=air_quality_germany.no2, air_timeline.o3=air_quality_germany.o3, air_timeline.pm10=null, air_timeline.so2=air_quality_germany.so2;"

		mysql -h"$DB_HOST" -u"$DB_USER" -p"$DB_PW" -D"$DB" -e "INSERT INTO air_timeline_co SELECT null,uid,co_8smw,null,co_8smw_time FROM air_quality_germany WHERE co_8smw IS NOT NULL ON DUPLICATE KEY UPDATE air_timeline_co.co_8smw=air_quality_germany.co_8smw;INSERT INTO air_timeline_co SELECT null,uid,null,co_8tmax,co_8tmax_time FROM air_quality_germany WHERE co_8tmax IS NOT NULL ON DUPLICATE KEY UPDATE air_timeline_co.co_8tmax=air_quality_germany.co_8tmax; \
INSERT INTO air_timeline_no2 SELECT null,uid,no2_1smw,null,no2_1smw_time FROM air_quality_germany WHERE no2_1smw IS NOT NULL ON DUPLICATE KEY UPDATE air_timeline_no2.no2_1smw=air_quality_germany.no2_1smw;INSERT INTO air_timeline_no2 SELECT null,uid,null,no2_1tmax,no2_1tmax_time FROM air_quality_germany WHERE no2_1tmax IS NOT NULL ON DUPLICATE KEY UPDATE air_timeline_no2.no2_1tmax=air_quality_germany.no2_1tmax; \
INSERT INTO air_timeline_o3 SELECT null,uid,o3_1smw,null,null,null,o3_1smw_time FROM air_quality_germany WHERE o3_1smw IS NOT NULL ON DUPLICATE KEY UPDATE air_timeline_o3.o3_1smw=air_quality_germany.o3_1smw;INSERT INTO air_timeline_o3 SELECT null,uid,null,o3_8smw,null,null,o3_8smw_time FROM air_quality_germany WHERE o3_8smw IS NOT NULL ON DUPLICATE KEY UPDATE air_timeline_o3.o3_8smw=air_quality_germany.o3_8smw;INSERT INTO air_timeline_o3 SELECT null,uid,null,null,o3_1tmax,null,o3_1tmax_time FROM air_quality_germany WHERE o3_1tmax IS NOT NULL ON DUPLICATE KEY UPDATE air_timeline_o3.o3_1tmax=air_quality_germany.o3_1tmax;INSERT INTO air_timeline_o3 SELECT null,uid,null,null,null,o3_8tmax,o3_8tmax_time FROM air_quality_germany WHERE o3_8tmax IS NOT NULL ON DUPLICATE KEY UPDATE air_timeline_o3.o3_8tmax=air_quality_germany.o3_8tmax; \
INSERT INTO air_timeline_so2 SELECT null,uid,so2_1smw,null,null,so2_1smw_time FROM air_quality_germany WHERE so2_1smw IS NOT NULL ON DUPLICATE KEY UPDATE air_timeline_so2.so2_1smw=air_quality_germany.so2_1smw;INSERT INTO air_timeline_so2 SELECT null,uid,null,so2_1tmax,null,so2_1tmax_time FROM air_quality_germany WHERE so2_1tmax IS NOT NULL ON DUPLICATE KEY UPDATE air_timeline_so2.so2_1tmax=air_quality_germany.so2_1tmax;INSERT INTO air_timeline_so2 SELECT null,uid,null,null,so2_1tmw,so2_1tmw_time FROM air_quality_germany WHERE so2_1tmw IS NOT NULL ON DUPLICATE KEY UPDATE air_timeline_so2.so2_1tmw=air_quality_germany.so2_1tmw;"

	fi
	logger "Database update complete!" 0
}

get_comp(){
	logger "Pollutant not given, getting most recent values for all polutants!" 1

	for COMP in $COMPS;	do

		logger "Compound: $COMP" 0

		# period
		PERIOD=${p}

		# time [1,2, ..., 13, ..., 24]
		TIME=${t}

		# date in YYMMDD format
		DATE=${d}

		# state
        	STATE=${s}

		get_manual_timestamp

		if [ -z "$PERIOD" ]; then
			get_period
		fi
			#if [ -z "$TIME" ]; then
			#	echo "getting time";
			#	get_time
			#fi



			# FIXME: should take state as an argument.
			if [ -z "$STATE" ]; then
				get_state
			else
				#echo "getting specific state";
				logger "Downloading $COMP - Period $PERIOD for state: $STATE on date: $TIMESTAMP" 1
				for PERIOD in $PERIODS; do
					if [ -z "${d}" ]; then
						get_date
					fi
					download
					echo "move1: ""$AIR_TEMP_DIR"/DE/"$OUTFILE"-clean".csv ""$AIR_TEMP_DIR"/DE/"$COMP"-"$PERIOD"-clean".csv"
					mv ""$AIR_TEMP_DIR"/DE/"$OUTFILE"-clean".csv ""$AIR_TEMP_DIR"/DE/"$COMP"-"$PERIOD"-clean".csv
				done
			fi
			#PERIOD="";
			#TIME="";
			#DATE="";
			#STATE="";

	done
	#exit
}

##### real script starts here #####

get_args

get_manual_timestamp

# Clean up left over files from previous runs
logger "Cleaning up temp files before run" 1
rm "$AIR_TEMP_DIR"/DE/*.csv > /dev/null 2>&1

logger "Creating directories if they don't exist" 1
mkdir -p ""$AIR_TEMP_DIR"/DE"

# Create empty files in case a download fails to prevent script interrupting when loading into database later
logger "Creating empty files" 1
cd ""$AIR_TEMP_DIR"/DE"
touch CO-8SMW-clean.csv CO-8SMW_MAX-clean.csv NO2-1SMW-clean.csv NO2-1SMW_MAX-clean.csv O3-1SMW-clean.csv O3-1SMW_MAX-clean.csv O3-8SMW-clean.csv O3-8SMW_MAX-clean.csv PM10-1TMW-clean.csv SO2-1SMW-clean.csv SO2-1SMW_MAX-clean.csv SO2-1TMW-clean.csv

# if pollutant component NOT specified run in full automatic modus and ignore all other arguments (FIXME!)
if [ -z "$COMP" ]; then
	get_comp
	update_db
	# Clean up left over files from previous runs
	logger "Cleaning up temp files after fully automatic run" 1
	#rm "$AIR_TEMP_DIR"/DE/*.csv > /dev/null 2>&1
	exit
fi

if [ -z "$PERIOD" ]; then
	get_period
fi

if [ -z "$DATE" ]; then
	get_date
fi

#if [ -z "$TIME" ]; then
#	get_time
#fi

if [ -z "$STATE" ]; then
	get_state
	update_db
else
	download
	echo "move2: ""$AIR_TEMP_DIR"/DE/"$OUTFILE"-clean".csv ""$AIR_TEMP_DIR"/DE/"$COMP"-"$PERIOD"-clean".csv"
	mv ""$AIR_TEMP_DIR"/DE/"$OUTFILE"-clean".csv ""$AIR_TEMP_DIR"/DE/"$COMP"-"$PERIOD"-clean".csv
	update_db
fi

echo "ENDE"
# Clean up left over files from previous runs
logger "Cleaning up temp files at the end" 1
#rm "$AIR_TEMP_DIR"/DE/*.csv #> /dev/null 2>&1
