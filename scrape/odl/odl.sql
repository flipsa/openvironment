# Select database
use ud_luft;

# needs not be set here!
#set session collation_database=utf8_unicode_ci;
#set session character_set_database=utf8;


CREATE TABLE IF NOT EXISTS `tmp_odl_germany_1smw` (
  `uuid` int NOT NULL,
  `uid` varchar(36) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lat` float(10,7) DEFAULT NULL,
  `lng` float(10,7) DEFAULT NULL,
  `odl_1smw` decimal(5,3) DEFAULT NULL,
  `odl_cos_1smw` decimal(5,3) DEFAULT NULL,
  `odl_ter_1smw` decimal(5,3) DEFAULT NULL,
  `odl_avg_1smw` decimal(5,3) NOT NULL,
  `status` int(1) DEFAULT NULL,
  `sensor_time` datetime NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY `uid` (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


CREATE TABLE IF NOT EXISTS `odl_germany` (
  `uuid` int NOT NULL,
  `uid` varchar(36) COLLATE utf8_unicode_ci DEFAULT NULL,
  `uid2` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `uid3` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `category` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `icon` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `agency` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `agency_code` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `agency_link` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `provider` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `provider_code` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `provider_link` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name_proper` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `start_date` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `end_date` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zip` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lat` float(10,7) DEFAULT NULL,
  `lng` float(10,7) DEFAULT NULL,
  `altitude` varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var1` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var2` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var3` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var4` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var5` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `odl_1smw` decimal(5,3) DEFAULT NULL,
  `odl_cos_1smw` decimal(5,3) DEFAULT NULL,
  `odl_ter_1smw` decimal(5,3) DEFAULT NULL,
  `odl_avg_1smw` decimal(5,3) NOT NULL,
  `odl_2smw` decimal(5,3) DEFAULT NULL,
  `odl_cos_2smw` decimal(5,3) DEFAULT NULL,
  `odl_ter_2smw` decimal(5,3) DEFAULT NULL,
  `odl_avg_2smw` decimal(5,3) NOT NULL,
  `status` int(1) DEFAULT NULL,
  `op_state` varchar(8) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'active',
  `sensor_time` datetime NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  KEY `lat` (`lat`,`lng`)
) ENGINE=InnoDB AUTO_INCREMENT=2048 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

# Truncate all tables 
truncate table tmp_odl_germany_1smw;
truncate table tmp_odl_germany_2smw;
truncate table odl_germany;

# Populate airq_stations table
load data local infile '/var/www/offene-umweltdaten.de/scrape/odl/ud_data/germany/odl_latest_utf8.csv' into table odl_germany fields terminated by ',' lines terminated by '\n' (uid, zip, name, lng, lat, sensor_time, odl_1smw, odl_cos_1smw, odl_ter_1smw, status);


#INSERT INTO odl_germany (uuid,uid,name,lat,lng,odl_1smw,odl_cos_1smw,odl_ter_1smw,odl_avg_1smw SELECT null, uid, name, lat, lng, odl_1smw, odl_cos_1smw, odl_ter_1smw, odl_avg_1smw,sensor_time FROM tmp_odl_germany_1smw;

#UPDATE odl_germany,tmp_odl_germany_2smw SET odl_germany.odl_2smw=tmp_odl_germany_2smw.odl_2smw, odl_germany.odl_cos_2smw=tmp_odl_germany_2smw.odl_cos_2smw, odl_germany.odl_ter_2smw=tmp_odl_germany_2smw.odl_ter_2smw, odl_germany.odl_avg_2smw=tmp_odl_germany_2smw.odl_avg_2smw;

# Set default values
UPDATE odl_germany SET country='de';
UPDATE odl_germany SET agency='Bundesamt für Strahlenschutz';
UPDATE odl_germany SET agency_code='BfS';
UPDATE odl_germany SET agency_link='http://odlinfo.bfs.de/DE/index.html';
UPDATE odl_germany SET provider='Bundesamt für Strahlenschutz';
UPDATE odl_germany SET provider_code='BfS';
UPDATE odl_germany SET provider_link=CONCAT('http://odlinfo.bfs.de/DE/aktuelles/messstelle/', `uid`, '.html');
UPDATE odl_germany SET category='radiation';
UPDATE odl_germany SET icon='/img/radiation.png';

# Calculate ODL average values for each station
UPDATE `odl_germany` SET `odl_avg_1smw`=(SELECT ROUND((AVG(odl_1smw)),3) FROM `radiation_timeline` WHERE `radiation_timeline`.`uid`=`odl_germany`.`uid`);

# Update station table
INSERT INTO stations SELECT null,uid,null,category,lat,lng,null,country,null,null,null,null,null,name,null,null,null,null,agency,agency_code,agency_link,provider,provider_code,provider_link,status,op_state,sensor_time,null FROM odl_germany ON DUPLICATE KEY UPDATE original_name=odl_germany.name, country=odl_germany.country, zip=odl_germany.zip, agency=odl_germany.agency, agency_code=odl_germany.agency_code, agency_link=odl_germany.agency_link, provider=odl_germany.provider, provider_code=odl_germany.provider_code, provider_link=odl_germany.provider_link, status=odl_germany.status, op_state=odl_germany.op_state, last_update=CASE WHEN `odl_germany`.`sensor_time` > `stations`.`last_update` THEN `odl_germany`.`sensor_time` ELSE `stations`.`last_update` END;

# Set proper uuid in the temp table
UPDATE `odl_germany`, `stations` SET `odl_germany`.`uuid`=`stations`.`uuid` WHERE `odl_germany`.`uid`=`stations`.`uid` AND `odl_germany`.`category`=`stations`.`category`;

CREATE TABLE IF NOT EXISTS `radiation_current` (
  `uuid` int NOT NULL,
  `uid` varchar(36) COLLATE utf8_unicode_ci DEFAULT NULL,
  `odl_1smw` decimal(5,3) DEFAULT NULL,
  `odl_cos_1smw` decimal(5,3) DEFAULT NULL,
  `odl_ter_1smw` decimal(5,3) DEFAULT NULL,
  `odl_avg_1smw` decimal(5,3) NOT NULL,
  `odl_1smw_time` datetime NOT NULL,
  `last_mod` timestamp on update CURRENT_TIMESTAMP,
  PRIMARY KEY (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


# Update permanent radiation table
INSERT INTO radiation_current SELECT uuid,uid,odl_1smw,odl_cos_1smw,odl_ter_1smw,odl_avg_1smw,sensor_time,null FROM `odl_germany` WHERE `op_state`='active' ON DUPLICATE KEY UPDATE `radiation_current`.`uuid`=`odl_germany`.`uuid`, `radiation_current`.`uid`=`odl_germany`.`uid`, `radiation_current`.`odl_1smw`=CASE WHEN `odl_germany`.`sensor_time` > `radiation_current`.`odl_1smw_time` THEN `odl_germany`.`odl_1smw` ELSE `radiation_current`.`odl_1smw` END, `radiation_current`.`odl_cos_1smw`=CASE WHEN `odl_germany`.`sensor_time` > `radiation_current`.`odl_1smw_time` THEN `odl_germany`.`odl_cos_1smw` ELSE `radiation_current`.`odl_cos_1smw` END, `radiation_current`.`odl_ter_1smw`=CASE WHEN `odl_germany`.`sensor_time` > `radiation_current`.`odl_1smw_time` THEN `odl_germany`.`odl_ter_1smw` ELSE `radiation_current`.`odl_ter_1smw` END, `radiation_current`.`odl_avg_1smw`=CASE WHEN `odl_germany`.`sensor_time` > `radiation_current`.`odl_1smw_time` THEN `odl_germany`.`odl_avg_1smw` ELSE `radiation_current`.`odl_avg_1smw` END, `radiation_current`.`odl_1smw_time`=CASE WHEN `odl_germany`.`sensor_time` > `radiation_current`.`odl_1smw_time` THEN `odl_germany`.`sensor_time` ELSE `radiation_current`.`odl_1smw_time` END;

CREATE TABLE IF NOT EXISTS `radiation_timeline` (
   `id` int NOT NULL AUTO_INCREMENT,
   `uuid` int NOT NULL,
   `uid` varchar(36) COLLATE utf8_unicode_ci DEFAULT NULL,   
   `odl_1smw` decimal(5,3) DEFAULT NULL,   
   `odl_cos_1smw` decimal(5,3) DEFAULT NULL,   
   `odl_ter_1smw` decimal(5,3) DEFAULT NULL,   
   `odl_avg_1smw` decimal(5,3) DEFAULT NULL,   
   `sensor_time` datetime NOT NULL,   
   `last_mod` timestamp on update CURRENT_TIMESTAMP,   
   PRIMARY KEY (`id`),   
   UNIQUE `uuid_time` (`uuid`,`sensor_time`)   
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

# Update Timeline
insert into radiation_timeline select null,uuid,uid,odl_1smw,odl_cos_1smw,odl_ter_1smw,odl_avg_1smw,sensor_time,null FROM odl_germany where op_state='active' ON DUPLICATE KEY UPDATE radiation_timeline.odl_1smw=odl_germany.odl_1smw, radiation_timeline.odl_cos_1smw=odl_germany.odl_cos_1smw, radiation_timeline.odl_ter_1smw=odl_germany.odl_ter_1smw, radiation_timeline.odl_avg_1smw=odl_germany.odl_avg_1smw;




