# Variables
DATA_DIR="/var/www/offene-umweltdaten.de/scrape/odl/ud_data/germany/" # Keep Trailing Slas
#OUTFILE="$DATA_DIR""$COMP"-"$DATE"

# Database credentials
. /var/www/offene-umweltdaten.de/scrape/settings


# Download only the latest files depending on time of day. New files are published 
# four (4) times a day at around 05:45, 11:45, 17:45 and 23:45. So we run a cronjob 
# at the next full hour after and then let the script handle which files to d/l and scrape

# We run the script at 05:50, 11:50 and 17:50, 23:50 hours, but the most recent data is always from 4.x hours before that
HOUR=$(date +"%-H" -d "4 hours ago")

#HOUR=11

# If hour is 06 or 12 then the subtraction gives a one-digit result, but we always need two digits to download the correct file
if [ $HOUR -eq  2 ] || [ $HOUR -eq  3 ] || [ $HOUR -eq  4 ] || [ $HOUR -eq  5 ] || [ $HOUR -eq  6 ] || [ $HOUR -eq  7 ]
	then latest="19 20 21 22 23 00"
elif [ $HOUR -eq  8 ] || [ $HOUR -eq  9 ] || [ $HOUR -eq  10 ] || [ $HOUR -eq  11 ] || [ $HOUR -eq  12 ] || [ $HOUR -eq  13 ]
    then latest="01 02 03 04 05 06"
elif [ $HOUR -eq  14 ] || [ $HOUR -eq  15 ] || [ $HOUR -eq  16 ] || [ $HOUR -eq  17 ] || [ $HOUR -eq  18 ] || [ $HOUR -eq  19 ]
	then latest="07 08 09 10 11 12"
elif [ $HOUR -eq  20 ] || [ $HOUR -eq  21 ] || [ $HOUR -eq  22 ] || [ $HOUR -eq  23 ] || [ $HOUR -eq  0 ] || [ $HOUR -eq  1 ]
	then latest="13 14 15 16 17 18"
fi

# Download all files on server but ignore already existing ones
cd $DATA_DIR

for this in $latest; do
	wget --user=offeneDaten --password=odaten -r -l0 -N -nd --no-parent -A.dat  "http://odlinfo.bfs.de/daten/1hct_"$this".dat" > wget.log 2>&1 

	# check the wget_log if the file was not retrieved because it already exists. if so, skip further processing!
	if grep -iq "not retrieving" wget.log; then

		echo "File already existed. Further processing stopped. Skipping to next file (if any left)..."
		continue

	else

		echo "Starting processing of downloaded file 1hct_"$this.dat""

		# change file name to generic
		cp 1hct_$this.dat odl_latest.dat

		# Replace delimiter "|" with "," (not needed for mysql where one can set an arbitrary delimiter but might be useful 
		sed 's/|/,/g' odl_latest.dat > odl_latest_utf8.csv

		# Change encoding
		#iconv --from-code=ISO-8859-1 --to-code=UTF-8 odl_latest.csv > odl_latest_utf8.csv

		echo "Inserting into odl_germany table"
		mysql "$DB" < /var/www/offene-umweltdaten.de/scrape/odl/odl.sql -u"$DB_USER" -p"$DB_PW" #> /dev/null 2>&1

	fi
done







