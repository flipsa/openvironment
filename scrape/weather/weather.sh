# run every hour at 30min past full. 
# usually there is one update per hour around 14min past full
# at 07 and 19 there is a change in the data format 
# at 07 and 19 there is also a 2nd file, published around 45 after full

# Variables
DATA_DIR="/var/www/offene-umweltdaten.de/scrape/weather/ud_data/observations/germany/" # Keep Trailing Slas
#OUTFILE="$DATA_DIR""$COMP"-"$DATE"


# Download all files on server but ignore already existing ones
cd $DATA_DIR
wget --user=gds33013 --password=oWotpxdQ  -nc ftp://gds33013:oWotpxdQ@ftp-outgoing2.dwd.de/gds/specials/observations/tables/germany/* 

# Find the most recent file
LATEST_FILE=`ls -t $DATA_DIR | head -1`

# Remove all control characters like ^M
tr -d '\r' < $LATEST_FILE > /dev/shm/clean1

# Remove all empty lines
sed '/^$/d' /dev/shm/clean1 > /dev/shm/clean2

# Change encoding
iconv --from-code=ISO-8859-1 --to-code=UTF-8 /dev/shm/clean2 > /dev/shm/clean3

# Get the date
tail -n +3 /dev/shm/clean3 > /dev/shm/clean4

day=`sed -n 1p /dev/shm/clean4 | awk '{print substr($0,length($0) - 17,2)}';`
month=`sed -n 1p /dev/shm/clean4 | awk '{print substr($0,length($0) - 14,2)}';`
year=`sed -n 1p /dev/shm/clean4 | awk '{print substr($0,length($0) - 11,4)}';`
hour=`sed -n 1p /dev/shm/clean4 | awk '{print substr($0,length($0) - 5,2)}';`
min=$(date +"%M");

# Delete first 7 lines
tail -n +5 /dev/shm/clean4 > /dev/shm/clean5

# Delete last 15 lines
sed -n -e :a -e '1,15!{P;N;D;};N;ba' /dev/shm/clean5 > /dev/shm/clean6

# Count lines of file for the for-loop later
lc=$(wc -l < /dev/shm/clean6)

# Loop through every line of the file and echo each line into the final csv file
for i in $(seq 1 $lc);
do 
name=`sed -n "$i"p /dev/shm/clean6 | awk '{print substr($0,0,19)}';`
alt=`sed -n "$i"p /dev/shm/clean6 | awk '{print substr($0,20,4)}';`
pressure=`sed -n "$i"p /dev/shm/clean6 | awk '{print substr($0,27,6)}';`
temp=`sed -n "$i"p /dev/shm/clean6| awk '{print substr($0,35,5)}';`
if [ "$hour" = "07" ] || [ "$hour" = "19" ]; then
	echo "7 or 19"
	precip=`sed -n "$i"p /dev/shm/clean6 | awk '{print substr($0,57,5)}';`
	snow=`sed -n "$i"p /dev/shm/clean6 | awk '{print substr($0,70,3)}';`
	wind_dir=`sed -n "$i"p /dev/shm/clean6 | awk '{print substr($0,75,3)}';`
	wind_speed=`sed -n "$i"p /dev/shm/clean6 | awk '{print substr($0,79,3)}';`
	wind_max=`sed -n "$i"p /dev/shm/clean6 | awk '{print substr($0,84,3)}';`
	condition=`sed -n "$i"p /dev/shm/clean6 | awk '{print substr($0,90,30)}';`
	squall=`sed -n "$i"p /dev/shm/clean6 | awk '{print substr($0,120)}';`
else
	precip=`sed -n "$i"p /dev/shm/clean6 | awk '{print substr($0,42,5)}';`
	wind_dir=`sed -n "$i"p /dev/shm/clean6 | awk '{print substr($0,49,3)}';`
	wind_speed=`sed -n "$i"p /dev/shm/clean6 | awk '{print substr($0,55,3)}';`
	wind_max=`sed -n "$i"p /dev/shm/clean6 | awk '{print substr($0,60,3)}';`
	condition=`sed -n "$i"p /dev/shm/clean6 | awk '{print substr($0,66,30)}';`
	squall=`sed -n "$i"p /dev/shm/clean6 | awk '{print substr($0,96)}';`
fi;
time=""$year"-"$month"-"$day" "$hour":"00""
#echo $name - $alt - $pressure - $temp - $precip - $wind_dir - $wind_speed - $wind_max - $condition - $squall
echo \"$name\"\,\"$alt\"\,\"$pressure\"\,\"$temp\"\,\"$precip\"\,\"$wind_dir\"\,\"$wind_speed\"\,\"$wind_max\"\,\"$condition\"\,\"$squall\"\,\"$time\" >> /dev/shm/weather_germany_latest.csv; 
done

# Move final file and delete temporary files 
mv /dev/shm/weather_germany_latest.csv /var/www/offene-umweltdaten.de/scrape/weather/ud_data/observations/germany/
rm -rf /dev/shm/clean*

mysql ud_luft < /var/www/offene-umweltdaten.de/scrape/weather/weather_germany_latest_new.sql -uwp_ud_user1 -pwp0815 > /dev/null 2>&1
rm /var/www/offene-umweltdaten.de/scrape/weather/ud_data/observations/germany/weather_germany_latest.csv > /dev/null 2>&1
curl -k "https://www.offene-umweltdaten.de/update_stations.php?category=weather_germany&action=create" > /dev/null 2>&1
curl -k "https://www.offene-umweltdaten.de/update_stations.php?category=weather_germany&action=update" > /dev/null 2>&1
