##################################
# SECTION WEATHER STATIONS START #
##################################

# Create table weather_stations
# 
#CREATE TABLE IF NOT EXISTS `weather_germany` ( `id` int(11) NOT NULL AUTO_INCREMENT, `uid` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL, `uid2` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL, `uid3` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL, `category` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL, `icon` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL, `name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL, `name_proper` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL, `start_date` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL, `end_date` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL, `country` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL, `lat` float(10,7) DEFAULT NULL, `lng` float(10,7) DEFAULT NULL, `altitude` varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL,  `var1` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL, `var2` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL, `var3` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL, `var4` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL, `var5` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL, `pressure` decimal(5,1) DEFAULT NULL, `temperature` decimal(5,1) DEFAULT NULL, `precipitation` decimal(5,1) DEFAULT NULL, `wind_direction` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL, `wind_speed` int(2) DEFAULT NULL, `wind_peak` int(2) DEFAULT NULL, `conditions` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL, `squall` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL, `op_state` varchar(8) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'active', `sensor_time` datetime NOT NULL, `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP, PRIMARY KEY (`id`), KEY `lat` (`lat`,`lng`) ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=128 ;

truncate weather_germany;
load data local infile '/var/www/offene-umweltdaten.de/scrape/weather/ud_data/stations/weather_stations_germany.csv' into table weather_germany fields terminated by ',' enclosed by "'" lines terminated by '\n' (uid, uid2, uid3, name, lat, lng, altitude, var1, var2, var3, var4, start_date, end_date);

# Delete inactive stations
#DELETE FROM `weather_stations` WHERE end_date<>'';



##################################
# SECTION WEATHER STATIONS END   #
##################################


##################################
# SECTION WEATHER GERMANY START  #
##################################

# Create table weather_germany_latest
CREATE TABLE IF NOT EXISTS `weather_germany_latest` ( `id` int(11) NOT NULL AUTO_INCREMENT, `station_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL, `altitude` int(4) DEFAULT NULL, `pressure` decimal(5,1) DEFAULT NULL, `temperature` decimal(3,1) DEFAULT NULL, `precipitation` decimal(3,1) DEFAULT NULL, `wind_direction` varchar(3) COLLATE utf8_unicode_ci DEFAULT NULL, `wind_speed` int(2) DEFAULT NULL, `wind_peak` int(2) DEFAULT NULL, `conditions` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL, `squall` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL, `op_state` varchar(8) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'active', `sensor_time` datetime NOT NULL, `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP, PRIMARY KEY (`id`) ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=128 ;

# Truncate old data
truncate weather_germany_latest;

# Load values into table
load data local infile '/var/www/offene-umweltdaten.de/scrape/weather/ud_data/observations/germany/weather_germany_latest.csv' into table weather_germany_latest fields terminated by ',' enclosed by '"' lines terminated by '\n' (station_name, altitude, pressure, temperature, precipitation, wind_direction, wind_speed, wind_peak, conditions, squall, sensor_time);

# Join stations table and weather latest table by ID #--> CAREFUL: semi-automatic mode. List length 75 lines, check order...
UPDATE weather_germany,weather_germany_latest SET weather_germany.pressure=weather_germany_latest.pressure, weather_germany.precipitation=weather_germany_latest.precipitation, weather_germany.temperature=weather_germany_latest.temperature, weather_germany.wind_direction=weather_germany_latest.wind_direction, weather_germany.wind_speed=weather_germany_latest.wind_speed, weather_germany.wind_peak=weather_germany_latest.wind_peak, weather_germany.conditions=weather_germany_latest.conditions, weather_germany.squall=weather_germany_latest.squall, weather_germany.sensor_time=weather_germany_latest.sensor_time WHERE weather_germany.id=weather_germany_latest.id;

# Set category and icon
UPDATE weather_germany SET category='weather';
UPDATE weather_germany SET icon='/img/weather.png';

