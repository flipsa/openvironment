-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Erstellungszeit: 03. Nov 2014 um 06:53
-- Server Version: 5.5.40-0ubuntu0.14.04.1-log
-- PHP-Version: 5.5.9-1ubuntu4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Datenbank: `ud_luft`
--
CREATE DATABASE IF NOT EXISTS `ud_luft` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
USE `ud_luft`;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `airq_co`
--

CREATE TABLE IF NOT EXISTS `airq_co` (
  `station_code` varchar(7) CHARACTER SET latin1 DEFAULT NULL,
  `CO` decimal(5,1) DEFAULT NULL,
  `time` varchar(18) COLLATE utf8_unicode_ci NOT NULL,
  KEY `station_code` (`station_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `airq_no2`
--

CREATE TABLE IF NOT EXISTS `airq_no2` (
  `station_code` varchar(7) CHARACTER SET latin1 DEFAULT NULL,
  `NO2` decimal(5,1) DEFAULT NULL,
  `time` varchar(18) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `airq_o3`
--

CREATE TABLE IF NOT EXISTS `airq_o3` (
  `station_code` varchar(7) CHARACTER SET latin1 DEFAULT NULL,
  `O3` decimal(5,1) DEFAULT NULL,
  `time` varchar(18) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `airq_pm10`
--

CREATE TABLE IF NOT EXISTS `airq_pm10` (
  `station_code` varchar(7) CHARACTER SET latin1 DEFAULT NULL,
  `PM10` decimal(5,1) DEFAULT NULL,
  `time` varchar(18) CHARACTER SET latin1 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `airq_so2`
--

CREATE TABLE IF NOT EXISTS `airq_so2` (
  `station_code` varchar(7) CHARACTER SET latin1 DEFAULT NULL,
  `SO2` decimal(5,1) DEFAULT NULL,
  `time` varchar(18) CHARACTER SET latin1 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `air_quality_germany`
--

CREATE TABLE IF NOT EXISTS `air_quality_germany` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `uid2` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `uid3` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `category` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `icon` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name_proper` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `start_date` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `end_date` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lat` float(10,7) DEFAULT NULL,
  `lng` float(10,7) DEFAULT NULL,
  `altitude` varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var1` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var2` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var3` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var4` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var5` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `co` decimal(5,1) DEFAULT NULL,
  `co_time` varchar(18) COLLATE utf8_unicode_ci DEFAULT NULL,
  `no2` decimal(5,1) DEFAULT NULL,
  `no2_time` varchar(18) COLLATE utf8_unicode_ci DEFAULT NULL,
  `o3` decimal(5,1) DEFAULT NULL,
  `o3_time` varchar(18) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pm10` decimal(5,1) DEFAULT NULL,
  `pm10_time` varchar(18) COLLATE utf8_unicode_ci DEFAULT NULL,
  `so2` decimal(5,1) DEFAULT NULL,
  `so2_time` varchar(18) COLLATE utf8_unicode_ci DEFAULT NULL,
  `op_state` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'active',
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1677 ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `bw_DE`
--

CREATE TABLE IF NOT EXISTS `bw_DE` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `uid2` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `uid3` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `category` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `icon` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name_proper` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `start_date` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `end_date` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lat` float(10,7) DEFAULT NULL,
  `lng` float(10,7) DEFAULT NULL,
  `altitude` varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var1` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var2` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var3` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var4` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var5` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bw_quality_2011` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bw_quality_2012` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bw_quality_2013` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `op_state` varchar(8) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'active',
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2418 ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `eprtr_2010_germany`
--

CREATE TABLE IF NOT EXISTS `eprtr_2010_germany` (
  `uid` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `uid2` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `uid3` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `category` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `icon` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name_proper` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `start_date` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `end_date` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lat` float(10,7) DEFAULT NULL,
  `lng` float(10,7) DEFAULT NULL,
  `altitude` varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var1` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var2` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var3` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var4` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var5` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `op_state` varchar(8) CHARACTER SET utf8 COLLATE utf8_swedish_ci NOT NULL DEFAULT 'active',
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `eptr_europe`
--

CREATE TABLE IF NOT EXISTS `eptr_europe` (
  `uid` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `uid2` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `uid3` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `category` varchar(15) CHARACTER SET latin1 DEFAULT NULL,
  `icon` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `name` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `name_proper` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `start_date` varchar(8) CHARACTER SET latin1 DEFAULT NULL,
  `end_date` varchar(8) CHARACTER SET latin1 DEFAULT NULL,
  `country` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `lat` float(10,7) DEFAULT NULL,
  `lng` float(10,7) DEFAULT NULL,
  `altitude` varchar(4) CHARACTER SET latin1 DEFAULT NULL,
  `var1` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `var2` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `var3` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `var4` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `var5` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `op_state` varchar(8) CHARACTER SET utf8 NOT NULL DEFAULT 'active',
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `odl_germany`
--

CREATE TABLE IF NOT EXISTS `odl_germany` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `uid2` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `uid3` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `category` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `icon` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name_proper` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `start_date` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `end_date` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lat` float(10,7) DEFAULT NULL,
  `lng` float(10,7) DEFAULT NULL,
  `altitude` varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var1` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var2` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var3` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var4` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var5` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `odl` decimal(5,4) DEFAULT NULL,
  `op_state` varchar(8) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'active',
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1716 ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `pegel_w`
--

CREATE TABLE IF NOT EXISTS `pegel_w` (
  `uid` varchar(36) COLLATE utf8_unicode_ci NOT NULL,
  `uid2` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
  `uid3` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `category` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'pegel',
  `icon` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT '/img/river.png',
  `name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `name_proper` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `km` decimal(6,2) NOT NULL,
  `agency` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `lng` decimal(19,14) NOT NULL,
  `lat` decimal(19,14) NOT NULL,
  `water_shortname` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `water_longname` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `w_shortname` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  `w_longname` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `w_unit` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `w_equidistance` int(3) NOT NULL,
  `w_timestamp` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `w_value` decimal(6,1) NOT NULL,
  `w_trend` int(2) NOT NULL,
  `w_stateMnwMhw` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `w_stateNswHsw` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `w_gaugeZero_unit` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
  `w_gaugeZero_value` varchar(6) COLLATE utf8_unicode_ci NOT NULL,
  `w_gaugeZero_validFrom` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `lt_shortname` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `lt_longname` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `lt_unit` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `lt_timestamp` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `lt_value` decimal(6,1) NOT NULL,
  `lt_trend` int(2) NOT NULL,
  `wt_shortname` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `wt_longname` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `wt_unit` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `wt_timestamp` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `wt_value` decimal(6,1) NOT NULL,
  `wt_trend` int(2) NOT NULL,
  `q_shortname` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `q_longname` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `q_unit` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `q_timestamp` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `q_value` decimal(6,1) NOT NULL,
  `q_trend` int(2) NOT NULL,
  `va_shortname` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `va_longname` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `va_unit` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `va_timestamp` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `va_value` decimal(6,1) NOT NULL,
  `va_trend` int(2) NOT NULL,
  `dfh_shortname` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `dfh_longname` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `dfh_unit` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `dfh_timestamp` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `dfh_value` decimal(6,1) NOT NULL,
  `dfh_trend` int(2) NOT NULL,
  `lf_shortname` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `lf_longname` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `lf_unit` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `lf_timestamp` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `lf_value` decimal(6,1) NOT NULL,
  `lf_trend` int(2) NOT NULL,
  `wg_shortname` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `wg_longname` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `wg_unit` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `wg_timestamp` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `wg_value` decimal(6,1) NOT NULL,
  `wg_trend` int(2) NOT NULL,
  `wr_shortname` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `wr_longname` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `wr_unit` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `wr_timestamp` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `wr_value` decimal(6,1) NOT NULL,
  `wr_trend` int(2) NOT NULL,
  `gru_shortname` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `gru_longname` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `gru_unit` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `gru_timestamp` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `gru_value` decimal(6,1) NOT NULL,
  `gru_trend` int(2) NOT NULL,
  `hl_shortname` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `hl_longname` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `hl_unit` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `hl_timestamp` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `hl_value` decimal(6,1) NOT NULL,
  `hl_trend` int(2) NOT NULL,
  `pl_shortname` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `pl_longname` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `pl_unit` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `pl_timestamp` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `pl_value` decimal(6,1) NOT NULL,
  `pl_trend` int(2) NOT NULL,
  `n_shortname` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `n_longname` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `n_unit` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `n_timestamp` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `n_value` decimal(6,1) NOT NULL,
  `n_trend` int(2) NOT NULL,
  `op_state` varchar(8) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'active',
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `renewable_energy_germany`
--

CREATE TABLE IF NOT EXISTS `renewable_energy_germany` (
  `uid` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `uid2` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `uid3` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `category` varchar(15) CHARACTER SET latin1 DEFAULT NULL,
  `icon` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `name` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `name_proper` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `start_date` varchar(10) CHARACTER SET latin1 DEFAULT NULL,
  `end_date` varchar(10) CHARACTER SET latin1 DEFAULT NULL,
  `country` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `lat` decimal(5,4) DEFAULT NULL,
  `lng` decimal(5,4) DEFAULT NULL,
  `altitude` varchar(4) CHARACTER SET latin1 DEFAULT NULL,
  `var1` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `var2` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `var3` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `var4` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `var5` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `co` decimal(3,1) DEFAULT NULL,
  `no2` decimal(3,1) DEFAULT NULL,
  `o3` decimal(3,1) DEFAULT NULL,
  `pm10` decimal(3,1) DEFAULT NULL,
  `so2` decimal(3,1) DEFAULT NULL,
  `pressure` decimal(5,1) DEFAULT NULL,
  `temperature` decimal(5,1) DEFAULT NULL,
  `precipitation` decimal(5,1) DEFAULT NULL,
  `wind_direction` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `wind_speed` int(2) DEFAULT NULL,
  `wind_peak` int(2) DEFAULT NULL,
  `conditions` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `squall` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `weather_germany`
--

CREATE TABLE IF NOT EXISTS `weather_germany` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `uid2` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `uid3` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `category` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `icon` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name_proper` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `start_date` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `end_date` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lat` float(10,7) DEFAULT NULL,
  `lng` float(10,7) DEFAULT NULL,
  `altitude` varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var1` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var2` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var3` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var4` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `var5` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pressure` decimal(5,1) DEFAULT NULL,
  `temperature` decimal(5,1) DEFAULT NULL,
  `precipitation` decimal(5,1) DEFAULT NULL,
  `wind_direction` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `wind_speed` int(2) DEFAULT NULL,
  `wind_peak` int(2) DEFAULT NULL,
  `conditions` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `squall` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `op_state` varchar(8) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'active',
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=76 ;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `weather_germany_latest`
--

CREATE TABLE IF NOT EXISTS `weather_germany_latest` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `station_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `altitude` int(4) DEFAULT NULL,
  `pressure` decimal(5,1) DEFAULT NULL,
  `temperature` decimal(3,1) DEFAULT NULL,
  `precipitation` decimal(3,1) DEFAULT NULL,
  `wind_direction` varchar(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `wind_speed` int(2) DEFAULT NULL,
  `wind_peak` int(2) DEFAULT NULL,
  `conditions` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `squall` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `op_state` varchar(8) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'active',
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=75 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
