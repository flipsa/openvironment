<?php

	// We need to start / pickup the PHP session here to check for the session key
	session_start();

	$DEBUG = false;

	// getting many (thousands) or results needs lotsa memory
	ini_set('memory_limit', '256M');

	if ($DEBUG == true){
		ini_set('display_errors',1);
		error_reporting(E_ALL);
	}

	// If the browser does not know the session key, then the user directly accessed this page, without browsing the website before (i.e. visiting index.html). We don't allow that here, because we offer the api.php page for that and require users to sign up for it!
	if ($_SESSION['key'] == true) {
		require_once('stations.php');

	} else {
		require_once("constants.php");
		echo $API_WRONG_URL;
	}

?>
