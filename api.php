<?php

	if ($DEBUG == true){
		ini_set('display_errors',1);  
		error_reporting(E_ALL);
	}

	require_once("db_connect.php");
	require_once("constants.php");

	$URL = $_SERVER['PHP_SELF'];

	if (isset ($_GET["key"])){
		$key = $_GET["key"];
	} else {
		echo $API_NO_KEY;
		die;
	}

	// Opens a connection to a mySQL server
	$connection=mysqli_connect ('localhost', $username, $password);

	if (!$connection) {
		die("Not connected : " . mysqli_error($connection));
	}

	// THE MAGIC LINE THAT SOLVES ALL CHARSET TROUBLE!!1!11!!! 
	mysqli_set_charset($connection,'utf8');

	// Set the active mySQL database
	$db_selected = mysqli_select_db($connection,$database);
	if (!$db_selected) {
		die ("Can\'t use db : " . mysql_error());
	}

	$query = sprintf("SELECT * FROM `api` WHERE `key`='%s'",
		mysqli_real_escape_string($connection,$key));

	$api = mysqli_query($connection,$query);
	$row = mysqli_fetch_assoc($api);
	$user = $row['uid'];
	$quota = $row['quota'];
	$queries = $row['queries'];

	// If no uid is returned, the api key is wrong!
	if (!$user) {
		echo $API_WRONG_KEY;
		die;
	} 

	// If uid is returned, check if quota is exceeded or not!
	if (($user) && ($queries <= $quota)) {

		$query = sprintf("UPDATE `api` SET `queries`=queries +1 WHERE `uid`='$user'",
			mysqli_real_escape_string($connection,$key));
			
		$update_queries = mysqli_query($connection,$query);
		mysqli_close($connection);

		require_once("stations.php");

	} else if (($user) && ($queries > $quota)) {
		echo $API_QUOTA_EXCEEDED;
	};

?>
