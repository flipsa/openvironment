<?php

if ($DEBUG == true){
	ini_set('display_errors',1);  
	error_reporting(E_ALL);
}

require_once("db_connect.php");

$db_handle = mysql_connect($server, $username, $password);
mysql_set_charset('utf8',$db_handle);
$db_found = mysql_select_db($database, $db_handle);


$uid=mysql_real_escape_string($_GET["uid"]);
$get=mysql_real_escape_string($_GET["get"]);
$category=mysql_real_escape_string($_GET["category"]);



	header("Content-Type: application/rss+xml; charset=UTF-8");

	$rssfeed = '<?xml version="1.0" encoding="UTF-8" ?>';
	$rssfeed .= '<rss version="2.0">';
	//$rssfeed .= '<atom:link href="http://offene-umweltdaten.de/rss.php" rel="self" type="application/rss+xml" />';
	$rssfeed .= '<channel>';
	$rssfeed .= '<title>www.openvironment.org </title>';
	$rssfeed .= '<link>https://www.openvironment.org</link>';
	$rssfeed .= '<description>Aktuelle Umweltdaten aus Ihrer Umgebung</description>';
	$rssfeed .= '<language>de-de</language>';
	//$rssfeed .= '<copyright>Copyright (c) 2014 www.offene-umweltdaten.de</copyright>';



if ($db_found) {

	switch ($category) {
		case "air_quality":
			if ($get == "all") {
				$query = "SELECT *,t.uid FROM (SELECT * FROM stations WHERE uid LIKE '$uid') as res LEFT JOIN air_current t ON res.uid=t.uid";
			} elseif ($get == "warn") {
				$query = "SELECT *,t.uid FROM (SELECT * FROM stations WHERE uid LIKE '$uid') as res LEFT JOIN air_current t ON res.uid=t.uid HAVING co_8smw>10 OR no2_1smw>200 OR o3_1smw>120 OR pm10_1tmw>50 OR so2_1smw>350";
			}
				$result = mysql_query($query) or die ("Could not execute query");

			while($row = mysql_fetch_array($result)) {
				extract($row);
				$rssfeed .= '<item>';
				$rssfeed .= '<title>Aktuelle Luftschadstoffe in: ' . $original_name . '</title>';
				$rssfeed .= '<description>' . 'Kohlenmonoxid (CO): ' . $co_8smw . ' mg/m³' . '&lt;p/&gt; Stickstoffdioxid (NO2): ' . $no2_1smw . ' µg/m³' . '&lt;p/&gt; Ozon (O3): ' . $o3_1smw . ' µg/m³' . '&lt;p/&gt; Feinstaub: ' . $pm10_1tmw . ' µg/m³' . '&lt;p/&gt; Schwefeldioxid (SO2): ' . $so2_1smw . ' µg/m³' . '&lt;p/&gt;' . '&lt;p/&gt; Datenquellen: &lt;a href="http://www.umweltbundesamt.de/daten/luftbelastung/aktuelle-luftdaten"&gt;Umweltbundesamt&lt;/a&gt;' . '&lt;p/&gt; Alle Daten ohne Gewähr!' . '</description>';
				$rssfeed .= '<link>https://www.openvironment.org</link>';
				$rssfeed .= '<pubDate>' . date("D, d M Y H:i:s O", strtotime($last_update)) . '</pubDate>';
				$rssfeed .= '</item>';
			}

			$rssfeed .= '</channel>';
			$rssfeed .= '</rss>';
			echo $rssfeed;

			break;

		case "weather":
			if ($get == "all") {
				$query = "SELECT *,t.uid FROM (SELECT * FROM stations WHERE uid LIKE '$uid') as res LEFT JOIN weather_current t ON res.uid=t.uid";
			// NOT IMPLEMENTED YET!!!
			} elseif ($get == "warn") {
				$query = "SELECT * FROM weather_germany WHERE uid LIKE '$uid' HAVING XXXXXX ";
			}
				$result = mysql_query($query) or die ("Could not execute query");

			while($row = mysql_fetch_array($result)) {
				extract($row);
				$rssfeed .= '<item>';
				$rssfeed .= '<title>Aktuelle Wetterdaten in: ' . $name . '</title>';
				$rssfeed .= '<description>' . 'Luftdruck: ' . $pressure . '&lt;p/&gt; Temperatur: ' . $temperature . '&lt;p/&gt; Niederschlag: ' . $precipitation . '&lt;p/&gt; Windrichtung: ' . $wind_direction . '&lt;p/&gt; Windgeschwindigkeit: ' . $wind_speed . '&lt;p/&gt; Windgeschwindigkeit Max: ' . $wind_peak . '&lt;p/&gt; Wetterbedingungen: ' . $conditions . '&lt;p/&gt; Böen: ' . $squall . '</description>';
				$rssfeed .= '<link>https://www.openvironment.org</link>';
				$rssfeed .= '<pubDate>' . date("D, d M Y H:i:s O", strtotime($time)) . '</pubDate>';
				$rssfeed .= '</item>';
			}

			$rssfeed .= '</channel>';
			$rssfeed .= '</rss>';
			echo $rssfeed;
			break;

		case "radiation":
			if ($get == "all") {
				$query = "SELECT *,t.uid FROM (SELECT * FROM stations WHERE uid LIKE '$uid') as res LEFT JOIN radiation_current t ON res.uid=t.uid";
			// NOT IMPLEMENTED YET!!!
			} elseif ($get == "warn") {
				$query = "SELECT *,t.uid FROM (SELECT * FROM stations WHERE uid LIKE '$uid') as res LEFT JOIN radiation_current t ON res.uid=t.uid HAVING odl>0.2397";
			}
				$result = mysql_query($query) or die ("Could not execute query");

			while($row = mysql_fetch_array($result)) {
				extract($row);
				$rssfeed .= '<item>';
				$rssfeed .= '<title>Aktuelle Strahlenbelastung in: ' . $original_name . '</title>';
				$rssfeed .= '<description>' . 'Gamma Ortsdosisleistung: ' . $odl_1smw . ' µSv/h</description>';
				$rssfeed .= '<link>https://www.openvironment.org</link>';
				$rssfeed .= '<pubDate>' . date("D, d M Y H:i:s O", strtotime($odl_1smw_time)) . '</pubDate>';
				$rssfeed .= '</item>';
			}

			$rssfeed .= '</channel>';
			$rssfeed .= '</rss>';
			echo $rssfeed;
			break;		

		case "pegel":
			if ($get == "all") {
				$query = "SELECT *,t.uid FROM (SELECT * FROM stations WHERE uid LIKE '$uid') as res LEFT JOIN waterways_current t ON res.uid=t.uid";
			// NOT IMPLEMENTED YET!!!
			} elseif ($get == "warn") {
				$query = "SELECT *,t.uid FROM (SELECT * FROM stations WHERE uid LIKE '$uid') as res LEFT JOIN radiation_current t ON res.uid=t.uid";
			}
				$result = mysql_query($query) or die ("Could not execute query");

			while($row = mysql_fetch_array($result)) {
				extract($row);
				$rssfeed .= '<item>';
				$rssfeed .= '<title>Aktuelle Schifffahrtsinformationen f&&uml;r: ' . $original_name . '</title>';
				$rssfeed .= '<description>' . 'Wasserstand: ' . $w . ' cm</description>';
				$rssfeed .= '<link>https://www.openvironment.org</link>';
				$rssfeed .= '<pubDate>' . date("D, d M Y H:i:s O", strtotime($w_timestamp)) . '</pubDate>';
				$rssfeed .= '</item>';
			}

			$rssfeed .= '</channel>';
			$rssfeed .= '</rss>';
			echo $rssfeed;
			break;	

	}



mysql_close($db_handle);

} else {
	print "Database NOT Found ";
	mysql_close($db_handle);
}

?>
