<?
	require_once("secret.php");
	require_once("ini.php");
	require_once("db_connect.php");
	require_once("constants.php");
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN" "http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" version="XHTML+RDFa 1.0" xmlns:og="http://ogp.me/ns#" xml:lang="en">

	<head>
		<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
		<meta name="viewport" content="initial-scale=1.0, width=device-width, user-scalable=no" />
		<title><? echo $TITLE ?></title>
		<meta name="description" content="<? echo $DESCRIPTION ?>" />
		<link rel="shortcut icon" href="<? echo $FAVICON ?>" type="image/x-icon" />
		<link rel="canonical" href="<? echo $CANONICAL ?>" />
		<meta name="keywords" content="<? echo $KEYWORDS ?>" />

		<!--FACEBOOK-->
		<meta property="og:type" content="website" />
		<meta property="og:title" content="<? echo $TITLE ?>" />
		<meta property="og:site_name" content="<? echo $CANONICAL ?>" />
		<meta property="og:url" content="<? echo $CANONICAL ?>" />
		<meta property="og:description" content="<? echo $DESCRIPTION ?>" />
		<meta property="og:image" content="<? echo $IMAGE ?>" />
		<meta property="og:locale" content="de_DE" />
		<meta property="fb:admins" content="<? echo $FACEBOOK_ID ?>" />

		<!--TWITTER-->
		<meta name="twitter:card" content="summary" />
		<meta name="twitter:url" content="<? echo $CANONICAL ?>" />
		<meta name="twitter:title" content="<? echo $TITLE ?>" />
		<meta name="twitter:description" content="<? echo $DESCRIPTION ?>" />
		<meta name="twitter:image" content="<? echo $IMAGE ?>" />
		<meta property="twitter:creator" content="<? echo $TWITTER_ID ?>" />

		<!--GOOGLE+-->
		<link rel="author" href="<? echo $GOOGLEPLUS_ID ?>" />

		<link rel="stylesheet" href="/css/ext/reset.css" type="text/css" />
			<!-- <link rel="stylesheet" href="/css/pace-center-simple.css" type="text/css" /> -->
		<link rel="stylesheet" href="/css/ext/jquery-ui.css" type="text/css" />
		<link rel="stylesheet" href="/css/ext/leaflet_new.css" type="text/css" />
			<!--<link rel="stylesheet" href="/css/Control.Geocoder.css" type="text/css" /> -->
		<link rel="stylesheet" href="/css/ext/leaflet.groupedlayercontrol.min.css" type="text/css" />
			<!--<link rel="stylesheet" href="/css/leaflet-minimap.min.css" type="text/css" />-->
		<link rel="stylesheet" href="/css/ext/introjs.css" type="text/css" />
		<link rel="stylesheet" href="/css/ext/chosen.min.css" type="text/css" />
		<link rel="stylesheet" href="/css/ext/toggles-full.css" type="text/css" />
		<link rel="stylesheet" href="/css/style.css" type="text/css" />
		<link rel="stylesheet" href="/css/mobile.css" type="text/css" />
			<!-- <link rel="stylesheet" type="text/css" href="https://csshake.surge.sh/csshake.min.css"> -->

		<!-- <link type="text/css" rel="stylesheet" href="/min/?b=css&amp;f=reset.css,jquery-ui.css,leaflet_new.css,leaflet.groupedlayercontrol.min.css,introjs.css,chosen.min.css,toggles-full.css,new-osm.css,mobile-FINAL1.css" />-->

		<!--[if de/map/near/51.0834196,10.4234469/50/air_qualityIE]>
				<style type="text/css">@import url(css/ie2.css);</style>
		<![endif]-->

	</head>

<!-- End head -->

<!-- Start body -->

	<body onload="load();init();">

		<? require_once("main.php"); ?>

		<?
			// This exports some non security-critical PHP session variables so we can access them later with Javascript
			echo "<script type='text/javascript'>
				var lang = " . json_encode($_SESSION['LANG']) . ";
				var settings_units = " . json_encode($_SESSION['UNITS']) . ";
				var distanceUnit = " . json_encode($_SESSION['DISTANCE_UNIT']) . ";
			</script>";
		?>

		<script type="text/javascript" src="/js/ext/jquery-1.11.1.min.js" ></script>

		<script type="text/javascript" src="/js/ext/jquery-ui.1.11.2.min.js"></script>
		<script type="text/javascript" src="/js/ext/jquery.simpleslider.package.min.js"></script>
		<script type="text/javascript" src="/js/ext/moments.locales.js"></script>
		<script type="text/javascript" src="/js/ext/livestamp.min.js"></script>
		<script type="text/javascript" src="/js/ext/moment-timezone.js"></script>
		<script type="text/javascript" src="/js/lang.js"></script>
		<script type="text/javascript" src="/js/ext/alertify.js"></script>
		<script type="text/javascript" src="/js/ext/intro.min.js"></script>
		<script type="text/javascript" src="/js/ext/dygraph-combined-1.1.1.js" ></script>
		<script type="text/javascript" src="/js/ext/leaflet.js"></script>
		<script type="text/javascript" src="/js/ext/leaflet-omnivore.min.js"></script>
		<script type="text/javascript" src="/js/ext/spin.min.js"></script>
		<script type="text/javascript" src="/js/ext/leaflet.spin.js"></script>
		<script type="text/javascript" src="/js/ext/leaflet.markercluster-src.js"></script>
		<script type="text/javascript" src="/js/ext/leaflet.label.js"></script>
		<script type="text/javascript" src="/js/ext/leaflet-providers.js"></script>
		<script type="text/javascript" src="/js/ext/leaflet-minimap.js"></script>
		<script type="text/javascript" src="/js/ext/L.Control.Layers.Minimap.js"></script>
		<script type="text/javascript" src="/js/ext/leaflet.groupedlayercontrol.js"></script>
		<script type="text/javascript" src="/js/ext/jquery.nicescroll.min.3.6.6.js"></script>
		<script type="text/javascript" src="/js/ext/screenfull.min.js"></script>
		<script type="text/javascript" src="/js/ext/jquery.socialshareprivacy2.js"></script>
		<script type="text/javascript" src="/js/ext/deadsee.js"></script>
		<script type="text/javascript" src="/js/ext/chosen.jquery.min.js"></script>
		<script type="text/javascript" src="/js/ext/gaugeMeter-2.0.0.min.js"></script>
		<script type="text/javascript" src="/js/ext/toggles.min.js"></script>
		<script type="text/javascript" src="/js/init.js"></script>
		<script type="text/javascript" src="/js/time.js"></script>
		<script type="text/javascript" src="/js/geo.js"></script>
		<script type="text/javascript" src="/js/popup.js"></script>
		<script type="text/javascript" src="/js/resultsBar.js"></script>
		<script type="text/javascript" src="/js/warnings.js"></script>
		<script type="text/javascript" src="/js/graphs.js"></script>
		<script type="text/javascript" src="/js/map.js"></script>
		<script type="text/javascript" src="/js/footer-scripts-osm.js"></script>
		<script type="text/javascript" src="/js/piwik.js" onload="piwik_loaded = true; if (debug == true) console.log('piwik: ' + piwik_loaded)"></script>
		<noscript><p><img src="//www.openvironment.org/piwik/piwik.php?idsite=1" style="border:0;" alt="" /></p></noscript>

		<!--<script type="text/javascript" src="/min/?b=js&amp;f=jquery-ui.1.11.2.min.js,jquery.simpleslider.package.min.js,moments.locales.js,livestamp.min.js,ext/moment-timezone.js,lang.js,alertify.js,intro.min.js,dygraph-combined-1.1.1.js,leaflet.js,leaflet-omnivore.min.js,spin.min.js,leaflet.spin.js,leaflet.markercluster-src.js,leaflet.label.js,leaflet-providers.js,leaflet-minimap.js,L.Control.Layers.Minimap.js,Control.Geocoder.js,leaflet.groupedlayercontrol.js,map.js,jquery.nicescroll.min.3.6.6.js,screenfull.min.js,jquery.socialshareprivacy2.js,deadsee.js,chosen.jquery.min.js,gaugeMeter-2.0.0.min.js,toggles.min.js,footer-scripts-osm.js"></script>-->

	</body>
</html>


<!-- End body -->
