<?php

if ( !isset ($LANG) ) {
	$LANG = "de";
};

$URL = "www.openvironment.org";

// HEAD
if ($LANG == "de") {
	
	
	$TITLE = "Aktuelle Luftverschmutzung, Wasserqualität, Radioaktivität, Pegelstände &amp; mehr in Ihrer Umgebung | $URL";
	$DESCRIPTION = "www.offene-umweltdaten.de bietet einfachen und schnellen Zugriff auf aktuelle Daten zu Luftverschmutzung, Gewässerqualität, Pegelständen, Radioaktivität, Wetter und mehr aus Ihrer Umgebung.";
	$CANONICAL = "https://$URL";
	$FAVICON = "/img/favicon.ico";
	$IMAGE = "/img/meta-image-f.png";
	$KEYWORDS = "Umwelt, Daten, Umweltdaten, Aktuell, Real-Time, Realtime, Schadstoffe, Luft, Luftqualität, Luftverschmutzung, Feinstaub, Ozon, Stickstoffdioxid, Schwefeldioxid, Kohlenstoffmonoxid, Radioaktivität, Gamma, Ortsdosisleistung, Strahlung, Gewässer, Pegel, Pegelstand, Pegelstände, Wasserqualität, Badegewässer, Badestellen, Wasserbelastung, Wetter, Temperatur, Wind, Windrichtung, Windstärke";
	$FACEBOOK_ID = "1557864503";
	$TWITTER_ID = "@Pol_Hackonomist";
	$GOOGLEPLUS_ID = "https://plus.google.com/109549626299003290890";

	// BODY

	$HEADLINE = "$URL";
	$SUBHEADLINE = "Aktuelle Umweltinformationen aus Ihrer Umgebung";

	// MENU
	$MENU = "Menü";
	$API = "API";
	$ABOUT = "&Uuml;ber";
	$CREDITS = 'Quellen';
	$IMPRINT = 'Impressum';

	// ABOUT
	$ABOUT_HEADLINE_INTRO='Aktuelle Umweltdaten aus <span style="color:green">deiner Gegend</span>';
	$ABOUT_PARAGRAPH_INTRO='Openvironment findet, dass die Menschen ein Recht auf Informationen haben, vor allem wenn diese so wichtige Dinge wie Umwelt und Gesundheit betreffen. <br /> <br />Doch obwohl diese Daten existieren, ist es oft schwierig sie zeitnah zu finden und zu interpretieren.<br /><br /><b>Openvironment will dies einfacher machen!</b><br /><br />Wir sammeln f&uuml;r dich die aktuellsten Umwelt-Messdaten und versuchen es dir so einfach wie m&ouml;glich zu machen, zu wissen was mit deiner Umwelt gerade passiert:';
	
	$ABOUT_HEADLINE_OPEN_DATA='Openvironment benutzt <span style="color:green">open data</span>';
	$ABOUT_PARAGRAPH_OPEN_DATA='Viele staatliche Beh&ouml;rden betreiben Messnetze die vollautomatisch Daten erheben. Einige von diesen Beh&ouml;rden bieten die so gewonnenen Daten im Internet zur weiteren Verwendung an. Diese Daten bilden die Grundlage für Openvironment, welches diese vollautomatisch downloaded, in einer Datenbank sammelt und anschließend visualisiert.';
	
	$ABOUT_HEADLINE_YOUR_HELP='Openvironment braucht <span style="color:green">deine Hilfe</span>:';
	$ABOUT_PARAGRAPH_YOUR_HELP='Openvironment ist ein neues Projekt und erst am Anfang einer großen Mission: m&ouml;glichst viele offene Datens&auml;tze aus dem Bereich Umwelt auf einer einzigen Plattform zur Verf&uuml;gung zu stellen. <br /><br />Aber Openvironment will noch besser werden: mehr Funktionen und besserer Code, mehr Datens&auml;tze (auch &uuml;ber L&auml;ndergrenzen hinaus), und nat&uuml;rlich Ideenaustausch und Kontakt zu anderen Projekten!';
	
	$ABOUT_YOUR_HELP_CODE='Schreibe Code';
	$ABOUT_YOUR_HELP_DATASETS='Empfehle Datensatz';
	$ABOUT_YOUR_HELP_CONTACT='Diskuttiere mit';

	// API
	$API_HEADLINE_INTRO='Openvironment Umweltdaten <span style="color:green">API</span>';
	$API_PARAGRAPH_INTRO='Nur geteilte Daten sind gute Daten, und deshalb bietet Openvironment neben der Darstellung auf dieser Website, auch eine ReST API (im <a href="https://en.wikipedia.org/wiki/GeoJSON">GeoJSON</a> Format) an. Die API ist derzeit noch in der Testphase, und einiges an Arbeit steht noch an. Doch wer schonmal reinschauen will, der kann dies auf zwei verschiedene Arten derzeit:';
	$API_HEADLINE_SEARCH_AREA='Suche im Umkreis einer <span style="color:green">Position</span>';
	$API_PARAGRAPH_SEARCH_AREA='Mit der Umkreis-Suche können Sie Stationen im Umkreis von einer Positionsangabe (lat/lng) erhalten.<br /><br />Beispiel URL um Luftqualitäts-Daten für 10 Stationen um den Stadtmittelpunkt Berlins zu erhalten:<br /><br /><span class="break small"><a href="/api.php?key=12345&type=near&lat=52.5170365&lng=13.3888599&radius=50&category=all&multi=null&amount=10" target="_blank">'.$URL.'/api.php?key=12345&type=near&lat=52.5170365&lng=13.3888599&radius=50&category=all&multi=null&amount=10</a></span>';
	$API_HEADLINE_SEARCH_STATION='Suche nach einer <span style="color:green">Station</span>';
	$API_PARAGRAPH_SEARCH_STATION='Mit der Stations-Suche beziehen Sie die Daten für genau eine Station, deren UUID sie kennen.<br /><br />Beispiel URL um die Daten der Luftqualitäts-Messstation mit der UUID "162" (Berlin, Frankfurter Allee) anzuzeigen:<br /><br /><span class="break small"><a href="/api.php?key=12345&type=station&category=air_quality&uuid=162&measurements=true" target="_blank">'.$URL.'/api.php?key=12345&type=station&category=air_quality&uuid=162&measurements=true</a></span>';
	$API_HEADLINE_TERMS='Nutzungs<span style="color:green">bedingungen</span>';
	$API_PARAGRAPH_TERMS='Um &uuml;ber &Auml;nderungen an der API informieren, und auch um sicher zu stellen, dass die Serverkapazitäten nicht überschritten werden, braucht man zur Nutzung der API einen Key. Um einen Key zu erhalten, schreibe einfach eine <a href="mailto:info@openvironment.org?s=API-Key">Email</a>, und erkl&auml;re kurz, was Du mit der API vorhast. Wer ohne eigenen Key schonmal testen m&ouml;chte, kann dies mit dem API Key "12345" tun. Es gibt jedoch ein Quota von 1000 Abfragen pro Tag mit diesem Key. Falls das Quota überschritten sein sollte wird die API einen entsprechenden Fehler anzeigen.<br /><br /><b>Mit der Nutzung der API stimmst Du den Nutzungsbedingungen der Urheber bzw. Anbieter der Daten zu. Insbesondere musst du sicherstellen, dass der Urheber der Daten korrekt angegeben / verlinkt wird.</b> ';

	// CREDITS
	$CREDITS_HEADLINE_CODE='Danke f&uuml;r <span style="color:green">Code, Bilder, etc.</span>:';
	$CREDITS_PARAGRAPH_CODE='';
	$CREDITS_HEADLINE_DATA='Danke f&uuml;r <span style="color:green">Bereitstellung der Daten</span>:';
	$CREDITS_PARAGRAPH_DATA='';

	// IMPRESSUM
	$IMPRINT_HEADLINE_INTRO='Impressum';
	$IMPRINT_PARAGRAPH_INTRO="$URL ist ein privates Projekt zur Georeferenzierung und Visualisierung von Umweltdaten staatlicher Behörden und anderer und Organisationen, die diese Daten zur freien Weiterverwendung bereitstellen.";
	$IMPRINT_HEADLINE_CONTACT='Kontakt';
	$IMPRINT_EMAIL='E-Mail';
	$IMPRINT_WEB='Website';
	$IMPRINT_TWITTER='Twitter';
	$IMPRINT_GIT='GitLab';
	$IMPRINT_LINKEDIN='LinkedIn';
	$IMPRINT_FOOTER="Openvironment wird entwickelt von: Stefan Heil, Weichselstr. 19, 10247 Berlin";

	// SEARCH
	$SEARCH = "Suche";
	$ADVANCED_SEARCH_ALT = "Suchoptionen";
	$ADDRESS = "Adresse";
	$PLEASE_ENTER_ADDRESS = "Adresse an der Sie suchen m&ouml;chten";
	$CATEGORY = "Kategorie";
	$PLEASE_SELECT_CATEGORY = "W&auml;hlen Sie eine Kategorie";
	$RADIUS = "Radius";
	$PLEASE_SELECT_RADIUS = "W&auml;hlen Sie den Radius";
	$DISTANCE_UNIT = "km";
	$LIMIT = "Anzahl";
	$PLEASE_SELECT_LIMIT = "W&auml;hlen Sie ein Limit";
	$NO_LIMIT = "&infin;";
	$PLEASE_SELECT_MULTI = "Zeige nur:";
	$SHOW_SELECTED_COMPOUNDS = "Nur bestimmte Messgr&ouml;&szlig;en anzeigen";
	$SHOW_SELECTED_COMPOUNDS_TOOLTIP = "Anstatt nach Kategorien k&ouml;nnen Sie auch nur nach einzelnen Messgr&ouml;&szlig;en suchen.";
	$SHOW_WITHIN_BOUNDARY = "Nur Stationen innerhalb Gebietsgrenze anzeigen";
	$SHOW_WITHIN_BOUNDARY_TOOLTIP = "Nur Stationen innerhalb der roten Gebietsgrenze (<img src='/img/polygon.png' style='padding:0px; height:10px; width:10px;'>) anzeigen.<br /><span style='color:red; font-size:80%;'>Vorsicht: Je gr&ouml;&szlig;er das Gebiet, desto l&auml;nger dauert die Berechnung!</span>"; 
	$GEOLOCATION_TOOLTIP = "Ihren derzeitigen Standort bestimmen";

	$ALL_CATEGORIES = "Alle Kategorien";
	$AIR_QUALITY = "Luftverschmutzung";
	$WEATHER = "Wetterdaten";
	$RADIATION = "Radioaktivit&auml;t";
	$WATERWAYS = "Wasserstraßen";
	$BATHING = "Badegew&auml;sser";
	$INDUSTRIAL_POLLUTION = "Industrieschadstoffe";
	$RENEWABLE_ENERGY = "Erneuerbare Energien";

	$RESULTS = "Ergebnisse";
	$RESULT_LIST_TOGGLE = "Ergebnis-Liste minimieren / maximieren";

	//$WARNING_NOTE = 'Bei <span style="color:red;">roten</span> Stationen sind Grenzwerte &uuml;berschritten!';

	// COMPOUNDS
	$CO = "Kohlenmonoxid";
	$NO2 = "Stickstoffdioxid";
	$PM10 = "Feinstaub (PM10)";
	$SO2 = "Schwefeldioxid";
	$O3 = "Ozon";

	$ODL = "Gamma Ortsdosisleistung";

	$W = "Pegelstand";
	$LT = "Lufttemperatur";
	$WT = "Wassertemperatur";
	$Q = "Abflussmenge";
	$VA = "Fließgeschwindigkeit";
	$DFH = "Durchfahrtsh&ouml;he";
	$LF = "Elektrische Leitfähigkeit";
	$WG = "Windgeschwindigkeit";
	$WR = "Windrichtung";
	$GRU = "Grundwasser";
	$HL = "Luftfeuchte";
	$PL = "Luftdruck";
	$O2 = "Sauerstoffgehalt";
	$PH = "PH Wert";
	$N = "Niederschlag";
	$N_INTENSITY = "Niederschlagsintensität";
	$CL = "Chlorid";
	$ORP = "Redoxspannung";


	// SIDEBAR
	$MAP_LAYER = "Karte ausw&auml;hlen";
	$LEGEND = "Legende";

	// SIDEBAR - CONTROL LAYERS MINIMAPS

	// SIDEBAR - LEGEND
	$STATIONS = "Mess-Stationen";
	$SENSORS = "Sensoren";
	$INTERVAL = "Aktuallisierungs-Interval";
	$WARNINGS = "Warnungen";
	$DATA_SOURCE = "Datenquelle";

	// PANEL
	$FULLSCREEN = "Fullscreen";
	$LAYERS_MENU = "Kartenebenen";
	$LEGEND = "Legende";
	$HELP = "Hilfe";
	$SETTINGS = "Einstellungen";
	$LANGUAGE = "Sprache";
	$UNITS = "Einheiten";
	$TIMEZONE = "Zeitzone";

	// API
	$API_QUOTA_EXCEEDED = "Quota für heute &uuml;berschritten :(";
	$API_WRONG_KEY = "Nicht authorisiert! <br />Ihr API Key ist ung&uuml;ltig";
	$API_NO_KEY = "Nicht authorisiert! <br />Sie m&uuml;ssen einen API Key angeben. Falls Sie noch keinen haben lesen Sie die Hinweise auf der Website!";
	$API_ERROR = "API Error! &Uuml;berpr&uuml;fen Sie die URL / Suchparameter";
	$API_WRONG_URL = "Dies ist nicht die öffentliche API. Falls Sie &uuml;ber eine API auf die Daten zugreifen m&ouml;chten lesen Sie bitte die Hinweise auf der Website (Sie brauchen einen Key...)!";


} else if ($LANG == "en") {
	
	$TITLE = "Current environmental data on air pollution, waterways, radioactivity, bathing water quality and more in your area | $URL";
	$DESCRIPTION = "$URL offers easy and quick access to recent data on air pollution, water quality, waterways, radioactivity, weather and more, in your area.";
	$CANONICAL = "https://$URL";
	$FAVICON = "/img/favicon.ico";
	$IMAGE = "/img/meta-image-f.png";
	$KEYWORDS = "environment, data, environmental data, recent, realtime, real-time, real time, pollutants, air, air quality, air pollution, fine dust, particulate matter, pm10, pm2.5, ozone, nitrous oxide, sulfur dioxide, carbon monoxide, radioactivity, gamma, radiation, water, waters, river, ocean, lake, water quality, bathing water, bathing, weather, temperature, wind, wind speed, wind direction";
	$FACEBOOK_ID = "1557864503";
	$TWITTER_ID = "@Pol_Hackonomist";
	$GOOGLEPLUS_ID = "https://plus.google.com/109549626299003290890";

	// BODY

	$HEADLINE = "$URL";
	$SUBHEADLINE = "Recent environmental data from your area";

	// MENU
	$MENU = "Menu";
	$API = "API";
	$ABOUT = "About";
	$CREDITS = 'Credits';
	$IMPRINT = 'Contact';

	// ABOUT
	$ABOUT_HEADLINE_INTRO='Current environmental data from <span style="color:green">your area</span>:';
	$ABOUT_PARAGRAPH_INTRO='Openvironment believes that citizens have a right to information, especially for such important issues as the environment and how it affects our health. <br /> <br />However, even though such data exists, it is often difficult to find and use.<br /><br /><b>Openvironment wants to make it easier!</b><br /><br />We collect the most recent environmental datasets for you, and make them easily accessible, so you can find out what is going on in your environment:';
	
	$ABOUT_HEADLINE_OPEN_DATA='Openvironment uses <span style="color:green">open data</span>';
	$ABOUT_PARAGRAPH_OPEN_DATA='Many government or administrative organizations operate networks of environmental sensors. Many of those organizations publish this data online and allow reuse. These datasets are the foundation of Openvironment, which automatically downloads, processes, stores and visualizes these datasets.';
	
	$ABOUT_HEADLINE_YOUR_HELP='Openvironment needs <span style="color:green">your help</span>:';
	$ABOUT_PARAGRAPH_YOUR_HELP='Openvironment is a new project and we have just started our mission: to collect as many environmental datasets as possible and make them accessible here. <br /><br />But we want to get much better yet: more features and improved code base, more datasets (also from other countries), and of course we want to engage with likeminded people and projects!';
	
	$ABOUT_YOUR_HELP_CODE='contribute code';
	$ABOUT_YOUR_HELP_DATASETS='suggest datasets';
	$ABOUT_YOUR_HELP_CONTACT='inspire us';

	// API
	$API_HEADLINE_INTRO='Openvironment <span style="color:green">API</span>';
	$API_PARAGRAPH_INTRO='Only shared data is good data, hence Openvironment offers a ReST API in <a href="https://en.wikipedia.org/wiki/GeoJSON">GeoJSON</a> format, besides the visualization on our website itself. The API is currently in active development, with a lot still to be done. But if you feel curious, check out these two ways to access data:';
	$API_HEADLINE_SEARCH_AREA='Search around a <span style="color:green">location</span>';
	$API_PARAGRAPH_SEARCH_AREA='The area search lets you search in a radius around a geo-location (lat/lng). <br /><br />Example URL to show air pollution data in a 20km radius around Berlin:<br /><br /><span class="break small"><a href="/api.php?key=12345&type=near&lat=52.5170365&lng=13.3888599&radius=50&category=all&multi=null&amount=100" target="_blank">'.$URL.'/api.php?key=12345&type=near&lat=52.5170365&lng=13.3888599&radius=50&category=all&multi=null&amount=100</a></span>';
	$API_HEADLINE_SEARCH_STATION='Search for a particular <span style="color:green">station</span>';
	$API_PARAGRAPH_SEARCH_STATION='The station search shows you data about one particular station, given its UUID.<br /><br />Example URL to get the current data for the air quality measuring station with UUID "DEBE065" (Berlin, Frankfurter Allee): <br /><br /><span class="break small"><a href="/api.php?key=12345&type=station&category=air_quality&uuid=162&measurements=true" target="_blank">'.$URL.'/api.php?key=12345&type=station&category=air_quality&uuid=162&measurements=true</a></span>';
	$API_HEADLINE_TERMS='Terms of <span style="color:green">use</span>';
	$API_PARAGRAPH_TERMS='To keep you informed about changes in the API, and to make sure the server does not run out of resources, you need a key to access the API. To apply for a key, just send a short email, preferably explaining what you want to do with the data. If you cannot wait for your own API key, you can use the public key "12345" to try it out. However, there is a quota of 1000 queries per day with this key. Once the daily quota is used up, you will see an error message.';

	// CREDITS
	$CREDITS_HEADLINE_CODE='Thanks for <span style="color:green">code, pics, etc.</span>:';
	$CREDITS_PARAGRAPH_CODE='';

	$CREDITS_HEADLINE_DATA='Thanks for <span style="color:green">providing the data</span>:';
	$CREDITS_PARAGRAPH_DATA='';
	
	// IMPRESSUM
	$IMPRINT_HEADLINE_INTRO='Who is <span style="color:green">behind Openvironment</span>';
	$IMPRINT_PARAGRAPH_INTRO="$URL is a personal project to map and visualize open environmental data provided by governmental and administrative orgranizations.</br /></br />Openvironment was started and is mostly developed by: </br /></br />Stefan Heil<br />Weichselstr. 19<br />10247 Berlin<br /><br />";
	
	$IMPRINT_HEADLINE_CONTACT='Contact';
	
	$IMPRINT_EMAIL='Email';
	$IMPRINT_WEB='Website';
	$IMPRINT_TWITTER='Twitter';
	$IMPRINT_GIT='GitLab';
	$IMPRINT_LINKEDIN='LinkedIn';
	
	// SEARCH
	$SEARCH = "Search";
	$ADVANCED_SEARCH_ALT = "Search options";

	$ADDRESS = "Address";
	$PLEASE_ENTER_ADDRESS = "Where do you want to search?";
	$CATEGORY = "Category";
	$PLEASE_SELECT_CATEGORY = "Choose a category";
	$RADIUS = "Radius";
	$DISTANCE_UNIT = "mi.";
	$PLEASE_SELECT_RADIUS = "Choose a radius";
	$LIMIT = "Amount";
	$PLEASE_SELECT_LIMIT = "Chose an amount";
	$NO_LIMIT = "&infin;";
	$PLEASE_SELECT_MULTI = "Show only:";
	$SHOW_SELECTED_COMPOUNDS = "Show only selected sensors";
	$SHOW_SELECTED_COMPOUNDS_TOOLTIP = "Instead of searching for categories, you can also select individual compounds";
	$SHOW_WITHIN_BOUNDARY = "Show only stations within boundary";
	$SHOW_WITHIN_BOUNDARY_TOOLTIP = "Only show stations within the red boundary (<img src='/img/polygon.png' style='padding:0px; height:10px; width:10px;'>) anzeigen.<br /><span style='color:red; font-size:80%;'>Note: The more stations and the bigger the area, the longer the calculation will take!</span>"; 
	$GEOLOCATION_TOOLTIP = "Where am I?";

	$ALL_CATEGORIES = "All categories";
	$AIR_QUALITY = "Air pollution";
	$WEATHER = "Weather data";
	$RADIATION = "Radioactivity";
	$WATERWAYS = "Waterways";
	$BATHING = "Bathing water";
	$INDUSTRIAL_POLLUTION = "Industrial pollution";
	$RENEWABLE_ENERGY = "Renewable energies";

	$RESULTS = "Results";
	$RESULT_LIST_TOGGLE = "Minimize / maximize result list";

	//$WARNING_NOTE = 'Bei <span style="color:red;">roten</span> Stationen sind Grenzwerte &uuml;berschritten!';

	// COMPOUNDS
	$CO = "Carbon monoxide";
	$NO2 = "Nitrous dioxide";
	$PM10 = "Fine dust (PM10)";
	$SO2 = "Sulfur dioxide";
	$O3 = "Ozone";

	$ODL = "Local dose rate";

	$W = "Water level";
	$LT = "Air temperature";
	$WT = "Water temperature";
	$Q = "Flow rate";
	$VA = "Flow velocity";
	$DFH = "Clearance height";
	$LF = "Electrical conductivity";
	$WG = "Wind speed";
	$WR = "Wind direction";
	$GRU = "Ground water";
	$HL = "Humidity";
	$PL = "Pressure";
	$O2 = "Oxygen level";
	$PH = "PH level";
	$N = "Precipitation (amount)";
	$N_INTENSITY = "Precipitation (rate)";
	$CL = "Chloride";
	$ORP = "Redox potential";


	// SIDEBAR
	$MAP_LAYER = "Choose a map";
	$LEGEND = "Legend";

	// SIDEBAR - CONTROL LAYERS MINIMAPS

	// SIDEBAR - LEGEND
	$STATIONS = "Measuring stations";
	$SENSORS = "Sensors";
	$INTERVAL = "Refresh interval";
	$WARNINGS = "Warnings";
	$DATA_SOURCE = "Data source";

	// PANEL
	$FULLSCREEN = "Fullscreen";
	$LAYERS_MENU = "Map Layers";
	$LEGEND = "Map Legend";
	$HELP = "Help";
	$SETTINGS = "Settings";
	$LANGUAGE = "Language";
	$UNITS = "Units";
	$TIMEZONE = "Time zone";

	// API
	$API_QUOTA_EXCEEDED = "Quota exceeded for today :(";
	$API_WRONG_KEY = "Not authorized! <br />Your API key is invalid!";
	$API_NO_KEY = "Not authorized! <br />You need to supply an API key. In case you don't have one yet, please check the website for information on how to get one.";
	$API_ERROR = "API Error! Please check URL / search parameters";
	$API_WRONG_URL = "This is not our public API. If you want to use the API, please follow the instructions on the website (you need a key!).";
	
};

?>
